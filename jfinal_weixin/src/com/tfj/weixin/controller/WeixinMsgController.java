/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.tfj.weixin.controller;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.PropKit;
import com.tfj.weixin.model.WeiXinOpenID;
import com.tfj.weixin.sdk.api.AccessTokenApi;
import com.tfj.weixin.sdk.api.ApiConfig;
import com.tfj.weixin.sdk.jfinal.MsgController;
import com.tfj.weixin.sdk.kit.WeiXinKit;
import com.tfj.weixin.sdk.msg.in.InImageMsg;
import com.tfj.weixin.sdk.msg.in.InLinkMsg;
import com.tfj.weixin.sdk.msg.in.InLocationMsg;
import com.tfj.weixin.sdk.msg.in.InShortVideoMsg;
import com.tfj.weixin.sdk.msg.in.InTextMsg;
import com.tfj.weixin.sdk.msg.in.InVideoMsg;
import com.tfj.weixin.sdk.msg.in.InVoiceMsg;
import com.tfj.weixin.sdk.msg.in.event.InFollowEvent;
import com.tfj.weixin.sdk.msg.in.event.InLocationEvent;
import com.tfj.weixin.sdk.msg.in.event.InMenuEvent;
import com.tfj.weixin.sdk.msg.in.event.InPicSysPhotoEvent;
import com.tfj.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.tfj.weixin.sdk.msg.in.event.InScanCodePushEvent;
import com.tfj.weixin.sdk.msg.in.event.InScanCodeWaitMsgEvent;
import com.tfj.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.tfj.weixin.sdk.msg.out.OutImageMsg;
import com.tfj.weixin.sdk.msg.out.OutNewsMsg;
import com.tfj.weixin.sdk.msg.out.OutTextMsg;
import com.tfj.weixin.sdk.msg.out.OutVoiceMsg;

/**
 * 将此 DemoController 在YourJFinalConfig 中注册路由， 并设置好weixin开发者中心的 URL 与 token ，使
 * URL 指向该 DemoController 继承自父类 WeixinController 的 index
 * 方法即可直接运行看效果，在此基础之上修改相关的方法即可进行实际项目开发
 */
public class WeixinMsgController extends MsgController {

	private static final String basePath = PropKit.get("base");
	private static final String bindActionKey = PropKit.get("bind_action_key");
	private static final String bindTitle = PropKit.get("bind_title");
	private static final String bindContent = PropKit.get("bind_content");
	private static final String bindImage = PropKit.get("bind_image");

	private static final String helpStr = "欢迎关注天方健，点击“溯•天眼”可以随时了解产品前世今生。点击“天方健商城”发现更多惊喜。";

	/**
	 * 如果要支持多公众账号，只需要在此返回各个公众号对应的 ApiConfig 对象即可 可以通过在请求 url 中挂参数来动态从数据库中获取
	 * ApiConfig 属性值
	 */
	public ApiConfig getApiConfig() {
		ApiConfig ac = new ApiConfig();

		// 配置微信 API 相关常量
		ac.setToken(PropKit.get("wechat.token"));
		ac.setAppId(PropKit.get("wechat.appid"));
		ac.setAppSecret(PropKit.get("wechat.appsecret"));

		/**
		 * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
		 * 2：false采用明文模式，同时也支持混合模式
		 */
		ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
		ac.setEncodingAesKey(PropKit.get("wechat.encodingaeskey",
				"setting it in config file"));
		return ac;
	}

	/**
	 * 实现父类抽方法，处理文本消息 本例子中根据消息中的不同文本内容分别做出了不同的响应，同时也是为了测试 jfinal weixin
	 * sdk的基本功能： 本方法仅测试了 OutTextMsg、OutNewsMsg、OutMusicMsg 三种类型的OutMsg，
	 * 其它类型的消息会在随后的方法中进行测试
	 */
	protected void processInTextMsg(InTextMsg inTextMsg) {
		String msgContent = inTextMsg.getContent().trim();

		// 帮助提示
		if ("help".equalsIgnoreCase(msgContent)) {
			OutTextMsg outMsg = new OutTextMsg(inTextMsg);
			outMsg.setContent(helpStr);
			render(outMsg);
		}
		/*
		 * // 图文消息测试 else if ("news".equalsIgnoreCase(msgContent)) {
		 * System.out.println("。。。。。" + msgContent + ":toUser:" +
		 * inTextMsg.getToUserName() + ":FromUser:" +
		 * inTextMsg.getFromUserName());
		 * 
		 * 
		 * OutNewsMsg outMsg = new OutNewsMsg(inTextMsg); outMsg.addNews(
		 * "JFinal 1.8 发布，JAVA 极速 WEB+ORM 框架",
		 * "现在就加入 JFinal 极速开发世界，节省更多时间去跟女友游山玩水 ^_^",
		 * "http://mmbiz.qpic.cn/mmbiz/zz3Q6WSrzq1ibBkhSA1BibMuMxLuHIvUfiaGsK7CC4kIzeh178IYSHbYQ5eg9tVxgEcbegAu22Qhwgl5IhZFWWXUw/0"
		 * ,
		 * "http://mp.weixin.qq.com/s?__biz=MjM5ODAwOTU3Mg==&mid=200313981&idx=1&sn=3bc5547ba4beae12a3e8762ababc8175#rd"
		 * ); outMsg.addNews( "JFinal 1.6 发布,JAVA极速WEB+ORM框架",
		 * "JFinal 1.6 主要升级了 ActiveRecord 插件，本次升级全面支持多数源、多方言、多缓",
		 * "http://mmbiz.qpic.cn/mmbiz/zz3Q6WSrzq0fcR8VmNCgugHXv7gVlxI6w95RBlKLdKUTjhOZIHGSWsGvjvHqnBnjIWHsicfcXmXlwOWE6sb39kA/0"
		 * ,
		 * "http://mp.weixin.qq.com/s?__biz=MjM5ODAwOTU3Mg==&mid=200121522&idx=1&sn=ee24f352e299b2859673b26ffa4a81f6#rd"
		 * ); render(outMsg); }else if ("11".equalsIgnoreCase(msgContent)) {
		 * 
		 * ApiConfig ac = new ApiConfig();
		 * 
		 * // 配置微信 API 相关常量 ac.setToken("weixin2015");
		 * ac.setAppId("wx909665f1c2a43e02");
		 * ac.setAppSecret("bd912c4da3139de597300c4b7fa6e5e4");
		 * 
		 * ac.setEncryptMessage(false); ac.setEncodingAesKey("");
		 * 
		 * ApiConfigKit.setThreadLocalApiConfig(ac); //String openId =
		 * "oiv_Wt4foBTBvBbpDTLXRLaWLPvQ"; String openId
		 * ="oiv_Wt-4uOb400vWg69wGzjAWoiY"; String accessToken =
		 * AccessTokenApi.getAccessToken().getAccessToken(); String url =
		 * "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" +
		 * accessToken + "&openid=" + openId + "&lang=zh_CN"; String userInfo =
		 * HttpKit.get(url); try { userInfo = new
		 * String(userInfo.getBytes(),"UTF-8"); } catch (Exception e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } WeiXinLog dao = new
		 * WeiXinLog(); dao.saveWeChatLog(userInfo);
		 * 
		 * OutMusicMsg outMsg = new OutMusicMsg(inTextMsg); render(outMsg);
		 * 
		 * } // 音乐消息测试 else if ("music".equalsIgnoreCase(msgContent)) {
		 * OutMusicMsg outMsg = new OutMusicMsg(inTextMsg);
		 * outMsg.setTitle("Listen To Your Heart");
		 * outMsg.setDescription("建议在 WIFI 环境下流畅欣赏此音乐");
		 * outMsg.setMusicUrl("http://www.jfinal.com/Listen_To_Your_Heart.mp3");
		 * outMsg
		 * .setHqMusicUrl("http://www.jfinal.com/Listen_To_Your_Heart.mp3");
		 * outMsg.setFuncFlag(true); render(outMsg); } else if
		 * ("美女".equalsIgnoreCase(msgContent)) { OutNewsMsg outMsg = new
		 * OutNewsMsg(inTextMsg); outMsg.addNews( "我们只看美女",
		 * "又一大波美女来袭，我们只看美女 ^_^",
		 * "https://mmbiz.qlogo.cn/mmbiz/zz3Q6WSrzq3DmIGiadDEicRIp69r1iccicwKEUOKuLhYgjibyU96ia581gCf5o3kicqz6ZLdsDyUtLib0q0hdgHtZOf4Wg/0"
		 * ,
		 * "http://mp.weixin.qq.com/s?__biz=MjM5ODAwOTU3Mg==&mid=202080887&idx=1&sn=0649c67de565e2d863bf3b8feee24da0#rd"
		 * ); outMsg.addNews( "秀色可餐", "JFinal Weixin 极速开发就是这么爽，有木有 ^_^",
		 * "http://mmbiz.qpic.cn/mmbiz/zz3Q6WSrzq2GJLC60ECD7rE7n1cvKWRNFvOyib4KGdic3N5APUWf4ia3LLPxJrtyIYRx93aPNkDtib3ADvdaBXmZJg/0"
		 * ,
		 * "http://mp.weixin.qq.com/s?__biz=MjM5ODAwOTU3Mg==&mid=200987822&idx=1&sn=7eb2918275fb0fa7b520768854fb7b80#rd"
		 * );
		 * 
		 * render(outMsg); } // 其它文本消息直接返回原值 + 帮助提示 else { OutTextMsg outMsg =
		 * new OutTextMsg(inTextMsg); outMsg.setContent("\t文本消息已成功接收，内容为： " +
		 * inTextMsg.getContent() + "\n\n" + helpStr); render(outMsg); }
		 */
	}

	/**
	 * 实现父类抽方法，处理图片消息
	 */
	protected void processInImageMsg(InImageMsg inImageMsg) {
		OutImageMsg outMsg = new OutImageMsg(inImageMsg);
		// 将刚发过来的图片再发回去
		outMsg.setMediaId(inImageMsg.getMediaId());
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理语音消息
	 */
	protected void processInVoiceMsg(InVoiceMsg inVoiceMsg) {
		OutVoiceMsg outMsg = new OutVoiceMsg(inVoiceMsg);
		// 将刚发过来的语音再发回去
		outMsg.setMediaId(inVoiceMsg.getMediaId());
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理视频消息
	 */
	protected void processInVideoMsg(InVideoMsg inVideoMsg) {
		/*
		 * 腾讯 api 有 bug，无法回复视频消息，暂时回复文本消息代码测试 OutVideoMsg outMsg = new
		 * OutVideoMsg(inVideoMsg); outMsg.setTitle("OutVideoMsg 发送");
		 * outMsg.setDescription("刚刚发来的视频再发回去"); // 将刚发过来的视频再发回去，经测试证明是腾讯官方的 api
		 * 有 bug，待 api bug 却除后再试 outMsg.setMediaId(inVideoMsg.getMediaId());
		 * render(outMsg);
		 */
		OutTextMsg outMsg = new OutTextMsg(inVideoMsg);
		outMsg.setContent("\t视频消息已成功接收，该视频的 mediaId 为: "
				+ inVideoMsg.getMediaId());
		render(outMsg);
	}

	@Override
	protected void processInShortVideoMsg(InShortVideoMsg inVideoMsg) {
		OutTextMsg outMsg = new OutTextMsg(inVideoMsg);
		outMsg.setContent("\t视频消息已成功接收，该视频的 mediaId 为: "
				+ inVideoMsg.getMediaId());
		render(outMsg);

	}

	/**
	 * 实现父类抽方法，处理地址位置消息
	 */
	protected void processInLocationMsg(InLocationMsg inLocationMsg) {
		OutTextMsg outMsg = new OutTextMsg(inLocationMsg);
		outMsg.setContent("已收到地理位置消息:" + "\nlocation_X = "
				+ inLocationMsg.getLocation_X() + "\nlocation_Y = "
				+ inLocationMsg.getLocation_Y() + "\nscale = "
				+ inLocationMsg.getScale() + "\nlabel = "
				+ inLocationMsg.getLabel());
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理链接消息 特别注意：测试时需要发送我的收藏中的曾经收藏过的图文消息，直接发送链接地址会当做文本消息来发送
	 */
	protected void processInLinkMsg(InLinkMsg inLinkMsg) {
		OutNewsMsg outMsg = new OutNewsMsg(inLinkMsg);
		outMsg.addNews("链接消息已成功接收",
				"链接使用图文消息的方式发回给你，还可以使用文本方式发回。点击图文消息可跳转到链接地址页面，是不是很好玩 :)", "",
				inLinkMsg.getUrl());
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理关注/取消关注消息
	 */
	protected void processInFollowEvent(InFollowEvent inFollowEvent) {

		OutTextMsg outMsg = new OutTextMsg(inFollowEvent);
		String accessToken = WeiXinKit.getAccessToken();
		String json = WeiXinKit
				.getUserInfo(accessToken, outMsg.getToUserName());
		// System.out.println("userInfo.." + json);
		JSONObject obj = JSON.parseObject(json);

		// 默认空信息
		String content = StringUtils.EMPTY;

		if (obj != null) {
			Integer subscribe = obj.getInteger(WeiXinOpenID.SUBSCRIBE);
			String open_Id = obj.getString(WeiXinOpenID.OPENID);
			if (subscribe == null || subscribe == 0) { // 不关注
				if (StringUtils.isNotBlank(open_Id)) {
					WeiXinOpenID.dao.setUserRoleToGuest(open_Id);
				} else {
					throw new IllegalArgumentException(
							"Openid is null! Openid: " + open_Id);
				}
			} else {
				// 欢迎信息
				content = helpStr;
				WeiXinOpenID.dao.setUserRoleToCustomer(obj, open_Id);
			}
		}
		outMsg.setContent(content);

		// 如果为取消关注事件，将无法接收到传回的信息
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理扫描带参数二维码事件
	 */
	protected void processInQrCodeEvent(InQrCodeEvent inQrCodeEvent) {
		OutTextMsg outMsg = new OutTextMsg(inQrCodeEvent);
		outMsg.setContent("processInQrCodeEvent() 方法测试成功");
		render(outMsg);
	}

	/**
	 * 处理扫码带提示应该事件回给用户，用户可收到消息
	 */
	protected void processInScanCodeWaitMsgEvent(
			InScanCodeWaitMsgEvent scanCodeWaitMsg) {
		OutTextMsg outMsg = new OutTextMsg(scanCodeWaitMsg);
		outMsg.setContent("processInScanCodeWaitMsgEvent() 方法测试成功 返回页面为："
				+ scanCodeWaitMsg.getScanCodeInfo().getScanResult());
		render(outMsg);

	}

	/**
	 * scancode_push，回应该事件给用户，用户不能收到消息
	 */
	protected void processInScanCodePushEvent(
			InScanCodePushEvent scanCodePushMsg) {
		OutTextMsg outMsg = new OutTextMsg(scanCodePushMsg);

		String accessToken = AccessTokenApi.getAccessToken().getAccessToken();
		// openId
		String openId = scanCodePushMsg.getFromUserName();
		//
		System.out.println("--openId------------------------>" + openId
				+ " accessToken:" + accessToken);
		//
		// String url =
		// "https://api.weixin.qq.com/cgi-bin/user/info?access_token="+accessToken+"&openid="+openId+"&lang=zh_CN";
		// String userInfo = HttpKit.get(url);
		// System.out.println("userInfo.."+userInfo);
		// WeiXinLog dao = new WeiXinLog();
		// dao.saveWeChatLog(userInfo);

		outMsg.setContent("processInScanCodePushEvent() 方法测试成功");
		render(outMsg);

	}

	/**
	 * 实现父类抽方法，处理上报地理位置事件
	 */
	protected void processInLocationEvent(InLocationEvent inLocationEvent) {
		OutTextMsg outMsg = new OutTextMsg(inLocationEvent);
		outMsg.setContent("processInLocationEvent() 方法测试成功");
		render(outMsg);
	}

	/**
	 * 实现父类抽方法，处理自定义菜单事件
	 */
	protected void processInMenuEvent(InMenuEvent inMenuEvent) {

		String openId =  inMenuEvent.getFromUserName();
		OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
		String eventKey = inMenuEvent.getEventKey();

		if (StringUtils.isNotBlank(eventKey)
				&& eventKey.equalsIgnoreCase(bindActionKey)) {
			
			String bingActionUrl  = basePath+"index/bindLogin?openid="+openId;
			outMsg.addNews(
					bindTitle,
					bindContent,
					bindImage,
					bingActionUrl);
		}

		render(outMsg);

		// renderOutTextMsg("processInMenuEvent() 方法测试成功");
	}

	/**
	 * 实现父类抽方法，处理接收语音识别结果
	 */
	protected void processInSpeechRecognitionResults(
			InSpeechRecognitionResults inSpeechRecognitionResults) {
		renderOutTextMsg("语音识别结果： "
				+ inSpeechRecognitionResults.getRecognition());
	}

	/**
	 * 拍图发照
	 */
	protected void processInPicSysPhotoEvent(InPicSysPhotoEvent msg) {
		renderOutTextMsg("processInPicSysPhotoEvent() 方法测试成功");

	}

	public void login() {
		System.err.println("------------------------------>login...");
	}

	public static void main(String[] args) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		// loadProp("wechat4j.properties", "wechat4j.properties");

	}

}
