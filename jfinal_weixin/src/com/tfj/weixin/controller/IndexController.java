package com.tfj.weixin.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;
import com.jfinal.kit.EncryptionKit;
import com.tfj.weixin.service.IndexService;

public class IndexController extends Controller {
	private IndexService service = new IndexService();

	/**
	 * 跳转到公司简介
	 */
	public void index() {
		render("/WEB-INF/page/index/comp_index.html");
	}

	/**
	 * 跳转到 企业绑定页面
	 */
	public void bindLogin() {

		String openId = this.getPara("openid");
		this.setAttr("openId", openId);
		setAttr("contextPath", getRequest().getContextPath());
		render("/WEB-INF/page/index/bind_login.html");
	}

	/**
	 * 企业绑定判断
	 */
	public void bindCheck() {

		String openId = this.getPara("openid");
		String username = this.getPara("username");
		String password = this.getPara("password");
		String compName = null;
		
		if (StringUtils.isBlank(openId)) {
			this.setAttr("msg", "openid失效,请重新点击企业号绑定按钮");	
		} else {
			password =  EncryptionKit.md5Encrypt(password).toUpperCase();
			
			// 从关联表查数据如果已经关联提示用户已经关联了
			compName = service.checkOpenidBind(openId);
			if (null != compName) {
				this.setAttr("msg", "用户已经成功绑定" +compName);
			} else {
				// 根据 用户名密码匹配 是否绑定 如果成功 绑定企业多一个openid
				
				
				compName =  service.bindCompanyByUserNameAndPwd(openId,
						username, password);
				if (StringUtils.isNotBlank(compName)) {
					this.setAttr("msg", "用户成功绑定" + compName);
				} else {
					this.setAttr("msg", "绑定失败,检查下用户名和密码!");
					this.setAttr("openId", openId);
					
				}
			}
			
		}
		setAttr("contextPath", getRequest().getContextPath());
		render("/WEB-INF/page/index/bind_login.html");

	}

}
