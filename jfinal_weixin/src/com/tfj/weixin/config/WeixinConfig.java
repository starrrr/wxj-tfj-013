/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.tfj.weixin.config;

import java.util.Properties;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.tx.TxByActionMethods;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.tfj.weixin.controller.IndexController;
import com.tfj.weixin.controller.WeixinMsgController;
import com.tfj.weixin.model.CompanyBind;
import com.tfj.weixin.model.CompanyOpenID;
import com.tfj.weixin.model.FrontEndRole;
import com.tfj.weixin.model.WeiXinLog;
import com.tfj.weixin.model.WeiXinOpenID;
import com.tfj.weixin.sdk.api.ApiConfigKit;
import com.tfj.weixin.sdk.kit.MenuKit;

public class WeixinConfig extends JFinalConfig {


	public Properties loadProp(String pro, String dev) {
		try {
			return loadPropertyFile(pro);
		} catch (Exception e) {
			return loadPropertyFile(dev);
		}
	}

	public void configConstant(Constants me) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadProp("wechat4j.properties", "wechat4j.properties");
		PropKit.use("wechat4j.properties");
		me.setDevMode(getPropertyToBoolean("devMode", false));
		
		me.setBaseViewPath("/webapp");

		// ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
		ApiConfigKit.setDevMode(me.getDevMode());
	}

	public void configRoute(Routes me) {

		me.add("/msg", WeixinMsgController.class);
		me.add("/index", IndexController.class);
		// me.add("/api", WeixinApiController.class, "/api");
	}

	public void configPlugin(Plugins plugins) {
		


		EhCachePlugin ecp = new EhCachePlugin();
		plugins.add(ecp);

		// 数据源插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbc.url"),
				getProperty("jdbc.username"), getProperty("jdbc.password"),
				getProperty("jdbc.driver"));
		plugins.add(c3p0Plugin);

		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		plugins.add(arp);
		arp.addMapping("t_weixin_log", WeiXinLog.class);	// 映射blog 表到 Blog模型
		arp.addMapping("t_weixin_openid", WeiXinOpenID.class);
		arp.addMapping("t_company_bind", CompanyBind.class);
		arp.addMapping("t_company_openid", CompanyOpenID.class);
		arp.addMapping("t_frontend_role", FrontEndRole.class);
		//     
		
		
	}

	public void configInterceptor(Interceptors me) {
		// 添加事务，对add、save、update和delete自动进行拦截
		me.add(new TxByActionMethods("add", "save", "update",
				"delete"));
	}

	public void configHandler(Handlers me) {

	}

	/**
	 * 创建一些菜单
	 */
	public void afterJFinalStart() {
		MenuKit.createMenu();
	
	};

	public static void main(String[] args) {
		JFinal.start("webapp", 80, "/", 5);
	}
}
