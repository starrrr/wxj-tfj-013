/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.tfj.weixin.sdk.msg.in;

/**
	接收视频消息

	<xml>
		<ToUserName><![CDATA[gh_15386516398f]]></ToUserName>
		<FromUserName><![CDATA[oRjbiwBFJ1zCuQ7mzMgRyolul_Lc]]></FromUserName>
		<CreateTime>1440404148</CreateTime>
		<MsgType><![CDATA[shortvideo]]></MsgType>
		<MediaId><![CDATA[qZhgfsarmz40-Y0IHPNv9VKvaBsxW5fSHND3ZblnW0E0oEiWj5jEbTuppdtmRBaZ]]></MediaId>
		<ThumbMediaId><![CDATA[dC1G4sfdyUso_bwJLCXa16klTHwGlLs7F_oMvWHWgVxpPR6YiTe0YiGp_d58xbfH]]></ThumbMediaId>
		<MsgId>6186488708891413362</MsgId>
	</xml>
*/
public class InShortVideoMsg extends InMsg {
	
	private String mediaId;
	private String thumbMediaId;
	private String msgId;
	
	public InShortVideoMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
		super(toUserName, fromUserName, createTime, msgType);
	}
	
	public String getMediaId() {
		return mediaId;
	}
	
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	
	public String getThumbMediaId() {
		return thumbMediaId;
	}
	
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
	
	public String getMsgId() {
		return msgId;
	}
	
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}



