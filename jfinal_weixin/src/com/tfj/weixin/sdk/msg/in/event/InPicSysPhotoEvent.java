/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.tfj.weixin.sdk.msg.in.event;

import org.sword.wechat4j.request.SendPicsInfo;

import com.tfj.weixin.sdk.msg.in.InMsg;

/** 
           拍照发图

<xml>
<ToUserName><![CDATA[gh_15386516398f]]></ToUserName>
<FromUserName><![CDATA[oRjbiwBFJ1zCuQ7mzMgRyolul_Lc]]></FromUserName>
<CreateTime>1440573185</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[pic_sysphoto]]></Event>
<EventKey><![CDATA[拍照发图]]></EventKey>
<SendPicsInfo><Count>1</Count>
	<PicList>
	<item><PicMd5Sum><![CDATA[8ec1fb71fca5864238a1032d2f9d1e28]]></PicMd5Sum></item>
	</PicList>
</SendPicsInfo>
</xml>
	
	
	2. 回应事件  回应该事件给用户，用户不可收到消息
<xml>
    <ToUserName><![CDATA[gh_82479813ed64]]></ToUserName>
    <FromUserName><![CDATA[ojpX_jig-gyi3_Q9fHXQ4rdHniQs]]></FromUserName>
    <CreateTime>1412075451</CreateTime>
    <MsgType><![CDATA[event]]></MsgType>
    <Event><![CDATA[scancode_push]]></Event>
    <EventKey><![CDATA[rselfmenu_0_1]]></EventKey>
    <ScanCodeInfo>
        <ScanType><![CDATA[qrcode]]></ScanType>
        <ScanResult><![CDATA[http://weixin.qq.com/r/pUNnf4HEX9wgrcUc9xa3]]></ScanResult>
        <EventKey><![CDATA[rselfmenu_0_1]]></EventKey>
    </ScanCodeInfo>
</xml>
 */
public class InPicSysPhotoEvent extends InMsg {
	
	//   扫码带提示
	private String event;
	
	
	private String eventKey;

	
	private  SendPicsInfo picsInfo;
	
	public InPicSysPhotoEvent(String toUserName, String fromUserName, Integer createTime, String msgType, SendPicsInfo picsInfo) {
		super(toUserName, fromUserName, createTime, msgType);
		this.picsInfo = picsInfo;
	}
	
	public String getEvent() {
		return event;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}
	
	public String getEventKey() {
		return eventKey;
	}
	
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public SendPicsInfo getPicsInfo() {
		return picsInfo;
	}

	public void setPicsInfo(SendPicsInfo picsInfo) {
		this.picsInfo = picsInfo;
	}
	
	

	
}




