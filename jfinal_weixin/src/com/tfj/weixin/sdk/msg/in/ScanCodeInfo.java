package com.tfj.weixin.sdk.msg.in;

public class ScanCodeInfo {

	private String scanType;
	private String scanResult;


	public ScanCodeInfo() {

	}

	public ScanCodeInfo(String scanType, String scanResult) {
		this.scanType = scanType;
		this.scanResult =scanResult;

	}

	public String getScanType() {
		return scanType;
	}

	public void setScanType(String scanType) {
		this.scanType = scanType;
	}

	public String getScanResult() {
		return scanResult;
	}

	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}


	
	
}
