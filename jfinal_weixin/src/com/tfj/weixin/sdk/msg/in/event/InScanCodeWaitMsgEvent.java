/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.tfj.weixin.sdk.msg.in.event;

import com.tfj.weixin.sdk.msg.in.InMsg;
import com.tfj.weixin.sdk.msg.in.ScanCodeInfo;

/** 
            扫码带提示
	回应该事件给用户，用户可收到消息

	<xml>
		<ToUserName><![CDATA[gh_15386516398f]]></ToUserName>
		<FromUserName><![CDATA[oRjbiwBFJ1zCuQ7mzMgRyolul_Lc]]></FromUserName>
		<CreateTime>1440570472</CreateTime>
		<MsgType><![CDATA[event]]></MsgType>
		<Event><![CDATA[scancode_waitmsg]]></Event>
		<EventKey><![CDATA[扫码带提示]]></EventKey>
		<ScanCodeInfo>
			<ScanType><![CDATA[qrcode]]></ScanType>
			<ScanResult><![CDATA[http://weixin.qq.com/r/kDvq8jjEEmVtrXyR926C]]></ScanResult>
		</ScanCodeInfo>
	</xml>
	
	
	2. 回应事件  回应该事件给用户，用户可收到消息
	<xml>
	    <ToUserName><![CDATA[gh_82479813ed64]]></ToUserName>
	    <FromUserName><![CDATA[ojpX_jig-gyi3_Q9fHXQ4rdHniQs]]></FromUserName>
	    <CreateTime>1412075435</CreateTime>
	    <MsgType><![CDATA[event]]></MsgType>
	    <Event><![CDATA[scancode_waitmsg]]></Event>
	    <EventKey><![CDATA[rselfmenu_0_0]]></EventKey>
	    <ScanCodeInfo>
	        <ScanType><![CDATA[qrcode]]></ScanType>
	        <ScanResult><![CDATA[http://weixin.qq.com/r/pUNnf4HEX9wgrcUc9xa3]]></ScanResult>
	        <EventKey><![CDATA[rselfmenu_0_0]]></EventKey>
	    </ScanCodeInfo>
	</xml>
 */
public class InScanCodeWaitMsgEvent extends InMsg {
	
	//   扫码带提示
	private String event;
	
	
	private String eventKey;

	
	private  ScanCodeInfo scanCodeInfo;
	
	public InScanCodeWaitMsgEvent(String toUserName, String fromUserName, Integer createTime, String msgType, ScanCodeInfo scanCodeInfo) {
		super(toUserName, fromUserName, createTime, msgType);
		this.scanCodeInfo = scanCodeInfo;
	}
	
	public String getEvent() {
		return event;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}
	
	public String getEventKey() {
		return eventKey;
	}
	
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	
	

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}
	
	
}




