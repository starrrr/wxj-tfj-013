package com.tfj.weixin.sdk.kit;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2015/6/9.
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

    private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

    private static SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM");

    private static SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat nowFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // 获得当前时间 精确到秒
    public static String getCurrentTime() {
        return nowFormat.format(new Date());
    }
    
    public static String getCurrentDay() {
    	return dayFormat.format(new Date());
    }
}
