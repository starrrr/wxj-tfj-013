package com.tfj.weixin.sdk.kit;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.tfj.weixin.sdk.api.AccessToken;
import com.tfj.weixin.sdk.api.AccessTokenApi;
import com.tfj.weixin.sdk.api.ApiConfig;
import com.tfj.weixin.sdk.api.ApiConfigKit;

public class WeiXinKit {

	public static String getUserInfo(String accessToken, String openId) {
		String userInfo = StringUtils.EMPTY;
		if (StringUtils.isNotBlank(openId)) {
			

		
			// TODO 用 logger
//			System.out.println("-------->[openId=" + openId + "][accessToken=" + accessToken + "]");
			String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + 
					 accessToken.trim() + "&openid=" + openId.trim() + "&lang=zh_CN";
			System.out.println("------------->"+url);
			userInfo = HttpKit.get(url);
			System.out.println("............"+userInfo);
		}
		return userInfo;
	}
	
	public static String getAccessToken(){
		ApiConfig ac = new ApiConfig();
		ac.setAppId(PropKit.get("wechat.appid"));
		ac.setAppSecret(PropKit.get("wechat.appsecret"));
		ApiConfigKit.setThreadLocalApiConfig(ac);
		
		AccessToken at =AccessTokenApi.getAccessToken();
		if (at.isAvailable())
			System.out.println("access_token : " + at.getAccessToken());
		else
			System.out.println(at.getErrorCode() + " : " + at.getErrorMsg());
		return at.getAccessToken();
	}
}