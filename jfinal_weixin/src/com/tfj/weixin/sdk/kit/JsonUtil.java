package com.tfj.weixin.sdk.kit;

import com.alibaba.fastjson.JSON;
import com.tfj.weixin.model.WeChatBean;


public class JsonUtil {

	/**
	 * 从json字符串中解析出java对象
	 * 
	 * @author:qiuchen
	 * @createTime:2012-7-8
	 * @param jsonStr
	 *            json格式字符串
	 * @param clazz
	 *            java类
	 * @return clazz的实例
	 */
	public static Object parseJavaObject(String jsonStr, Class clazz) {
		return JSON.toJavaObject(JSON.parseObject(jsonStr), clazz);
	}

	public static String parseObjectToJson(Object obj) {

		String json = JSON.toJSONString(obj, true);
		return json;
	}

	/**
	 * 测试方法
	 * 
	 * @author:qiuchen
	 * @createTime:2012-7-8
	 * @param args
	 */
	public static void main(String[] args) {
		
		String json ="{'subscribe':1,'openid':'oRjbiwBFJ1zCuQ7mzMgRyolul_Lc','nickname':'小书童','sex':1,'language':'zh_CN','city':'广州','province':'广东','country':'中国','headimgurl':'e','subscribe_time':1440405643,'remark':'张龙浩粉丝','groupid':0}";
		WeChatBean log = (WeChatBean) JsonUtil.parseJavaObject(json, WeChatBean.class);
		System.out.println(log.getNickname());
		
		

	}
}
