package com.tfj.weixin.sdk.kit;

import java.util.ArrayList;
import java.util.List;

import org.sword.wechat4j.event.EventType;
import org.sword.wechat4j.exception.WeChatException;
import org.sword.wechat4j.menu.Menu;
import org.sword.wechat4j.menu.MenuButton;
import org.sword.wechat4j.menu.MenuManager;

import com.jfinal.kit.PropKit;

public class MenuKit {
	private static MenuManager manager = new MenuManager();

	public static void createMenu() {

	
		MenuButton scanMenuBtn = new MenuButton();
		scanMenuBtn.setName("溯•天眼");
		scanMenuBtn.setKey("WECAHT_SCAN");
		scanMenuBtn.setType(EventType.scancode_push);

		
		MenuButton bindMenuBtn = new MenuButton();
		bindMenuBtn.setName("企业绑一绑");
		bindMenuBtn.setKey("COMPANY_BIND");
		bindMenuBtn.setType(EventType.click);
		
		List<MenuButton> subBut1 = new ArrayList<MenuButton>();
		subBut1.add(scanMenuBtn);
		subBut1.add(bindMenuBtn);

		MenuButton whdMenuBtn = new MenuButton();
		whdMenuBtn.setName("微活动");
		whdMenuBtn.setKey("STY");
		whdMenuBtn.setSubButton(subBut1);

		String indexURL = PropKit.get("tfj_index_url");
		MenuButton indexMenuBtn = new MenuButton();
		indexMenuBtn.setName("知•天方健");
		indexMenuBtn.setKey("COMPANY_INDEX");
		indexMenuBtn.setType(EventType.view);
		indexMenuBtn.setUrl(indexURL);

		String weishopURL = PropKit.get("tfj_weishop_url");
		MenuButton wechatSaleMenuBtn = new MenuButton();
		wechatSaleMenuBtn.setName("天方健商城");
		wechatSaleMenuBtn.setKey("COMPANY_SALE");
		wechatSaleMenuBtn.setUrl(weishopURL);
		wechatSaleMenuBtn.setType(EventType.view);

		List<MenuButton> buttons = new ArrayList<MenuButton>();
		buttons.add(indexMenuBtn);
		buttons.add(whdMenuBtn);
		buttons.add(wechatSaleMenuBtn);

		Menu menu = new Menu();
		menu.setButton(buttons);

		try {

			manager.create(menu);
			System.out.println("菜单创建成功!!!!!!!!!!!!!!!!!!!");
		} catch (WeChatException e) {
			System.out.println("菜单创建失败3333");
			e.printStackTrace();
		}
	}
}
