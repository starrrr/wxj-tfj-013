package com.tfj.weixin.service;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.tfj.weixin.model.CompanyBind;
import com.tfj.weixin.model.CompanyOpenID;
import com.tfj.weixin.model.FrontEndRole;
import com.tfj.weixin.model.WeiXinOpenID;

public class IndexService {

	public static final CompanyBind cmpbindDao = new CompanyBind();
	public static final CompanyOpenID cmpOpenIdDao = new CompanyOpenID();
	public static final FrontEndRole frontRoleDao = new FrontEndRole();
	public static final WeiXinOpenID openIdDao = new WeiXinOpenID();

	public String checkOpenidBind(String openId) {
		CompanyOpenID compOpenID = cmpOpenIdDao.findByOpenId(openId);
		String companyName = null;
		if (null != compOpenID && StringUtils.isNotBlank(compOpenID.getLong(CompanyOpenID.COMPANY_ID).toString())) {

			CompanyBind cmpBind = cmpbindDao.findByCompanyId(compOpenID
					.getLong(CompanyOpenID.COMPANY_ID).toString());
			companyName = cmpBind.getStr(CompanyBind.NAME);
		}
		return companyName;
	}
	
	@Before(Tx.class)
	public String bindCompanyByUserNameAndPwd(String openId, String userName,String password){
	
		CompanyBind cmpBind = cmpbindDao.findByUserNameAndPwd(userName, password);
		String cmpName = null;
		if(null!=cmpBind){
			cmpName = cmpBind.getStr(CompanyBind.NAME);
			
			//关联表增加一条信息 把openId role换成B
			CompanyOpenID cmpOpenId = new CompanyOpenID();
			cmpOpenId.set(CompanyOpenID.COMPANY_ID, cmpBind.getLong(CompanyBind.ID));
			cmpOpenId.set(CompanyOpenID.FRONT_ROLE, "B");
			cmpOpenId.set(CompanyOpenID.OPENID, openId);
			cmpOpenId.set(CompanyOpenID.IS_BIND, 1);
			cmpOpenId.save();
			
			// 根究当前的 openID 角色改成B
			FrontEndRole role = frontRoleDao.findFrontEndRoleByRoleName("B");
			
			WeiXinOpenID  weChatOpenId = openIdDao.findByOpenId(openId);
			weChatOpenId.set("role_id", role.getLong(FrontEndRole.ID));
			weChatOpenId.set("role_name", role.getStr(FrontEndRole.NAME));
			weChatOpenId.update();
			
	
		}
		
		return cmpName;
		
	}

}
