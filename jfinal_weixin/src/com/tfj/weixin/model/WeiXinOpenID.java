package com.tfj.weixin.model;

import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.weixin.model.enumeration.FrontEndRole;
import com.tfj.weixin.sdk.kit.WeiXinKit;

public class WeiXinOpenID extends Model<WeiXinOpenID> {

	private static final long serialVersionUID = 5701398753533478081L;
	
	public static final WeiXinOpenID dao = new WeiXinOpenID();
	
	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";

	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subscribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";

	public static final String REMARK = "remark";
	public static final String GROUPID = "groupid";
	public static final String ROLE_ID = "role_id";
	public static final String ROLE_NAME = "role_name";
	
	public void setUserRoleToCustomer (JSONObject obj, String open_id) {
		WeiXinOpenID user = findByOpenId(open_id);
		if (user != null) {
			if (user.getLong(WeiXinOpenID.ROLE_ID) != FrontEndRole.C.getKey()) {
				user.set(WeiXinOpenID.SUBSCRIBE, 1);
				user.set(WeiXinOpenID.ROLE_ID, FrontEndRole.C.getKey());
				user.set(WeiXinOpenID.ROLE_NAME, FrontEndRole.C.name());
				user.update();
			}
		} else {
			user = constructVO(obj, FrontEndRole.C);
			user.save();
		}
	}
	
	public void setUserRoleToGuest(String open_id) {
		WeiXinOpenID user = findByOpenId(open_id);
		if (user != null) {
			user.set(WeiXinOpenID.SUBSCRIBE, 0);
			user.set(WeiXinOpenID.ROLE_ID, FrontEndRole.G.getKey());
			user.set(WeiXinOpenID.ROLE_NAME, FrontEndRole.G.name());
			user.update();
		}
	}
	
	public WeiXinOpenID findByOpenId(String openid) {
		String sql = "select * FROM t_weixin_openid WHERE openid = ?";
		return dao.findFirst(sql, openid);
	}

	/**
	 * 构造
	 * @param bean
	 * @return
	 */
	public WeiXinOpenID constructVO(JSONObject bean, FrontEndRole role) {

		WeiXinOpenID user = new WeiXinOpenID();
		
		user.set(WeiXinOpenID.SUBSCRIBE, bean.getString(WeiXinOpenID.SUBSCRIBE));
		user.set(WeiXinOpenID.OPENID, bean.getString(WeiXinOpenID.OPENID));
		String nickName = bean.getString(WeiXinOpenID.NICKNAME);
		if(StringUtils.isNotBlank(nickName)){
			 nickName = StringFilter(nickName).replaceAll("\"", "").trim();
		}else{
			 nickName = StringUtils.EMPTY;
		}
		user.set(WeiXinOpenID.NICKNAME, nickName);

		user.set(WeiXinOpenID.SEX, bean.getInteger(WeiXinOpenID.SEX));
		user.set(WeiXinOpenID.LANGUAGE, bean.getString(WeiXinOpenID.LANGUAGE));
		user.set(WeiXinOpenID.CITY, bean.getString(WeiXinOpenID.CITY));
		user.set(WeiXinOpenID.PROVINCE, bean.getString(WeiXinOpenID.PROVINCE));
		user.set(WeiXinOpenID.COUNTRY, bean.getString(WeiXinOpenID.COUNTRY));
		user.set(WeiXinOpenID.SUBSCRIBE_TIME, bean.getLong(WeiXinOpenID.SUBSCRIBE_TIME));
		user.set(WeiXinOpenID.HEADIMGURL, bean.getString(WeiXinOpenID.HEADIMGURL));
		user.set(WeiXinOpenID.REMARK, bean.getString(WeiXinOpenID.REMARK));
		user.set(WeiXinOpenID.GROUPID, bean.getInteger(WeiXinOpenID.GROUPID));
		
		user.set(WeiXinOpenID.ROLE_ID, role.getKey());
		user.set(WeiXinOpenID.ROLE_NAME, role.name());
		return user;
	}

	public static String StringFilter(String str) throws PatternSyntaxException {
		if(null == str){
			return "";
		}
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		return str.replaceAll(reg, "").trim();
	}
	
	public void demo() {
		String accessToken = WeiXinKit.getAccessToken();
		String select = "select * from t_weixin_openid";
		Page<WeiXinOpenID> all = paginate(1, Integer.MAX_VALUE, select, StringUtils.EMPTY);
		if (all != null) {
			List<WeiXinOpenID> ds = all.getList();
			for (WeiXinOpenID wxoi : ds) {
				String openId = wxoi.getStr(WeiXinOpenID.OPENID);
				String nickname = wxoi.getStr(WeiXinOpenID.NICKNAME);
				
				if (StringUtils.isBlank(nickname) && StringUtils.isNotBlank(openId)) {
					String userInfo = WeiXinKit.getUserInfo(accessToken,openId);
					JSONObject obj = JSON.parseObject(userInfo);
					if (obj != null) {
						nickname = obj.getString(WeiXinOpenID.NICKNAME);
						if(StringUtils.isNotBlank(nickname)){
							nickname = StringFilter(nickname).replaceAll("\"", "").trim();
						}else{
							nickname = StringUtils.EMPTY;
						}
						
						wxoi.set(WeiXinOpenID.NICKNAME, nickname);
						wxoi.set(WeiXinOpenID.SEX, obj.getInteger(WeiXinOpenID.SEX));
						wxoi.set(WeiXinOpenID.LANGUAGE, obj.getString(WeiXinOpenID.LANGUAGE));
						wxoi.set(WeiXinOpenID.CITY, obj.getString(WeiXinOpenID.CITY));
						wxoi.set(WeiXinOpenID.PROVINCE, obj.getString(WeiXinOpenID.PROVINCE));
						wxoi.set(WeiXinOpenID.COUNTRY, obj.getString(WeiXinOpenID.COUNTRY));
						wxoi.set(WeiXinOpenID.SUBSCRIBE_TIME, obj.getLong(WeiXinOpenID.SUBSCRIBE_TIME));
						wxoi.set(WeiXinOpenID.HEADIMGURL, obj.getString(WeiXinOpenID.HEADIMGURL));
						wxoi.set(WeiXinOpenID.UNIONID, obj.getString(WeiXinOpenID.UNIONID));
						wxoi.set(WeiXinOpenID.REMARK, obj.getString(WeiXinOpenID.REMARK));
						wxoi.set(WeiXinOpenID.GROUPID, obj.getInteger(WeiXinOpenID.GROUPID));
						wxoi.update();
					}
				}
			}
		}
	}


	public static void main(String[] args) {
		//String str = "*adCVs*34_a _09_b5*[/435^*&城池()^$$&*).{}+.|.)%%*(*.中国}34{45[]12.fd'*&999下面是中文的字符￥……{}【】。，；’“‘”？";
//		String str ="飞天猪'!?,.\"@&$)(;:-🙅💖🌻";
//		System.out.println(str);
//		System.out.println(StringFilter(str));
		String s1="我是正确测试数据aasdf2342343ASFASDF";
		String s2="飞天猪'!?,.\"@&$)(;:-🙅💖🌻我是错误测试数据@#！@#";
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		System.out.println(s1.replaceAll(reg,""));
		System.out.println(s2.replaceAll(reg,""));
	}

}
