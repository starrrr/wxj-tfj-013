package com.tfj.weixin.model;

import com.jfinal.plugin.activerecord.Model;
public class CompanyBind extends Model<CompanyBind> {

	private static final long serialVersionUID = -8085563989702939875L;
	
	public static final CompanyBind dao = new CompanyBind();
	
	public static final String ID = "id";
	public static final String COMPANY_USER = "company_user";
	public static final String COMPANY_PWD = "company_pwd";
	public static final String NAME = "name";
	public static final String TELEPHONE = "telephone";
	public static final String ADDRESS = "address";
	public static final String website = "website";
	public static final String operate_time = "operate_time";
	public static final String is_company = "is_company";
	
	public CompanyBind findByCompanyId(String companyId) {
		String sql = "select * FROM t_company_bind WHERE id = ?";
		return dao.findFirst(sql, companyId);
	}
	
	public CompanyBind findByUserNameAndPwd(String userName , String pwd) {
		String sql = "select * FROM t_company_bind WHERE company_user = ? and company_pwd =?";
		return dao.findFirst(sql, userName, pwd);
	}
	

}
