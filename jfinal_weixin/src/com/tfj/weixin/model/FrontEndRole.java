package com.tfj.weixin.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */

public class FrontEndRole extends Model<FrontEndRole> {
   
	private static final long serialVersionUID = 7073686964207254969L;
	public static final FrontEndRole dao = new FrontEndRole();
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String UPDATE_TIME = "update_time";
    
    // 前端角色组
    public static final String GUEST = "G";
    public static final String CUSTOMER = "C";
    public static final String BUSINESS = "B";

    public FrontEndRole findFrontEndRoleByRoleName(String roleName) {
		String sql = "SELECT * FROM t_frontend_role WHERE name='" + roleName+ "'";
		return dao.findFirst(sql);
	}
}
