package com.tfj.weixin.model.enumeration;

public enum FrontEndRole {
	
	G(1L, "游客组"),
	B(2L, "企业用户组"), 
	C(3L, "关注用户组");
	
	private Long key;
	
	private String remark;
	
	public Long getKey() {
		return key;
	}
	
	public String getRemark() {
		return remark;
	}
	
	private FrontEndRole(Long key, String remark) {
		this.key = key;
		this.remark = remark;
	}
}
