package com.tfj.weixin.model;

import com.jfinal.plugin.activerecord.Model;

public class CompanyOpenID extends Model<CompanyOpenID> {

	private static final long serialVersionUID = -8085563989702939875L;
	
	public static final CompanyOpenID dao = new CompanyOpenID();
	
	public static final String ID = "id";
	public static final String COMPANY_ID = "company_id";
	public static final String OPENID = "openid";
	public static final String FRONT_ROLE = "front_role";
	public static final String IS_BIND = "is_bind";

	
	public  CompanyOpenID findByOpenId(String openid) {
		String sql = "select * FROM t_company_openid WHERE openid = ?";
		return dao.findFirst(sql, openid);
	}
	
}
