package com.tfj.weixin.model;


import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.plugin.activerecord.Model;
import com.tfj.weixin.sdk.kit.DateUtils;
import com.tfj.weixin.sdk.kit.JsonUtil;

public class WeiXinLog extends Model<WeiXinLog> {

	private static final long serialVersionUID = 5701398753533478081L;
	public static final WeiXinLog dao = new WeiXinLog();
	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";

	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subscribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";

	public static final String REMARK = "remark";
	public static final String GROUPID = "groupid";
	public static final String SCAN_TIME = "scan_time";

	public void saveWeChatLog(String json) {
		WeChatBean bean = (WeChatBean) JsonUtil.parseJavaObject(json,
				WeChatBean.class);
		if (null != bean) {
			WeiXinLog log = constructWeiXinLog(bean);
			log.save();
		}
	}

	public WeiXinLog constructWeiXinLog(WeChatBean bean) {

		WeiXinLog log = new WeiXinLog();

		System.out.println(bean.getNickname()+ "-----" + StringFilter(bean.getNickname()).replaceAll("\"", "").replaceAll("🙅", ""));

		String currentDate = DateUtils.getCurrentTime();
		log.set("subscribe", bean.getSubscribe());
		log.set("openid", bean.getOpenid());
		
		if (null != bean.getNickname()) {
			log.set("nickname", StringFilter(bean.getNickname()).replaceAll("\"", ""));
		} else {
			log.set("nickname", StringUtils.EMPTY);
		}

		log.set("sex", bean.getSex());
		log.set("language", bean.getLanguage());
		log.set("city", bean.getCity());
		log.set("province", bean.getProvince());
		log.set("country", bean.getCountry());
		log.set("subscribe_time", bean.getSubscribeTime());
		log.set("headimgurl", bean.getHeadimgurl());
		log.set("unionid", bean.getUnionid());
		log.set("remark", bean.getRemark());
		log.set("groupid", bean.getGroupid());
		log.set("scan_time", currentDate);
		return log;
	}

	public static String StringFilter(String str) throws PatternSyntaxException {
		if(null== str){
			return "";
		}
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		return str.replaceAll(reg,"").trim();
	}


	public static void main(String[] args) {
		//String str = "*adCVs*34_a _09_b5*[/435^*&城池()^$$&*).{}+.|.)%%*(*.中国}34{45[]12.fd'*&999下面是中文的字符￥……{}【】。，；’“‘”？";
//		String str ="飞天猪'!?,.\"@&$)(;:-🙅💖🌻";
//		System.out.println(str);
//		System.out.println(StringFilter(str));
		String s1="我是正确测试数据aasdf2342343ASFASDF";
		String s2="飞天猪'!?,.\"@&$)(;:-🙅💖🌻我是错误测试数据@#！@#";
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		System.out.println(s1.replaceAll(reg,""));
		System.out.println(s2.replaceAll(reg,""));
	}

}
