package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.tfj.beta.utils.JsonUtil;
import com.tfj.beta.utils.PropertiesUtil;

public class TestGetJson {
	private static Connection conn = null;
	static {
		try {
			Class.forName(PropertiesUtil.getJDBCDriver());
			conn = DriverManager.getConnection(PropertiesUtil.getJDBCUrl(),
					PropertiesUtil.getJDBCUsername(),
					PropertiesUtil.getJDBCPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		TestGetJson t=new TestGetJson();
		System.err.println(JsonUtil.parseObjectToJson(t.getJsonBean()));
	}

	public List<JsonBean> getJsonBean() {
		List<JsonBean> beans = new ArrayList<JsonBean>();
		List<User> users = findFu();
		for (User user : users) {
			constructJSONBean(beans, user);
			//break;
		}
		return beans;

	}

	public JsonBean constructJSONBean(List<JsonBean> beans, User user) {
		JsonBean bean = null;
		bean=createBean(bean,user,beans);
		return bean;
	}

	private JsonBean createBean(JsonBean bean,User user,List<JsonBean> beans) {
		bean= new JsonBean();
		bean.setId(user.getId());
		bean.setPid(user.getPid());
		bean.setContent(user.getContent());
		beans.add(bean);
		
		List<User> users = findUserByPid(user.getId());
		List<JsonBean> childs = new ArrayList<JsonBean>();
		for (User u: users) {
			constructJSONBean(childs, u);
		}
		bean.setChild(childs);
		return bean;
	}

	private List<User> findUserByPid(int id) {
		List<User> users = new ArrayList<User>();
		Statement stmt = null;
		ResultSet res = null;
		User user = null;
		try {
			stmt = conn.createStatement();
			res = stmt.executeQuery("select * from pinlun where pid="+id);
			while (res.next()) {
				user=new User();
				user.setId(res.getInt("id"));
				user.setPid(res.getInt("pid"));
				user.setContent(res.getString("content"));
				users.add(user);
			}
		} catch (Exception e) {
		}
		return users;
	}

	public static List<User> findFu() {
		List<User> users = new ArrayList<User>();
		Statement stmt = null;
		ResultSet res = null;
		User user = null;
		try {
			stmt = conn.createStatement();
			res = stmt.executeQuery("select * from pinlun where pid=0");
			while (res.next()) {
				user=new User();
				int id=res.getInt("id");
				int pid=res.getInt("pid");
				String content=res.getString("content");
				user.setId(id);
				user.setPid(pid);
				user.setContent(content);
				users.add(user);
			}
		} catch (Exception e) {
		}
		return users;
	}

}
