package com.test;

import java.util.List;

public class JsonBean {
	
	private int id;
	private int pid;
	private String content;
	private List<JsonBean> child;
	public int getId() {
		return id;
	}
	public List<JsonBean> getChild() {
		return child;
	}
	public void setChild(List<JsonBean> child) {
		this.child = child;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[id="+id  
                +",pid="+pid
                +",content="+content
                +",child="+child+"]"; 
	}

}
