package com.test;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.tfj.beta.model.Product;

public class ChartGraphics {
	BufferedImage image;

	void createImage(String fileLocation) {
		try {
			FileOutputStream fos = new FileOutputStream(fileLocation);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(bos);
			encoder.encode(image);
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void graphicsGeneration(String name, String id, String classname,
			File file,String flow_batch_num,Product product) {
		int imageWidth = 400;// 图片的宽度
		int imageHeight = 500;// 图片的高度
		image = new BufferedImage(imageWidth, imageHeight,
				BufferedImage.TYPE_INT_RGB);
		Graphics graphics = image.getGraphics();
		Font f = new Font("宋体", Font.BOLD, 25);  
		graphics.setFont(f);  
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, imageWidth, imageHeight);
		graphics.setColor(Color.BLACK);
		
		graphics.drawString("微信扫一扫 ", 130, 40);
		graphics.drawString("名称： "+product.getStr(Product.CATEGORY_NAME),50,350);
		graphics.drawString("批号： "+flow_batch_num,50,380);
		graphics.drawString("天方健(中国)药业有限公司",50,410);
		graphics.drawString("全程可追溯系统",50,440);
		BufferedImage bimg = null;
		try {
			bimg = javax.imageio.ImageIO.read(file);
		} catch (Exception e) {
		}
		if (bimg != null)
			graphics.drawImage(bimg,50,45, null);
		graphics.dispose();
		createImage(file.getPath());
		// ImageIcon imageIcon = new ImageIcon(imgurl);
		// graphics.drawImage(imageIcon.getImage(), 230, 0, null);
		// 改成这样:
	}
	/*public static void main(String[] args) {
		ChartGraphics cg = new ChartGraphics();
		try {
			cg.graphicsGeneration("ewew", "1", "12",
					"D:/QRCode_admin1444357787494.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
}