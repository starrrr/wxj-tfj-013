package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tfj.beta.utils.PropertiesUtil;

public class ReaderText2OpenId {
	
	private static Connection conn=null;
	static{
		 try {
			Class.forName(PropertiesUtil.getJDBCDriver()) ;
			conn =DriverManager.getConnection(PropertiesUtil.getJDBCUrl() , PropertiesUtil.getJDBCUsername() , PropertiesUtil.getJDBCPassword() ) ;   
		} catch (Exception e) {
			e.printStackTrace();
		}   
	}
	
	public static void main(String[] args) {
		readText("D:\\error.txt");
	}
	
	public static List<String> readText(String path){
		List<String> list= new ArrayList<String>();
		BufferedReader br=null;
		try {
			br=new BufferedReader(new FileReader(new File(path)));
			String line = null;
			while ((line = br.readLine())!= null) {
				addData(line);
			}
		} catch (Exception e) {
		}finally{
			try {
				if(br!=null){
					br.close();
				}
			} catch (IOException e) {
			}
		}
		return list;
	}
	
	public static void addData(String line){
		Statement stmt=null;
		ResultSet res =null;
		Boolean isFlag=false;
		try {
			stmt = conn.createStatement() ;
			res = stmt.executeQuery("select * from t_weixin_openid");
			String openid="";
			while(res.next()){
				openid=res.getString("openid");
				if(openid.equals(line)){
					isFlag=true;
					break;
				}
			}
			if(isFlag){
				stmt.executeUpdate("update t_weixin_openid set role_name='C',role_id='3' WHERE openid='"+openid+"'");
				System.err.println("===update===:"+line);
			}else{
				stmt.executeUpdate("insert into t_weixin_openid(openid,role_id,role_name) values('"+line+"','3','C')");
				System.err.println("===insert===:"+line);
			}
		} catch (Exception e) {
		}
	}
}
