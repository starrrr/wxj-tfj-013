<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="">
  	</#if>
  </span>
</#if>

<span class="rightValue">
	<a name="${anchor_name}">
		<span class="emptyPosition"></span>
	</a>
	
	<#if show_release>
		<a class="blue tooltip-success unlock" href="#" name="${input_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  	</#if>
  	
	<#if disabled != 'disabled'>
	<span class="box">
		<a href="javascript:void(0);"  class="link">上传文件</a>
		<span class="textbox" name="copyFile">未选择文件</span>
		<input class="uploadFile ${process_type!}-widget" type="file" id="${input_id!}" name="${input_name!}" value="${file_value!}" size="30" ${disabled}>
	</span>
	</#if>
</span>
