<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="">
  	</#if>
  </span>
</#if>

<span class="rightValue">
  <a name="${anchor_name}">
  	<span class="emptyPosition"></span>
  </a>
  
  <#if show_release>
  	<a class="blue tooltip-success unlock" href="#" name="${select_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  </#if>
  
  <select name="${select_name!}" class="${process_type!}-widget" ${disabled} style="width : 160px">
    <option value="">---请选择---</option>
    <#if data_source?exists>
      <#list data_source?keys as dsKey>
    <option value ="${dsKey}" <#if (dsKey?? && select_value??) && dsKey = select_value>selected="selected"</#if>>${data_source[dsKey]}</option>
      </#list>
    </#if>
  </select>
</span>