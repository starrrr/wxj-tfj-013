<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="">
  	</#if>
  </span>
</#if>

<span class="rightValue dateRangeChange">
  <a name="${anchor_name}">
  	<span class="emptyPosition"></span>
  </a>
  
  <#if show_release>
  	<a class="blue tooltip-success unlock" href="#" name="${input_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  </#if>

  <span class="input-daterange input-group ${process_type!}-widget" name="${input_name}" data-date-format="yyyy-mm-dd" ${disabled}>
    <input type="text" class="form-control" id="${input_name}_start" name="start" value="${date_start}" ${disabled} >
    <span class="input-group-addon">至</span>
    <input type="text" class="form-control" id="${input_name}_end" name="end" value="${date_end}" ${disabled}>
  </span>
</span>