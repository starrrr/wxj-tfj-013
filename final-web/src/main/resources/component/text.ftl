<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="+">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="-">
  	</#if>
  </span>
</#if>



<span class="rightValue">
  <a name="${anchor_name}">
  	<span class="emptyPosition"></span>
  </a>
  
  <#if show_release>
  	<a class="blue tooltip-success unlock" href="#" name="${input_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  </#if>
  
  <input class="${process_type!}-widget" type="text" name="${input_name!}" value="${input_value}" ${disabled}>
  <#if mix?? && mix = 'MIX' && disabled != 'disabled'>
    <a class="green tooltip-success edit toPvForm" href="${contextPath}admin/productValue/dataPage?category_id=${category_id!}" data-rel="tooltip" title="混批" data-toggle="modal" data-target="#editModal">
  	  <img class="getPvVal" src="${contextPath}static/admin/index/img/icon_makeup.png" alt="${input_name!}">
    </a>
  </#if>
</span>