<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="">
  	</#if>
  </span>
</#if>

<span class="rightValue dateChange">
  <a name="${anchor_name}">
  	<span class="emptyPosition"></span>
  </a>
  
  <#if show_release>
  	<a class="blue tooltip-success unlock" href="#" name="${input_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  </#if>
  
  <span class="input-group">
    <input type="text" class="form-control date-picker ${process_type!}-widget" name="${input_name!}" value="${input_value}" data-date-format="yyyy-mm-dd" ${disabled}>
      <span class="input-group-addon">
        <i class="fa fa-calendar bigger-110"></i>
      </span>
  </span>
</span>