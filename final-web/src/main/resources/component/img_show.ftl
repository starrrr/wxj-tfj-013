<span class="imgShow">
	<img src="${contextPath}${file_value!}" alt="">
	
	<#if show_delete>
		<span class="closeBtn">
			<img name="${input_name!}" onclick="removeImgValue(this)" src="${contextPath}static/admin/index/img/delete_btn_red.png"/>
		</span>
	</#if>
</span>