<#if multiVal??>
  <span class="multiVal">
  	<#if multiVal = 'plus'>
  	  <img class="addPvVal" src="${contextPath}static/admin/index/img/add.png" alt="">
  	</#if>
  	<#if multiVal = 'minus'>
  	  <img class="delPvVal" src="${contextPath}static/admin/index/img/remove.png" alt="">
  	</#if>
  </span>
</#if>

<span class="rightValue">
	<a name="${anchor_name}">
		<span class="emptyPosition"></span>
	</a>
	
	<#if show_release>
		<a class="blue tooltip-success unlock" href="#" name="${input_name!}" data-rel="tooltip" title="释放" data-toggle="modal"><i class="ace-icon fa fa-unlock bigger-170"></i></a>
  	</#if>
  
	<#if disabled != 'disabled'>
	<span class="box">
		<a href="javascript:void(0);" class="link">上传图片</a>
		<!-- 
		<img class="delImgVal" src="${contextPath}static/admin/index/img/btn_close.png" alt="">
		 -->
		<span class="textbox" name="copyFile">未选择文件</span>
		<input class="uploadFile uploadImg ${process_type!}-widget" type="file" id="${input_id!}" name="${input_name!}" ${disabled}/>
	</span>
	</#if>
</span>

<!-- span.imgShow 修改后, 需要复制一份到 img_show.ftl -->
<span class="imgShow">
	<img src="${contextPath}${file_value!}" alt="">
	
	<#if show_delete>
		<span class="closeBtn">
			<img name="${input_name!}" onclick="removeImgValue(this)" src="${contextPath}static/admin/index/img/delete_btn_red.png"/>
		</span>
	</#if>
</span>