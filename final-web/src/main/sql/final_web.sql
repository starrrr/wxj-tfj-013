/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : final_web

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2015-09-08 16:42:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `s_dict`
-- ----------------------------
DROP TABLE IF EXISTS `s_dict`;
CREATE TABLE `s_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键 无逻辑意义',
  `pid` int(11) DEFAULT NULL COMMENT '父id',
  `code` varchar(50) DEFAULT NULL COMMENT '字典代码',
  `name` varchar(50) DEFAULT NULL COMMENT '字典名称',
  `sort` int(3) DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `enable` char(1) DEFAULT '0' COMMENT '停用 默认(可用):0   停用：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_dict
-- ----------------------------
INSERT INTO `s_dict` VALUES ('1', '0', 'fa', 'test', '1', '2015-07-21 09:12:46', '0');

-- ----------------------------
-- Table structure for `s_login_log`
-- ----------------------------
DROP TABLE IF EXISTS `s_login_log`;
CREATE TABLE `s_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'user id',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(15) DEFAULT NULL COMMENT '登录ip',
  `logout_time` datetime DEFAULT NULL COMMENT '退出时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_login_log
-- ----------------------------
INSERT INTO `s_login_log` VALUES ('1', '1', '2015-06-11 17:26:22', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('2', '1', '2015-06-11 17:29:31', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('3', '1', '2015-06-11 18:15:37', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('4', '1', '2015-06-12 09:35:34', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('5', '1', '2015-06-12 09:50:17', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('6', '1', '2015-06-12 09:50:26', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('7', '1', '2015-06-12 09:50:30', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('8', '1', '2015-06-12 09:50:33', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('9', '1', '2015-06-12 10:01:27', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('10', '1', '2015-06-12 10:06:45', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('11', '1', '2015-06-12 10:10:43', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('12', '1', '2015-06-12 10:16:22', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('13', '1', '2015-06-12 10:24:52', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('14', '1', '2015-06-12 10:42:03', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('15', '1', '2015-06-12 11:30:52', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('16', '1', '2015-06-12 11:59:11', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('17', '1', '2015-06-12 12:00:12', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('18', '1', '2015-06-12 12:50:11', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('19', '1', '2015-06-12 13:42:55', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('20', '1', '2015-06-12 14:49:51', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('21', '1', '2015-06-12 14:56:08', '127.0.0.1', '2015-06-12 14:56:13');
INSERT INTO `s_login_log` VALUES ('22', '1', '2015-06-12 15:24:12', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('23', '1', '2015-06-12 16:20:59', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('24', '1', '2015-06-12 16:49:31', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('25', '1', '2015-06-12 17:01:10', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('26', '1', '2015-06-15 11:21:36', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('27', '1', '2015-06-15 11:57:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('28', '1', '2015-06-15 13:55:28', '127.0.0.1', '2015-06-15 13:55:33');
INSERT INTO `s_login_log` VALUES ('29', '1', '2015-06-15 13:55:41', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('30', '1', '2015-06-15 14:21:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('31', '1', '2015-06-15 15:04:01', '127.0.0.1', '2015-06-15 15:17:18');
INSERT INTO `s_login_log` VALUES ('32', '1', '2015-06-15 15:17:19', '127.0.0.1', '2015-06-15 15:17:20');
INSERT INTO `s_login_log` VALUES ('33', '1', '2015-06-15 15:17:21', '127.0.0.1', '2015-06-15 15:17:23');
INSERT INTO `s_login_log` VALUES ('34', '1', '2015-06-15 15:17:24', '127.0.0.1', '2015-06-15 15:17:25');
INSERT INTO `s_login_log` VALUES ('35', '1', '2015-06-15 15:17:25', '127.0.0.1', '2015-06-15 15:17:27');
INSERT INTO `s_login_log` VALUES ('36', '1', '2015-06-15 15:17:28', '127.0.0.1', '2015-06-15 15:17:29');
INSERT INTO `s_login_log` VALUES ('37', '1', '2015-06-15 15:17:31', '127.0.0.1', '2015-06-15 15:17:33');
INSERT INTO `s_login_log` VALUES ('38', '1', '2015-06-15 15:17:34', '127.0.0.1', '2015-06-15 15:17:36');
INSERT INTO `s_login_log` VALUES ('39', '1', '2015-06-15 15:17:37', '127.0.0.1', '2015-06-15 15:17:38');
INSERT INTO `s_login_log` VALUES ('40', '1', '2015-06-15 15:17:39', '127.0.0.1', '2015-06-15 15:17:40');
INSERT INTO `s_login_log` VALUES ('41', '1', '2015-06-15 15:17:41', '127.0.0.1', '2015-06-15 15:17:42');
INSERT INTO `s_login_log` VALUES ('42', '1', '2015-06-15 15:17:43', '127.0.0.1', '2015-06-15 15:17:57');
INSERT INTO `s_login_log` VALUES ('43', '1', '2015-06-15 15:17:59', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('44', '1', '2015-06-15 15:21:18', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('45', '1', '2015-06-15 15:21:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('46', '1', '2015-06-15 15:33:29', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('47', '1', '2015-06-15 16:14:07', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('48', '1', '2015-06-15 17:05:45', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('49', '1', '2015-06-15 17:12:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('50', '1', '2015-06-15 17:36:04', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('51', '1', '2015-06-15 17:37:08', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('52', '1', '2015-06-15 17:38:15', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('53', '1', '2015-06-16 09:35:43', '127.0.0.1', '2015-06-16 10:21:49');
INSERT INTO `s_login_log` VALUES ('54', '1', '2015-06-16 12:31:08', '127.0.0.1', '2015-06-16 12:32:03');
INSERT INTO `s_login_log` VALUES ('55', '1', '2015-06-16 12:32:30', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('56', '1', '2015-06-16 12:44:03', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('57', '1', '2015-06-16 14:39:03', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('58', '1', '2015-06-16 18:12:18', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('59', '1', '2015-06-16 18:12:48', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('60', '1', '2015-06-17 10:10:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('61', '1', '2015-06-17 11:52:23', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('62', '1', '2015-06-17 11:55:27', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('63', '1', '2015-06-17 11:57:28', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('64', '1', '2015-06-17 12:02:36', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('65', '1', '2015-06-17 12:36:16', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('66', '1', '2015-06-17 13:53:55', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('67', '1', '2015-06-17 14:25:23', '127.0.0.1', '2015-06-17 15:30:24');
INSERT INTO `s_login_log` VALUES ('68', '1', '2015-06-17 15:30:25', '127.0.0.1', '2015-06-17 15:30:27');
INSERT INTO `s_login_log` VALUES ('69', '1', '2015-06-17 15:43:46', '127.0.0.1', '2015-06-17 15:44:13');
INSERT INTO `s_login_log` VALUES ('70', '1', '2015-06-17 15:50:13', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('71', '1', '2015-06-17 16:03:39', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('72', '1', '2015-06-17 16:31:24', '127.0.0.1', '2015-06-17 16:31:31');
INSERT INTO `s_login_log` VALUES ('73', '1', '2015-06-17 16:31:32', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('74', '1', '2015-06-17 16:55:32', '127.0.0.1', '2015-06-17 16:57:13');
INSERT INTO `s_login_log` VALUES ('75', '3', '2015-06-17 16:57:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('76', '1', '2015-06-17 17:04:55', '127.0.0.1', '2015-06-17 17:05:00');
INSERT INTO `s_login_log` VALUES ('77', '1', '2015-06-17 17:05:01', '127.0.0.1', '2015-06-17 17:05:07');
INSERT INTO `s_login_log` VALUES ('78', '1', '2015-06-17 17:05:45', '127.0.0.1', '2015-06-17 17:05:47');
INSERT INTO `s_login_log` VALUES ('79', '1', '2015-06-17 17:05:52', '127.0.0.1', '2015-06-17 17:05:54');
INSERT INTO `s_login_log` VALUES ('80', '1', '2015-06-17 17:06:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('81', '1', '2015-06-17 17:27:20', '127.0.0.1', '2015-06-17 18:42:08');
INSERT INTO `s_login_log` VALUES ('82', '1', '2015-06-17 18:42:22', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('83', '1', '2015-06-17 18:51:42', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('84', '1', '2015-06-18 09:33:29', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('85', '1', '2015-06-18 09:36:48', '127.0.0.1', '2015-06-18 09:36:55');
INSERT INTO `s_login_log` VALUES ('86', '1', '2015-06-18 09:36:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('87', '1', '2015-06-18 09:43:42', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('88', '1', '2015-06-18 10:16:15', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('89', '3', '2015-06-18 10:17:39', '127.0.0.1', '2015-06-18 10:17:52');
INSERT INTO `s_login_log` VALUES ('90', '3', '2015-06-18 10:25:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('91', '3', '2015-06-18 10:34:20', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('92', '1', '2015-06-18 10:53:06', '127.0.0.1', '2015-06-18 10:53:15');
INSERT INTO `s_login_log` VALUES ('93', '3', '2015-06-18 11:04:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('94', '3', '2015-06-18 11:08:19', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('95', '1', '2015-06-18 12:41:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('96', '1', '2015-06-18 12:52:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('97', '1', '2015-06-18 13:32:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('98', '1', '2015-06-18 13:44:23', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('99', '1', '2015-06-18 14:09:43', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('100', '1', '2015-06-18 14:49:42', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('101', '1', '2015-06-18 14:57:26', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('102', '1', '2015-06-18 15:10:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('103', '1', '2015-06-18 15:14:07', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('104', '1', '2015-06-18 15:41:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('105', '1', '2015-06-18 15:56:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('106', '1', '2015-06-18 16:12:12', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('107', '1', '2015-06-18 16:30:05', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('108', '1', '2015-06-18 16:31:29', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('109', '1', '2015-06-18 17:52:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('110', '1', '2015-06-18 18:34:00', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('111', '1', '2015-06-18 19:17:43', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('112', '1', '2015-06-18 19:40:02', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('113', '1', '2015-06-18 19:49:17', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('114', '1', '2015-06-18 20:00:38', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('115', '1', '2015-06-18 20:16:30', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('116', '1', '2015-06-18 21:08:37', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('117', '1', '2015-06-18 22:31:34', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('118', '1', '2015-06-18 22:41:57', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('119', '1', '2015-06-18 22:42:51', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('120', '1', '2015-06-18 22:54:07', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('121', '1', '2015-06-19 12:43:40', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('122', '1', '2015-06-19 12:49:05', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('123', '1', '2015-06-19 12:51:24', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('124', '1', '2015-06-19 12:55:50', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('125', '1', '2015-06-19 13:09:48', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('126', '1', '2015-06-19 13:12:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('127', '1', '2015-06-19 13:13:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('128', '1', '2015-06-19 13:15:14', '127.0.0.1', '2015-06-19 13:15:37');
INSERT INTO `s_login_log` VALUES ('129', '1', '2015-06-19 13:15:41', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('130', '1', '2015-06-23 11:42:38', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('131', '1', '2015-06-23 13:15:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('132', '1', '2015-06-23 18:23:37', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('133', '1', '2015-06-23 18:26:31', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('134', '1', '2015-06-24 09:45:32', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('135', '1', '2015-06-24 10:17:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('136', '1', '2015-06-24 13:05:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('137', '1', '2015-06-24 13:33:50', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('138', '1', '2015-06-24 14:43:59', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('139', '1', '2015-06-24 15:17:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('140', '1', '2015-06-24 16:51:49', '127.0.0.1', '2015-06-24 17:05:24');
INSERT INTO `s_login_log` VALUES ('141', '1', '2015-06-24 17:08:16', '127.0.0.1', '2015-06-24 17:08:21');
INSERT INTO `s_login_log` VALUES ('142', '1', '2015-06-24 17:08:23', '127.0.0.1', '2015-06-24 17:08:25');
INSERT INTO `s_login_log` VALUES ('143', '1', '2015-06-24 17:08:27', '127.0.0.1', '2015-06-24 17:08:29');
INSERT INTO `s_login_log` VALUES ('144', '1', '2015-06-24 17:34:01', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('145', '1', '2015-06-24 17:52:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('146', '1', '2015-06-24 18:32:45', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('147', '2', '2015-07-20 16:37:55', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('148', '2', '2015-07-20 17:53:22', '127.0.0.1', '2015-07-20 18:00:22');
INSERT INTO `s_login_log` VALUES ('149', '2', '2015-07-20 18:00:24', '127.0.0.1', '2015-07-20 18:00:42');
INSERT INTO `s_login_log` VALUES ('150', '1', '2015-07-20 18:00:49', '127.0.0.1', '2015-07-20 18:01:31');
INSERT INTO `s_login_log` VALUES ('151', '1', '2015-07-20 18:01:33', '127.0.0.1', '2015-07-20 18:02:32');
INSERT INTO `s_login_log` VALUES ('152', '1', '2015-07-20 18:02:34', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('153', '2', '2015-07-21 08:37:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('154', '2', '2015-07-21 09:30:11', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('155', '2', '2015-07-21 10:19:33', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('156', '1', '2015-07-21 11:05:15', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('157', '2', '2015-07-21 11:13:13', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('158', '2', '2015-07-21 11:18:06', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('159', '2', '2015-07-21 11:25:56', '127.0.0.1', '2015-07-21 11:26:04');
INSERT INTO `s_login_log` VALUES ('160', '1', '2015-07-21 11:26:08', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('161', '2', '2015-07-21 11:44:53', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('162', '2', '2015-07-21 11:54:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('163', '2', '2015-07-21 11:56:06', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('164', '2', '2015-07-21 11:59:29', '127.0.0.1', '2015-07-21 12:00:07');
INSERT INTO `s_login_log` VALUES ('165', '1', '2015-07-21 12:00:10', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('166', '2', '2015-07-21 12:21:44', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('167', '2', '2015-07-21 14:07:47', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('168', '2', '2015-07-21 14:27:36', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('169', '2', '2015-07-21 14:43:20', '127.0.0.1', '2015-07-21 14:43:46');
INSERT INTO `s_login_log` VALUES ('170', '1', '2015-07-21 14:43:50', '127.0.0.1', '2015-07-21 14:46:51');
INSERT INTO `s_login_log` VALUES ('171', '2', '2015-07-21 14:46:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('172', '2', '2015-07-21 14:47:33', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('173', '2', '2015-07-21 15:33:28', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('174', '2', '2015-07-21 15:34:48', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('175', '2', '2015-07-21 15:36:33', '127.0.0.1', '2015-07-21 15:36:43');
INSERT INTO `s_login_log` VALUES ('176', '1', '2015-07-21 15:36:47', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('177', '1', '2015-07-21 15:45:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('178', '1', '2015-07-21 15:45:31', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('179', '2', '2015-07-21 16:04:00', '127.0.0.1', '2015-07-21 16:06:29');
INSERT INTO `s_login_log` VALUES ('180', '1', '2015-07-21 16:06:34', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('181', '1', '2015-07-21 16:07:19', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('182', '1', '2015-07-21 16:14:53', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('183', '2', '2015-07-21 16:19:30', '127.0.0.1', '2015-07-21 16:19:51');
INSERT INTO `s_login_log` VALUES ('184', '1', '2015-07-21 16:19:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('185', '1', '2015-07-21 16:30:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('186', '1', '2015-07-21 16:38:44', '127.0.0.1', '2015-07-21 16:39:51');
INSERT INTO `s_login_log` VALUES ('187', '2', '2015-07-21 16:39:55', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('188', '2', '2015-07-21 16:44:11', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('189', '1', '2015-07-21 16:56:24', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('190', '2', '2015-07-21 17:00:08', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('191', '2', '2015-07-21 17:06:04', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('192', '2', '2015-07-21 17:08:14', '127.0.0.1', '2015-07-21 17:08:58');
INSERT INTO `s_login_log` VALUES ('193', '1', '2015-07-21 17:09:02', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('194', '1', '2015-07-21 17:10:51', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('195', '1', '2015-07-21 17:13:44', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('196', '1', '2015-07-21 17:16:26', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('197', '1', '2015-07-21 17:28:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('198', '1', '2015-07-21 17:30:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('199', '1', '2015-07-21 17:33:28', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('200', '1', '2015-07-21 17:34:31', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('201', '1', '2015-07-21 17:35:24', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('202', '1', '2015-07-21 17:37:33', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('203', '1', '2015-07-21 17:41:42', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('204', '1', '2015-07-21 17:44:19', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('205', '1', '2015-07-21 17:47:06', '127.0.0.1', '2015-07-21 17:47:44');
INSERT INTO `s_login_log` VALUES ('206', '2', '2015-07-21 17:47:47', '127.0.0.1', '2015-07-21 17:47:55');
INSERT INTO `s_login_log` VALUES ('207', '1', '2015-07-21 17:47:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('208', '1', '2015-07-21 17:52:45', '127.0.0.1', '2015-07-21 17:54:42');
INSERT INTO `s_login_log` VALUES ('209', '1', '2015-07-21 17:54:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('210', '1', '2015-07-22 09:10:27', '127.0.0.1', '2015-07-22 09:18:20');
INSERT INTO `s_login_log` VALUES ('211', '1', '2015-07-22 09:18:21', '127.0.0.1', '2015-07-22 09:19:59');
INSERT INTO `s_login_log` VALUES ('212', '1', '2015-07-22 09:20:01', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('213', '1', '2015-07-22 09:21:17', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('214', '2', '2015-07-22 09:38:58', '127.0.0.1', '2015-07-22 09:40:04');
INSERT INTO `s_login_log` VALUES ('215', '2', '2015-07-22 09:44:52', '127.0.0.1', '2015-07-22 09:45:10');
INSERT INTO `s_login_log` VALUES ('216', '1', '2015-07-22 09:47:26', '127.0.0.1', '2015-07-22 09:48:10');
INSERT INTO `s_login_log` VALUES ('217', '1', '2015-07-22 09:48:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('218', '1', '2015-07-22 09:55:33', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('219', '1', '2015-07-22 10:05:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('220', '1', '2015-07-22 10:10:29', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('221', '1', '2015-07-22 10:18:17', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('222', '1', '2015-07-22 10:28:08', '127.0.0.1', '2015-07-22 10:33:26');
INSERT INTO `s_login_log` VALUES ('223', '2', '2015-07-22 10:33:30', '127.0.0.1', '2015-07-22 10:35:19');
INSERT INTO `s_login_log` VALUES ('224', '1', '2015-07-22 10:35:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('225', '1', '2015-07-22 10:36:11', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('226', '1', '2015-07-22 10:44:22', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('227', '1', '2015-07-22 10:46:02', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('228', '1', '2015-07-22 10:48:15', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('229', '1', '2015-07-22 11:01:40', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('230', '1', '2015-07-22 11:03:27', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('231', '1', '2015-07-22 11:19:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('232', '1', '2015-07-22 11:21:19', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('233', '1', '2015-07-22 11:22:40', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('234', '1', '2015-07-22 11:23:44', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('235', '1', '2015-07-22 11:25:56', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('236', '1', '2015-07-22 11:29:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('237', '1', '2015-07-22 11:38:32', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('238', '1', '2015-07-22 11:39:47', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('239', '1', '2015-07-22 11:42:40', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('240', '1', '2015-07-22 12:16:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('241', '1', '2015-07-22 12:27:18', '127.0.0.1', '2015-07-22 12:28:17');
INSERT INTO `s_login_log` VALUES ('242', '1', '2015-07-22 12:29:05', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('243', '1', '2015-07-22 12:30:47', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('244', '1', '2015-07-22 12:36:43', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('245', '1', '2015-07-22 14:09:48', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('246', '1', '2015-07-22 14:11:45', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('247', '1', '2015-07-22 14:15:54', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('248', '1', '2015-07-22 14:18:50', '127.0.0.1', '2015-07-22 14:22:24');
INSERT INTO `s_login_log` VALUES ('249', '1', '2015-07-22 14:22:26', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('250', '1', '2015-07-22 14:22:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('251', '1', '2015-07-22 14:52:46', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('252', '2', '2015-07-22 15:09:08', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('253', '1', '2015-07-22 15:14:48', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('254', '1', '2015-07-23 10:10:16', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('255', '1', '2015-07-23 10:29:09', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('256', '1', '2015-07-23 10:39:58', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('257', '2', '2015-07-23 10:41:41', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('258', '2', '2015-07-23 10:56:24', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('259', '2', '2015-07-23 11:00:21', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('260', '2', '2015-07-23 11:05:16', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('261', '2', '2015-07-23 11:07:55', '127.0.0.1', '2015-07-23 11:08:15');
INSERT INTO `s_login_log` VALUES ('262', '2', '2015-07-24 09:23:35', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('263', '2', '2015-07-24 09:26:54', '127.0.0.1', '2015-07-24 09:36:21');
INSERT INTO `s_login_log` VALUES ('264', '1', '2015-07-24 09:36:27', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('265', '1', '2015-07-24 09:41:57', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('266', '2', '2015-08-18 17:27:31', null, null);
INSERT INTO `s_login_log` VALUES ('267', '2', '2015-08-20 10:32:08', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('268', '2', '2015-08-20 14:26:07', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('269', '2', '2015-08-20 15:46:37', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('270', '1', '2015-09-01 16:08:57', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('271', '1', '2015-09-01 17:09:14', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('272', '1', '2015-09-08 11:39:50', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('273', '1', '2015-09-08 14:40:42', '127.0.0.1', '2015-09-08 14:41:00');
INSERT INTO `s_login_log` VALUES ('274', '2', '2015-09-08 14:41:07', '127.0.0.1', null);
INSERT INTO `s_login_log` VALUES ('275', '1', '2015-09-08 15:43:35', '127.0.0.1', null);

-- ----------------------------
-- Table structure for `s_menuresc`
-- ----------------------------
DROP TABLE IF EXISTS `s_menuresc`;
CREATE TABLE `s_menuresc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(11) DEFAULT NULL COMMENT '父id',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `sort` int(2) DEFAULT NULL COMMENT '排序(不超过100)',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `type` char(1) DEFAULT '0' COMMENT '权限类型 默认：菜单0 资源权限：1',
  `url` varchar(150) DEFAULT NULL COMMENT 'URL',
  `target` varchar(10) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '创建时间',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `permission` varchar(100) DEFAULT NULL COMMENT '权限名',
  `enable` char(1) DEFAULT '0' COMMENT '是否停用 默认：0 停用：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_menuresc
-- ----------------------------
INSERT INTO `s_menuresc` VALUES ('1', '0', '用户权限', '1', null, '0', '/', null, '2015-08-20 14:52:23', 'fa-users orange', 'auth', '0');
INSERT INTO `s_menuresc` VALUES ('2', '0', '系统管理', '1', null, '0', '/', null, '2015-08-20 14:57:18', 'fa-cogs grey', 'system', '0');
INSERT INTO `s_menuresc` VALUES ('3', '1', '用户管理', '1', null, '0', '/admin/user', null, '2015-08-20 14:55:22', 'fa-user blue', 'auth:admin', '0');
INSERT INTO `s_menuresc` VALUES ('4', '1', '角色管理', '2', null, '0', '/admin/role', null, '2015-06-18 16:22:33', 'fa-slideshare light-orange', 'auth:role', '0');
INSERT INTO `s_menuresc` VALUES ('5', '2', '菜单资源', '1', null, '0', '/admin/menuresc', null, '2015-06-24 18:35:55', 'fa-list-alt light-blue', 'system:menu', '0');
INSERT INTO `s_menuresc` VALUES ('6', '2', '数据字典', '2', null, '0', '/admin/dict', null, '2015-07-20 16:48:14', 'fa-book green', 'system:dict', '0');
INSERT INTO `s_menuresc` VALUES ('7', '2', '登录日志', '3', null, '0', '/admin/loginLog', null, '2015-06-18 16:24:19', '', 'system:loginlog', '0');
INSERT INTO `s_menuresc` VALUES ('11', '2', '数据库监控', '4', null, '0', '/admin/druid', '_blank', '2015-06-18 22:36:53', 'fa-database dark', 'system:ambri', '0');

-- ----------------------------
-- Table structure for `s_role`
-- ----------------------------
DROP TABLE IF EXISTS `s_role`;
CREATE TABLE `s_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `sort` int(2) DEFAULT NULL COMMENT '排序(不超过100)',
  `update_time` datetime DEFAULT NULL COMMENT '创建时间',
  `enable` char(1) DEFAULT '0' COMMENT '是否停用 默认：0 停用：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_role
-- ----------------------------
INSERT INTO `s_role` VALUES ('1', '管理员', '管理员角色', '1', '2015-06-18 12:57:37', '0');
INSERT INTO `s_role` VALUES ('4', '客服', '客服角色', '2', '2015-06-18 15:11:50', '1');
INSERT INTO `s_role` VALUES ('5', '运营', '运营角色', '3', '2015-06-18 15:12:09', '0');

-- ----------------------------
-- Table structure for `s_role_resc`
-- ----------------------------
DROP TABLE IF EXISTS `s_role_resc`;
CREATE TABLE `s_role_resc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) DEFAULT NULL COMMENT 'role id',
  `resc_id` int(11) DEFAULT NULL COMMENT 'resc_id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_role_resc
-- ----------------------------
INSERT INTO `s_role_resc` VALUES ('1', '1', '3');
INSERT INTO `s_role_resc` VALUES ('2', '4', '3');
INSERT INTO `s_role_resc` VALUES ('8', '1', '4');
INSERT INTO `s_role_resc` VALUES ('9', '1', '4');
INSERT INTO `s_role_resc` VALUES ('13', '1', '4');
INSERT INTO `s_role_resc` VALUES ('16', '5', '1');
INSERT INTO `s_role_resc` VALUES ('17', '5', '3');
INSERT INTO `s_role_resc` VALUES ('18', '5', '4');
INSERT INTO `s_role_resc` VALUES ('20', '1', '6');
INSERT INTO `s_role_resc` VALUES ('21', '1', '5');
INSERT INTO `s_role_resc` VALUES ('22', '1', '7');
INSERT INTO `s_role_resc` VALUES ('23', '1', '11');
INSERT INTO `s_role_resc` VALUES ('25', '1', '1');
INSERT INTO `s_role_resc` VALUES ('26', '1', '2');

-- ----------------------------
-- Table structure for `s_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `s_role_user`;
CREATE TABLE `s_role_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) DEFAULT NULL COMMENT 'role id',
  `user_id` bigint(20) DEFAULT NULL COMMENT 'user id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_role_user
-- ----------------------------
INSERT INTO `s_role_user` VALUES ('5', '1', '2');
INSERT INTO `s_role_user` VALUES ('13', '5', '3');

-- ----------------------------
-- Table structure for `s_user`
-- ----------------------------
DROP TABLE IF EXISTS `s_user`;
CREATE TABLE `s_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(80) DEFAULT NULL COMMENT '登录账号',
  `password` varchar(64) DEFAULT NULL COMMENT '登录密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `sex` char(1) DEFAULT '0' COMMENT '性别 默认：0（男） 女：1',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `dept_id` int(11) DEFAULT NULL COMMENT '所在部门',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `type` char(1) DEFAULT '0' COMMENT '用户类型 默认：0（普通用户） 管理员：1',
  `login_status` char(1) DEFAULT '0' COMMENT '是否登录状态 默认：0 在线：1',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` bigint(20) NOT NULL COMMENT '创建者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(15) DEFAULT NULL COMMENT '最后登录IP',
  `delete_status` char(1) DEFAULT '0' COMMENT '删除状态 默认：0 删除：1',
  `enable` char(1) DEFAULT '0' COMMENT '是否停用 默认：0 停用：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_user
-- ----------------------------
INSERT INTO `s_user` VALUES ('1', 'cyb', 'e10adc3949ba59abbe56e057f20f883e', 'cybbeta', '陈勇博', '0', '2015-06-15', null, '13510904291', '806507707@qq.com', '0', '1', '223', '2015-06-15 17:44:31', '0', '2015-09-08 14:44:06', '2015-09-08 15:43:35', '127.0.0.1', '0', '0');
INSERT INTO `s_user` VALUES ('2', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '管理员', '管理员', '1', '2015-06-02', null, '13510904291', '806507707@qq.com', '1', '1', '46', '2015-06-17 16:31:57', '1', '2015-06-17 16:55:41', '2015-09-08 14:41:07', '127.0.0.1', '0', '0');
INSERT INTO `s_user` VALUES ('3', 'cyb1', '8cdfa972d285d19c3d631489dde3aaab', 'cybbeta', '陈勇博', '0', '2015-06-17', null, '13510904291', '806507707@qq.com', '0', '1', '6', '2015-06-17 16:56:54', '1', '2015-09-01 17:23:43', '2015-06-18 11:08:19', '127.0.0.1', '0', '0');
INSERT INTO `s_user` VALUES ('4', 'cyb2', '8cdfa972d285d19c3d631489dde3aaab', 'cybbeta', '陈勇博', '0', '2015-06-03', null, '13510904291', '806507707@qq.com', '0', '0', '0', '2015-06-17 16:58:36', '3', '2015-06-18 12:53:02', null, null, '1', '1');
INSERT INTO `s_user` VALUES ('5', 'zlh', '8cdfa972d285d19c3d631489dde3aaab', 'zllh', '123', '0', '2015-07-16', null, '123213213213', 'dsfds@153.com', '1', '0', '0', '2015-07-20 16:39:43', '2', null, null, null, '0', '0');

-- ----------------------------
-- Table structure for `s_webconfig`
-- ----------------------------
DROP TABLE IF EXISTS `s_webconfig`;
CREATE TABLE `s_webconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(1000) DEFAULT NULL,
  `desc` varchar(500) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_system` char(255) DEFAULT '0' COMMENT '是否系统配置 否：0 是：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_webconfig
-- ----------------------------
INSERT INTO `s_webconfig` VALUES ('1', 'webName', 'GM系统', '网站名称', '2015-01-07 17:59:33', null, '1');
INSERT INTO `s_webconfig` VALUES ('2', 'ICP', '鄂ICP备14020682号-1', '网站ICp', '2015-01-07 18:00:07', null, '1');
INSERT INTO `s_webconfig` VALUES ('3', 'webCopyright', 'Copyright © 2014-2015 <a href=\"http://www.tczc168.com\" target=\"_blank\">湖北天驰汽车租赁有限公司</a>.All right reserved', '网站底部copyright', '2015-01-07 18:01:38', null, '1');
INSERT INTO `s_webconfig` VALUES ('4', 'webUrl', '', '网站地址', '2015-01-07 19:03:31', null, '1');
INSERT INTO `s_webconfig` VALUES ('5', 'keywords', '', '网站关键词', '2015-01-08 14:02:18', null, '1');
INSERT INTO `s_webconfig` VALUES ('6', 'description', '', '网站描述', '2015-01-08 14:02:51', null, '1');
