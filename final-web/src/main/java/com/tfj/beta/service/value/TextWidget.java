package com.tfj.beta.service.value;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.FreemarkerUtil;

public class TextWidget extends Widget {
	
	private final static String MIX_CONDITION = "{MIX}";
	
	private final static String MIX = "mix";
	
	private final static String DEFAULT_TEMPLATE = "text.ftl";
	
	public TextWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		
		String def_val = StringUtils.defaultString(node.getDefault_value());
		String val = StringUtils.defaultString(node.getValue(), def_val);
		String inputName = String.format(NAME, 
				String.valueOf(node.getId()), defaultString(node.getPv_id()));
		
		params.put("input_name", inputName);
		// TODO 注释掉, 模板错误处理
		params.put("input_value", escapeSequence(val));
		
		if (MIX_CONDITION.equals(node.getField_develop())) {
			params.put(MIX, "MIX");
			params.put(T_MULTIVAL, null);
			params.put("category_id", node.getCategory_id());
		}
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}

	private String escapeSequence(String val) {
		return val.replaceAll("\"", "&quot;");
	}
}
