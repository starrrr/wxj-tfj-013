package com.tfj.beta.service.value;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.FreemarkerUtil;

public class FileWidget extends Widget {
	
	private final static String DEFAULT_TEMPLATE = "file.ftl";

	public FileWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		String inputName = String.format(NAME, String.valueOf(node.getId()), defaultString(node.getPv_id()));
		String val = StringUtils.defaultString(node.getValue());
		
		params.put("input_id", node.getProcessType() + SPLIT + inputName);
		params.put("input_name", inputName);
		params.put("file_value", val);
		
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}

}
