package com.tfj.beta.service;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.Role;

/**
 * Created by Administrator on 2015/6/12.
 */
public class DictService {

	@RequiresPermissions(value = { "system:dict" })
	public Page<Dict> getDictPage(int pageNumber, int pageSize, Dict dict) {
		return Dict.dao.searchPaginate(pageNumber, pageSize, dict);
	}

	public boolean modifyDict(Dict dict) {
		boolean flag = true;
		Long id = dict.getLong(Dict.ID);
		if (null == id) {
			dict.save();
		} else {
			dict.update();
		}
		return flag;
	}
	
	public boolean deleteDicts(String[] ids){
		  //删除 关联信息
        return Dict.dao.deleteByIds(ids);
	}

	public Dict findDicByNameAndType(Object object,String name,String type) {
		
		return Dict.dao.findDicByNameAndType(object,name,type);
	}

	public int insertDictByTypeAndName(Object obj,String planttype,String type) {
		return Dict.dao.insertDictByTypeAndName(obj,planttype,type);
	}

}
