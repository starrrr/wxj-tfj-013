package com.tfj.beta.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class ServiceFactory {
	
	private static volatile Map<String, Object> staticServices = new ConcurrentHashMap<String, Object>();
	
	@SuppressWarnings("unchecked")
	public static <T> T getStaticService(Class<T> clazz) {
		try {
			String clazzName = clazz.getName();
			Object obj = staticServices.get(clazzName);
			if (obj == null) {
				obj = clazz.newInstance();
				staticServices.put(clazzName, obj);
			}
			return (T) obj;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 新建一个 Service
	 * @param clazz
	 * @return
	 */
	public static <T> T getNewService(Class<T> clazz) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

//	
//	@SuppressWarnings("unchecked")
//	public static <T> T getService(Class<T> clazz) {
//		try {
//			String clazzName = clazz.getName();
//			Object obj = staticServices.get(clazzName);
//			if (obj == null) {
//				T t = clazz.newInstance();
//				staticServices.put(clazzName, t);
//			} else {
//				return (T) obj;
//			}
//		} catch (InstantiationException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
}
