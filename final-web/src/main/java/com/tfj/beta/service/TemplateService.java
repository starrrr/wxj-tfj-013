package com.tfj.beta.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.tfj.beta.model.FieldType;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.view.JsonBean;
import com.tfj.beta.model.view.TreeBean;

/**
 * Created by Administrator on 2015/6/12.
 */
public class TemplateService {
	public static final Template dao = new Template();
	public static final Product productDao=new Product();
	public static final ProductValue productValueDao=new ProductValue();

	public static List<JsonBean> getJsonBean(String processType, String categoryId) {
		if (StrKit.isBlank(processType)) {
			return null;
		}
		List<JsonBean> beans = new ArrayList<JsonBean>();
		List<Template> templateList = dao
				.getTemplateListByProcessType(processType,categoryId);
		if (null != templateList && templateList.size() > 0) {
			for (Template template : templateList) {
				beans.add(dao.constructJSONBean(template));
			}
		}
		return beans;
	}

	public static List<TreeBean> getTreeBean(String processType,String categoryId) {

		if (StrKit.isBlank(processType)) {
			return null;
		}
		List<TreeBean> beans = new ArrayList<TreeBean>();
		List<Template> templateList = dao
				.getTemplateListByProcessType(processType,categoryId);
		if (null != templateList && templateList.size() > 0) {
			for (Template template : templateList) {
				dao.constructTreeBean(beans, template);
			}
		}
		return beans;

	}

	/**
	 * 删除节点
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteNode(String id) {
		boolean isFlag=false;
		// 判断该节点下是否有子节点
		List<Template> list = this.dao.getChildByPid(Long.parseLong(id));
		if (null == list || list.size() == 0) {
			return deleteProductValue(Long.parseLong(id));
		} else {
			return isFlag;
		}

	}
	
	public int setTemplatePidMaxCount(Long pid) {
		List<Template> children = Template.dao.getChildByPid(pid);
		if (children == null || children.isEmpty()) {
			return 1;
		} else {
			return children.size() + 1;
		}
	}
	public boolean modifyTemplate(final Template template) {
		boolean isFlag=false;
		Long id = template.getLong(Template.ID);
		try {
			if (null == id) {
				isFlag=addProductValue(template);
			} else {
				//isFlag=template.update();
				isFlag=updateProductValue(template);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Template> getTemplateByCtId(Long category_id) {
		return Template.dao.findTemplateByCtId(category_id);
	}
	
	/**
	 * 新增节点
	 * @param template
	 * @return
	 */
	private boolean addProductValue(final Template template) {
		return Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				boolean isFlag=false;
				isFlag=template.save();
				long categoryId=template.getLong(Template.CATEGORY_ID);
				String fileType=template.getStr(Template.FIELD_TYPE);
				List<Product> products=productDao.getProductsByCategoryId(String.valueOf(categoryId));
				for(Product product:products){
					dao.doInsertFilter(product,fileType,template);
				}
				return isFlag;
			}
		});
	}
	/**
	 * 删除节点（同时删除对应的模板值记录）
	 * @param templateId
	 * @return
	 */
	private boolean deleteProductValue(final long templateId) {
		final Template template=Template.dao.findById(templateId);//记录原来的数据
		return Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				boolean isTMFlag=dao.deleteById(templateId);//删除模板
				long categoryId=template.getLong(Template.CATEGORY_ID);//删除模板对应的模板值记录
				List<Product> products=productDao.getProductsByCategoryId(String.valueOf(categoryId));
				for(Product product:products){
					long productId=product.getLong(Product.ID);
					productValueDao.deleteByTemplateIdAndProductId(templateId,productId);
				}
				return isTMFlag;
			}
		});
	}
	/**
	 * 模板修改
	 * @param template
	 * @return
	 */
	private boolean updateProductValue(final Template template) {
		return Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				String fileType=template.getStr(Template.FIELD_TYPE);
				long templateId=template.getLong(Template.ID);
				long categoryId=template.getLong(Template.CATEGORY_ID);//删除模板对应的模板值记录
				Template oldTemplate=dao.findById(templateId);
				List<Product> products=productDao.getProductsByCategoryId(String.valueOf(categoryId));
				if(!FieldType.BLANK.getName().equals(fileType)&&template.getLong(Template.LEVELNUM)>0){//xx 转 非空白（1、空白转非空白:模板值添加；2、非空白转非空白：模板值不变）
					if(FieldType.BLANK.getName().equals(oldTemplate.getStr(Template.FIELD_TYPE))){//空白转非空白   模板值添加
						for(Product product:products){
							dao.doInsertFilter(product,fileType,template);
						}
					}
				}else {//xx 转 空白---->删除原来的模板值
					for(Product product:products){
						long productId=product.getLong(Product.ID);
						productValueDao.deleteByTemplateIdAndProductId(templateId,productId);
					}
				}
				template.update();
				return true;
			}
		});
	}
	public List<Template> getChildsByPid(long pid){
		return dao.findChilByPid(pid);
	}

	public void updateSort(final List<Template> childs) {
		Db.tx(new IAtom(){
			public boolean run() throws SQLException {
				
				return false;
			}
		});
	}
}
