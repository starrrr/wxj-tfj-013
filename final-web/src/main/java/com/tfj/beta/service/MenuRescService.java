package com.tfj.beta.service;

import java.util.List;

import com.google.common.collect.Lists;
import com.tfj.beta.model.MenuResc;

/**
 * Created by Administrator on 2015/6/18.
 */
public class MenuRescService extends BaseService {

    public List<MenuResc> getMenuRescList() {
        return MenuResc.dao.searchAllEnable();
    }

    // 对菜单按父子关系排序
    public List<MenuResc> getSortMenuRescList() {
        List<MenuResc> list = Lists.newArrayList();
        List<MenuResc> menuRescs = MenuResc.dao.searchAll();

        for (int i = 0; i < menuRescs.size(); i++) {
            MenuResc e = menuRescs.get(i);
            if (e.getInt(MenuResc.PID) == null || e.getInt(MenuResc.PID) == 0) {
                list.add(e);
                findChild(list, menuRescs, e.getInt(MenuResc.ID));
            }
        }
        return list;
    }

    public void findChild(List<MenuResc> list, List<MenuResc> sourceList, Integer pid) {
        for (int i = 0; i < sourceList.size(); i++) {
            MenuResc e = sourceList.get(i);
            if (e.getInt(MenuResc.PID) == pid) {
                list.add(e);
                findChild(list, sourceList, e.getInt(MenuResc.ID));
            }
        }
    }
    
   
    public MenuResc findMenuRescById(Integer id) {
        return MenuResc.dao.findById(id);
    }
    
}
