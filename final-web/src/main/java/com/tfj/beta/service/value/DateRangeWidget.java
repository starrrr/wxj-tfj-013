package com.tfj.beta.service.value;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.FreemarkerUtil;

public class DateRangeWidget extends Widget {
	
	private final static String DEFAULT_TEMPLATE = "date_range.ftl";
	
	private final static String DATE_START = "date_start";
	private final static String DATE_END = "date_end";
	// XXX 传到页面, 与页面分隔一致
	public final static String TO = " 至 ";

	public DateRangeWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		String def_val = getDateDefaultVal(node.getDefault_value());
		String val = StringUtils.defaultString(node.getValue(), def_val);
		String inputName = String.format(NAME, String.valueOf(node.getId()), defaultString(node.getPv_id()));
		
//		params.put("anchor_name", node.getName());
		params.put("input_name", inputName);
		
		String[] dates = val.split(TO);
		
		if (dates.length == 2) {
			params.put(DATE_START, dates[0]);
			params.put(DATE_END, dates[1]);
		} else {
			params.put(DATE_START, StringUtils.EMPTY);
			params.put(DATE_END, StringUtils.EMPTY);
		}
		
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}
	
	private String getDateDefaultVal(String defVal) {
		String def_val = StringUtils.defaultString(defVal);
		String nowadays = DateUtils.getCurrentDay();
		return nowadays + TO + nowadays;
	}
}
