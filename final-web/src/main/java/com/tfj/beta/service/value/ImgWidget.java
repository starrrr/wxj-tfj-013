package com.tfj.beta.service.value;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.FreemarkerUtil;
import com.tfj.beta.utils.PropertiesUtil;

public class ImgWidget extends Widget {
	
	public final static String PRODUCT_VALUE_FOLDER = "upload/product_value/";
	
	public final static String EXAMPLE_PNG = "upload/product_value/example.png";
	
	public final static String IMG_FILENAME = "img-%s-%s%s";
	
	private final static String FILE_VALUE = "file_value";
	
	private final static String DEFAULT_TEMPLATE = "img.ftl";
	
	private final static String T_SHOW_DELETE = "show_delete";
	
	private final static String IMG_SHOW_FTL = "img_show.ftl";
	
	public ImgWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		return generateHTMLByTemplate(node, params, DEFAULT_TEMPLATE);
	}
	
	public static String generateImgShowHTML(PvBean node) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(T_CONTEXT_PATH, PropertiesUtil.getBase());
		
		return generateHTMLByTemplate(node, params, IMG_SHOW_FTL);
	}

	private static String generateHTMLByTemplate(PvBean node, Map<String, Object> params, String template) {
		String inputName = String.format(NAME, String.valueOf(node.getId()), defaultString(node.getPv_id()));
		String val = node.getValue();
		
		params.put("input_id", node.getProcessType() + SPLIT + inputName);
		params.put("input_name", inputName);
		
		if (StringUtils.isNotBlank(val)) {
			params.put(FILE_VALUE, val + "?t=" + Math.random());
			if (node.isAudited()) {
				params.put(T_SHOW_DELETE, false);
			} else {
				params.put(T_SHOW_DELETE, true);
			}
		} else {
			params.put(FILE_VALUE, EXAMPLE_PNG);
			params.put(T_SHOW_DELETE, false);
		}
		
		return FreemarkerUtil.getInstantce().fprint(template, params);
	}
	
	public static String getFileName(String template_id, String pv_id, String suffix) {
		return String.format(IMG_FILENAME, template_id, pv_id, suffix);
	}
}
