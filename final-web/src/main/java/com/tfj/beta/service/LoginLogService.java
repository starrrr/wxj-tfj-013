package com.tfj.beta.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.TableMapping;
import com.tfj.beta.model.LoginLog;
import com.tfj.beta.model.User;

/**
 * loginLog 业务逻辑层
 * Created by Administrator on 2015/6/12.
 */
public class LoginLogService {

    public Page<LoginLog> getLoginLogPage(int pageNumber, int pageSize, User user, String startTime, String endTime) {
        // 构建select sql
        StringBuffer selectStringBuffer = new StringBuffer("SELECT ");
        selectStringBuffer.append("u.").append(User.USERNAME).append(", ")
                .append("u.").append(User.MOBILE).append(", ")
                .append("u.").append(User.EMAIL).append(", ")
                .append("u.").append(User.NAME).append(", ")
                .append("ll.").append(LoginLog.ID).append(", ")
                .append("ll.").append(LoginLog.LOGIN_TIME).append(", ")
                .append("ll.").append(LoginLog.LOGIN_IP).append(", ")
                .append("ll.").append(LoginLog.LOGOUT_TIME).append(" ");

        // 构建from sql
        StringBuffer fromStringBuffer = new StringBuffer("FROM ");
        fromStringBuffer.append(TableMapping.me().getTable(LoginLog.class).getName()).append(" ll ")
                .append("LEFT JOIN ").append(TableMapping.me().getTable(User.class).getName()).append(" u ON (").append("ll.").append(LoginLog.USER_ID).append(" = u.").append(User.ID).append(") ")
                .append("WHERE 1=1 ");

        // 判断姓名是否为空，不为空添加条件
        if (null != user.getStr(User.USERNAME) && !"".equals(user.getStr(User.USERNAME))) {
            fromStringBuffer.append("AND u.").append(User.USERNAME).append(" like '%").append(user.getStr(User.USERNAME).trim().equals("'")?"''":user.getStr(User.USERNAME).trim()).append("%' ");
        }
        // 判断开始时间和结束时间都不为空则查找包含时间段，否则不包含时间段查找
        if (null != startTime && !"".equals(startTime) && null != endTime && !"".equals(endTime)) {
            fromStringBuffer.append("AND ll.").append(LoginLog.LOGIN_TIME).append(" BETWEEN '").append(startTime).append("' AND '").append(endTime).append("' ");
        }
        // 开始时间为空，结束时间不为空，查询第一条记录 至 输入结束的时间
        if (null == startTime || "".equals(startTime) && null != endTime && !"".equals(endTime)) {
            fromStringBuffer.append("AND ll.").append(LoginLog.LOGIN_TIME).append(" <= '").append(endTime).append("' ");
        }
        
        // 开始时间不为空，结束时间为空，查询输入开始的时间 至 最后一条记录
        if (null != startTime || !"".equals(startTime) && null == endTime && "".equals(endTime)) {
            fromStringBuffer.append("AND ll.").append(LoginLog.LOGIN_TIME).append(" >= '").append(startTime).append("' ");
        }
        
        fromStringBuffer.append("ORDER BY ll.").append(LoginLog.ID).append(" DESC");

        return LoginLog.dao.paginate(pageNumber, pageSize, selectStringBuffer.toString(), fromStringBuffer.toString());
    }
}
