package com.tfj.beta.service;

import java.util.List;

import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleResc;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.User;

/**
 * Created by Administrator on 2015/6/18.
 */
public class RoleService extends BaseService {
    public List<Role> getRoleList() {
        return Role.dao.searchAll();
    }

    public List<RoleResc> getRoleRescList(Role role) {
        return RoleResc.dao.searchRoleRescByRoleId(role.getInt(Role.ID));
    }

    public boolean deleteRoles(String[] ids) {
        //删除 关联信息
        return Role.dao.deleteByIds(ids) && RoleResc.dao.delete(ids);
    }

    public boolean deleteResc(RoleResc roleResc) {
        return RoleResc.dao.delete(roleResc);
    }
    public List<RoleUser> getRoleByUserId(User User) {
		return RoleUser.dao.searchRoleByUserID(User.getLong(Role.ID));
	}

   
}
