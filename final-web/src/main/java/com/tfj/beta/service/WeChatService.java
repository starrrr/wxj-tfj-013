package com.tfj.beta.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleResc;
import com.tfj.beta.model.WeChat;
import com.tfj.beta.model.WeChatOpenId;
import com.tfj.beta.model.WeChatRole;

public class WeChatService extends BaseService{
	
	/**
	 * 微信扫描记录
	 * @param pageNumber
	 * @param pageSize
	 * @param weChat
	 * @return
	 */
	public Page<WeChat> getWeChatPage(int pageNumber, int pageSize, WeChat weChat) {
		return WeChat.dao.searchPaginate(pageNumber, pageSize, weChat);
	}
	/**
	 * 微信用户管理openid
	 * @param pageNumber
	 * @param pageSize
	 * @param weChat
	 * @return
	 */
	public Page<WeChatOpenId> getWeChatOpenidPage(int pageNumber, int pageSize, WeChatOpenId weChatOpenId) {
		return WeChatOpenId.dao.searchPaginate(pageNumber, pageSize, weChatOpenId);
	}
	/**
	 * 微信角色管理
	 * @param pageNumber
	 * @param pageSize
	 * @param weChat
	 * @return
	 */
	public Page<WeChatRole> getWeChatRolePage(int pageNumber, int pageSize,
			WeChatRole weChatRole) {
		return WeChatRole.dao.searchPaginate(pageNumber, pageSize, weChatRole);
	}
	/**
	 * 微信角色管理-删除
	 * @param pageNumber
	 * @param pageSize
	 * @param weChat
	 * @return 
	 * @return
	 */
	public boolean deleteRoles(String[] ids) {
        return WeChatRole.dao.deleteByIds(ids);
	}
	/**
	 * 查找所以角色
	 * @return
	 */
	public List<WeChatRole> findAllWeChatRose(){
		 return WeChatRole.dao.getWeChatRoles();
	}
	
	/**
	 * 查找某个用户类型的总人数，role_id为空的时候查找所有人数
	 */
	public Long countOpenIdByRoleId(String role_id){
		return WeChatOpenId.dao.countOpenIdByRoleId(role_id);
	}
}
