package com.tfj.beta.service.value;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.PropertiesUtil;

public abstract class Widget {
	
	public final static String SPLIT = "-"; 
	
	public final static String NAME = "%s" + SPLIT + "%s";
	
	
	protected final static String T_CONTEXT_PATH = "contextPath";
	protected final static String T_PROCESS_TYPE = "process_type";
	protected final static String T_DISABLED = "disabled";
	protected final static String T_MULTIVAL = "multiVal";
	protected final static String T_SHOW_RELEASE = "show_release";
	protected final static String T_ANCHOR_NAME = "anchor_name";
	
	protected final static String V_DISABLED = "disabled";
	protected final static String V_PLUS = "plus";
	protected final static String V_MINUS = "minus";
	
	private final static Integer TWO = new Integer(2);
	
	protected PvBean node;

	public Widget(PvBean node) {
		this.node = node;
	}
	
    public static String defaultString(Object obj) {
        return obj == null ? StringUtils.EMPTY : obj.toString();
    }
	
	public String generateHTML() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(T_CONTEXT_PATH, PropertiesUtil.getBase());
		// 设置模板类型
		setProcessType(params);
		// 设置锚点名称
		setAnchorName(params);
		// 是否 disable
		setDisable(params);
		// 设置释放审核值权限
		setRelease(params);
		// 设置多值属性
		setMultiValFunction(params);
		// 生成HTML
		return generateHTML(params);
	}
	
	/**
	 * 设置模板类型
	 * @param params
	 */
	protected void setProcessType(Map<String, Object> params) {
		params.put(T_PROCESS_TYPE, node.getProcessType());
	}
	
	/**
	 * 设置锚点名称
	 * @param params
	 */
	protected void setAnchorName(Map<String, Object> params) {
		params.put(T_ANCHOR_NAME, node.getName());
	}
	
	/**
	 * 是否 disable
	 * @param params
	 */
	protected void setDisable(Map<String, Object> params) {
		if (node.getPv_id() == null) { // 不存在 Product_Value id
			if (TWO.equals(node.getProductAuditFlag())) {
				params.put(T_DISABLED, V_DISABLED);
			} else {
				params.put(T_DISABLED, StringUtils.EMPTY);
			}
		} else { // 存在 Product_Value id
			if (node.isAudited()) {
				params.put(T_DISABLED, V_DISABLED);
			} else {
				params.put(T_DISABLED, StringUtils.EMPTY);
			}
		}
	}
	
	protected void setRelease(Map<String, Object> params) {
		if (PvBean.ADMIN.equals(node.getUser_role())) {
			if (node.isAudited() || 
					(TWO.equals(node.getProductAuditFlag()) && node.getAuditFlag() == null)) {
				params.put(T_SHOW_RELEASE, true);
				return;
			}
		}
		params.put(T_SHOW_RELEASE, false);
	}
	
	/**
	 * 设置多值属性，如果是 plus 对应界面 + 操作, minus 对应界面 - 操作
	 * @param params
	 */
	protected void setMultiValFunction(Map<String, Object> params) {
		if (!node.isAudited() && !node.hasChildren() && 
				!TWO.equals(node.getProductAuditFlag())) {
			if (node.getPv_sort() == null) {
				params.put(T_MULTIVAL, V_PLUS);
			} else {
				params.put(T_MULTIVAL, V_MINUS);
			}
		}
	}
	
	public abstract String generateHTML(Map<String, Object> params);
}