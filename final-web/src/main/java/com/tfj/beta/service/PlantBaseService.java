package com.tfj.beta.service;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.County;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.IndexCount;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.PlantCate;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.User;

/**
 * Created by Administrator on 2015/6/12.
 */
public class PlantBaseService {
	DictService dictService = new DictService();
	//@RequiresPermissions(value = { "system:dict" })
	public Page<PlantBase> getPlantBasePage(int pageNumber, int pageSize, PlantBase plantBase,List<Unit> units,String admin) {
		return PlantBase.dao.searchPaginate(pageNumber, pageSize, plantBase,units,admin);
	}

	public boolean modifyPlantBase(final PlantBase plantBase, UploadFile uploadFile,final String categoryIds) {
		String picture="";//记录修改前的图片名称
		boolean flag = true;
		Long id = plantBase.getLong(PlantBase.ID);
		String categropyId=plantBase.getStr("category_id");
		if(categropyId=="-1"||"-1".equals(categropyId)){
			plantBase.put(plantBase.HERBAL_NAME, "");
		}
		String soil=plantBase.getStr(PlantBase.SOIL);
		duleSoilType(plantBase,soil);
		if (null == id) {
			plantBase.set(plantBase.AUDITFLAG, 0);//添加时，审核初始状态：否
			if(uploadFile==null||"".equals(uploadFile)){//添加时，是否有上传图片作出标记   有：1  否：0
				plantBase.set(plantBase.ENV_TEST_REPORT, 0);
			}else {
				plantBase.set(plantBase.ENV_TEST_REPORT, 1);
			}
			return Db.tx(new IAtom(){//事物处理
				public boolean run() throws SQLException {
					boolean isSavePlant=plantBase.save();
					boolean isSavePlantCate=savePlantBaseCate(plantBase,categoryIds);//保存基地与种类的关系
					IndexCount.dao.updatePlantBase(true);
					County.dao.saveNums(true,plantBase.getStr(plantBase.COUNTY_CODE));
				return isSavePlant&&isSavePlantCate;
			}});
			//plantBase.save();
			//IndexCount.dao.updatePlantBase(true);
			//County.dao.saveNums(true,plantBase.getStr(plantBase.COUNTY_CODE));
		} else {
			if(uploadFile==null||"".equals(uploadFile)){//添加时，是否有上传图片作出标记   有：1  否：0
				plantBase.set(plantBase.ENV_TEST_REPORT, 0);
			}else {
				plantBase.set(plantBase.ENV_TEST_REPORT, 1);
			}
			if(plantBase.dao.findById(id).get(PlantBase.BASE_MAP)!=null){//有可能添加时没图片，修改时添加图片，要作空判断
				picture=plantBase.dao.findById(id).get(PlantBase.BASE_MAP);
				picture=picture.substring(picture.lastIndexOf("/")+1);
			}
			/*plantBase.update();
			if(uploadFile!=null){//判断是否修改图片  空：不修改
				deletepicture(picture);
			}*/
			if(uploadFile!=null){//判断是否修改图片  空：不修改
				deletepicture(picture);
			}
			return Db.tx(new IAtom(){//事物处理
				public boolean run() throws SQLException {
					boolean isUpdatePlant=plantBase.update();
					boolean isSavePlantCate=PlantBase.dao.dulePlantCateUpdate(plantBase,categoryIds);
				return isUpdatePlant&&isSavePlantCate;
			}});
		}
	}
	private boolean savePlantBaseCate(PlantBase plantBase,String categoryId) {
		String[] categoryIds=null;
		if(categoryId.indexOf(",")>0){
			categoryIds=categoryId.split(",");
		}else if(!"".equals(categoryId)){
			categoryIds=new String[1];
			categoryIds[0]=categoryId;
		}
		return categoryIds==null?true:PlantCate.dao.savePlantCateRelation(categoryIds,plantBase.getLong(PlantBase.ID));
	}
	//处理土地
	 private boolean duleSoilType(PlantBase plantBase,String soiltype) {
			boolean isFlage=false;
			boolean isExistDICT=false;//标记存在对数据字典库插入数据
			int i=0;
			if("".equals(soiltype)){
				plantBase.put(PlantBase.SOIL,"");
			}else{
				//判断数字字典表是否存在该种植方式:存在，正常操作，不存在，对字典表进行相应操作
				Dict dict=dictService.findDicByNameAndType(plantBase,soiltype,"STLX");
				if(dict==null){
					isExistDICT=true;
					i=dictService.insertDictByTypeAndName(plantBase,soiltype,"STLX");
				}else {
					if(!plantBase.getStr(PlantBase.SOIL).startsWith("STLX_")){//不是以STLX_开始的，表示input传递过来
						plantBase.set(PlantBase.SOIL, dict.getStr(Dict.CODE));
					}
				}
			}
			if((isExistDICT&&i>0)||(!isExistDICT&&i==0)){
				isFlage=true;
			}
			return isFlage;
		}
	
	/**
	 * 修改需要把原来储存在upload文件夹的图片删除
	 */
	public void deletepicture(String picture) {
		File file=new File(JFinal.me().getServletContext().getRealPath("/upload/plantbase"));
		delete(file,picture);
	}
	public void delete(File file,String picture) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File file2 : files) {
				delete(file2,picture);
			}
		} else {
			String name = file.getName();
			if (picture.equals(name)) {
				file.delete();
			}
		}
	}

	public boolean deletePlantBase(String[] ids,PlantBase plantBase){
		  //删除 关联信息
        return PlantCate.dao.deleteByPlantId(plantBase.getLong(PlantBase.ID))&&PlantBase.dao.deleteByIds(ids) && IndexCount.dao.updatePlantBase(false) && County.dao.saveNums(false,plantBase.getStr(plantBase.COUNTY_CODE));
	}
	
	public boolean queryAssociated(String id) {
		// TODO Auto-generated method stub
		return PlantBase.dao.queryAssociatedById(id);
	}
	public boolean modifyCategoryaudit(PlantBase plantBase) {
		boolean flag = true;
		Long id = plantBase.getLong(Category.ID);
		if (null == id) {
			plantBase.put("auditflag", 0);
			plantBase.save();
		} else {
			plantBase.update();
		}
		return flag;
	}
	public List<PlantBase> getPlantBaseByCategoryIdAndUnitId(String unit_id,String category_id) {
		return PlantBase.dao.findPlantBaseByCategoryIdAndUnitId(unit_id,category_id);
	}


	public Map<String,Map> getAllPlantBase(){
		Map<String,Map> resultMap = new HashMap<String,Map>();
		List<County> countyList = County.dao.findListByNums();
		for (County county : countyList) {
			Map valueMap = new HashMap();
			String plantbaseStr ="";
			List<PlantBase> plantBaseList = PlantBase.dao.getPlantBaseByCountyCode(county.getStr(county.CODE));
			for (PlantBase plantBase : plantBaseList) {
				plantbaseStr = plantbaseStr+plantBase.getStr(plantBase.NAME)+"</br>";
			}
			valueMap.put("nums", county.get(county.NUMS));
			valueMap.put("name", "【"+county.get(county.NAME)+"】");
			valueMap.put("plantbase", plantbaseStr);
			
			resultMap.put(county.getStr(county.CODE),valueMap);
		}
		return resultMap;
	}

}
