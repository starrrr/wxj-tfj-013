package com.tfj.beta.service;

import java.sql.SQLException;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitCate;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.utils.DateUtils;


public class UnitService {

	public Page<Unit> getUnitPage(int pageNumber, int pageSize, Unit unit) {
		return Unit.dao.searchPaginate(pageNumber, pageSize, unit);
	}

	public boolean modifyUnit(final Unit unit) {
		boolean flag = true;
		Long id = unit.getLong(Unit.ID);
		if (null == id) {
			unit.set(Unit.CREATETIME,DateUtils.getCurrentTime());
			//unit.save();
			return Db.tx(new IAtom(){//事物处理
				public boolean run() throws SQLException {
					boolean isSave1=unit.save();
					boolean isSave2=bindAdminUnit(unit.getLong(Unit.ID));//需要绑定管理员单位
					return isSave1&&isSave2;
			}});
		} else {
			unit.update();
		}
		return flag;
	}
	
	private boolean bindAdminUnit(Long unitId) {
		List<RoleUser> roleUsers=RoleUser.dao.searchAdmin();
		return UnitUser.dao.SaveAdminUnit(roleUsers,unitId);
	}

	public boolean deleteUnit(String[] ids){
		  //删除 关联信息
        return Unit.dao.deleteByIds(ids);

	}

	/*public List<Category> findAll() {
		return Category.dao.getCategoryList();
	}*/

	public Unit findUnitById(String unitId) {
		// TODO Auto-generated method stub
		return Unit.dao.findUnitIdById(unitId);
	}

	public List<Unit> getUnitList() {
        return Unit.dao.searchUnit();
    }

	public String getCategoryUnit(Long id) {
		StringBuilder stringBuilder = new StringBuilder();
		List<UnitCate> unitCates = UnitCate.dao.searchCategoryUnitByCategoryId(id);
		stringBuilder.append("-");
		for (UnitCate unitCate : unitCates) {
			stringBuilder.append(unitCate.getLong(UnitCate.UNIT_ID));
			stringBuilder.append("-");
		}
		return stringBuilder.toString();
	}

	public List<Unit> getUnitByRoles(List<RoleUser> roleUsers) {
		return Unit.dao.searchUnitByRoles(roleUsers);
	}

	public List<Unit> getUnitByCategory(List<UnitCate> unitCates) {
		return Unit.dao.searchUnitByCategory(unitCates);
	}

	public List<Unit> getUnitByPlantBaseId(String plant_id) {
		return Unit.dao.searchUnitByPlantBaseId(plant_id);
	}

	public List<Category> getCategoryByUnitId(String unit_id) {
		return Category.dao.searchCategoryByUnitId(unit_id);
	}

	public List<UnitUser> getUnitUserByUserId(String user_id) {
		return UnitUser.dao.searchUnitUserByUserId(user_id);
	}

	public List<Unit> getUnitByUser(List<UnitUser> unitUsers) {
		return Unit.dao.searchUnitByUser(unitUsers);
	}

	
}
