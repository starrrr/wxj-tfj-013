package com.tfj.beta.service;

import java.util.List;

import net.sf.ehcache.util.SetAsList;

import org.apache.shiro.SecurityUtils;

import com.jfinal.kit.EncryptionKit;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/6/15.
 */
public class UserService {
	public Page<User> getUserPage(int pageNumber, int pageSize, User user) {
		return User.dao.searchPaginate(pageNumber, pageSize, user);
	}

	public boolean modifyUser(User user, String[] roleIds,String[] unitIds) {
		boolean flag = true;
		Long id = user.getLong(User.ID);
		String enable=user.get(User.ENABLE);
		if (null == id) {
			User checkUser = User.dao.findFirst(
					"select * from s_user where username=?",
					user.getStr(User.USERNAME));
			if (null == checkUser) {
				User loginUser = (User) SecurityUtils.getSubject().getSession()
						.getAttribute(Constant.SHIRO_USER);
				// 创建时间，删除状态，启用状态
				user.set(User.CREATE_TIME, DateUtils.getCurrentTime())
						.set(User.CREATE_USER_ID, loginUser.getLong(User.ID))
						.set(User.PASSWORD,
								EncryptionKit.md5Encrypt(user
										.getStr(User.PASSWORD)))
						.set(User.DELETE_STATUS,"0".equals(enable)?"0":"1")
						// .set(User.PASSWORD, Constant.DEFAULT_PASSWORD)
						.save();
			} else {
				// message = "用户名已经存在";
				flag = false;
			}
		} else {
			RoleUser.dao.deleteByUserId(id); // 删除用户角色信息
			UnitUser.dao.deleteByUserId(id); // 删除用户所有单位
			// 更新时间
			user.set(User.UPDATE_TIME, DateUtils.getCurrentTime())
			.set(User.DELETE_STATUS, "0".equals(enable)?"0":"1")
			.update();
		}
		// 用户绑定角色
		if (null != roleIds && flag) {
			RoleUser roleUser = null;
			for (String roleId : roleIds) {
				roleUser = new RoleUser();
				if(id==null){//
					roleUser.set(RoleUser.ROLE_ID, roleId)
					.set(RoleUser.USER_ID,user.get(User.ID)).save();
				}else {
					roleUser.set(RoleUser.ROLE_ID, roleId)
					.set(RoleUser.USER_ID, id).save();
				}
			}
		}
		// 用户绑定单位
		if (null != unitIds && flag) {
			UnitUser unitUser = null;
			for (String unitId : unitIds) {
				unitUser = new UnitUser();
				if(id==null){//
					unitUser.set(UnitUser.UNIT_ID, unitId)
					.set(UnitUser.USER_ID,user.get(User.ID)).save();
				}else {
					unitUser.set(UnitUser.UNIT_ID, unitId)
					.set(UnitUser.USER_ID,id).save();
				}
			}
		}		
		return flag;
	}

	public boolean deleteUsers(String[] ids) {
		// TODO 删除绑定角色

		return User.dao.deleteByIds(ids);
	}

	public String getUserRole(Long userId) {
		StringBuilder stringBuilder = new StringBuilder();
		List<RoleUser> roleUsers = RoleUser.dao.searchRoleUserByUserId(userId);
		stringBuilder.append("-");
		for (RoleUser roleUser : roleUsers) {
			stringBuilder.append(roleUser.getInt(RoleUser.ROLE_ID));
			stringBuilder.append("-");
		}
		return stringBuilder.toString();
	}

	public boolean checkUserIsAdminitor(Long userId) {
		boolean flag = false;

		List<RoleUser> roleUsers = RoleUser.dao.searchRoleUserByUserId(userId);
		if (null != roleUsers && roleUsers.size() > 0) {
			for (RoleUser roleUser : roleUsers) {
				//System.out.println(roleUser + "::::" + roleUser.get("role_id"));
				if (null != roleUser) {
					
					Role role = Role.dao.findById(roleUser.get("role_id"));
					//System.out.println(role);
					if (role.get("name").equals("管理员")) {
						flag = true;
						break;
					}
				}
			}

		}

		return flag;
	}
	
	public User checkUserIsExist(String userName){
		return User.dao.checkUserByUserName(userName);
		
	}
}
