package com.tfj.beta.service.value;

import java.util.List;
import java.util.Map;

import com.tfj.beta.model.Template;
import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.FreemarkerUtil;

public class NoWidget extends Widget {
	
	private final static String DEFAULT_TEMPLATE = "blank.ftl";

	public NoWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		if(dofilter()){
			params.put("sign","sign");
			params.put("template_id",node.getId());
		}
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}
	
	public boolean dofilter(){//过滤找出末梢节点
		boolean isFlag=true;
		List<Template> templates=Template.dao.getChildByPid(node.getId());
		if(templates.size()>0){
			List<Template> tems=null;
			for(Template template:templates){
				tems=Template.dao.getChildByPid(template.getLong(Template.ID));
				if(tems.size()>0){
					isFlag=false;
					break;
				}
			}
		}
		return isFlag;
	}

}
