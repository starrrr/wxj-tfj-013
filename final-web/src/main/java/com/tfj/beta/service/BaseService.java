package com.tfj.beta.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.TableMapping;

/**
 * Created by Administrator on 2015/6/18.
 */
public class BaseService {
    public boolean modifyModel(Model model) {
        if (null == model.get(TableMapping.me().getTable(model.getClass()).getPrimaryKey())) {
            model.save();
            return true;
        } else {
            model.update();
            return true;
        }
    }
}
