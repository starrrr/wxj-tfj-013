package com.tfj.beta.service;

import com.jfinal.kit.EncryptionKit;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.Enterprise;
import com.tfj.beta.utils.DateUtils;

public class EnterpriseService {

	public Page<Enterprise> getEnterprisePage(int pageNumber, int pageSize,
			Enterprise enterprise) {
		return Enterprise.dao.searchPaginate(pageNumber, pageSize, enterprise);
	}
	
	public Page<Enterprise> getEnterprisePage(int pageNumber, int pageSize, Long id,
			String bind_user) {
		return Enterprise.dao.searchPaginate(pageNumber, pageSize, id,bind_user);
	}
	
	public boolean modifyEnterprise(Enterprise enterprise) {
		boolean flag = true;
		Long id = enterprise.getLong(Enterprise.ID);
		if (null == id) {
			//enterprise.put(Enterprise.COMPANY_PWD, EncryptionKit.md5Encrypt(enterprise.getStr(Enterprise.COMPANY_PWD)));
			enterprise.put(Enterprise.OPERATE_TIME,DateUtils.getCurrentTime());
			enterprise.save();
		} else {
			enterprise.update();
		}
		return flag;
	}

	public boolean deleteEnterprise(String[] ids){
		  //删除 关联信息
        return Enterprise.dao.deleteByIds(ids);

	}

	public Enterprise checkEnterpriseUserIsExist(String enterpriseusername) {
		return Enterprise.dao.checkEnterpriseUserByUserName(enterpriseusername);
	}

	public boolean removeBind(String openids) {
		return Enterprise.dao.removeBind(openids);
	}

	public void resetPW(Enterprise enterprise) {
		String pw=EncryptionKit.md5Encrypt(enterprise.getStr(Enterprise.COMPANY_PWD)).toUpperCase();
		Long id=enterprise.getLong(Enterprise.ID);
		Enterprise.dao.updatePW(pw,id);
	}

	public boolean checkHasBindUser(String id) {
		return Enterprise.dao.isExitBindUser(id);
		
	}

	


}
