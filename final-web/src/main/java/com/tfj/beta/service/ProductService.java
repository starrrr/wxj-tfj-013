package com.tfj.beta.service;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.FieldType;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.User;
import com.tfj.beta.service.value.ImgWidget;
import com.tfj.beta.utils.StringUtils;

/**
 * Created by ZhouD on 2015/9/18.
 */
public class ProductService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
	ProductValueService productValueService = new ProductValueService();
	ProductResultService productResultService = new ProductResultService();
	CategoryService categoryService = new CategoryService();
	DictService dictService = new DictService();

	public Page<Product> getProductPage(int pageNumber, int pageSize, Product product, String orderby,List<Unit> units,String admin) {
		return Product.dao.searchPaginate(pageNumber, pageSize, product, orderby, units,admin);
	}
	
	public boolean modifyProduct(Product product) {
		Long id = product.getLong(Product.ID);
		Long categropyId=product.getLong(Product.CATEGORY_ID);
		String planttype=product.getStr(Product.PLANT_TYPE);
		if(categropyId != null && categropyId == -1){
			product.put(Product.CATEGORY_NAME, "");
		}else if(categropyId != null){
			categoryService.updateCategoryNums(categropyId);
		}
		dulePlantType(product,planttype);
		if (null == id) {
			product.put("auditflag", 0);
			product.put("remark", 0);
			return product.save();
		} else {
			return product.update();
		}
	}
	//处理种植方式
	private boolean dulePlantType(Product product,String planttype) {
		boolean isFlage=false;
		boolean isExistDICT=false;//标记存在对数据字典库插入数据
		int i=0;
		if("-1".equals(planttype)){
			product.put(Product.PLANT_TYPE,"");
		}else{
			//判断数字字典表是否存在该种植方式:存在，正常操作，不存在，对字典表进行相应操作
			Dict dict=dictService.findDicByNameAndType(product,planttype,"PLANTTYPE");
			if(dict==null){
				isExistDICT=true;
				i=dictService.insertDictByTypeAndName(product,planttype,"PLANTTYPE");
			}
		}
		if((isExistDICT&&i>0)||(!isExistDICT&&i==0)){
			isFlage=true;
		}
		return isFlage;
	}

	public boolean deleteProduct(Product product){
	  //删除 关联信息
      return Product.dao.deleteProduct(product);
	}
	
	public Product findById(Long id) {
		return Product.dao.findById(id);
	}
	
	@Before(Tx.class)
	public boolean copyProductAndValues(Long product_id, String product_name, 
			String batches, String types) {
		try {
			Product product = findById(product_id);
			Product newProduct = new Product();
			
			// 复制产品
			newProduct.setAttrs(product);
			newProduct.set(Product.ID, null);
			newProduct.set(Product.AUDITFLAG, 0);
			newProduct.set(Product.PRODUCT_NAME, product_name);
			newProduct.save();
			
			// 复制产品4个环节的 ProductValue 值
			if (StringUtils.isNotBlank(types)) {
				String[] typeArray = types.split(",");
				
				String ptCondition = "and t.processType in " + StringUtils.toParentheseString(typeArray);
				
				Long new_pid = newProduct.getLong(Product.ID);
				ProductValue.dao.copyByProductIdAndProcessType(new_pid, product_id, ptCondition);
				
				copyProductValueFile(new_pid, ptCondition);
				
				// 赋指定批号值到复制出来的产品
				assignSpecificValue(batches, new_pid, ptCondition);
			}

		} catch (Exception ex) {
			logger.error("copyProductAndValues has a error!", ex);
			return false;
		}
		return true;
	}
	
	/**
	 * 复制节点文件
	 * @param new_id
	 * @param ptCondition
	 * @throws Exception 
	 */
	private void copyProductValueFile(Long new_id, String ptCondition) throws Exception {
		List<ProductValue> imgPvs = ProductValue.dao.findByFieldType(new_id, FieldType.IMG, ptCondition);
		
		// 创建图片文件夹
		String pvPath = ImgWidget.PRODUCT_VALUE_FOLDER + "product_" + new_id;
		File newFolder = new File(JFinal.me().getServletContext().getRealPath(pvPath));
		if(!newFolder.exists()) {
			newFolder.mkdirs();
		}
		
		for (int i = 0; i < imgPvs.size(); i++) {
			ProductValue imgPv = imgPvs.get(i);
			String url = imgPv.getStr(ProductValue.PRODUCT_VALUE);
			if (StringUtils.isNotBlank(url)) {
				String suffix = url.substring(url.lastIndexOf(".")).toLowerCase();
				String str_template_id = String.valueOf(imgPv.getLong(ProductValue.TEMPLATE_ID));
				String str_pv_id = String.valueOf(imgPv.getLong(ProductValue.ID));
				
				File srcFile = new File(JFinal.me().getServletContext().getRealPath(url));
				
				String fileName = ImgWidget.getFileName(str_template_id, str_pv_id, suffix);
				String pv = pvPath + "/" + fileName;
				
				File destFile = new File(newFolder, fileName);
				if (srcFile.exists()) {
					FileUtils.copyFile(srcFile, destFile);
				}
				
				imgPv.set(ProductValue.PRODUCT_VALUE, pv);
				imgPv.update();
			}
		}
	}
	
	/**
	 * 赋指定批号值到复制出来的产品
	 * @param batches
	 * @param new_id
	 * @param typesCondition
	 */
	private void assignSpecificValue(String batches, Long new_id, String ptCondition) {
		// 批号赋值
		if (StringUtils.isNotBlank(batches)) {
			JSONObject obj = JSON.parseObject(batches);
			String seed_batch = obj.getString("seed_batch");
			String material_batch = obj.getString("material_batch");
			String product_batch = obj.getString("product_batch");
			String flow_batch = obj.getString("flow_batch");
			
			// 查询出特殊批号的 ProductValue
			List<ProductValue> batchPvs = ProductValue.dao.findSpecifiedBatchNum(new_id, ptCondition);
			
			for (ProductValue pv : batchPvs) {
				String processType = pv.getStr(Template.PROCESSTYPE) == null ? null : pv.getStr(Template.PROCESSTYPE).trim();
				String key_name = pv.getStr(Template.KEY_NAME) == null ? null : pv.getStr(Template.KEY_NAME).trim();
				// 种植环节：种子批号、原料批号
				if (Constant.PLANT_PROCESS.equals(processType)) { 
					if (ArrayUtils.contains(ProductResult.seedBatchType, key_name)) {
						if (StringUtils.isNotBlank(seed_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, seed_batch);
							pv.update();
						}
					} else if (ArrayUtils.contains(ProductResult.materialBatchType, key_name)) {
						pv.set(ProductValue.PRODUCT_VALUE, material_batch);
						pv.update();
					}
				// 加工环节：原料批号、成品批号
				} else if (Constant.SUPERVISE_PROCESS.equals(processType)) { 
					if (ArrayUtils.contains(ProductResult.materialBatchType, key_name)) {
						if (StringUtils.isNotBlank(material_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, material_batch);
							pv.update();
						}
					} else if (ArrayUtils.contains(ProductResult.productBatchType, key_name)) {
						if (StringUtils.isNotBlank(product_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, product_batch);
							pv.update();
						}
					}
				
				// 仓管流通环节：成品批号、流通批号
				} else if (Constant.WAREHOUSE_PROCESS.equals(processType)) {
					if (ArrayUtils.contains(ProductResult.productBatchType, key_name)) {
						if (StringUtils.isNotBlank(product_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, product_batch);
							pv.update();
						}
					} else if (ArrayUtils.contains(ProductResult.flowBatchType, key_name)) {
						if (StringUtils.isNotBlank(flow_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, flow_batch);
							pv.update();
						}
					}
				// 销售环节： 流通批号
				} else if (Constant.SALE_PROCESS.equals(processType)) {
					 if (ArrayUtils.contains(ProductResult.flowBatchType, key_name)) {
						 if (StringUtils.isNotBlank(flow_batch)) {
							pv.set(ProductValue.PRODUCT_VALUE, flow_batch);
							pv.update();
						 }
					}
				}
			}
		}
	}
	
	public boolean queryAssociated(String id) {
		return Product.dao.queryAssociatedById(id);
	}

	public boolean mandatoryDeleteProduct(String id){
		
		boolean QRCode=true;//是否为二维码
		List<ProductValue> productValues=productValueService.findProductValuesById(Long.parseLong(id));
		ProductResult productResult=productResultService.getPResultByProductId(Long.parseLong(id));
		
		try {
			//删除二维码
			if(productResult!=null){
				String remark=productResult.getStr("remark");
				if(StringUtils.isNotBlank(remark)&&remark.indexOf(",")>-1){
					String img1=productResult.getStr("remark").split(",")[0];//获取路径;
					String img2=productResult.getStr("remark").split(",")[1];
					String img1Name=img1.substring(img1.lastIndexOf("/")+1);//截取名称
					String img2Name=img2.substring(img1.lastIndexOf("/")+1);
					deletepicture(img1Name,QRCode);//截取图片名称，删除图片
					deletepicture(img2Name,QRCode);//
					
				}
			}
			//删除产品值图片
			if(productValues.size()>0){
				for(ProductValue pv:productValues){
					String pvalue=pv.getStr(ProductValue.PRODUCT_VALUE);
					if(StringUtils.isNotBlank(pvalue)){
						if(pvalue.startsWith("upload/product_value/")){//判断是否为图片
							String img=pvalue.substring(pvalue.lastIndexOf("/")+1);//获取图片名称
								if("upload/product_value/example.png".endsWith(pvalue)){//判断不为默认图片
									deletepicture(img,false);//删除所有图片
								}
						}
					}
				}
			}
	
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		//删除 关联信息
		return Product.dao.mandatoryDeleteByIds(id);
	}
	
	private void deletepicture(String picture,boolean QRCode) {
		File file=null;//为了加快检索速度，做出判断
		if(QRCode){
			file=new File(JFinal.me().getServletContext().getRealPath("/upload/QRCode"));
		}else {
			file=new File(JFinal.me().getServletContext().getRealPath("/upload/product_value"));
		}
		
		delete(file,picture);
	}
	private void delete(File file,String picture) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File file2 : files) {
				delete(file2,picture);
			}
		} else {
			String name = file.getName();
			if (picture.equals(name)) {
				file.delete();
			}
		}
	}

	public void finishInit(final Long product_id) {
		Product.dao.finishInit(product_id);
	}

	public boolean releaseAudit(Product product) {
		return Product.dao.releaseAudit(product);
	}
	
	public List<Product> findProductsByCategoryId(String categoryId){
		return Product.dao.getProductsByCategoryId(categoryId);
	}
}
