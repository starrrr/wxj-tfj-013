package com.tfj.beta.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.tfj.beta.model.FieldType;
import com.tfj.beta.model.FrontEndRole;
import com.tfj.beta.model.ProcessType;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.User;
import com.tfj.beta.model.view.ProductTreeNode;
import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.service.value.ImgWidget;
import com.tfj.beta.service.value.Widget;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.StringUtils;

public class ProductValueService {
	
	private final static Logger logger = LoggerFactory.getLogger(ProductValueService.class);
	
	private RedisCacheService redisCacheService = new RedisCacheService();
	
	/**
	 * 获取 PvBean 树
	 * @param processType
	 * @param categoryId
	 * @return
	 */
	public List<PvBean> getPvBean(User user, 
			Long product_id, String processType, Long category_id) {
		
		if (StrKit.isBlank(processType)) {
			return null;
		}
		
		boolean isAdmin = checkIsAdmin(user);
		
		Product product = Product.dao.findById(product_id);
		
		List<ProductTreeNode> treeNode = ProductTreeNode.dao.getListByProcessType(
				product_id, processType, category_id);
//		List<Template> templateList = Template.dao.getListByProcessType(processType, categoryId);
		
		List<PvBean> roots = new ArrayList<PvBean>();
		if (null != treeNode && treeNode.size() > 0) {
			
			// 根据 ProductTreeNode 创建 PvBean 树节点
			List<PvBean> beans = new ArrayList<PvBean>();
			for (ProductTreeNode ptn : treeNode) {
				PvBean bean = createPvBean(ptn, product, isAdmin);
				if (ptn.getLong(Template.PID) == 0) {
					roots.add(bean);
				}
				beans.add(bean);
			}
			
			// clone 为查找到了节点后移除用, 不再重复遍历
			List<PvBean> clone = new ArrayList<PvBean>(beans);
			// 通过循环 beans 去找 beans 的下一层节点
			for (PvBean root : beans) {
				
				List<PvBean> children = new ArrayList<PvBean>();
				for(PvBean bean : clone) {
					if (root.getId() == bean.getPid()) {
						children.add(bean);
					}
				}
				
				if (!children.isEmpty()) {
					root.setChildrenSort(children);
					// 已经被 bean 找到的下一层节点可以移除，下次遍历不需使用
					clone.removeAll(children);
				}
			}
		}
		return roots;
	}
	
	/**
	 * 检查当前用户是否管理员
	 * @param user
	 * @return
	 */
	private boolean checkIsAdmin(User user) {
		if (user != null) {
			UserService userService = ServiceFactory.getStaticService(UserService.class);
			Long user_id = user.getLong(User.ID);
			if (user_id != null) {
				return userService.checkUserIsAdminitor(user.getLong(User.ID));
			}
		}
		return false;
	}
	
	/**
	 * 通过 t_product_value.id 构建 PvBean
	 * @param pv_id
	 * @return
	 */
	private PvBean constructPvBean(Long pv_id) {
		ProductTreeNode node = ProductTreeNode.dao.findById(pv_id);
		// XXX 场景用不上 isAdmin, 所以第二个参数是null, 第三个参数是 false?
		PvBean bean = createPvBean(node, null, false);
//		/**
//		 * 此处为了 Widget 中 setMultiValFunction 判断而准备
//		 * 不进行递归获取子节点的子节点, 只获取当前 pv_id 下的子节点(children)
//		 */
//		List<ProductTreeNode> children = ProductTreeNode.dao.findChildrenByPid(bean.getPid());
//		if (children != null && !children.isEmpty()) {
//			bean.setupFakeChildren();
//		}
		return bean;
	}
	
	/**
	 * 创建 PvBean 对象
	 * @param tn
	 * @return
	 */
	private PvBean createPvBean(ProductTreeNode tn, Product product, boolean isAdmin) {
		PvBean bean = new PvBean();
		
		// t_product_value
		bean.setValue(tn.getStr(ProductValue.PRODUCT_VALUE));
		bean.setAuditFlag(tn.getInt(ProductValue.AUDITFLAG));
		
		// ProductTreeNode
		bean.setPv_id(tn.getLong(ProductTreeNode.PV_ID));
		bean.setCategory_id(tn.getLong(ProductTreeNode.CATEGORY_ID));
		bean.setPv_sort(tn.getInt(ProductTreeNode.PV_SORT));
		
		// t_template
		bean.setId(tn.getLong(Template.ID));
		bean.setName(tn.getStr(Template.KEY_NAME));
		bean.setLevel(tn.getInt(Template.LEVELNUM));
		bean.setPid(tn.getLong(Template.PID));
		bean.setProcessType(tn.getStr(Template.PROCESSTYPE));
		bean.setIconPath(tn.getStr(Template.ICON));
		bean.setDefault_value(tn.getStr(Template.DEFAULT_VALUE));
		bean.setField_develop(tn.getStr(Template.FIELD_DEVELOP));
		bean.setSort(tn.getLong(Template.SORT));
		
		// 角色判断
		bean.setIsAdmin(isAdmin);
		if (product != null) {
			bean.setProductAuditFlag(product.getInt(Product.AUDITFLAG));
		}

		// set field type and widget, be careful and look inside!!!
		bean.setFileTypeAndWidget(tn.getStr(Template.FIELD_TYPE));
		return bean;
	}
	
	public boolean delMultiProductValue(Long pv_id) {
		return ProductValue.dao.deleteById(pv_id);
	}
	
	/**
	 * 新增多值类型产品值
	 * @param product_id
	 * @param category_id
	 * @param user
	 * @param array
	 * @return
	 */
	public boolean addMultiProductValue(Long product_id, Long category_id, JSONArray array) {
		
		JSONObject[] arr = array.toArray(new JSONObject[]{});
		
		// 原本节点不存在值, 新建
		Object[][] saveInfo = new Object[arr.length][];
		
		String[] ids = arr[0].getString("name").split(Widget.SPLIT);
		Long template_id = Long.parseLong(ids[0]);
		
		Long count = ProductValue.dao.countByProductIdAndTemplateId(product_id, template_id);
		
		
		if (arr.length == 1) { // 第一个节点有值，继续新增
			String val = StringUtils.defaultString(arr[0].getString("value"));
			saveInfo[0] = new Object[]{product_id, template_id, val, count};
			ProductValue vo = createMiniProductValue(product_id, template_id, val, count);
			return vo.save();
		} else { // 第一个节点没值，新建值保存、新增
			String val1 = StringUtils.defaultString(arr[0].getString("value"));
			String val2 = StringUtils.defaultString(arr[1].getString("value"));
			
//			ProductValue vo1 = createMiniProductValue(product_id, template_id, val1, null);
//			ProductValue vo2 = createMiniProductValue(product_id, template_id, val2, 1L);
//			return vo1.save() && vo2.save();
			
			saveInfo[0] = new Object[]{product_id, template_id, val1, null};
			saveInfo[1] = new Object[]{product_id, template_id, val2, 1};
			
			// 输出测试
			debugSaveProductVal(saveInfo, null);
			return ProductValue.dao.addProductValue(saveInfo);
		}
	}
	
	private ProductValue createMiniProductValue(Long product_id, Long template_id, 
			String val, Long sort) {
		ProductValue vo = new ProductValue();
		vo.set(ProductValue.PRODUCT_ID, product_id);
		vo.set(ProductValue.TEMPLATE_ID, template_id);
		vo.set(ProductValue.PRODUCT_VALUE, val);
		vo.set(ProductValue.AUDITFLAG, 0);
		vo.set(ProductValue.SORT, sort);
		return vo;
	}
	
	/**
	 * 
	 * @param product_id
	 * @param category_id
	 * @param processType
	 * @param focusTemplate_id
	 * @param array
	 * @param user
	 * @return
	 */
	public boolean saveProductValue(Long product_id, Long category_id, 
			String processType, Long focusTemplate_id, JSONArray array, User user) {
		
		JSONObject[] arr = array.toArray(new JSONObject[]{});
		
		// 原本节点不存在值, 新建
		List<Object[]> saveInfo = new ArrayList<Object[]>();
		// 节点存在值, 更新
		List<Object[]> updateInfo = new ArrayList<Object[]>();
		
		String dateTime = DateUtils.getCurrentTime();
		
		for (int i = 0; i < arr.length; i++) {
			
			JSONObject jObj = arr[i];
			// 0 : template_id, 1 : pv_id
			String[] ids = jObj.getString("name").split(Widget.SPLIT);
			String val = StringUtils.defaultString(jObj.getString("value"));
			// 至少存在 template_id
			Long template_id = Long.valueOf(ids[0]);
			
			if (ids.length == 1) { // 新建 product_value
				
				if (StringUtils.isNotBlank(val)) {
					saveInfo.add(new Object[]{product_id, template_id, val, 
							user.getLong(User.ID), user.getStr(User.USERNAME), dateTime});
				}
			} else { // 更新 product_value
				Long id = Long.valueOf(ids[1]);
				
				// FIXME val 为 "" 需要删除值 ???
				updateInfo.add(new Object[]{val, user.getLong(User.ID), 
						user.getStr(User.USERNAME), dateTime, id});
			}
				
		}
		
		final Object[][] saveData = saveInfo.toArray(new Object[][]{});
		final Object[][] updateData = updateInfo.toArray(new Object[][]{});
		
		// 输出测试
		debugSaveProductVal(saveData, updateData);
		
		boolean saveResult = ProductValue.dao.saveAndUpdateProductValue(saveData, updateData);
		
		// 保存当前录入值位置
		if (saveResult) {
			ProductResult result = ProductResult.dao.getProductResultByProductId(String.valueOf(product_id));
			if (result != null) {
				result.set(ProductResult.PROCESSTYPE, processType);
				result.set(ProductResult.CURRENT_TEMP_ID, focusTemplate_id);
				saveResult = saveResult && result.update();
			} else {
				result = new ProductResult();
				result.set(ProductResult.CATEGORY_ID, category_id);
				result.set(ProductResult.PRODUCT_ID, product_id);
				result.set(ProductResult.PROCESSTYPE, processType);
				result.set(ProductResult.CURRENT_TEMP_ID, focusTemplate_id);
				saveResult = saveResult && result.save();
			}
		}
		
		// redis 录入数据保存后 统一 写入到redis
		redisCacheService.cacheJson(category_id, product_id, processType,FrontEndRole.GUEST,true);
		redisCacheService.cacheJson(category_id, product_id, processType,FrontEndRole.BUSINESS,true);
		redisCacheService.cacheJson(category_id, product_id, processType,FrontEndRole.CUSTOMER,true);
		
		return saveResult;
	}
	
	/**
	 * 移除图片节点值，并删除图片
	 * @param pv_id
	 * @return
	 */
	public String removeImgValue(User user, Long pv_id) {
		if (user != null && pv_id != null && pv_id > 0) {
			ProductValue pv = findById(pv_id);
			// 删除图片
			try {
				String url = pv.getStr(ProductValue.PRODUCT_VALUE);
				File file = new File(JFinal.me().getServletContext().getRealPath(url));
				if (file.exists()) {
					file.delete();
				}
			} catch (Exception e) {
				logger.warn("清除数据库图片值删除文件错误.", e);
			}

			// 置空值, 保存
			pv.set(ProductValue.PRODUCT_VALUE, null);
			pv.set(ProductValue.RECORD_ID, user.getLong(User.ID));
			pv.set(ProductValue.RECORD_NAME, user.getStr(User.NAME));
			pv.set(ProductValue.RECORD_TIME, DateUtils.getCurrentTime());
			
			if (pv.update()) {
				return generateImgShowHTML(pv_id);
			}
		}
		return "FAILED";
	}
	
	public String generateImgShowHTML(Long pv_id) {
		PvBean bean = constructPvBean(pv_id);
		return ImgWidget.generateImgShowHTML(bean);
	}
	
	public boolean unauditProductValues(User user, Long product_id, Long[] tids) {
		// TODO 批量 unaudit
		return false;
	}
	
	/**
	 * 取消审核
	 * @param user
	 * @param pv_id
	 * @return
	 */
	public boolean unauditProductValue(User user, Long product_id, Long template_id, Long pv_id) {
		if (user != null) {
			boolean result = false;
			if (pv_id != null) {
				ProductValue pv = findById(pv_id);
				// TODO 释放审核是否需要记录审核相关信息
//				pv.set(ProductValue.AUDIT_TIME, DateUtils.getCurrentTime());
//				pv.set(ProductValue.AUDITOR, user.getStr(User.NAME));
//				pv.set(ProductValue.AUDITORID, user.getLong(User.ID));
				
				if (pv != null) {
					Integer auditFlag = pv.getInt(ProductValue.AUDITFLAG);
					if (auditFlag == null || auditFlag != 0) {
						pv.set(ProductValue.AUDITFLAG, 0);
						result = pv.update();
					} else if (auditFlag == 0){
						result =  true;
					}
				}
			} else {
				// 如果释放的值是不存在的 (pv_id == null), 则新建此值
				if (product_id != null) {
					ProductValue pv = createMiniProductValue(
							product_id, template_id, StringUtils.EMPTY, null);
					result = pv.save();
				}
			}
			
			
			Product p = Product.dao.findById(product_id);
			p.set(Product.AUDITFLAG, 1);
			return result && p.update();
		}
		return false;
	}
	
	/**
	 * 
	 * TODO 需要事务处理
	 * @param product_id
	 * @param processType
	 */
	@Before(Tx.class)
	public boolean auditProductValue(String admin,User user, Long product_id,Long category_id, String processType) {
		
		ProductValue.dao.updateAuditFlag(user, product_id,category_id, processType);
		
		auditProduct(user, product_id);
		
		List<ProductValue> list = ProductValue.dao.getResultDataSource(product_id, processType);
		
		boolean flag = ProductResult.dao.modifyProductResult(admin,product_id,user,list, processType);
		
		// redis 录入数据保存后 统一 写入到redis
		Product product = Product.dao.get(String.valueOf(product_id));
		if(null!=product){
			Long categoryId = product.getLong(Product.CATEGORY_ID);
			if(null!=categoryId){
				
				redisCacheService.cacheJson(categoryId, product_id, processType,FrontEndRole.GUEST,true);
				redisCacheService.cacheJson(categoryId, product_id, processType,FrontEndRole.BUSINESS,true);
				redisCacheService.cacheJson(categoryId, product_id, processType,FrontEndRole.CUSTOMER,true);
			}
			
		}
		
		return flag;
	}
	
	/**
	 * 审核产品
	 * @param user
	 * @param product_id
	 * @return
	 */
	private boolean auditProduct(User user, Long product_id) {
		Product p = Product.dao.findById(product_id);
		if (p != null) {
			Integer auditflag = p.getInt(Product.AUDITFLAG);
			if (!TWO.equals(auditflag)) {
				// 审核通过
				p.set(Product.AUDITFLAG, 1);
				// 审核者信息
				p.set(Product.AUDITORID, user.getLong(User.ID));
				p.set(Product.AUDITOR, user.getStr(User.NAME));
				p.set(Product.AUDIT_TIME, DateUtils.getCurrentTime());
				return p.update();
			}
		}
		return false;
	}
	
	public ProductValue findById(Long id) {
		return ProductValue.dao.findById(id);
	}
	
	/**
	 * 写日志
	 * @param sData
	 * @param uData
	 * @return
	 */
	private void debugSaveProductVal(Object[][] sData, Object[][] uData) {
		StringBuffer debug = new StringBuffer();
		debug.append("save data: ");
		if (sData != null) {
			for (Object[] obj : sData) {
				debug.append(Arrays.toString(obj));
			}
		}
		debug.append("\r\n");
		
		debug.append("update data: ");
		if (uData != null) {
			for (Object[] obj : uData) {
				debug.append(Arrays.toString(obj));
			}
		}
		debug.append("\r\n");
		
		logger.debug(debug.toString());
	}
	
	public boolean hasProductValue(Long product_id) {
		Long count = ProductValue.dao.countByProductId(product_id);
		if (count != null) {
			return count > 0;
		}
		return false;
	}
	
	private String checkFirstImgProductValue(Long product_id, Long category_id, ProcessType type) {
		// 种植环节
		String processType = type.name();
		String processTypeName = type.getCn();
		ProductValue firstImgPv = ProductValue.dao.getProductValueByProductId(product_id, 
				category_id, processType);
		if (firstImgPv != null) {
			String fieldType = firstImgPv.getStr(Template.FIELD_TYPE);
			String key_name = firstImgPv.getStr(Template.KEY_NAME);
			String product_value = firstImgPv.getStr(ProductValue.PRODUCT_VALUE);
			if (FieldType.IMG.nameEquals(fieldType)) {
				if (StringUtils.isBlank(product_value)) {
					return "【" + processTypeName + "】下【图片】节点，首张产品图片还没有上传。";
				}
			}
		}
		return StringUtils.SUCCESS;
	}
	
	public String checkProductPVImagesHasVale(Long product_id) {
		Product p = Product.dao.findById(product_id);
		Long category_id = p.getLong(Product.CATEGORY_ID);
		
		// 种植环节
		String result = checkFirstImgProductValue(product_id, category_id, 
				ProcessType.PLANT_PROCESS);
		if (!StringUtils.SUCCESS.equals(result)) {
			return result;
		}
		
		result = checkFirstImgProductValue(product_id, category_id, 
				ProcessType.SUPERVISE_PROCESS);
		if (!StringUtils.SUCCESS.equals(result)) {
			return result;
		}

		result = checkFirstImgProductValue(product_id, category_id, 
				ProcessType.WAREHOUSE_PROCESS);
		if (!StringUtils.SUCCESS.equals(result)) {
			return result;
		}
		
		result = checkFirstImgProductValue(product_id, category_id, 
				ProcessType.SALE_PROCESS);
		if (!StringUtils.SUCCESS.equals(result)) {
			return result;
		}
		
//		return "你check不过啦，混蛋！";
		return StringUtils.SUCCESS;
	}
	
	private final static Integer TWO = new Integer(2);
	
	@Before(Tx.class)
	public boolean finishEntry(User user, Long product_id) {
		ProductValue.dao.updateProductAuditFlag(user, product_id);
		Product product = Product.dao.findById(product_id);
		Integer auditFlag = product.getInt(Product.AUDITFLAG);
		if (!TWO.equals(auditFlag)) {
			product.set(Product.AUDITFLAG, TWO);
			boolean result = product.update();
			// 清除记录的录入位置记录
			if (result) {
				ProductResult vo = ProductResult.dao.getProductResultByProductId(String.valueOf(product_id));
				if (vo != null) {
					vo.set(ProductResult.PROCESSTYPE, null);
					vo.set(ProductResult.CURRENT_TEMP_ID, null);
					vo.update();
				}
			}
			return result;
		}
		return false;
	}

	public String checkProductPvHasValueORIsAudit(Long product_id) {
		Product p = Product.dao.findById(product_id);
		Long category_id = p.getLong(Product.CATEGORY_ID);
		String result="";
		String plant_result = checkProductValueORIsAudit(product_id, category_id, 
				ProcessType.PLANT_PROCESS);
		if (!StringUtils.SUCCESS.equals(plant_result)) {
			String[] plant_results=plant_result.split("><");
			if(plant_result.startsWith("><")){//不存在未审核的值
				result=result+"<div align='left'>【" + "种植环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + plant_results[1].substring(0,plant_results[1].lastIndexOf("、")) + "】</div>";
			}else if(plant_result.endsWith("><")){//不存在空值
				result=result+"<div align='left'>【" + "种植环节" + "】：</div>";
				result=result+"<div align='left'>未审核节点：【" + plant_results[0].substring(0,plant_results[0].lastIndexOf("、")) + "】</div>";
			}else{//都存在
				result=result+"<div align='left'>【" + "种植环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + plant_results[1].substring(0, plant_results[1].lastIndexOf("、")) + "】</div>";
				result=result+"<div align='left'>未审核节点：【" + plant_results[0].substring(0, plant_results[0].lastIndexOf("、")) + "】</div>";
			}
		}
		String supervise_result = checkProductValueORIsAudit(product_id, category_id, 
				ProcessType.SUPERVISE_PROCESS);
		if (!StringUtils.SUCCESS.equals(supervise_result)) {
			String[] supervise_results=supervise_result.split("><");
			if(supervise_result.startsWith("><")){//不存在未审核的值
				result=result+"</br><div align='left'>【" + "加工环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + supervise_results[1].substring(0,supervise_results[1].lastIndexOf("、")) + "】</div>";
			}else if(supervise_result.endsWith("><")){//不存在空值
				result=result+"</br><div align='left'>【" + "加工环节" + "】：</div>";
				result=result+"<div align='left'>未审核节点：【" + supervise_results[0].substring(0,supervise_results[0].lastIndexOf("、")) + "】</div>";
			}else{//都存在
				result=result+"</br><div align='left'>【" + "加工环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + supervise_results[1].substring(0,supervise_results[1].lastIndexOf("、")) + "】</div>";
				result=result+"<div align='left'>未审核节点：【" + supervise_results[0].substring(0,supervise_results[0].lastIndexOf("、")) + "】</div>";
			}
		}
		String warehouse_result = checkProductValueORIsAudit(product_id, category_id, 
				ProcessType.WAREHOUSE_PROCESS);
		if (!StringUtils.SUCCESS.equals(warehouse_result)) {
			String[] warehouse_results=warehouse_result.split("><");
			if(warehouse_result.startsWith("><")){//不存在未审核的值
				result=result+"</br><div align='left'>【" + "流通环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + warehouse_results[1].substring(0,warehouse_results[1].lastIndexOf("、")) + "】</div>";
			}else if(warehouse_result.endsWith("><")){//不存在空值
				result=result+"</br><div align='left'>【" + "流通环节" + "】：</div>";
				result=result+"<div align='left'>未审核节点：【" + warehouse_results[0].substring(0,warehouse_results[0].lastIndexOf("、")) + "】</div>";
			}else{//都存在
				result=result+"</br><div align='left'>【" + "流通环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + warehouse_results[1].substring(0,warehouse_results[1].lastIndexOf("、")) + "】</div>";
				result=result+"<div align='left'>未审核节点：【" + warehouse_results[0].substring(0,warehouse_results[0].lastIndexOf("、")) + "】</div>";
			}	
		}
		String sale_result = checkProductValueORIsAudit(product_id, category_id, 
				ProcessType.SALE_PROCESS);
		if (!StringUtils.SUCCESS.equals(sale_result)) {
			String[] sale_results=sale_result.split("><");
			if(sale_result.startsWith("><")){//不存在未审核的值
				result=result+"</br><div align='left'>【" + "销售环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + sale_results[1].substring(0,sale_results[1].lastIndexOf("、")) + "】</div>";
			}else if(sale_result.endsWith("><")){//不存在空值
				result=result+"</br><div align='left'>【" + "销售环节" + "】：</div>";
				result=result+"<div align='left'>未审核节点：【" + sale_results[0].substring(0,sale_results[0].lastIndexOf("、")) + "】</div>";
			}else{//都存在
				result=result+"</br><div align='left'>【" + "销售环节" + "】：</div>";
				result=result+"<div align='left'>空值节点：【" + sale_results[1].substring(0,sale_results[1].lastIndexOf("、")) + "】</div>";
				result=result+"<div align='left'>未审核节点：【" + sale_results[0].substring(0,sale_results[0].lastIndexOf("、")) + "】</div>";
			}	
		}
		if("".equals(result)){
			return StringUtils.SUCCESS;
		}else {
			return result;
		}
		
	}

	private String checkProductValueORIsAudit(Long product_id,
			Long category_id, ProcessType type) {
		String str="";//审核字符串
		String nullstr="";//空值字符串
		String processType = type.name();
		List<ProductValue> Pvlist = ProductValue.dao.checkExistProductValueORisAuditByProductId(product_id, 
				category_id, processType);
		for(ProductValue pv:Pvlist){
			String fieldType = pv.getStr(Template.FIELD_TYPE);
			String key_name = pv.getStr(Template.KEY_NAME);
			String product_value = pv.getStr(ProductValue.PRODUCT_VALUE);
			String auditFlag = String.valueOf(pv.getInt(ProductValue.AUDITFLAG));
			
			if("image".equals(key_name)){//重新对图片命名
				key_name="图片";
			}
			//处理未审核的字段（因为值不为空才能给审核，所以只要判断不为空的字段就行）
			if(StringUtils.isNotBlank(product_value)){
				//判断未审核的
				if("0".equals(auditFlag)){
					str=str+key_name+"、";
				}
			}
			
			//有值才能审核 判断空值
			if(FieldType.FILE.getName().equals(fieldType)){
				if("-1".equals(product_value)){
					nullstr=nullstr+key_name+"、";
				}
			}else if(("".equals(product_value)||StringUtils.isBlank(product_value))&&!fieldType.equals(FieldType.BLANK.getName())){
				nullstr=nullstr+key_name+"、";
			}
			
		}
		return (str==""&&nullstr=="")?StringUtils.SUCCESS:str+"><"+nullstr;
	}
	public List<ProductValue> findProductValuesById(long id) {
		// TODO Auto-generated method stub
		return ProductValue.dao.getProductVlaueById(id);
	}

	public boolean saveProductValue(List<Template> templates,final Long product_id,final Long category_id) {
		return ProductValue.dao.initProductValue(templates,product_id,category_id);
	}
	/**
	 * 强制完成
	 * @param product_id
	 * @param loginUser
	 */
	public boolean mdfinish(Long product_id, User loginUser) {
		return ProductValue.dao.mdfinish(product_id,loginUser);
	}

}
