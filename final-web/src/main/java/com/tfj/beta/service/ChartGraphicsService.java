package com.tfj.beta.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.tfj.beta.model.Product;

public class ChartGraphicsService {
	BufferedImage image;

	void createImage(String fileLocation) {
		try {
//			FileOutputStream fos = new FileOutputStream(fileLocation);
//			BufferedOutputStream bos = new BufferedOutputStream(fos);
//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(bos);
//			encoder.encode(image);
//			bos.close();
			String formatName = fileLocation.substring(fileLocation.lastIndexOf(".") + 1);
	         
	         ImageIO.write(image, formatName, new File(fileLocation) );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void graphicsGeneration(File file,String flow_batch_num,Product product) {
		int imageWidth = 360;// 图片的宽度
		int imageHeight = 576;// 图片的高度
		image = new BufferedImage(imageWidth, imageHeight,
				BufferedImage.TYPE_INT_RGB);
		Graphics graphics = image.getGraphics();

		Font fontTitle = new Font("宋体", Font.BOLD, 25);  
		graphics.setFont(fontTitle);
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, imageWidth, imageHeight);
		graphics.setColor(Color.BLACK);
		graphics.drawString("微信扫一扫 ",120,45);
		graphics.drawRect(30,69,300,300);//二维码框
		graphics.drawString("名称:"+product.getStr(Product.CATEGORY_NAME),20,420);
		graphics.drawString("批号:"+flow_batch_num,20,460);
		graphics.drawString("天方健(中国)药业有限公司",20,500);
		graphics.drawString("中药材全程可追溯系统",20,540);
		graphics.drawRect(10,10,340,560);//外框
		
		BufferedImage bimg = null;
		try {
			bimg = javax.imageio.ImageIO.read(file);
		} catch (Exception e) {
		}
		if (bimg != null)
			graphics.drawImage(bimg,31,70, null);
		graphics.dispose();
		createImage(file.getPath());
	}
}