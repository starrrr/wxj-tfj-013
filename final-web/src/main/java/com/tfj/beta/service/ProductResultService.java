package com.tfj.beta.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.User;

/**
 * Created by Administrator on 2015/6/12.
 */
public class ProductResultService {

	// @RequiresPermissions(value = { "system:dict" })
	public Page<ProductResult> getProductResultPage(String admin,List<Unit> units,int pageNumber,
			int pageSize, ProductResult productResult, String productName,
			String flowBatchFlag,String tag) {
		return ProductResult.dao.searchPaginate(admin,units,pageNumber, pageSize,
				productResult, productName, flowBatchFlag,tag);
	}
	
	public ProductResult getProductResultByProductId(Long product_id) {
		return ProductResult.dao.getModelByProductId(String.valueOf(product_id));
	}

	public ProductResult getPResultByProductId(long id) {
		return ProductResult.dao.getProductResultByProductId(String.valueOf(id));
	}
}
