package com.tfj.beta.service;

import com.tfj.beta.core.Constant;
import com.tfj.beta.model.FrontEndRole;
import com.tfj.beta.utils.PropertiesUtil;
import com.tfj.beta.utils.RedisClusterUtil;
import com.tfj.beta.utils.RedisUtil;

/**
 * Created by Administrator on 2015/6/12.
 */
public class RedisCacheService {

	private IndexService indexService = new IndexService();
	Boolean flag = PropertiesUtil.getRedisClusterMode();
	/**
	 * 
	 * @param categoryId
	 * @param productId
	 * @param processType：环节
	 * @param role_name:角色
	 * @param flushIndex:是否刷新主页
	 * @return
	 */
	public boolean cacheJson(Long categoryId, Long productId, String processType,String role_name,Boolean flushIndex) {

		// 判断是否集群还是单点redis
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		try {
			this.cacheStrByKey(flag, categoryId, productId, processType,role_name,flushIndex);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	/**
	 * 带json数据储存到缓存
	 * @param categoryId
	 * @param productId
	 * @param processType
	 * @param role_name
	 * @param flushIndex
	 * @param json
	 * @return
	 */
	public boolean cacheJson(Long categoryId, Long productId, String processType,String role_name,Boolean flushIndex,String json) {

		// 判断是否集群还是单点redis
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		try {
			this.cacheStrByKey(flag, categoryId, productId, processType,role_name,flushIndex,json);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	public String getCacheJson(String processType, String category_id,
			String product_id, String role_name){
		// 判断是否集群还是单点redis
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		String json = null;
		if(processType==null){//主页
			if (flag) {
				// Y RedisCluster RedisClusterUtil
				json = (String) RedisClusterUtil.getCacheValueByKey(category_id
						+ "+" + product_id+"+" +role_name);
			} else {
				// N single RedisUtil
				json = (String) RedisUtil.getCacheValueByKey(category_id + "+"
						+ product_id+"+" +role_name);
			}
		}else{
			if (flag) {
				// Y RedisCluster RedisClusterUtil
				json = (String) RedisClusterUtil.getCacheValueByKey(category_id
						+ "+" + product_id + "+" + processType+ "+" +role_name);
			} else {
				// N single RedisUtil
				json = (String) RedisUtil.getCacheValueByKey(category_id + "+"
						+ product_id + "+" + processType+"+" +role_name);
			}
		}
		
		return json;
	} 

	public void cacheStrByKey(boolean flag, Long categoryId, Long productId,
			String processType,String role_name,Boolean flushIndex) {
	

		if (Constant.PLANT_PROCESS.equals(processType)) {
			String plantKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+" + Constant.PLANT_PROCESS+"+"+role_name;
			String plantJson = indexService.getbranchJson(
					Constant.PLANT_PROCESS, String.valueOf(categoryId),
					String.valueOf(productId),role_name);

			if (flag) {
				RedisClusterUtil.delCacheKey(plantKey);
				RedisClusterUtil.cacheStrByKey(plantKey, plantJson);

			} else {
				RedisUtil.delCacheKey(plantKey);
				RedisUtil.cacheStrByKey(plantKey, plantJson);
			}
		}

		if (Constant.SUPERVISE_PROCESS.equals(processType)) {
			String superviseKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+"
					+ Constant.SUPERVISE_PROCESS+"+"+role_name;
			String superviseJson = indexService.getbranchJson(
					Constant.SUPERVISE_PROCESS, String.valueOf(categoryId),
					String.valueOf(productId),role_name);
			if (flag) {
				RedisClusterUtil.delCacheKey(superviseKey);
				RedisClusterUtil.cacheStrByKey(superviseKey, superviseJson);
			} else {
				RedisUtil.delCacheKey(superviseKey);
				RedisUtil.cacheStrByKey(superviseKey, superviseJson);
			}
		}
		if (Constant.WAREHOUSE_PROCESS.equals(processType)) {
			String wareHouseKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+"
					+ Constant.WAREHOUSE_PROCESS+"+"+role_name;
			String warehouseJson = indexService.getbranchJson(
					Constant.WAREHOUSE_PROCESS, String.valueOf(categoryId),
					String.valueOf(productId),role_name);

			if (flag) {
				RedisClusterUtil.delCacheKey(wareHouseKey);
				RedisClusterUtil.cacheStrByKey(wareHouseKey, warehouseJson);
			} else {
				RedisUtil.delCacheKey(wareHouseKey);
				RedisUtil.cacheStrByKey(wareHouseKey, warehouseJson);
			}

		}
		if (Constant.SALE_PROCESS.equals(processType)) {
			String saleKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+" + Constant.SALE_PROCESS+"+"+role_name;
			String saleJson = indexService.getbranchJson(Constant.SALE_PROCESS,
					String.valueOf(categoryId), String.valueOf(productId),role_name);

			if (flag) {
				RedisClusterUtil.delCacheKey(saleKey);
				RedisClusterUtil.cacheStrByKey(saleKey, saleJson);
			} else {
				RedisUtil.delCacheKey(saleKey);
				RedisUtil.cacheStrByKey(saleKey, saleJson);
			}
		}
		
		//首页 cache 更新
		if(flushIndex){
			String indexKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId)+"+"+role_name;
			String indexJson = indexService.getIndexJson(String.valueOf(categoryId),String.valueOf(productId),role_name);
			if (flag) {
				RedisClusterUtil.delCacheKey(indexKey);
				RedisClusterUtil.cacheStrByKey(indexKey, indexJson);
			} else {
				RedisUtil.delCacheKey(indexKey);
				RedisUtil.cacheStrByKey(indexKey, indexJson);
			}
		}
	}
	
	public void cacheStrByKey(boolean flag, Long categoryId, Long productId,
			String processType,String role_name,Boolean flushIndex,String json) {
		if (Constant.PLANT_PROCESS.equals(processType)) {
			String plantKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+" + Constant.PLANT_PROCESS+"+"+role_name;
			if (flag) {
				RedisClusterUtil.delCacheKey(plantKey);
				RedisClusterUtil.cacheStrByKey(plantKey, json);
			} else {
				RedisUtil.delCacheKey(plantKey);
				RedisUtil.cacheStrByKey(plantKey, json);
			}
		}

		if (Constant.SUPERVISE_PROCESS.equals(processType)) {
			String superviseKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+"
					+ Constant.SUPERVISE_PROCESS+"+"+role_name;
			if (flag) {
				RedisClusterUtil.delCacheKey(superviseKey);
				RedisClusterUtil.cacheStrByKey(superviseKey, json);
			} else {
				RedisUtil.delCacheKey(superviseKey);
				RedisUtil.cacheStrByKey(superviseKey, json);
			}
		}
		if (Constant.WAREHOUSE_PROCESS.equals(processType)) {
			String wareHouseKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+"
					+ Constant.WAREHOUSE_PROCESS+"+"+role_name;
			if (flag) {
				RedisClusterUtil.delCacheKey(wareHouseKey);
				RedisClusterUtil.cacheStrByKey(wareHouseKey, json);
			} else {
				RedisUtil.delCacheKey(wareHouseKey);
				RedisUtil.cacheStrByKey(wareHouseKey, json);
			}

		}
		if (Constant.SALE_PROCESS.equals(processType)) {
			String saleKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId) + "+" + Constant.SALE_PROCESS+"+"+role_name;
			if (flag) {
				RedisClusterUtil.delCacheKey(saleKey);
				RedisClusterUtil.cacheStrByKey(saleKey, json);
			} else {
				RedisUtil.delCacheKey(saleKey);
				RedisUtil.cacheStrByKey(saleKey, json);
			}
		}
		//首页 cache 更新
		if(flushIndex){
			String indexKey = String.valueOf(categoryId) + "+"
					+ String.valueOf(productId)+"+"+role_name;
			if (flag) {
				RedisClusterUtil.delCacheKey(indexKey);
				RedisClusterUtil.cacheStrByKey(indexKey, json);
			} else {
				RedisUtil.delCacheKey(indexKey);
				RedisUtil.cacheStrByKey(indexKey, json);
			}
		}
	}
	
	public void fushCacheByCIdAndPId(long categoryId,long productId){
		this.cacheJson(flag, categoryId, productId,FrontEndRole.GUEST);//缓存三种不同的角色  （G:游客；B：企业用户；C：关注用户）
		this.cacheJson(flag, categoryId, productId,FrontEndRole.BUSINESS);
		this.cacheJson(flag, categoryId, productId,FrontEndRole.CUSTOMER);
	}
	public void cacheJson(boolean flag,Long categoryId,Long productId,String role_name){
		String indexJson=indexService.getIndexJson(String.valueOf(categoryId),String.valueOf(productId),role_name);
		String plantJson=indexService.getbranchJson(Constant.PLANT_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String superviseJson=indexService.getbranchJson(Constant.SUPERVISE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String warehouseJson=indexService.getbranchJson(Constant.WAREHOUSE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String saleJson=indexService.getbranchJson(Constant.SALE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String indexKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+role_name;
		String plantKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.PLANT_PROCESS+"+"+role_name;
		String superviseKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.SUPERVISE_PROCESS+"+"+role_name;
		String wareHouseKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.WAREHOUSE_PROCESS+"+"+role_name;
		String saleKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.SALE_PROCESS+"+"+role_name;
		if(flag){
				//Y
				//System.out.println("---->" + RedisClusterUtil.getJedis().info());
				//缓存前先删除原来的key
				RedisClusterUtil.delCacheKey(indexKey);

				RedisClusterUtil.cacheStrByKey(indexKey, indexJson);
					
				RedisClusterUtil.delCacheKey(plantKey);
				RedisClusterUtil.cacheStrByKey(plantKey, plantJson);
					
				RedisClusterUtil.delCacheKey(superviseKey);
				RedisClusterUtil.cacheStrByKey(superviseKey, superviseJson);
					
				RedisClusterUtil.delCacheKey(wareHouseKey);
				RedisClusterUtil.cacheStrByKey(wareHouseKey, warehouseJson);
					
				RedisClusterUtil.delCacheKey(saleKey);
				RedisClusterUtil.cacheStrByKey(saleKey, saleJson);
			}else {
				//N
				RedisUtil.delCacheKey(indexKey);
				RedisUtil.cacheStrByKey(indexKey, indexJson);
					
				RedisUtil.delCacheKey(plantKey);
				RedisUtil.cacheStrByKey(plantKey, plantJson);
					
				RedisUtil.delCacheKey(superviseKey);
				RedisUtil.cacheStrByKey(superviseKey, superviseJson);
					
				RedisUtil.delCacheKey(wareHouseKey);
				RedisUtil.cacheStrByKey(wareHouseKey, warehouseJson);
					
				RedisUtil.delCacheKey(saleKey);
				RedisUtil.cacheStrByKey(saleKey, saleJson);
			}
	}
}
