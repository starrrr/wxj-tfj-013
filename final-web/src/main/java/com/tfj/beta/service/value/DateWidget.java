package com.tfj.beta.service.value;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.FreemarkerUtil;

public class DateWidget extends Widget {
	
	private final static String DEFAULT_TEMPLATE = "date.ftl";

	public DateWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		String def_val = getDateDefaultVal(node.getDefault_value());
		String val = StringUtils.defaultString(node.getValue(), def_val);
		String inputName = String.format(NAME, String.valueOf(node.getId()), defaultString(node.getPv_id()));
		
//		params.put("anchor_name", node.getName());
		params.put("input_name", inputName);
		params.put("input_value", val);
		
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}
	
	private String getDateDefaultVal(String defVal) {
		@SuppressWarnings("unused")
		String def_val = StringUtils.defaultString(defVal);
		return DateUtils.getCurrentDay();
	}
}
