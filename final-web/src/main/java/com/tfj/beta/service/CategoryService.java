package com.tfj.beta.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.sun.swing.internal.plaf.basic.resources.basic;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.IndexCount;
import com.tfj.beta.model.PlantCate;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.User;
import com.tfj.beta.model.Product;
import com.tfj.beta.utils.PinYinUtil;

/**
 * Created by Administrator on 2015/6/12.
 */
public class CategoryService {

	//@RequiresPermissions(value = { "system:dict" })
	public Page<Category> getCategoryPage(int pageNumber, int pageSize, Category category,List<Unit> units,String admin) {
		return Category.dao.searchPaginate(pageNumber, pageSize, category,units,admin);
	}

	public boolean modifyCategory(final Category category,final String[] unitIds) {
		boolean flag = true;
		Long id = category.getLong(Category.ID);
		if (null == id) {
			Category cate=Category.dao.findCategoryByName(category.NAME);
			if(cate==null){//新增数据  要对t_category和t_unit_cate表操作
				category.put("auditflag", 0);
				category.put("nums", 0);
				Db.tx(new IAtom(){//事物处理
					public boolean run() throws SQLException {
						boolean ifSaveCategory=category.save();
						boolean ifSaveUnitCategory=Category.dao.duleUnitCate(category,unitIds);
						boolean ifIndexCount = IndexCount.dao.updateCountCategory(true);
					return ifSaveCategory&&ifSaveUnitCategory&&ifIndexCount;
				}});
			}else{//对t_unit_cate表操作
				Db.tx(new IAtom(){//事物处理
					public boolean run() throws SQLException {
						boolean ifSaveUnitCategory=Category.dao.duleUnitCate(category,unitIds);
					return ifSaveUnitCategory;
				}});
			}
		} else {
			//category.update();
			Db.tx(new IAtom(){//事物处理
				public boolean run() throws SQLException {
					boolean iSupdate=category.update();
					boolean iSupdateUnitCategory=Category.dao.duleUnitCateUpdate(category,unitIds);
				return iSupdate&&iSupdateUnitCategory;
			}});
		}
		return flag;
	}
	public boolean modifyCategory(Category category) {
		boolean flag = true;
		Long id = category.getLong(Category.ID);
		if (null == id) {
			category.put("auditflag", 0);
			category.put("nums", 0);
			category.save();
		} else {
			category.update();
		}
		return flag;
	}
	
	public boolean deleteCategory(final String[] ids){
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
				boolean isFlag1=Category.dao.deleteByIds(ids);
				boolean isFlag2=Category.dao.deleteUnitCate(ids);
				List<Template> templates=Template.dao.findTemplateByCIds(ids);
				boolean isFlag3=Template.dao.findTemplateByCIds(ids).size()==0?true:Category.dao.deleteTemplate(ids);
				boolean ifIndexCount = IndexCount.dao.updateCountCategory(false);
				return isFlag1&&isFlag2&&isFlag3&&ifIndexCount;
		}});
	}

	/*public List<Category> findAll() {
		return Category.dao.getCategoryList();
	}*/

	public boolean queryAssociated(String id) {
		// TODO Auto-generated method stub
		return Category.dao.queryAssociatedById(id);
	}

	public Category findCategoryById(String categoryId) {
		// TODO Auto-generated method stub
		return Category.dao.findCategoryById(categoryId);
	}
	public Category checkCategoryIsExist(String categoryName) {
		// TODO Auto-generated method stub
		return Category.dao.checkCategoryByCategoryName(categoryName);
	}

	public Long countCategory(){
		return Category.dao.countCategory();
	}
	
	public List<Category> getIndexCategory(){
		List<Category> resultList = new ArrayList();
		List<Category> list = Category.dao.findCategoryNameList();
		for (Category category : list) {
			category.put("PY", PinYinUtil.getPinYinHeadChar(category.getStr(Category.NAME)));
			resultList.add(category);
			
		}
		return resultList;
	}
	
	public boolean updateCategoryNums(Long categoryId){
		return Category.dao.updataNum(categoryId) && Product.dao.updateProductAddNum();
	}

	public List<Category> getCategoryPlantCate(List<PlantCate> plantCates) {
		return Category.dao.CategoryPlantCate(plantCates);
	}
}
