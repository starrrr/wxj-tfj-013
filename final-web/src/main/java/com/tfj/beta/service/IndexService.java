package com.tfj.beta.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.kit.StrKit;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.FieldType;
import com.tfj.beta.model.IndexCount;
import com.tfj.beta.model.IndexTemplate;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.view.JsonBean;
import com.tfj.beta.model.view.ProductBean;
import com.tfj.beta.utils.JsonUtil;
import com.tfj.beta.utils.PropertiesUtil;


public class IndexService {

	private static final IndexTemplate dao = new IndexTemplate();
	private static final ProductValue productValuedao = new ProductValue();
	private static final Product productdao = new Product();
	private static final ProductResult productResultDao=new ProductResult();
	private static final PlantBase plantbasedao = new PlantBase();
	private static final Boolean ISINDEX=true;//标记，是否首页   是：true  否：false
	
	/**
	 * 入口
	 * @param processType
	 * @param categoryId
	 * @param product_id
	 * @param ISINDEX
	 * @param role_name
	 * @return
	 */
	public static List<JsonBean> getJsonBean(String processType, 
			String categoryId, String product_id,Boolean ISINDEX,String role_name) {
		if (StrKit.isBlank(processType)) {
			return null;
		}
		List<JsonBean> beans = new ArrayList<JsonBean>();
		List<Template> templateList = dao.getTemplateListByProcessType(
				processType, categoryId,ISINDEX,role_name);
		
		if (null != templateList && templateList.size() > 0) {
			for (Template template : templateList) {
				constructJSONBean(beans, template, product_id,ISINDEX,role_name);
			}
		}
		return beans;
	}

	// 构造jsonBean
	public static JsonBean constructJSONBean(List<JsonBean> beans,
			Template template, String product_id,Boolean ISINDEX,String role_name) {
		JsonBean bean =null;
		// 获取模板值(模板可能是大图片，可能是值，要做出处理)
		List<ProductValue> productValues = productValuedao.getProductValueByTemplate(
				template, product_id);
		if(productValues.size()!=0){//一个模板可能有多个模板值(不等于0表示为空白模板)
			for(ProductValue productValue:productValues){
				//多值的情况下   需要判断是否为空  空：不再创建jsonbean
				if(productValue.getStr(ProductValue.PRODUCT_VALUE)!=null&&
						!"".equals(productValue.getStr(ProductValue.PRODUCT_VALUE))){
					bean=createBean(beans,template,product_id,ISINDEX,bean,productValue,role_name);
				}else if("图片类型".equals(template.getStr(Template.FIELD_TYPE))){
					bean=createBean(beans,template,product_id,ISINDEX,bean,productValue,role_name);
				}
				//bean=createBean(beans,template,product_id,ISINDEX,bean,productValue,role_name);
			}
		}else {
			bean=createBean(beans,template,product_id,ISINDEX,bean,null,role_name);
		}
		/*屏蔽首页时间
		if(ISINDEX){
			if(template.getInt(dao.LEVELNUM)==1){//首页
				List<Template> templateList = dao.getChildByPidAndIndex(template.getLong(dao.ID));
				if(templateList.size()>0){
					ProductValue productValue=productValuedao.getProductValueByTemplateId(templateList.get(templateList.size()-1).getLong(dao.ID));
					if(productValue!=null){
						//Data date=productValue.get(productValuedao.AUDIT_TIME);
						if(productValue.get(productValuedao.AUDIT_TIME)!=null){
							bean.setValue(new SimpleDateFormat("yyyy-MM-dd").format(productValue.get(productValuedao.AUDIT_TIME)));
						}else {
							bean.setValue("");
						}
						
					}
				}
			}
		}*/
		return bean;
	}
	

	
	//创建bean
	private static JsonBean createBean(List<JsonBean> beans, Template template,
			String product_id, Boolean ISINDEX,JsonBean bean,ProductValue productValue,String role_name) {
		// TODO Auto-generated method stub
		bean= new JsonBean();
		if (productValue != null) {
				/*if (productValue.getStr(productValuedao.PRODUCT_VALUE).startsWith("upload/")) {*/
			if ("图片类型".equals(template.get(Template.FIELD_TYPE))||"文件上传".equals(template.get(Template.FIELD_TYPE))) {
					bean.setImgPath(productValue.getStr(productValuedao.PRODUCT_VALUE)==null?PropertiesUtil.getBase()+"upload/product_value/example.png":PropertiesUtil.getBase()
							+ productValue.getStr(productValuedao.PRODUCT_VALUE));
					bean.setValue("");
				} else {
					bean.setValue(productValue.getStr(productValuedao.PRODUCT_VALUE)==null?"":productValue
							.getStr(productValuedao.PRODUCT_VALUE));
					bean.setImgPath("");
				}
			//混批情况
			if("原料批号".equals(template.get(Template.KEY_NAME))){
				ProductResult p=productResultDao.getProductResultByProductId(product_id);
				//if(StringUtils.isNotBlank(productResultDao.getProductResultByProductId(product_id).getStr(ProductResult.MIX_BATCH_ID))&&productResultDao.getProductResultByProductId(product_id).getStr(ProductResult.MIX_BATCH_ID).indexOf(",")>0){
				if(p!=null&&p.getStr(ProductResult.MIX_BATCH_ID)!=null&&p.getStr(ProductResult.MIX_BATCH_ID).indexOf(",")>0){
					String[] products=productResultDao.getProductResultByProductId(product_id).getStr(ProductResult.MIX_BATCH_ID).split(",");
					StringBuffer sb=new StringBuffer();
					for(int i=0;i<products.length;i++){
						sb.append(productResultDao.getProductResultByProductId(products[i]).getStr(ProductResult.MATERIAL_BATCH_NUM)+",");
						bean.setValue(sb.toString().substring(0,sb.toString().lastIndexOf(",")));
					}
				}
			}
			
		}else{
			bean.setValue("");
			bean.setImgPath("");
		}
		bean.setId(template.getLong(dao.ID));// id
		if(template.getStr(dao.KEY_NAME)!=null){
			if("图片类型".equals(template.get(Template.FIELD_TYPE))||"文件上传".equals(template.get(Template.FIELD_TYPE))){
				bean.setName("image");
			}else {
				bean.setName(template.getStr(dao.KEY_NAME));
			}
		}else {
			bean.setName("");
		}
		
		bean.setLevel(template.getInt("levelNum"));// level
		bean.setProcessType(template.getStr(dao.PROCESSTYPE));// processType
		bean.setSort(Integer.parseInt(template.get(dao.SORT).toString()));
		
		
		bean.setIconPath(template.getStr(dao.ICON) == null ?
				"image".equalsIgnoreCase(template.getStr(dao.KEY_NAME))?"":"暂时不填，默认图标"// 获取图标
			:PropertiesUtil.getBase() + template.getStr(dao.ICON));
			
		bean.setVisable(true);// 判断是否可视
		if(ISINDEX){//首页，存在备注内容
			if(template.getStr(dao.REMARK)!=null){
				bean.setRemark(template.getStr(dao.REMARK));
			}else {
				bean.setRemark("");
			}
		}
		beans.add(bean);
		
		if(ISINDEX){//是首页
			List<Template> templateList = dao.getChildByPidIndex(template
					.getLong(dao.ID),role_name);
			if (null != templateList && templateList.size() > 0) {
				List<JsonBean> childs = new ArrayList<JsonBean>();
				for (Template child : templateList) {
					if (child.getInt(dao.INDEX_SHOW) != null
							&& child.getInt(dao.INDEX_SHOW) == 1) {
						constructJSONBean(childs, child, product_id,ISINDEX,role_name);
					}
				}
				bean.setChild(childs);
			}
		}else {
			List<Template> templateList = dao.getChildByPid(template
					.getLong(dao.ID),role_name,product_id);
			if (null != templateList && templateList.size() > 0) {
				List<JsonBean> childs = new ArrayList<JsonBean>();
				for (Template child : templateList) {
					//判断，如果值为空，模板不显示
					/*if(!"空白".equals(child.getStr(Template.FIELD_TYPE))&&!"下拉菜单".equals(child.getStr(Template.FIELD_TYPE))&&!"图片类型".equals(child.getStr(Template.FIELD_TYPE))){
						ProductValue prv=productValuedao.getProductValueByTemplateId(child.getLong(Template.ID));
						System.err.println("ssssss:"+prv.getStr("product_value"));
						if(!"".equals(prv.getStr("product_value"))){
							constructJSONBean(childs, child, product_id,ISINDEX,role_name);
						}
					}else {
						constructJSONBean(childs, child, product_id,ISINDEX,role_name);
					}*/
					constructJSONBean(childs, child, product_id,ISINDEX,role_name);
				}
				bean.setChild(childs);
				bean.setIconPath(template.getStr(Template.ICON)==null?"":template.getStr(Template.ICON)+";static/admin/index/img/icon_down.png");//如果有下一级节点，模板则有两个图标
				
				//初始化时：下拉菜单的默认值为-1（未选择） 这时前端页面应该为空值
				if(FieldType.SELECT.getName().equals(template.get(Template.FIELD_TYPE))){
					if("-1".equals(productValue.get(ProductValue.PRODUCT_VALUE))){
						bean.setValue("");
						bean.setIconPath("");
					}
				}
			}
		}
		return bean;
	}

	public Product getProduct(String product_id) {
		return productdao.findById(Integer.parseInt(product_id));
	}
	public PlantBase getPlantById(Long plant_id) {
		// TODO Auto-generated method stub
		return plantbasedao.findById(plant_id);
	}
	
	//启动tomcat就开始把数据加载到缓存中
	public String getIndexJson(String category_id,String product_id,String role_name){
			String[] strs={Constant.PLANT_PROCESS,Constant.SUPERVISE_PROCESS,Constant.WAREHOUSE_PROCESS,Constant.SALE_PROCESS}; 
			Map<String,Object> map = new HashMap<String,Object>();
			ProductBean productBean = new ProductBean();//首页头部
			Product product=getProduct(product_id);
			String json=null;
			if(product!=null){
				//主页面头部
				productBean.setProductName(product.getStr(product.PRODUCT_NAME));
				productBean.setPlantType(product.getStr(product.PLANT_TYPE));//类型
				PlantBase plantBase=getPlantById(product.getLong(product.PLANTBASE_ID));
				productBean.setProductLocation(plantBase.getStr(PlantBase.BASE_LOCATION));//地点
				productBean.setYear(product.getStr(product.PRODUCT_YEAR));//年份
				productBean.setProductLevel(product.getStr(product.PRODUCT_LEVEL));
				productBean.setImgPath(plantBase.getStr(PlantBase.BASE_MAP)==null?"":PropertiesUtil.getBase()+plantBase.getStr(PlantBase.BASE_MAP));//图片
				map.put("product", productBean);
				
				for(int i=0;i<strs.length;i++){
					if(strs[i].equals(Constant.PLANT_PROCESS)){
						if(getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).size()>0){
							JsonBean jsonBean=getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).get(0);
							map.put(Constant.PLANT_PROCESS,getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).get(0));
						}else {
							map.put(Constant.PLANT_PROCESS, "");
						}
					}
					if(strs[i].equals(Constant.SUPERVISE_PROCESS)){
						if(getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).size()>0){
							map.put(Constant.SUPERVISE_PROCESS,getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).get(0));
						}else {
							map.put(Constant.SUPERVISE_PROCESS, "");
						}
					}
					if(strs[i].equals(Constant.WAREHOUSE_PROCESS)){
						if(getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).size()>0){
							map.put(Constant.WAREHOUSE_PROCESS,getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).get(0));
						}else {
							map.put(Constant.WAREHOUSE_PROCESS, "");
						}
					}
					if(strs[i].equals(Constant.SALE_PROCESS)){
						if(getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).size()>0){
							map.put(Constant.SALE_PROCESS,getJsonBean(strs[i],category_id,product_id,ISINDEX,role_name).get(0));
						}else {
							map.put(Constant.SALE_PROCESS, "");
						}
					}
				}
				json=JsonUtil.parseObjectToJson(map);
			}
			return json;
		}
	
	public String getbranchJson(String processType,String category_id,String product_id,String role_name){
		List<JsonBean> list =getJsonBean(processType,
				category_id, product_id, false,role_name);
		return JsonUtil.parseObjectToJson(list);
	}
	/**
	 * jxx
	 * @return 所有统计数据
	 */
	public IndexCount findCount(){
		return IndexCount.dao.find();
	}

}
