package com.tfj.beta.service.value;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.Dict;
import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.utils.FreemarkerUtil;

public class SelectWidget extends Widget {
	
	private final static String DEFAULT_TEMPLATE = "select.ftl";

	public SelectWidget(PvBean node) {
		super(node);
	}

	@Override
	public String generateHTML(Map<String, Object> params) {
		String field_develop = node.getField_develop();
		String val = StringUtils.defaultString(node.getValue());
		String selectName = String.format(NAME, String.valueOf(node.getId()), defaultString(node.getPv_id()));
		
		// 字典表数据
		List<Dict> data = null;
		// 下拉菜单数据源
		Map<String, String> ds = new HashMap<String, String>();
		if (StringUtils.isNotBlank(field_develop)) {
			data = Dict.dao.findByType(field_develop);
			if (data != null && !data.isEmpty()) {
				for (Dict dict : data) {
					String dict_name = dict.getStr(Dict.NAME);
					ds.put(dict_name, dict_name);
				}
			}
		}
		
		params.put("data_source", ds);
		params.put("select_name", selectName);
		params.put("select_value", val);
		
		return FreemarkerUtil.getInstantce().fprint(DEFAULT_TEMPLATE, params);
	}

}
