package com.tfj.beta.service;

import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.model.OperationLog;
import com.tfj.beta.model.User;


public class OperationLogService {
	
	/**
	 * 记录用户信息
	 * @param moduleCode 模块编码
	 * @param moduleName 模块名称
	 * @param baseName 操作对象名称
	 * @param opeartionCode 操作类型编码
	 * @param operationName 操作类型
	 * @param userId  操作用户id
	 * @param userName 操作用户名
	 */
	public void recordOperationLog(User loginUser,String moduleCode,String moduleName,String baseName, String opeartionCode,String operationName,
			Long userId,String userName){
		OperationLog.dao.insertDate(loginUser,moduleCode,moduleName,baseName,opeartionCode,operationName,userId,userName);
	}

	public Page<OperationLog> getOperationLogPage(User loginUser,String admin,int pageNumber, int pageSize,
			OperationLog operationLog, String startTime, String endTime) {
		return OperationLog.dao.searchPaginate(loginUser,admin,pageNumber, pageSize, operationLog,startTime,endTime);
	}

	public boolean deleteOperationLog(String[] ids) {
		 //删除 关联信息
        return OperationLog.dao.deleteByIds(ids);
	}
	
	
	public boolean batchDeleteOperationLog(String startTime, String endTime) {
		 return OperationLog.dao.batchDelete(startTime,endTime);
	}
	

}
