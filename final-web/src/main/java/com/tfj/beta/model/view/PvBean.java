package com.tfj.beta.model.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tfj.beta.model.FieldType;
import com.tfj.beta.service.value.Widget;
import com.tfj.beta.utils.HtmlUtils;
import com.tfj.beta.utils.PropertiesUtil;

public class PvBean  implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;

	// 字段
	private String name;
	// 字段对应值
	private String value;
	// 父类id
	private long pid;
	// 环节名称
	private String processType;
	// 是否可见
	private boolean isVisable;
	// 层级
	private int level;
	// 排序
	private Long sort;
	// 子类List
	private List<PvBean> children;
	//
	private String iconPath;

	// 图片路径
	private String imgPath;
	
	//是否展开
	private boolean open;
	
	//是否根节点
	private Boolean isParent;
	
	private String fileType;
	
	private Widget widget;
	
	private Long pv_id;
	
	private Integer audit_flag;
	
	// 其他
	private String field_develop;
	
	// 默认值
	private String default_value;
	
	private Integer pv_sort;
	
	private Long category_id;
	
	private String user_role;
	
	private Integer productAuditFlag;
	
	public final static String ADMIN = "ADMIN";
	
	private final static List<PvBean> FAKE_CHILDREN = new ArrayList<PvBean>();
	
	private final static PvBean EMPTY_BEN = new PvBean();
	
	static {
		FAKE_CHILDREN.add(EMPTY_BEN);
	}
	
	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}
	
	public Long getCategory_id() {
		return category_id;
	}
	
	public Integer getPv_sort() {
		return pv_sort;
	}
	
	public void setPv_sort(Integer pv_sort) {
		this.pv_sort = pv_sort;
	}
	
	private final static Integer ONE = new Integer(1);
	
	public void setField_develop(String field_develop) {
		this.field_develop = field_develop;
	}
	
	public String getField_develop() {
		return field_develop;
	}
	
	public void setDefault_value(String default_value) {
		this.default_value = default_value;
	}
	
	public String getDefault_value() {
		return default_value;
	}
	
	public void setAuditFlag(Integer audit_flag) {
		this.audit_flag = audit_flag;
	}
	
	public Integer getAuditFlag() {
		return audit_flag;
	}
	
	public boolean isAudited() {
		return ONE.equals(audit_flag);
	}
	
	/**
	 * 获取 Product_Value 的 主键 id
	 * @return
	 */
	public Long getPv_id() {
		return pv_id;
	}
	
	/**
	 * 设置 PvBean 的 pv_id 属性, pv_id 为 Product_Value 的 主键 id
	 * @param pv_id
	 */
	public void setPv_id(Long pv_id) {
		this.pv_id = pv_id;
	}

	public String getName() {
		return name;
	}

	public String getDebugName() {
		if (PropertiesUtil.getAppDevMode(false)) {
			return name + "(" + id + "_" + pv_id + ")";
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * template 的 pid
	 * @return
	 */
	public long getPid() {
		return pid;
	}

	/**
	 * 设置 PvBean 的 pid 属性, pid 为 template 的 pid
	 * @param pid
	 */
	public void setPid(long pid) {
		this.pid = pid;
	}

	public String getValue() {
		return StringUtils.defaultString(value);
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public boolean isVisable() {
		return isVisable;
	}

	public void setVisable(boolean isVisable) {
		this.isVisable = isVisable;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public String getLeveCSS() {
		int level = getLevel();
		if (level == 2) {
			return "secondStep";
		} else if (level == 3) {
			return "thirdStep";
		} else if (level == 4) {
			return "fourthStep";
		}
		return StringUtils.EMPTY;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getIconPath() {
		if (StringUtils.isBlank(iconPath)) {
			return HtmlUtils.PIC_PATH + "/ejtb.png";
		}
		return PropertiesUtil.getBase() + iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public List<PvBean> getChildren() {
		return children;
	}

	public void setupFakeChildren() {
		this.children = FAKE_CHILDREN;
	}
	
	public void setChildren(List<PvBean> children) {
		this.children = children;
	}
	
	public Integer getProductAuditFlag() {
		return productAuditFlag;
	}
	
	public void setProductAuditFlag(Integer productAuditFlag) {
		this.productAuditFlag = productAuditFlag;
	}
	
	/**
	 * for setChildrenSort(List<PvBean>) method
	 */
	private final static Comparator<PvBean> SORT = new Comparator<PvBean>() {
		public int compare(PvBean pv1, PvBean pv2) {
			Long sort1 = pv1.getSort();
			Long sort2 = pv2.getSort();
			
			if (sort1.compareTo(sort2) == 0) {
				Long sort_id1 = pv1.getId();
				Long sort_id2 = pv2.getId();
				if (sort_id1.compareTo(sort_id2) == 0) {
					Integer pvSort1 = pv1.getPv_sort() == null ? -1 : pv1.getPv_sort();
					Integer pvSort2 = pv2.getPv_sort() == null ? -1 : pv2.getPv_sort();
					return pvSort1.compareTo(pvSort2);
				} else {
					return sort_id1.compareTo(sort_id2);
				}
			} else {
				return sort1.compareTo(sort2);
			}
		}
	};
	
	public void setChildrenSort(List<PvBean> children) {
		// 根据节点 sort 排序
		Collections.sort(children, SORT);
		this.children = children;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}


	public boolean hasChildren() {
		return children != null && !children.isEmpty();
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public void setFileTypeAndWidget(String fileType) {
		this.fileType = fileType;
		this.widget = FieldType.createNotNullWidget(this);
	}
	
	public Widget getWidget() {
		return widget;
	}
	
	public String getWidgetHTML() {
		if (widget != null) {
			return widget.generateHTML();
		}
		return StringUtils.EMPTY;
	}
	
	public void setIsAdmin(boolean isAdmin) {
		if (isAdmin) {
			this.user_role = ADMIN;
		}
	}
	
	public String getUser_role() {
		return user_role;
	}
	
	private String StrFormat(String template, Object... args) {
		return String.format(template, args);
	}
	
	private String toStepTitleHTML() {
		String contentHTML = StrFormat(HtmlUtils.TOP, getDebugName(), getWidgetHTML());
		return StrFormat(HtmlUtils.STEP_TITLE, contentHTML);
	}
	
	private String formatRightValCSS(String template) {		
		return StrFormat(template, 
				getIconPath(), getDebugName(), getWidgetHTML());
	}
	
	public String generateHTML() {
		StringBuffer html = new StringBuffer();
		if (getLevel() == 0) {
			
			html.append(HtmlUtils.EVERY_STEP_START);
			
			html.append(toStepTitleHTML());
			
			if (hasChildren()) {
				html.append(HtmlUtils.DETAIL_STEP_START);
				
				for (PvBean child : getChildren()) {
					html.append(child.generateHTML());
				}
				
				html.append(HtmlUtils.DETAIL_STEP_END);
			}
			
			html.append(HtmlUtils.EVERY_STEP_END);
		} else {
			if (hasChildren()) {
				
				html.append(HtmlUtils.LI_START);
				
				html.append(formatRightValCSS(HtmlUtils.NODE_HAS_CHILDREN));
				
				StringBuffer childrenHTML = new StringBuffer();
				for (PvBean child : getChildren()) {
					childrenHTML.append(child.generateHTML());
				}
				
				html.append(StrFormat(HtmlUtils.UL_WRAP, getLeveCSS(), childrenHTML.toString()));
				
				html.append(HtmlUtils.LI_END);
			} else {
				String content = formatRightValCSS(HtmlUtils.NODE_NO_CHILDREN);
				
				html.append(StrFormat(HtmlUtils.LI_WRAP, content));
			}
		}
		return html.toString();
	}
}