package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;


@TableBind(tableName = "t_unit_cate", pkName = "id")
public class UnitCate extends Model<UnitCate> {
    public static final UnitCate dao = new UnitCate();

    public static final String ID = "id";
    public static final String UNIT_ID = "unit_id"; // unit_id
    public static final String CATEGORY_ID = "category_id"; // category_id


    /**
     * 根据category_id查找单位
     */
    public List<UnitCate> searchCategoryUnitByCategoryId(long category_id) {
        String sql = "SELECT * FROM t_unit_cate WHERE category_id = ?";
        return find(sql, category_id);
    }
    public List<UnitCate> searchCategoryUnitByCategoryId(String category_id) {
        String sql = "SELECT * FROM t_unit_cate WHERE category_id = ?";
        return find(sql, category_id);
    }

}
