package com.tfj.beta.model;

public enum AuditState {

	UNAUDITED("未审核"), 
	AUDITED("已审核"), 
	FINISHED("已完成");
	
	private String name;
	
	AuditState(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static String getName(int index) {
		AuditState[] states = values();
		for (AuditState state : states) {
			if (index == state.ordinal()) {
				return state.getName();
			}
		}
		throw new ArrayIndexOutOfBoundsException(index);
	}
}
