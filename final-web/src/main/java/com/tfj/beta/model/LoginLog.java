package com.tfj.beta.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/1/7.
 */
@TableBind(tableName = "s_login_log")
public class LoginLog extends Model<LoginLog> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginLog.class);

    public static final LoginLog dao = new LoginLog();

    public static final String ID = "id";
    public static final String USER_ID = "user_id"; // user_id
    public static final String LOGIN_TIME = "login_time"; // 登录时间
    public static final String LOGIN_IP = "login_ip"; // 登录ip
    public static final String LOGOUT_TIME = "logout_time"; // 退出时间

    public void updateLogoutTime(Long userId, String loginTime) {
        try {
            Db.update("update s_login_log set logout_time=? where user_id=? and login_time=?", new Object[]{DateUtils.getCurrentTime(), userId, loginTime});
        } catch (Exception e) {
            LOGGER.error("db 异常:{}", e.getMessage(), e);
        }
    }
}
