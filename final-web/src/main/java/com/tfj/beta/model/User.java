package com.tfj.beta.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * Created by Administrator on 2015/6/9.
 */
@TableBind(tableName = "s_user", pkName = "id")
public class User extends Model<User> {
    public static final User dao = new User();

    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String NICKNAME = "nickname";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String LAST_LOGIN_TIME = "last_login_time";
    public static final String LAST_LOGIN_IP = "last_login_ip";
    public static final String LOGIN_STATUS = "login_status";
    public static final String LOGIN_COUNT = "login_count";
    public static final String UPDATE_TIME = "update_time";
    

    public static final String CREATE_TIME = "create_time";
    public static final String CREATE_USER_ID = "create_user_id";
    public static final String DELETE_STATUS="delete_status";
    
    //性别 默认：0（男） 女：1
    public static final String SEX = "sex";
    //用户类型 默认：0（普通用户） 管理员：1
    public static final String TYPE = "type";
    // 停用 默认:0, 停用:1
    public static final String ENABLE = "enable";

    /**
     * 登录查找指定活跃用户
     */
    public User searchActiveByUserName(String userName) {
        return findFirst("select * from s_user where enable='0' and username= ?", userName);
    }

    /**
     * 根据条件查询用户分页数据
     */
    public Page<User> searchPaginate(int pageNumber, int pageSize, User user) {
        String select = "SELECT * ";
        StringBuffer stringBuffer = new StringBuffer("FROM s_user WHERE 1=1 ");//delete_status = '0'
        // 判断用户名是否为空，不为空添加条件
        if (null != user.getStr(USERNAME) && !"".equals(user.getStr(USERNAME))) {
            stringBuffer.append("AND ").append(USERNAME).append(" like '%").append(user.getStr(USERNAME).trim().equals("'")?"''":user.getStr(USERNAME).trim()).append("%' "); // 用户名向后模糊查找
        }
        // 判断姓名是否为空，不为空添加条件
        if (null != user.getStr(NAME) && !"".equals(user.getStr(NAME))) {
            stringBuffer.append("AND ").append(NAME).append(" like '%").append(user.getStr(NAME).trim().equals("'")?"''":user.getStr(NAME).trim()).append("%' "); // 姓名向后模糊查找
        }
        // 判断性别是否为空，不为空添加条件
        if (null != user.getStr(SEX) && !"".equals(user.getStr(SEX))) {
            stringBuffer.append("AND ").append(SEX).append(" = '").append(user.getStr(SEX)).append("' ");
        }
        // 判断用户类型是否为空，不为空添加条件
        if (null != user.getStr(TYPE) && !"".equals(user.getStr(TYPE))) {
            stringBuffer.append("AND ").append(TYPE).append(" = '").append(user.getStr(TYPE)).append("' ");
        }
        // 判断在线状态是否为空，不为空添加条件
        if (null != user.getStr(LOGIN_STATUS) && !"".equals(user.getStr(LOGIN_STATUS))) {
            stringBuffer.append("AND ").append(LOGIN_STATUS).append(" = '").append(user.getStr(LOGIN_STATUS)).append("' ");
        }
        // 判断启用状态是否为空，不为空添加条件
        if (null != user.getStr(ENABLE) && !"".equals(user.getStr(ENABLE))) {
            stringBuffer.append("AND ").append(ENABLE).append(" = '").append(user.getStr(ENABLE)).append("' ");
        }

        stringBuffer.append("ORDER BY id DESC");
        
        System.err.println(stringBuffer.toString());

        return paginate(pageNumber, pageSize, select, stringBuffer.toString());
    }

    /**
     * 根据ids批量删除用户（实际：更新delete_status = '1'）
     *
     * @param ids
     * @return boolean
     */
    public boolean deleteByIds(String[] ids) {
        StringBuffer stringBuffer = new StringBuffer("UPDATE s_user SET delete_status = '1',enable = '1' WHERE ");
        if (null != ids && 1 == ids.length) {
            stringBuffer.append("id = ?");
            return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false : true;
        } else if (null != ids && ids.length > 1) {
            stringBuffer.append("id in('");
            for (String id : ids) {
                stringBuffer.append(id).append("','");
            }
            stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length()); // 去掉最后一个,和'
            stringBuffer.append(")");
            return 0 == Db.update(stringBuffer.toString()) ? false : true;
        }
        return false;
    }

	public User checkUserByUserName(String userName) {
		// TODO Auto-generated method stub
		return findFirst("select * from s_user where username= ?", userName);
	}
	/**
     * 用户名查找用户
     */
    public User searchUserByUserName(String userName) {
        return findFirst("select * from s_user where username= ?", userName);
    }
}
