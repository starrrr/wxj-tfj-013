package com.tfj.beta.model;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_operation_log", pkName = "id")
public class OperationLog extends Model<OperationLog> {

	public static final OperationLog dao = new OperationLog();
	public static final String ID = "id";
	public static final String MODULE_CODE = "module_code";
	public static final String MODULE_NAME = "module_name";
	public static final String BASE_NAME = "base_name";
	public static final String OPERATION_NAME = "operation_name";
	public static final String OPERATION_CODE = "operation_code";
	public static final String OPERATION_USER_RID = "operation_use_rid";
	public static final String OPERATION_USER = "operation_user";
	public static final String OPERATION_DATETIME = "operation_datetime";

	public Page<OperationLog> searchPaginate(User loginUser,String admin,int pageNumber, int pageSize,
			OperationLog operationLog,String startTime, String endTime) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
				"FROM t_operation_log WHERE 1=1 ");
		if (null != operationLog.getStr(MODULE_NAME) && !"".equals(operationLog.getStr(MODULE_NAME))) {
			stringBuffer.append("AND ").append(MODULE_NAME).append(" like '%")
					.append(operationLog.getStr(MODULE_NAME)).append("%' ");
		}
		if (null != operationLog.getStr(OPERATION_NAME) && !"".equals(operationLog.getStr(OPERATION_NAME))) {
			stringBuffer.append("AND ").append(OPERATION_NAME).append(" like '%")
					.append(operationLog.getStr(OPERATION_NAME).trim()).append("%' ");
		}
	    // 判断开始时间和结束时间都不为空则查找包含时间段，否则不包含时间段查找
        if (null != startTime && !"".equals(startTime) && null != endTime && !"".equals(endTime)) {
        	stringBuffer.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" BETWEEN '").append(startTime).append("' AND '").append(endTime).append("' ");
        }
        // 开始时间为空，结束时间不为空，查询第一条记录 至 输入结束的时间
        if (null == startTime || "".equals(startTime) && null != endTime && !"".equals(endTime)) {
        	stringBuffer.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" <= '").append(endTime).append("' ");
        }
        
        // 开始时间不为空，结束时间为空，查询输入开始的时间 至 最后一条记录
        if (null != startTime || !"".equals(startTime) && null == endTime && "".equals(endTime)) {
        	stringBuffer.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" >= '").append(startTime).append("' ");
        }
        
        if(admin!="true"||!"true".equals(admin)){//不是管理员
			stringBuffer.append(" AND ").append("unit_id")
			.append("=").append(loginUser.getLong("unit_id"));
		}
        
		stringBuffer.append(" order by id desc "); 
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	public void insertDate(User loginUser,String moduleCode, String moduleName,
			String baseName, String opeartionCode, String operationName,
			Long userId,String userName) {
		String insertSql="INSERT INTO t_operation_log("
				+ "module_code,module_name,base_name,operation_name,operation_code,"
				+ "operation_use_rid,operation_user,operation_datetime) VALUES(?,?,?,?,?,?,?,?)";
		Db.update(insertSql, moduleCode, moduleName,baseName,operationName,opeartionCode,userId,userName,new Date());
		
	}

	public boolean deleteByIds(String[] ids) {
		 StringBuffer stringBuffer = new StringBuffer("delete from `t_operation_log` where ");
	        if (null != ids && 1 == ids.length) {
	            stringBuffer.append("id = ?");
	            return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false : true;
	        } else if (null != ids && ids.length > 1) {
	            stringBuffer.append("id in('");
	            for (String id : ids) {
	                stringBuffer.append(id).append("','");
	            }
	            stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length()); // 去掉最后一个,和'
	            stringBuffer.append(")");
	            return 0 == Db.update(stringBuffer.toString()) ? false : true;
	        }
	        return false;
	}

	public boolean batchDelete(String startTime, String endTime) {
		final StringBuffer delete = new StringBuffer("DELETE FROM t_operation_log WHERE 1=1 ");
		StringBuffer select = new StringBuffer("SELECT * FROM t_operation_log WHERE 1=1 ");
		// 判断开始时间和结束时间都不为空则查找包含时间段，否则不包含时间段查找
        if (null != startTime && !"".equals(startTime) && null != endTime && !"".equals(endTime)) {
        	delete.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" BETWEEN '").append(startTime).append("' AND '").append(endTime).append("' ");
        	select.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" BETWEEN '").append(startTime).append("' AND '").append(endTime).append("' ");
        }
        // 开始时间为空，结束时间不为空，查询第一条记录 至 输入结束的时间
        if (null == startTime || "".equals(startTime) && null != endTime && !"".equals(endTime)) {
        	delete.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" <= '").append(endTime).append("' ");
        	select.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" <= '").append(endTime).append("' ");
        }
        // 开始时间不为空，结束时间为空，查询输入开始的时间 至 最后一条记录
        if (null != startTime || !"".equals(startTime) && null == endTime && "".equals(endTime)) {
        	delete.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" >= '").append(startTime).append("' ");
        	select.append("AND ").append(OperationLog.OPERATION_DATETIME).append(" >= '").append(startTime).append("' ");
        }
        final List<OperationLog> operationLogs=this.find(select.toString());
        return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
				return operationLogs.size()==Db.update(delete.toString());
			}
		});
	}
}
