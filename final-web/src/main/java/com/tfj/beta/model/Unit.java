package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;


@TableBind(tableName = "t_unit", pkName = "id")
public class Unit extends Model<Unit> {
    public static final Unit dao = new Unit();
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String CREATETIME = "createtime";
    public static final String REMARK = "remark";
    
    /**
     * 根据条件查询用户分页数据
     */
    public Page<Unit> searchPaginate(int pageNumber, int pageSize, Unit unit) {
        String select = "SELECT * ";
        StringBuffer stringBuffer = new StringBuffer("FROM t_unit WHERE 1=1 ");
        // 判断用户名是否为空，不为空添加条件
        if (null !=unit.getStr(NAME) && !"".equals(unit.getStr(NAME))) {
			stringBuffer.append("AND ").append(NAME).append(" like '%")
					.append(unit.getStr(NAME).trim().equals("'")?"''":unit.getStr(NAME).trim()).append("%' "); // 用户名向后模糊查找
		}
        stringBuffer.append(" order by createtime desc");
        return paginate(pageNumber, pageSize, select, stringBuffer.toString());
    }

	public boolean deleteByIds(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_unit` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}

	public List<Unit> searchUnit() {
		 String sql = "SELECT * FROM t_unit ORDER BY id DESC";
	     return find(sql);
	}
	public List<Unit> searchUnitByRoles(List<RoleUser> roleUsers) {
		 StringBuffer stringBuffer = new StringBuffer("SELECT * FROM t_unit WHERE 1=1");
		 String str="";
			StringBuffer sb=new StringBuffer();
			if(roleUsers.size()>0){
				for(RoleUser roleUser:roleUsers){
					sb.append(roleUser.getInt(RoleUser.ROLE_ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND id in(")
			.append("SELECT unit_id FROM s_role_unit WHERE role_id in(").append(str).append(")")
			.append(")");
	     return find(stringBuffer.toString());
	}
	public Unit findUnitIdById(String unitId) {
		return findById(unitId);
	}

	public List<Unit> searchUnitByCategory(List<UnitCate> unitCates) {
		 StringBuffer stringBuffer = new StringBuffer("SELECT * FROM t_unit WHERE 1=1");
		 String str=null;
		 StringBuffer sb=new StringBuffer();
		 if(unitCates.size()>0){
			for(UnitCate unitCate:unitCates){
				sb.append(unitCate.getLong(UnitCate.UNIT_ID)+",");
			}
			int endIndex=sb.toString().lastIndexOf(",");
			str=sb.toString().substring(0, endIndex);
		}
		stringBuffer.append(" AND id in("+str)
		.append(")");
	    return find(stringBuffer.toString());
	}

	public List<Unit> searchUnitByPlantBaseId(String plant_id) {
		return this.find("SELECT * FROM t_unit WHERE id=(SELECT unit_id FROM t_plantbase WHERE id="+plant_id+")");
	}

	public List<Unit> searchUnitByUser(List<UnitUser> unitUsers) {
		 StringBuffer stringBuffer = new StringBuffer("SELECT * FROM t_unit WHERE 1=1");
		 String str=null;
		 StringBuffer sb=new StringBuffer();
		 if(unitUsers.size()>0){
			for(UnitUser unitUser:unitUsers){
				sb.append(unitUser.getLong(UnitUser.UNIT_ID)+",");
			}
			int endIndex=sb.toString().lastIndexOf(",");
			str=sb.toString().substring(0, endIndex);
		}
		stringBuffer.append(" AND id in("+str)
		.append(")");
	    return find(stringBuffer.toString());
	}

	

}
