package com.tfj.beta.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */
@TableBind(tableName = "s_menuresc", pkName = "id")
public class MenuResc extends Model<MenuResc> {
    public static final MenuResc dao = new MenuResc();

    public static final String PID = "pid";
    public static final String ID = "id";
    public static final String URL = "url";
    public static final String NAME = "name";
    public static final String UPDATE_TIME = "update_time";
    public static final String PERMISSION = "permission";
    public static final String TYPE = "type";

    public MenuResc find(int id){
        String sql = "select *, (select name from s_menuresc p where p.id= c.pid) pname from s_menuresc c WHERE id=?";
        return findFirst(sql, id);
    }

    public List<MenuResc> searchAllEnable() {
        String sql = "SELECT * FROM s_menuresc WHERE enable = '0' ORDER BY sort ASC";
        return find(sql);
    }
    
    public Map<String,String> getAllPermission(){
    	List<MenuResc> menuList =  dao.searchAllEnable();
    	Map<String,String> map = new HashMap<String,String>();
    	if(null!=menuList && menuList.size()>0){
    		for(MenuResc menu : menuList){
    			map.put(menu.getStr(MenuResc.URL), menu.getStr(MenuResc.PERMISSION));
    		}
    	}
    	return map;
    	
    }

    public List<MenuResc> searchAll() {
        String sql = "SELECT * FROM s_menuresc ORDER BY sort ASC";
        return find(sql);
    }
}
