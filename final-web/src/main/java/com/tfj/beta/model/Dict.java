package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/6/12.
 */
@TableBind(tableName = "s_dict", pkName = "id")
public class Dict extends Model<Dict> {
    public static final Dict dao = new Dict();
    public static final String ID = "id";
    public static final String TYPE = "dict_type";
    public static final String CODE = "dict_code";
    public static final String NAME = "dict_name";
    
    /**
     * 根据条件查询用户分页数据
     */
    public Page<Dict> searchPaginate(int pageNumber, int pageSize, Dict dict) {
        String select = "SELECT * ";
        StringBuffer stringBuffer = new StringBuffer("FROM s_dict WHERE 1=1 ");
        // 判断用户名是否为空，不为空添加条件
        if (null != dict.getStr(TYPE) && !"".equals(dict.getStr(TYPE))) {
            stringBuffer.append("AND ").append(TYPE).append(" like '%").append(dict.getStr(TYPE).trim().equals("'")?"''":dict.getStr(TYPE).trim()).append("%' "); // 用户名向后模糊查找
        }
        if (null != dict.getStr(CODE) && !"".equals(dict.getStr(CODE))) {
            stringBuffer.append("AND ").append(CODE).append(" like '%").append(dict.getStr(CODE).trim().equals("'")?"''":dict.getStr(CODE).trim()).append("%' "); // 用户名向后模糊查找
        }
        if (null != dict.getStr(NAME) && !"".equals(dict.getStr(NAME))) {
            stringBuffer.append("AND ").append(NAME).append(" like '%").append(dict.getStr(NAME).trim().equals("'")?"''":dict.getStr(NAME).trim()).append("%' "); // 用户名向后模糊查找
        }
        stringBuffer.append(" order by dict_type ");
        return paginate(pageNumber, pageSize, select, stringBuffer.toString());
    }

    
    /**
     * 根据ids批量删除
     *
     * @param ids
     * @return boolean
     */
    public boolean deleteByIds(String[] ids) {
        StringBuffer stringBuffer = new StringBuffer("delete from `s_dict` where ");
        if (null != ids && 1 == ids.length) {
            stringBuffer.append("id = ?");
            return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false : true;
        } else if (null != ids && ids.length > 1) {
            stringBuffer.append("id in('");
            for (String id : ids) {
                stringBuffer.append(id).append("','");
            }
            stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length()); // 去掉最后一个,和'
            stringBuffer.append(")");
            return 0 == Db.update(stringBuffer.toString()) ? false : true;
        }
        return false;
    }
    
    
	public List<Dict> getDictList() {

		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `s_dict`");
		
		return this.find(stringBuffer.toString());
	}
	
	public List<Dict> findByType(String dict_type) {
		String sql = "select * from s_dict where dict_type = ?";
		return find(sql, dict_type);
	}
	
	public  List<Dict> getDictListByType(String dictType) {

		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `s_dict` where dict_type='"+dictType+"'");
		
		return this.find(stringBuffer.toString());
	}
    
	public  List<Dict> getDictListGroupByNotes() {

		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM s_dict WHERE notes is not null GROUP BY notes ");
		
		return this.find(stringBuffer.toString());
	}


	public Dict findDicByNameAndType(Object object,String name, String type) {
		String sql="";
		if("PlantBase".equals(object.getClass().getSimpleName())){
			sql="SELECT * FROM s_dict WHERE dict_code=? AND dict_type=?";//基地类型要通过dict_code来判断   因为基地存储的是dict_code,产品存储的是name
		}else if("Product".equals(object.getClass().getSimpleName())){
			sql="SELECT * FROM s_dict WHERE dict_name=? AND dict_type=?";
		}
		return this.findFirst(sql,name,type);
	}


	public int insertDictByTypeAndName(Object obj,String name, String type) {
		String dict_code="";
		String remark="";
		if ("PLANTTYPE".equals(type)) {
			dict_code="ZZFS_"+DateUtils.getCurrentTime();
			remark="种植方式";
		}else if("STLX".equals(type)){
			dict_code="STLX_"+DateUtils.getCurrentTime();
			remark="土壤性质";
			PlantBase plantBase=(PlantBase) obj;
			plantBase.set(PlantBase.SOIL, dict_code);
		}
		return Db.update("INSERT INTO s_dict(dict_type,dict_code,dict_name,notes) VALUES(?,?,?,?)", type,dict_code,name,remark);
	}
}
