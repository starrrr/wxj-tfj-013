package com.tfj.beta.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.core.Constant;

/**
 * Created by zlh on 2015/6/12.
 */
@TableBind(tableName = "t_product_result", pkName = "id")
public class ProductResult extends Model<ProductResult> {

	private static final long serialVersionUID = 1L;

	public static final ProductResult dao = new ProductResult();

	public static final String ID = "id";
	public static final String CATEGORY_ID = "category_id";
	public static final String PRODUCT_ID = "product_id";

	// public static final String FLOW_BATCH_FLAG = "flow_batch_flag";

	public static final String PRODUCT_NAME = "product_name";
	// 种子批号
	public static final String SEED_BATCH_NUM = "seed_batch_num";
	// 原料批号
	public static final String MATERIAL_BATCH_NUM = "material_batch_num";
	// 产品批号
	public static final String PRODUCT_BATCH_NUM = "product_batch_num";
	// 流通批号
	public static final String FLOW_BATCH_NUM = "flow_batch_num";
	// 当前录入template
	public static final String CURRENT_TEMP_ID = "current_temp_id";
	// 当前环节
	public static final String PROCESSTYPE = "processType";
	// 混批id
	public static final String MIX_BATCH_ID = "mix_batch_id";

	public static final String TEMPLATE_NAME = "template_name";

	public static final String[] seedBatchType = { "菌种批号", "种子批号" };
	public static final String[] materialBatchType = { "原料批号" };
	public static final String[] productBatchType = { "成品批号" };
	public static final String[] flowBatchType = { "流通批号" };

	/**
	 * 根据条件查询用户分页数据
	 */
	public Page<ProductResult> searchPaginate(String admin,List<Unit> units,int pageNumber, int pageSize,
			ProductResult productResult, String productName,
			String flowBatchFlag,String tag) {
		String select = "SELECT id,category_id, product_id,"
				+ "(select category_name from t_category  where id = r.category_id) as category_name,"
				+ "(select product_name from t_product  where id = r.product_id) as product_name ,"
				+ "seed_batch_num, flow_batch_num,material_batch_num, product_batch_num";
		StringBuffer stringBuffer =null;
		stringBuffer = new StringBuffer(
				" FROM t_product_result r  WHERE 1=1");

		// 判断中草药是否选择
		if (null != productResult.getLong(CATEGORY_ID)
				&& StringUtils.isNotBlank(productResult.getLong(CATEGORY_ID)
						.toString())) {
			stringBuffer.append(" AND category_id = "
					+ productResult.getLong(CATEGORY_ID).toString());
		}
		// 判断产品名称是否录入
		if (StringUtils.isNotBlank(productName)) {
			stringBuffer
					.append(" AND product_id in (select p.id from t_product p where p.product_name LIKE '%"
							+ productName + "%')");
		}
		// 判断 流通批号状态
		if (StringUtils.isNotBlank(flowBatchFlag) && flowBatchFlag.equals("1")) {
			stringBuffer.append(" AND flow_batch_num is not null ");
		} else if (StringUtils.isNotBlank(flowBatchFlag)
				&& flowBatchFlag.equals("0")) {
			stringBuffer.append(" AND flow_batch_num is  null ");
		}
		if(!"true".equals(admin)){
			String str="";//获取所有unitId  组成字符串
			StringBuffer sb=new StringBuffer();
			if(units.size()>0){
				for(Unit unit:units){
					sb.append(unit.getLong(Unit.ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND unit_id in(")
			.append("".equals(str)?"''":str)
			.append(")");
		}
		
		if (StringUtils.isNotBlank(tag)) {
			stringBuffer.append(" order by " + tag);
		} else {
			stringBuffer.append(" order by id desc");
		}
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	/**
	 * 根据productId获取产品结果
	 * 
	 * @param productId
	 * @return
	 */
	public ProductResult getProductResultByProductId(String productId) {
		String sql = "select * from t_product_result where product_id = "
				+ productId;
		ProductResult productResult = this.findFirst(sql);
		return productResult;
	}

	/**
	 * 根据productId获取产品结果
	 * 
	 * @param productId
	 * @return
	 */
	public ProductResult getModelByProductId(String productId) {
		String sql = "select pr.id, pr.processType, pr.current_temp_id, t.key_name template_name "
				+ "from t_product_result pr left join t_template t on pr.current_temp_id = t.id "
				+ "where product_id = ?";
		return findFirst(sql, productId);
	}

	public boolean modifyProductResult(String admin,Long product_id,User user,List<ProductValue> pvList,
			String processType) {

		if (null != pvList && pvList.size() > 0) {
			try {
				for (ProductValue pv : pvList) {
					Long productId = pv.getLong("product_id");
					// 需要先判断结果表是否新增还是修改
					ProductResult result = getProductResultByProductId(String
							.valueOf(productId));
					constructAndSaveProductResult(admin,product_id,user,result, pv, processType);

				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		return true;
	}

	public ProductResult constructAndSaveProductResult(String admin,Long product_id,User user,ProductResult result,
			ProductValue pv, String processType) {
		if (null == result) {
			result = new ProductResult();
		}
		result.set("product_id", pv.getLong("product_id"));
		result.set("category_id", pv.getLong("category_id"));
		result.set("processType", processType);
		result.set("unit_id",Product.dao.findById(product_id).getLong("unit_id"));
		/*if(admin=="true"){
			result.set("unit_id",Product.dao.findById(product_id).getLong("unit_id"));
		}else{
			result.set("unit_id", user.getLong("unit_id"));//单位
		}*/
		// Long templateId = pv.getLong("template_id");
		String templateName = pv.getStr("key_name");
		String productValue = pv.getStr("product_value");

		// 种植环节
		if (processType.equals(Constant.PLANT_PROCESS)) {
			for (String name : seedBatchType) {
				if (name.equals(templateName)) { // 种子批号
					result.set("seed_batch_num", productValue);
				}
			}
			for (String name : materialBatchType) {
				if (name.equals(templateName)) { // 原料批号
					result.set("material_batch_num", productValue);
				}
			}
		}
		// 加工環節
		if (processType.equals(Constant.SUPERVISE_PROCESS)) {

			for (String name : materialBatchType) {
				if (name.equals(templateName)) { // 原料批号
				
					
					// 根据 原料批号找到对应的混批id，并持久化到数据库
					if(StringUtils.isNotBlank(productValue)&& productValue.indexOf(",")>0){
						StringBuffer maxBatch = new StringBuffer("");
						String[] materalNums = productValue.split(",");
						boolean flag = false;
						for(String batchNum :materalNums){
							//根据 原料批号 获取id 
							ProductResult productResult = this.findDataByMaterialNum(batchNum);
							if(null!=productResult && null!=productResult.getLong("product_id")){
								
								if(flag){
									maxBatch.append(",");
								}
								maxBatch.append(String.valueOf(productResult.getLong("product_id")));
								flag = true;
							}
						}
						//写到混批id
						result.set("mix_batch_id", maxBatch.toString());
						
					}
					
					
				}
			}
			
			for (String name : productBatchType) {
				if (name.equals(templateName)) { // 成品批号
					result.set("product_batch_num", productValue);
				}
			}

		}
		// 流通仓储
		if (processType.equals(Constant.WAREHOUSE_PROCESS)) {
			for (String name : flowBatchType){
				if (name.equals(templateName)) { // 流通批号
					result.set("flow_batch_num", productValue);
				}
			}
		}
		if (null == result.getLong("id")) {
			result.save();
		} else {
			result.update();
		}

		return result;
	}

	public ProductResult findDataByMaterialNum(String materialNum) {
		StringBuffer stringBuffer = new StringBuffer(
				"SELECT * FROM `t_product_result` where material_batch_num ='"
						+ materialNum.trim() + "'");
		return this.findFirst(stringBuffer.toString());
	}
	
	public ProductResult findDataByflownum(String flownum) {
		StringBuffer stringBuffer = new StringBuffer(
				"SELECT * FROM `t_product_result` where flow_batch_num ='"
						+ flownum.trim() + "'");
		return this.findFirst(stringBuffer.toString());
	}
	
	/**
	 * 满足条件的打印二维码的List
	 * @return
	 */
	public List<ProductResult> findQRCodeList() {
		String sql = "SELECT * FROM `t_product_result` where flow_batch_num  is not null";
		List<ProductResult> list = this.find(sql);
		return list;
	}

}
