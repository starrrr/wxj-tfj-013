package com.tfj.beta.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.tfj.beta.model.view.JsonBean;
import com.tfj.beta.model.view.TreeBean;

@TableBind(tableName = "t_template", pkName = "id")
public class Template extends Model<Template> {

	private static final long serialVersionUID = -8494059756575178814L;

	public static final Template dao = new Template();
	public static final String ID = "id";
	public static final String CATEGORY_ID = "category_id";
	public static final String KEY_NAME = "key_name";
	public static final String SORT = "sort";
	public static final String PID = "pid";
	public static final String PROCESSTYPE = "processType";
	public static final String LEVELNUM = "levelNum";
	public static final String REMARK = "remark";
	public static final String ICON = "icon";
	// 字段属性 　img 代表 icon 或图片 video 代表媒体文件默认text

	public static final String FIELD_TYPE = "field_type";
	public static final String FIELD_DEVELOP = "field_develop";
	public static final String INDEX_SHOW = "index_show";
	public static final String PROCESS_SHOW = "process_show";
	public static final String DEFAULT_VALUE = "default_value";


	public void constructTreeBean(List<TreeBean> beans, Template template) {

		TreeBean bean = new TreeBean();
		bean.setId(template.getLong(ID));
		bean.setSort(template.getLong(SORT));
		bean.setName(template.getStr(KEY_NAME));
		bean.setLevelNum(template.getInt(LEVELNUM));
		bean.setPId(template.getLong(PID));
		bean.setProcessType(template.getStr(PROCESSTYPE));
		bean.setIconPath(template.getStr(ICON));
		bean.setFieldType(template.getStr(FIELD_TYPE));
		bean.setFieldDevelop(template.getStr(FIELD_DEVELOP));
		bean.setProcessShow(template.getStr(PROCESS_SHOW));

		if(null!= template.getInt(INDEX_SHOW)&&template.getInt(INDEX_SHOW)==1){
			bean.setIndexShow(true);
		}else{
			bean.setIndexShow(false);
		}
		//bean.setIndexShow(template.getInt(INDEX_SHOW)==0?false:true);
		bean.setDefaultValue(template.getStr(DEFAULT_VALUE));
		bean.setOpen(Boolean.TRUE);
		bean.setNotes(template.getStr(REMARK));
	

		beans.add(bean);
		List<Template> templateList = getChildByPid(template.getLong(ID));

		if (null != templateList && templateList.size() > 0) {
			bean.setIsParent(true);

			for (Template child : templateList) {
				constructTreeBean(beans, child);
			}

		}

	}

	// 构造BaseBean

	public JsonBean constructJSONBean(Template template) {
		JsonBean bean = new JsonBean();
		bean.setId(template.getLong(ID));
		bean.setName(template.getStr(KEY_NAME));
		bean.setLevel(template.getInt(LEVELNUM));
		bean.setPid(template.getLong(PID));
		bean.setProcessType(template.getStr(PROCESSTYPE));
		bean.setIconPath(template.getStr(ICON));

		List<Template> templateList = getChildByPid(template.getLong(ID));

		if (null != templateList && templateList.size() > 0) {
			List<JsonBean> childs = new ArrayList();
			for (Template child : templateList) {
				constructJSONBean(child);
			}
			bean.setChild(childs);
		}

		return bean;
	}

	public List<Template> getTemplateListByProcessType(String processType,
			String categoryId) {
		StringBuffer sql = new StringBuffer(
				"select * FROM t_template WHERE processType ='" + processType
						+ "'AND category_id=" + categoryId + " AND pid = 0 order by sort ASC");
		List<Template> templateList = dao.find(sql.toString());
		return templateList;
	}

	// 根据pid获取child

	public List<Template> getChildByPid(long pid) {
		StringBuffer sql = new StringBuffer(
				"select * FROM t_template WHERE pid ='" + pid + "' order by sort ASC");
		List<Template> templateList = dao.find(sql.toString());
		return templateList;
	}
	
	public List<Template> findTemplateByCtId(Long category_id) {
		String sql="SELECT * from t_template WHERE category_id=?";
		return find(sql,category_id);
	}
	public static void main(String[] args) {
		// List<BaseBean> beanList = dao.getTreeBean(Constant.PLANT_PROCESS);

		// System.out.println(beanList);


	}

	public List<Template> findByCidAndPProcess(Long category_id,
			String processType) {
		String sql="SELECT * from t_template WHERE category_id=? And processType=?";
		return find(sql,category_id,processType);
	}

	public boolean insertTemplate(Template template) {
		String insertSql="INSERT INTO t_template(category_id,key_name,sort,pid,processType,levelNum,remark,icon,"
				+ "field_type,field_develop,process_show,index_show,default_value) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int i=Db.update(insertSql,template.getLong(Template.CATEGORY_ID),template.getStr(Template.KEY_NAME),template.getInt(Template.SORT),
				template.getLong(Template.PID),template.getStr(Template.PROCESSTYPE),template.getLong(Template.LEVELNUM),
				template.getStr(Template.REMARK),template.getStr(Template.ICON),template.getStr(Template.FIELD_TYPE),
				template.getStr(Template.FIELD_DEVELOP),template.getStr(Template.PROCESS_SHOW),template.get(INDEX_SHOW)?1:0,
				template.getStr(Template.DEFAULT_VALUE));
		return i>0?true:false;
	}

	public void doInsertFilter(Product product,String fileType,Template template) {
		// TODO Auto-generated method stub
		long productId=product.getLong(Product.ID);
		if("1".equals(product.get(Product.REMARK))){//判断是否已经初始化  1：已经初始化
			if(!FieldType.BLANK.getName().equals(fileType)&&template.getLong(Template.LEVELNUM)>0){//排除不存在值得模板
				String value="";
				String sql="";
				if(FieldType.IMG.getName().equals(fileType)){//判断类型
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else if(FieldType.DATE_RANGE.getName().equals(fileType)){//昨天 至 今天
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else if(FieldType.TEXT.getName().equals(fileType)){
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else if(FieldType.DATE.getName().equals(fileType)){
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else if(FieldType.FILE.getName().equals(fileType)){
					//value="upload/product_value/example.png";
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else if(FieldType.SELECT.getName().equals(fileType)){
					value="-1";
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}else {//其它或没配置的  设为空
					sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
				}
				Db.update(sql,productId,template.getLong(Template.ID),0);
			}
		}
	}
	
	public List<Template> findChilByPid(long pid){
		return this.find("SELECT * FROM t_template WHERE pid=? ORDER BY sort",pid);
	}

	public void updateSort(int sort,Template template) {
		Db.update("UPDATE t_template SET sort=? WHERE id=?",sort,template.getLong(Template.ID));
	}
	public List<Template> findTemplateByCIds(String[] category_id) {
		String sql="SELECT * FROM t_template WHERE category_id in("+category_id[0]+")";
		return find(sql);
	}
}
