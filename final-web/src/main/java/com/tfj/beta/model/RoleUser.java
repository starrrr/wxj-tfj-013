package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */
@TableBind(tableName = "s_role_user", pkName = "id")
public class RoleUser extends Model<RoleUser> {
    public static final RoleUser dao = new RoleUser();

    public static final String ID = "id";
    public static final String ROLE_ID = "role_id"; // role_id
    public static final String USER_ID = "user_id"; // user_id

    /**
     * 根据userid查找用户的角色
     */
    public List<RoleUser> searchRoleUserByUserId(long userId) {
        String sql = "SELECT * FROM s_role_user WHERE user_id = ?";
        return find(sql, userId);
    }

    /**
     * 根据userid查找有效用户的角色
     */
    public List<RoleUser> searchEnableRoleUserByUserId(long userId) {
        String sql = "SELECT * FROM s_role_user WHERE  user_id = ?";
        return find(sql, userId);
    }

    /**
     * 根据userid来删除role_user
     */
    public boolean deleteByUserId(Long userId) {
        String sql = "DELETE FROM s_role_user WHERE user_id = ?";
        return 0 == Db.update(sql, userId) ? true : false;
    }

	public List<RoleUser> searchRoleByUserID(Long userId) {
        String sql = "SELECT * FROM s_role_user WHERE user_id = ?";
        return find(sql, userId);
    }

	public List<RoleUser> searchAdmin() {
		String sql="SELECT * FROM s_role_user WHERE role_id=(SELECT id FROM s_role WHERE name='管理员')"; 
		return this.find(sql);
	}
}
