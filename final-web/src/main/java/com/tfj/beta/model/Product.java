package com.tfj.beta.model;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_product", pkName = "id")
public class Product extends Model<Product> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9082359035209547829L;
	
	public static final Product dao = new Product();
	public static final ProductValue productValueDao = new ProductValue();
	public static final ProductResult productResultDao = new ProductResult();
	public static final String ID = "id";
	public static final String PRODUCT_NAME = "product_name";
	public static final String CATEGORY_ID = "category_id";
	public static final String CATEGORY_NAME = "category_name";
	public static final String PLANT_TYPE = "plant_type";
	public static final String PRODUCT_LEVEL = "product_level";
	public static final String PRODUCT_YEAR = "product_year";
	public static final String PLANTBASE_ID = "plantbase_id";
	public static final String PLANTBASE_NAME = "plantbase_name";
	public static final String OPERATOR = "operator";
	public static final String AUDITFLAG = "auditflag";
	public static final String AUDITORID = "auditorid";
	public static final String AUDITOR = "auditor";
	public static final String AUDIT_TIME = "audit_time";
	public static final String REMARK="remark";
	public static final String DICTYPE="PLANTTYPE";//数字字典种植方式
	public static final String DICTYPE_DJ="Product_Rank";//数字字典等级方式
	
	public String getAuditflagName() {
		Integer auditFlag = getInt(AUDITFLAG);
		if (auditFlag != null) {
			return AuditState.getName(auditFlag);
		}
		return AuditState.UNAUDITED.getName();
	}
	
	public List<Product> getProductList() {

		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `t_product` where auditflag = 1");
		
		return this.find(stringBuffer.toString());
	}
	
	public Page<Product> searchPaginate(int pageNumber, int pageSize,
			Product product, String orderby,List<Unit> units,String admin) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
				"FROM t_product WHERE 1=1 ");
		if (null != product.getStr(PRODUCT_NAME) && !"".equals(product.getStr(PRODUCT_NAME))) {
			stringBuffer.append("AND ").append(PRODUCT_NAME).append(" like '%")
					.append(product.getStr(PRODUCT_NAME).trim().equals("'")?"''":product.getStr(PRODUCT_NAME).trim()).append("%' "); // 用户名向后模糊查找
		}

		if (null != product.getLong(CATEGORY_ID) && -1 != product.getLong(CATEGORY_ID)) {
			stringBuffer.append(" AND ").append(CATEGORY_ID).append("=").append(product.getLong(CATEGORY_ID));
		}

		
		if (null != product.getInt(AUDITFLAG) && -1 != product.getInt(AUDITFLAG)) {
			stringBuffer.append(" AND ").append(AUDITFLAG).append("=").append(product.getInt(AUDITFLAG)); 
		}
		if(!"true".equals(admin)){
			String str=null;//获取所有unitId  组成字符串
			StringBuffer sb=new StringBuffer();
			if(units.size()>0){
				for(Unit unit:units){
					sb.append(unit.getLong(Unit.ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND unit_id in(")
			.append(str)
			.append(")");
		}
		
		if (StringUtils.isNotBlank(orderby)) {
			stringBuffer.append(" order by " + orderby);
		} else {
			stringBuffer.append(" order by id desc");
		}
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}
	
	public boolean deleteByIds(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_product` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}
	
	public boolean queryAssociatedById(String id) {
		boolean isFlag=false;
		Product product=this.findFirst("SELECT * FROM t_product WHERE id="+id);
		if(product.getInt(Product.AUDITFLAG)!=null&&product.getInt(Product.AUDITFLAG)!=0){//值为未审核时，不管是否有关联，都删除（ ）
			String sql="SELECT * from t_product,t_product_result where t_product_result.product_id="+id+" LIMIT 1";
			if(this.findFirst(sql)==null){
				sql="SELECT * from t_product,t_product_value where t_product_value.product_id="+id+" LIMIT 1";
			}
			if(this.findFirst(sql)!=null){
				isFlag=true;
			}
			//return this.findFirst(sql)==null?false:true;
		}
		return isFlag;
	}

	/**
	 * 强制删除
	 * @param id
	 * @return
	 * @throws SQLException 
	 */
	public boolean mandatoryDeleteByIds(final String id){
		//Product product = this.findById(id);
		final String tpsql="DELETE from t_product WHERE id=?";
		final String pvsql="DELETE from t_product_result WHERE product_id=?";
		final String prql="DELETE from t_product_value WHERE product_id=?";
		//final String casql="UPDATE t_category SET nums=nums-1 where id="+product.getLong(Product.CATEGORY_ID);
		//final String icsql="UPDATE t_index_count SET count_categoryrecord=count_categoryrecord-1";
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
			int tp=Db.update(tpsql,id);	
			int tv=Db.update(pvsql,id);
			int pr=Db.update(prql,id);	
			//int ca=Db.update(casql);
			//int ic=Db.update(icsql);
			return (/*ic > 0 && ca > 0 && */tp > 0) || tv > 0 || pr > 0;
		}});
	}
	public void finishInit(final Long product_id) {
		Db.update("UPDATE t_product SET remark='1' where id="+product_id);
	}

	public boolean releaseAudit(final Product product) {
		int all=0;
		List<ProductValue> pvs=productValueDao.getProductValuesByProductId(product.getLong(Product.ID));
		
		for(ProductValue pv:pvs){//统计插入条数
			all=all+1;
		}
		final int result=all;
		final String prsql="UPDATE t_product SET auditflag=0  WHERE id=?";
		final String pvql="UPDATE t_product_value SET auditflag=0 WHERE product_id=?";
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
			int pr=Db.update(prsql,product.getLong(Product.ID));
			int pv=Db.update(pvql,product.getLong(Product.ID));
			return pr > 0 && pv == result;
		}});
	}
	
	/**
	 * 产品删除
	 * @param product
	 * @return
	 */
	public boolean deleteProduct(final Product product) {
		int pvall=0;//产品值条数初始化为0
		int psall=0;//产品二维码表初始化为0
		List<ProductValue> pvs=productValueDao.getProductValuesByProductId(product.getLong(Product.ID));
		ProductResult productResult=productResultDao.getProductResultByProductId(String.valueOf(product.getLong(Product.ID)));
		
		for(ProductValue pv:pvs){//统计插入条数
			pvall=pvall+1;
		}
		if(productResult!=null){
			psall=1;
		}
		final int pvresult=pvall;//产品值总条数
		final int psresult=psall;//产品值总条数
		final String pvsql="DELETE FROM t_product_value WHERE product_id=?";
		final String prql="DELETE FROM t_product WHERE id=?";
		final String psql="DELETE FROM t_product_result WHERE product_id=?";
		//更新种类的产品数量
		final String casql="UPDATE t_category SET nums=nums-1 where id="+product.getLong(Product.CATEGORY_ID);
		final String icsql="UPDATE t_index_count SET count_categoryrecord=count_categoryrecord-1";
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
			int pv=Db.update(pvsql,product.getLong(Product.ID));
			int pr=Db.update(prql,product.getLong(Product.ID));	
			int ps=Db.update(psql,product.getLong(Product.ID));	
			int ca=Db.update(casql);
			int ic=Db.update(icsql);
			return pr > 0 && pv == pvresult && ps==psresult&&ca>0 &&ic > 0;
		}});
	}
	/**
	 * 根据种类ID查找所以的产品
	 * @param categoryId
	 * @return
	 */
	public List<Product> getProductsByCategoryId(String categoryId){
		return this.find("SELECT * FROM t_product WHERE category_id="+categoryId);
	}
	
	public Boolean updateProductAddNum(){
		String sql = "UPDATE t_index_count SET count_categoryrecord=count_categoryrecord+1";
		return 0==Db.update(sql)?false:true;
	}
	
}
