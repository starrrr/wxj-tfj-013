package com.tfj.beta.model.view;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;

@TableBind(tableName = "t_template", pkName = "id")
public class ProductTreeNode extends Model<ProductTreeNode> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8132989259069126943L;
	
	public static final ProductTreeNode dao = new ProductTreeNode();
	
	public static final String SORT = "sort";
	public static final String REMARK = "remark";
	// 字段属性 　img 代表 icon 或图片 video 代表媒体文件默认text
	public static final String INDEX_SHOW = "index_show";
	public static final String CATEGORY_ID = "category_id";
	// t_product_value
	public static final String PV_ID = "pv_id";
	public static final String AUDITFLAG = "auditflag";
	public static final String PV_SORT = "pv_sort";
	
	
	
	public List<ProductTreeNode> getListByProcessType(Long product_id, 
			String processType, Long category_id) {
		String sql = 
				"SELECT t.id, t.category_id, t.key_name, t.sort, t.pid, t.processType, t.levelNum, " +
				"t.remark, t.icon, t.field_type, t.field_develop, t.default_value, t.index_show, " +
				"pv.id pv_id, pv.product_id, pv.template_id, pv.product_value, pv.auditflag, pv.auditorid, " +
				"pv.auditor, pv.audit_time, pv.sort pv_sort " +  
				"FROM t_template t LEFT JOIN t_product_value pv ON t.id = pv.template_id AND pv.product_id = ? " + 
			    "WHERE t.processType = ? AND t.category_id = ?";
		return find(sql, product_id, processType, category_id);
	}
	
	public ProductTreeNode findById(Long pv_id) {
		String sql = 
				"SELECT t.id, t.category_id, t.key_name, t.sort, t.pid, t.processType, t.levelNum, " +
				"t.remark, t.icon, t.field_type, t.field_develop, t.default_value, t.index_show, " +
				"pv.id pv_id, pv.product_id, pv.template_id, pv.product_value, pv.auditflag, pv.auditorid, " +
				"pv.auditor, pv.audit_time, pv.sort pv_sort " +  
				"FROM t_template t LEFT JOIN t_product_value pv ON t.id = pv.template_id " + 
			    "WHERE pv.id = ?";
		return findFirst(sql, pv_id);
	}
	
	/**
	 * 查询子节点
	 * @param pid
	 * @return
	 */
	public List<ProductTreeNode> findChildrenByPid(Long pid) {
		String sql = 
				"SELECT t.id, t.category_id, t.key_name, t.sort, t.pid, t.processType, t.levelNum, " +
				"t.remark, t.icon, t.field_type, t.field_develop, t.default_value, t.index_show, " +
				"pv.id pv_id, pv.product_id, pv.template_id, pv.product_value, pv.auditflag, pv.auditorid, " +
				"pv.auditor, pv.audit_time, pv.sort pv_sort " +  
				"FROM t_template t LEFT JOIN t_product_value pv ON t.id = pv.template_id " + 
			    "WHERE t.pid = ?";
		return find(sql, pid);
	}
	
}
