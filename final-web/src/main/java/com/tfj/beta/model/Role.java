package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */
@TableBind(tableName = "s_role", pkName = "id")
public class Role extends Model<Role> {
    public static final Role dao = new Role();
    public static final String ID = "id";
    public static final String UPDATE_TIME = "update_time";
    public static final String NAME = "name";

    public List<Role> searchAll() {
        String sql = "SELECT * FROM s_role ORDER BY sort ASC, id DESC";
        return find(sql);
    }

    /**
     * 根据ids批量删除用户（实际：更新delete_status = '1'）
     *
     * @param ids
     * @return boolean
     */
    public boolean deleteByIds(String[] ids) {
        StringBuffer stringBuffer = new StringBuffer("delete from `s_role` where ");
        if (null != ids && 1 == ids.length) {
            stringBuffer.append("id = ?");
            return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false : true;
        } else if (null != ids && ids.length > 1) {
            stringBuffer.append("id in('");
            for (String id : ids) {
                stringBuffer.append(id).append("','");
            }
            stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length()); // 去掉最后一个,和'
            stringBuffer.append(")");
            return 0 == Db.update(stringBuffer.toString()) ? false : true;
        }
        return false;
    }

	public Role findRoleIdByUserId(Long id) {
		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `s_role_user` where user_id = "+id);
		return this.findFirst((stringBuffer.toString()));
	}

}
