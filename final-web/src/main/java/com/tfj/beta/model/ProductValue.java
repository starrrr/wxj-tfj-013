package com.tfj.beta.model;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.tfj.beta.core.Constant;
import com.tfj.beta.utils.DateUtils;

@TableBind(tableName = "t_product_value", pkName = "id")
public class ProductValue extends Model<ProductValue> {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductValue.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 4240702253532584405L;

	public static final ProductValue dao = new ProductValue();
	
	public static final String ID = "id";
	public static final String PRODUCT_ID = "product_id";
	public static final String TEMPLATE_ID = "template_id";
	public static final String CATEGORY_ID = "category_id";
	public static final String KEY_NAME = "key_name";
	public static final String PRODUCT_VALUE = "product_value";
	public static final String AUDITFLAG = "auditflag";
	public static final String AUDITORID = "auditorid";
	public static final String AUDITOR = "auditor";
	public static final String AUDIT_TIME = "audit_time";
	public static final String RECORD_ID = "record_id";
	public static final String RECORD_NAME = "record_name";
	public static final String RECORD_TIME = "record_time";
	public static final String SORT = "sort";
	
	public List<ProductValue> getProductValueListByFieldType(int product_id, int template_id) {

		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `t_product` where auditflag = 1");
		
		return this.find(stringBuffer.toString());
	}
	
	/**
	 * 根据模板和产品Id 获取模板值
	 * @param template
	 * @param product_id
	 * @return 
	 */
	public List<ProductValue> getProductValueByTemplate(Template template, String product_id) {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer(
				"select * FROM t_product_value WHERE product_id ='" + product_id
						+ "' AND template_id=" + template.getLong(ID)+" order by sort" +"");
		return dao.find(sql.toString());
	}
	
	public ProductValue getProductValueByTemplateId(long template_id){
		StringBuffer sql = new StringBuffer(
				"select * FROM t_product_value WHERE template_id ='" + template_id+"'");
		
		return dao.findFirst(sql.toString());
	}
	
	
	public ProductValue getProductValueByTemplateId(long template_id,String product_id){
		StringBuffer sql = new StringBuffer(
				"select * FROM t_product_value WHERE product_id="+product_id+" AND template_id ='" + template_id+"'");
		return dao.findFirst(sql.toString());
	}
	
	public int updateAuditFlag(User user, Long product_id,Long category_id,String processType) {
		List<Template> templates=Template.dao.findByCidAndPProcess(category_id,processType);//查找模板
		String ids="";
		for(Template template:templates){
			ids=ids+String.valueOf(template.getLong(Template.ID))+",";
		}
		ids="("+ids.substring(0,ids.lastIndexOf(","))+")";
		Long user_id = user.getLong(User.ID);
		String username = user.getStr(User.USERNAME);
		String sql ="UPDATE t_product_value SET auditflag = 1, auditorid = ?, auditor = ?, audit_time = ? WHERE product_value is not null AND product_value<>'' AND  product_id=? and template_id in "+ids;
		System.err.println(sql);
		return Db.update(sql, user_id, username, DateUtils.getCurrentTime(), product_id);
	}
	
	public int updateProductAuditFlag(User user, Long product_id) {
		Long user_id = user.getLong(User.ID);
		String username = user.getStr(User.USERNAME);
		String sql = "UPDATE t_product_value SET auditflag = 1, auditorid = ?, auditor = ?, audit_time = ? WHERE product_id = ?";
		return Db.update(sql, user_id, username, DateUtils.getCurrentTime(), product_id);
	}
	
	/**
	 * 获取某个产品在某个环节下的特定几个批号
	 * 
	 * @param product_id
	 * @param processType
	 * @return
	 */
	public List<ProductValue> getResultDataSource(Long product_id,
			String processType) {
		
		StringBuffer sql = new StringBuffer(
				"SELECT pv.product_id, t.category_id,  t.key_name, pv.product_value FROM t_product_value pv LEFT JOIN t_template t ON pv.template_id = t.id "
						+ "WHERE pv.product_id = "+product_id+" AND t.processType = '"+processType+"' ");
		System.err.println(sql);
		// 种植环节
		if (processType.equals(Constant.PLANT_PROCESS)) {
			boolean flag = false;
			sql.append("AND t.key_name in (");
			for (String templateName : ProductResult.seedBatchType) {
				if (flag) {
					sql.append(",");
				}
				sql.append("'" + templateName + "'");
				flag = true;
			}
			
			for (String templateName : ProductResult.materialBatchType) {
				if (flag) {
					sql.append(",");
				}
				sql.append("'" + templateName + "'");
				flag = true;
			}

			sql.append(")");
		}
		// 加工環節
		if (processType.equals(Constant.SUPERVISE_PROCESS)) {  //成品批号
			boolean flag = false;
			sql.append("AND t.key_name in (");
			for (String templateName : ProductResult.productBatchType) {
				if (flag) {
					sql.append(",");
				}
				sql.append("'" + templateName + "'");
				flag = true;
			}
			sql.append(")");

		}
		// 流通仓储
		if (processType.equals(Constant.WAREHOUSE_PROCESS)) {  //流通批号
			boolean flag = false;
			sql.append("AND t.key_name in (");
			for (String templateName : ProductResult.flowBatchType) {
				if (flag) {
					sql.append(",");
				}
				sql.append("'" + templateName + "'");
				flag = true;
			}
			sql.append(")");
		}

		
		return dao.find(sql.toString());
	}
	
	public boolean addProductValue(final Object[][] sData) {
		return Db.tx(new IAtom() {
			public boolean run() throws SQLException {
				
				boolean flag = true;
				int[] result = null;
				
				if (sData != null && sData.length > 0) {
					//TODO update 原来已经初始化好的值 
					String insertSql = "INSERT INTO t_product_value (product_id, template_id, category_id, " +
									   "product_value, auditflag, sort) VALUES (?, ?, null, ?, 0, ?)";
					
					result = Db.batch(insertSql, sData, 1000);
					logger.error("save result: " + Arrays.toString(result));
					
					for (int r : result) {
						flag = (flag && r == 1);
					}
				}
				return flag;
			}
		});
	}
	
	/**
	 * 
	 * @param product_id
	 * @param template_id
	 * @return
	 */
	public Long countByProductIdAndTemplateId(Long product_id, Long template_id) {
		String sql = "select count(*) FROM t_product_value WHERE product_id = ? and template_id = ?";
		return Db.queryLong(sql, product_id, template_id);
	}
	
	/**
	 * 
	 * @param product_id
	 * @return
	 */
	public Long countByProductId(Long product_id) {
		String sql = "select count(*) FROM t_product_value WHERE product_id = ?";
		return Db.queryLong(sql, product_id);
	}
	
	public ProductValue getProductValueByProductId(Long product_id, Long category_id, String processType){
		String sql = "SELECT t.id tid, pv.id, pv.product_value, t.field_type, t.key_name, t.processType, " + 
					 "t.sort, t.pid, t.category_id FROM t_template t LEFT JOIN t_product_value pv ON pv.template_id = t.id " + 
	                 "AND pv.product_id = ? WHERE t.category_id = ? AND t.processType = ? " + 
					 "and t.levelNum = 1 ORDER BY t.sort";
		return findFirst(sql, product_id, category_id, processType);
	}
	
	public int copyByProductIdAndProcessType(Long newProduct_id, Long product_id, String ptCondition) {
		String sql = "INSERT INTO t_product_value (product_id, template_id, category_id, product_value, " + 
				     "auditflag, auditorid, auditor, audit_time, record_id, record_name, record_time, sort) " + 
				     "(SELECT * FROM (SELECT ? product_id, pv.template_id, pv.category_id, pv.product_value, " + 
	                 "0 auditflag, pv.auditorid, pv.auditor, pv.audit_time, pv.record_id, pv.record_name, " + 
	                 "pv.record_time, pv.sort " + 
                     "FROM t_product_value pv LEFT JOIN t_template t ON pv.template_id = t.id " + 
                     "where pv.product_id = ? " + ptCondition + ") AS tb)";
		return Db.update(sql, newProduct_id, product_id);
	}
	
	public List<ProductValue> findByFieldType(Long product_id, FieldType type, String ptCondition) {
		String sql = "SELECT pv.id, pv.template_id, pv.category_id, pv.product_value, pv.auditflag, pv.auditorid, " + 
					 "pv.auditor, pv.audit_time, pv.record_id, pv.record_name, pv.record_time, pv.sort, t.processType, t.key_name " + 
	                 "FROM t_product_value pv LEFT JOIN t_template t ON pv.template_id = t.id " + 
	                 "WHERE pv.product_id = ? and t.field_type = ? " + ptCondition;
		return dao.find(sql, product_id, type.getName());
	}
	
	public List<ProductValue> findSpecifiedBatchNum(Long product_id, String ptCondition) {
		String sql = "SELECT pv.id, pv.template_id, pv.category_id, pv.product_value, pv.auditflag, pv.auditorid, " + 
					 "pv.auditor, pv.audit_time, pv.record_id, pv.record_name, pv.record_time, pv.sort, t.processType, t.key_name " + 
	                 "FROM t_product_value pv LEFT JOIN t_template t ON pv.template_id = t.id " + 
	                 "WHERE pv.product_id = ? and t.key_name in ('菌种批号', '种子批号', '原料批号', '成品批号', '流通批号') " + ptCondition;
		return dao.find(sql, product_id);
	}
	
	/**
	 * 
	 * @param sData
	 * @param uData
	 * @return
	 */
	public boolean saveAndUpdateProductValue(final Object[][] sData, final Object[][] uData) {
		return Db.tx(new IAtom() {

			public boolean run() throws SQLException {
				
				boolean flag = true;
				int[] result = null;
				
				if (sData != null && sData.length > 0) {
					
					String insertSql = "INSERT INTO t_product_value (product_id, template_id, product_value, " +
									   "auditflag, record_id, record_name, record_time) " +
									   "VALUES (?, ?, ?, 0, ?, ?, ?)";
					
					result = Db.batch2(insertSql, sData, 1000);
					logger.error("save result: " + Arrays.toString(result));
					
					for (int r : result) {
						flag = (flag && r == 1);
					}
					if(!flag) return flag;
				}
				
				if (uData != null && uData.length > 0) {
					String updateSql = "update t_product_value set product_value = ?, record_id = ?, " +
									   "record_name = ?, record_time = ? where id = ?";
					
					result = Db.batch2(updateSql, uData, 1000);
					logger.error("update result: " + Arrays.toString(result));
					
					for (int r : result) {
						flag = (flag && r == 1);
					}
		 			return flag;
				}
				return flag;
			}
		});
	}
	
	public List<ProductValue> checkExistProductValueORisAuditByProductId(
			Long product_id, Long category_id, String processType) {
		String sql="SELECT t.id tid, pv.id,pv.auditflag, pv.product_value, t.field_type, t.key_name, t.processType, " + 
				 "t.sort, t.pid, t.category_id FROM t_template t LEFT JOIN t_product_value pv ON pv.template_id = t.id " + 
                 "AND pv.product_id = ? WHERE t.category_id = ? AND t.processType = ? " + 
				 "and t.levelNum > 0 ORDER BY pv.id";//t.levelNum > 1
		return dao.find(sql, product_id, category_id, processType);
	}

	public List<ProductValue> getProductVlaueById(long id) {
		return this.find("SELECT * from t_product_value WHERE product_id="+id);
	}

	/**
	 * 产品初始化
	 * @param product_id
	 * @param template_id
	 * @param value
	 */
	public boolean initProductValue(final List<Template> templates,final Long product_id,final Long category_id) {
		int all=0;
		for(Template template:templates){//统计插入条数
			if(!FieldType.BLANK.getName().equals(template.getStr(Template.FIELD_TYPE))&&template.getInt(Template.LEVELNUM)>0){
				all=all+1;
			}
		}
		final int result=all;
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
				int num=0;
				for(Template template:templates){
					if(!FieldType.BLANK.getName().equals(template.getStr(Template.FIELD_TYPE))&&template.getInt(Template.LEVELNUM)>0){//排除不存在值得模板
						String fileType=template.getStr(Template.FIELD_TYPE);
						Long template_id=template.getLong(Template.ID);
						String value="";
						String sql="";
						if(FieldType.IMG.getName().equals(fileType)){//判断类型
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else if(FieldType.DATE_RANGE.getName().equals(fileType)){//昨天 至 今天
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else if(FieldType.TEXT.getName().equals(fileType)){
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else if(FieldType.DATE.getName().equals(fileType)){
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else if(FieldType.FILE.getName().equals(fileType)){
							//value="upload/product_value/example.png";
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else if(FieldType.SELECT.getName().equals(fileType)){
							value="-1";
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}else {//其它或没配置的  设为空
							sql="INSERT INTO t_product_value(product_id,template_id,product_value,auditflag) VALUES(?,?,'"+value+"',?)";
						}
						int i=Db.update(sql,product_id,template_id,0);
						num=num+i;
					}
				}
				return result==num;
		}});
	}
	/**
	 * 强制完成
	 * @param product_id2
	 * @param loginUser
	 */
	public boolean mdfinish(final Long product_id, final User loginUser) {
		int all=0;
		List<ProductValue> pvs=getProductValuesByProductId(product_id);
		for(ProductValue pv:pvs){//统计插入条数
			all=all+1;
		}
		final int result=all;
		return Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
				String pdsql="UPDATE t_product set auditflag=2,auditor='"+loginUser.getStr(User.USERNAME)+"' WHERE id=?";
				String pvsql="UPDATE t_product_value set auditflag=1,auditor='"+loginUser.getStr(User.USERNAME)+"' WHERE product_id=?";
				int pd=Db.update(pdsql,product_id);
				int pv=Db.update(pvsql,product_id);
				return (pd>0&&pv==result)?true:false;
		}});
	}

	public  List<ProductValue> getProductValuesByProductId(Long product_id) {
		return this.find("select * from t_product_value where product_id="+product_id);
	}

	public int deleteByTemplateIdAndProductId(long templateId, long productId) {
		return Db.update("DELETE FROM t_product_value WHERE template_id=? AND product_id=?",templateId,productId);
	}
	
}
