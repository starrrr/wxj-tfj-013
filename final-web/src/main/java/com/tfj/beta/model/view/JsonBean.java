package com.tfj.beta.model.view;

import java.util.List;

public class JsonBean implements java.io.Serializable {

	private static final long serialVersionUID = 7736849476360588890L;

	private long id;
	// 字段
	private String name;
	// 字段对应值
	private String value;
	// 父类id
	private long pid;
	// 环节名称
	private String processType;
	// 是否可见
	private boolean isVisable;
	// 层级
	private int level;
	// 排序
	private int sort;
	// 子类List
	private List<JsonBean> child;
	//图标
	private String iconPath;
	// 图片路径
	private String imgPath;
	//是否展开
	private boolean open;
	//是否根节点
	private Boolean isParent;
	//备注
	private String remark;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public boolean isVisable() {
		return isVisable;
	}

	public void setVisable(boolean isVisable) {
		this.isVisable = isVisable;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public List<JsonBean> getChild() {
		return child;
	}

	public void setChild(List<JsonBean> child) {
		this.child = child;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


	
	

}
