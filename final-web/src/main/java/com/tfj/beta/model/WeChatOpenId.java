package com.tfj.beta.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_weixin_openid", pkName = "id")
public class WeChatOpenId extends Model<WeChatOpenId>{
	
	public static final WeChatOpenId dao = new WeChatOpenId();
	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";
	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subcribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";
	public static final String REMARK = "remark";
	public static final String GROUP_ID = "group_id";
	public static final String ROLE_ID = "role_id";
	public static final String ROLE_NAME = "role_name";
	
	public Page<WeChatOpenId> searchPaginate(int pageNumber, int pageSize,
			WeChatOpenId weChatOpenId) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
						"FROM t_weixin_openid,t_frontend_role WHERE t_weixin_openid.role_id=t_frontend_role.id ");

		if (null != weChatOpenId.getStr(NICKNAME) && !"".equals(weChatOpenId.getStr(NICKNAME))) {
			stringBuffer.append(" AND ").append(NICKNAME).append(" like '%")
					.append(weChatOpenId.getStr(NICKNAME).trim().equals("'")?"''":weChatOpenId.getStr(NICKNAME).trim()).append("%' "); // 用户名向后模糊查找
		}
		if (null != weChatOpenId.getStr(REMARK) && !"".equals(weChatOpenId.getStr(REMARK))) {
			stringBuffer.append(" AND ").append("t_frontend_role.").append(REMARK).append(" like '%")
					.append(weChatOpenId.getStr(REMARK).trim().equals("'")?"''":weChatOpenId.getStr(REMARK).trim()).append("%' "); // 用户名向后模糊查找
		}
		stringBuffer.append(" ORDER BY t_weixin_openid.id DESC "); 
		
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	public WeChatOpenId getMessByOpenid(String openids) {
		String sql="select * from t_weixin_openid where openid=?";
		return findFirst(sql,openids);
	}
	
	public Long countOpenIdByRoleId(String role_id){
		String sql = "select count(*) from t_weixin_openid";
		if(null!=role_id&&!"".equals(role_id)){
			sql += " where role_id="+role_id;
		}
		return Db.queryLong(sql);
	}
	
}
