package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */
@TableBind(tableName = "s_role_resc", pkName = "id")
public class RoleResc extends Model<RoleResc> {
    public static final RoleResc dao = new RoleResc();

    public static final String ROLE_ID = "role_id";
    public static final String RESC_ID = "resc_id";

    public List<RoleResc> searchRoleRescByRoleId(Integer roleId) {
        String sql = "SELECT * FROM s_role_resc WHERE role_id = ?";
        return find(sql, roleId);
    }

    public boolean delete(RoleResc roleResc) {
        return Db.update("DELETE FROM s_role_resc where role_id=? and resc_id=?", roleResc.getInt(ROLE_ID), roleResc.getInt(RESC_ID)) >= 0;
    }

    public boolean delete(String[] roleIds) {
        for (String roleId : roleIds) {
            Db.update("DELETE FROM s_role_resc where role_id=? ", roleId);
        }
        return true;
    }
    public boolean isExistRescId(Integer RESC_ID,Integer ROLE_ID){
    	String sql="SELECT * FROM s_role_resc WHERE resc_id=? AND role_id=?";
    	return find(sql, RESC_ID,ROLE_ID).size()>0?false:true;
    }
}
