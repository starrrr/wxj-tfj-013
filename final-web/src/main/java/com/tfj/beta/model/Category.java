package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_category", pkName = "id")
public class Category extends Model<Category> {

	public static final Category dao = new Category();
	public static final String ID = "id";
	public static final String NOTE = "note";
	public static final String CODE = "category_code";
	public static final String NAME = "category_name";
	public static final String AUDITFLAG = "auditflag";
	public static final String AUDITORID = "auditorid";
	public static final String AUDITOR = "auditor";
	public static final String AUDIT_TIME = "audit_time";
	public static final String NUMS = "nums";
	public static final String PY = "PY";

	public Page<Category> searchPaginate(int pageNumber, int pageSize,
			Category category,List<Unit> units,String admin) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
				"FROM t_category WHERE 1=1 ");

		if (null != category.getStr(CODE) && !"".equals(category.getStr(CODE))) {
			stringBuffer.append("AND ").append(CODE).append(" like '%")
					.append(category.getStr(CODE).trim().equals("'")?"''":category.getStr(CODE).trim()).append("%' "); // 用户名向后模糊查找
		}
		if (null != category.getStr(NAME) && !"".equals(category.getStr(NAME))) {
			stringBuffer.append("AND ").append(NAME).append(" like '%")
					.append(category.getStr(NAME).trim().equals("'")?"''":category.getStr(NAME).trim()).append("%' "); // 用户名向后模糊查找
		}
		
		if (null != category.getInt(AUDITFLAG)   && -1 !=category.getInt(AUDITFLAG) ) {
			stringBuffer.append("AND ").append(AUDITFLAG).append("=").append(category.getInt(AUDITFLAG)); 
		}
		if(!"true".equals(admin)){//不是管理
			String str=null;//获取所有unitId  组成字符串
			StringBuffer sb=new StringBuffer();
			if(units.size()>0){
				for(Unit unit:units){
					sb.append(unit.getLong(Unit.ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND id in(")
			.append("SELECT category_id FROM t_unit_cate WHERE unit_id in(")
			.append(str+")")
			.append(")");
		}
		stringBuffer.append(" order by id desc "); 
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	public boolean deleteByIds(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_category` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}

	public List<Category> searchCategoryByUnits(List<Unit> units,String admin) {
		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `t_category` where auditflag = 1");
		if(!"true".equals(admin)){
			String str=null;//获取所有unitId  组成字符串
			StringBuffer sb=new StringBuffer();
			if(units.size()>0){
				for(Unit unit:units){
					sb.append(unit.getLong(Unit.ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND id in(")
			.append("SELECT category_id FROM t_unit_cate WHERE unit_id in(")
			.append(str)
			.append(")")
			.append(")");
		}
		return this.find(stringBuffer.toString());
	}
	public boolean queryAssociatedById(String id) {//数据不为空，证明有关联(模板判断注释掉)
		//String sql="SELECT * from t_category,t_template where t_template.category_id="+id+" LIMIT 1";
		//if(this.findFirst(sql)==null){
		//String sql="SELECT * from t_category,t_plantbase where t_plantbase.category_id="+id+" LIMIT 1";
		String sql="SELECT * FROM t_category tc,t_plantbase tp,t_plant_cate tpc WHERE tc.id=tpc.category_id AND tp.id=tpc.plantbase_id AND tc.id="+id+" LIMIT 1";	
		if(this.findFirst(sql)==null){
				sql="SELECT * from t_category,t_product where t_product.category_id="+id+" LIMIT 1";
				if(this.findFirst(sql)==null){
					sql="SELECT * from t_category,t_product_result where t_product_result.category_id="+id+" LIMIT 1";
				}
			}
		//}
		return this.findFirst(sql)==null?false:true;
	}
	
	public Category findCategoryById(String categoryId) {
		return this.findFirst("SELECT * FROM t_category WHERE id="+categoryId);
	}
	
	public List<Category> findAllCategory(){
		return this.find("SELECT * FROM t_category");
	}

	public Category findCategoryByName(String name) {
		return this.findFirst("SELECT * FROM t_category WHERE category_name=?",name);
	}

	public Category checkCategoryByCategoryName(String categoryName) {
		// TODO Auto-generated method stub
		return findFirst("select * from t_category where category_name= ?", categoryName);
	}

	public boolean duleUnitCate(Category category, String[] unitIds) {
		int i=0;
		for(String unitId:unitIds){
			Db.update("INSERT INTO t_unit_cate(unit_id,category_id) values("+unitId+","+category.getLong(ID)+")");
			i++;
		}
		return unitIds.length==i;
	}
	public boolean duleUnitCateUpdate(Category category, String[] unitIds) {
		boolean isFlag=true;
		int i=0;
		Db.update("DELETE FROM t_unit_cate WHERE category_id=?",category.getLong(ID));
		if(unitIds!=null){
			for(String unitId:unitIds){
				Db.update("INSERT INTO t_unit_cate(unit_id,category_id) values("+unitId+","+category.getLong(ID)+")");
				i++;
			}
			if(unitIds.length!=i){
				isFlag=false; 
			}
		}
		
		return isFlag;
	}

	public boolean deleteUnitCate(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_unit_cate` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("category_id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("category_id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}
	public List<Category> searchCategoryByUnitId(String unit_id) {
		return this.find("SELECT * FROM t_category WHERE id IN(SELECT category_id FROM t_unit_cate WHERE unit_id=?)",unit_id);
	}

	public Long countCategory(){
		return Db.queryLong("SELECT count(*) FROM `t_category` where auditflag = 1");
	}
	
	public List<Category> findCategoryNameList(){
		StringBuffer sql = new StringBuffer("SELECT nums,category_name from t_category order by convert(category_name USING gbk) COLLATE gbk_chinese_ci asc"); 
		return this.find(sql.toString());
	}
	
	/**
	 * 更新产品数量
	 * @return
	 */
	public boolean updataNum(Long catgoryId){
		return 0 == Db.update("UPDATE t_category SET nums=nums+1 where id="+catgoryId)?false:true;
	}

	public boolean deleteTemplate(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_template` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("category_id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("category_id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}

	public String findCategoryNameByIds(String ids) {
		String categoryName="";
		StringBuffer stringBuffer = new StringBuffer(
				"SELECT * FROM t_category where ");
		if(!"".equals(ids)){
			stringBuffer.append("id in("+ids+")");
		}
		List<Category> categories=this.find(stringBuffer.toString());
		for(Category category:categories){
			categoryName=categoryName+","+category.getStr(Category.NAME);
		}
		return "".equals(categoryName)?categoryName:categoryName.substring(1);
	}

	public List<Category> CategoryPlantCate(List<PlantCate> plantCates) {
		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM t_category WHERE 1=1");
		 String str=null;
		 StringBuffer sb=new StringBuffer();
		 if(plantCates.size()>0){
			for(PlantCate plantCate:plantCates){
				sb.append(plantCate.getLong(PlantCate.CATEGORY_ID)+",");
			}
			int endIndex=sb.toString().lastIndexOf(",");
			str=sb.toString().substring(0, endIndex);
		}
		stringBuffer.append(" AND id in("+str)
		.append(")");
	    return find(stringBuffer.toString());
	}

}
