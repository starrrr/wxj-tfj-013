package com.tfj.beta.model;

import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;
import com.tfj.beta.utils.JsonUtil;

@TableBind(tableName = "t_weixin_openid", pkName = "id")
public class WeChatOpenIdMS extends Model<WeChatOpenIdMS> {
	private static final Logger log = LoggerFactory
			.getLogger(WeChatOpenIdMS.class);
	private static final long serialVersionUID = 5701398753533478081L;
	public static final WeChatOpenIdMS dao = new WeChatOpenIdMS();
	public static final FrontEndRole roleDao = new FrontEndRole();

	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";

	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subscribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";

	public static final String REMARK = "remark";
	public static final String GROUPID = "groupid";
	public static final String ROLE_ID = "role_id";
	public static final String ROLE_NAME = "role_name";

	public WeChatOpenIdMS findWeChatMSByOpenId(String openId) {
		String sql = "SELECT * FROM t_weixin_openid WHERE openid='" + openId
				+ "'";
		return dao.findFirst(sql);
	}

	public boolean saveWeChatOpenIdMs(String wechatJson, String frontRoleName) {
		try {
			FrontEndRole role = roleDao
					.findFrontEndRoleByRoleName(frontRoleName);
			WeChatBean bean = (WeChatBean) JsonUtil.parseJavaObject(wechatJson,
					WeChatBean.class);
			bean.setSubscribe("0");
			bean.setRoleId(role.getLong(role.ID));
			bean.setRoleName(role.getStr(role.NAME));
			WeChatOpenIdMS wechatOpen = constructWeChatOpenId(bean);
			if (StringUtils.isNotBlank(bean.getOpenid())) {
				wechatOpen.save();
			}
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	/**
	 * 构造 wechatOpenId
	 * @param bean
	 * @return
	 */
	public WeChatOpenIdMS constructWeChatOpenId(WeChatBean bean) {

		 WeChatOpenIdMS openId = new WeChatOpenIdMS();
		
		 openId.set("subscribe", bean.getSubscribe());
		 openId.set("openid", bean.getOpenid());
		 String nickName = StringFilter(bean.getNickname()).replaceAll("\"",
		 "");
		 openId.set("nickname", nickName.trim());
		 openId.set("sex", bean.getSex());
		 openId.set("language", bean.getLanguage());
		 openId.set("city", bean.getCity());
		 openId.set("province", bean.getProvince());
		 openId.set("country", bean.getCountry());
		 openId.set("subscribe_time", bean.getSubscribeTime());
		 openId.set("headimgurl", bean.getHeadimgurl());
		 openId.set("unionid", bean.getUnionid());
		 openId.set("remark", bean.getRemark());
		 openId.set("groupid", bean.getGroupid());
		 openId.set("role_id", bean.getRoleId());
		 openId.set("role_name", bean.getRoleName());

		return openId;
	}

	public static String StringFilter(String str) throws PatternSyntaxException {
		if (null == str) {
			return "";
		}
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		return str.replaceAll(reg, "").trim();
	}

}
