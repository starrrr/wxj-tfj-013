package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_frontend_role", pkName = "id")
public class WeChatRole extends Model<WeChatRole>{
	
	public static final WeChatRole dao = new WeChatRole();
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String REMARK = "remark";
	public static final String SORT = "sort";
	public static final String UPDATE_TIME = "update_time";
	public static final String ENABLE = "enable";
	
	public Page<WeChatRole> searchPaginate(int pageNumber, int pageSize,
			WeChatRole weChatOpenId) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
						"FROM t_frontend_role WHERE 1=1 ORDER BY sort");
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	/**
     * 根据ids批量删除用户（实际：更新delete_status = '1'）
     *
     * @param ids
     * @return boolean
     */
    public boolean deleteByIds(String[] ids) {
        StringBuffer stringBuffer = new StringBuffer("delete from `t_frontend_role` where ");
        if (null != ids && 1 == ids.length) {
            stringBuffer.append("id = ?");
            return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false : true;
        } else if (null != ids && ids.length > 1) {
            stringBuffer.append("id in('");
            for (String id : ids) {
                stringBuffer.append(id).append("','");
            }
            stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length()); // 去掉最后一个,和'
            stringBuffer.append(")");
            return 0 == Db.update(stringBuffer.toString()) ? false : true;
        }
        return false;
    }
    /**
     * 查找所有角色
     */
    public List<WeChatRole> getWeChatRoles(){
		return this.find("select * from  t_frontend_role");
    }
	
}
