package com.tfj.beta.model;

public enum ProcessType {

    PLANT_PROCESS("种植环节"), 
    SUPERVISE_PROCESS("加工环节"), 
    WAREHOUSE_PROCESS("流通环节"), 
    SALE_PROCESS("销售环节");
    
    private String cn;
    
    ProcessType(String cn) {
    	this.cn = cn;
    }
    
    public String getCn() {
		return cn;
	}
    
    public static String getCn(String str) {
    	return valueOf(str).getCn();
    }
}
