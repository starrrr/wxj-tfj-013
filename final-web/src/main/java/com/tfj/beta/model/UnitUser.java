package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2015/6/18.
 */
@TableBind(tableName = "s_unit_user", pkName = "id")
public class UnitUser extends Model<UnitUser> {
    public static final UnitUser dao = new UnitUser();

    public static final String ID = "id";
    public static final String UNIT_ID = "unit_id"; // unit_id
    public static final String USER_ID = "user_id"; // user_id
    /**
     * 根据用户查找用户与单位的关系
     * @param loginUser
     * @return
     */
	public List<UnitUser> searchUnitUserByUserId(String user_id) {
		return this.find("SELECT * FROM s_unit_user WHERE user_id=?",user_id);
	}
	 /**
     * 根据userid来删除unit_user
     */
    public boolean deleteByUserId(Long userId) {
        String sql = "DELETE FROM s_unit_user WHERE user_id = ?";
        return 0 == Db.update(sql, userId) ? true : false;
    }
	public boolean SaveAdminUnit(List<RoleUser> roleUsers,Long unitId) {
		int i=0;
		String sql="INSERT INTO s_unit_user(user_id,unit_id) VALUES(?,?)";
		for(RoleUser roleUser:roleUsers){
			Db.update(sql,roleUser.getLong(RoleUser.USER_ID),unitId);
			i++;
		}
		return i==roleUsers.size();
	}
}
