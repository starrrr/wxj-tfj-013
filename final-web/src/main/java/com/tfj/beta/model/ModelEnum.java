package com.tfj.beta.model;

/**
 * Created by Administrator on 2015/1/7.
 */
public class ModelEnum {
    /**
     * 否, 是
     */
    public enum YesOrNO {
        NO, YES
    }

    /**
     * 离线, 在线
     */
    public enum OnlineOrOffline {
        OFFLINE, ONLINE
    }
}
