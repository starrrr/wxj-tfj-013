package com.tfj.beta.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_company_bind", pkName = "id")
public class Enterprise extends Model<Enterprise>{
	
	public static final Enterprise dao = new Enterprise();
	public static final WeChatRole wechatdao = new WeChatRole();//引入微信角色实体
	public static final String ID = "id";
	public static final String COMPANY_USER = "company_user";
	public static final String COMPANY_PWD = "company_pwd";
	public static final String NAME = "name";
	public static final String TELEPHONE = "telephone";
	public static final String ADDRESS = "address";
	public static final String WEBSIT = "websit";
	public static final String OPERATE_TIME = "operate_time";
	public static final String IS_COMPANY = "is_company";
	
	/**
     * 根据条件查询用户分页数据
     */
    public Page<Enterprise> searchPaginate(int pageNumber, int pageSize, Enterprise enterprise) {
        String select = "SELECT * ";
        StringBuffer stringBuffer = new StringBuffer("FROM t_company_bind WHERE 1=1 ");
        if (null != enterprise.getStr(COMPANY_USER) && !"".equals(enterprise.getStr(COMPANY_USER))) {
			stringBuffer.append(" AND ").append(COMPANY_USER).append(" like '%")
					.append(enterprise.getStr(COMPANY_USER).trim().equals("'")?"''":enterprise.getStr(COMPANY_USER).trim()).append("%' "); // 用户名向后模糊查找
		}
        stringBuffer.append(" order by operate_time desc");
        return paginate(pageNumber, pageSize, select, stringBuffer.toString());
    }
    
    public Page<Enterprise> searchPaginate(int pageNumber, int pageSize,
			Long id, String bind_user) {
    	 String select = "SELECT * ";
	     StringBuffer stringBuffer = new StringBuffer("FROM t_company_bind tcb,t_company_openid tco,t_weixin_openid two WHERE tco.is_bind=1 And tcb.id="+id+" And tco.company_id="+id+" And tco.openid=two.openid And two.nickname like '%"+bind_user+"%'");
	     return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}
    

	public boolean deleteByIds(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_company_bind` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}

	public Page<Enterprise> findManagePageById(int pageNumber,int pageSize, Long id) {
			 String select = "SELECT * ";
		     StringBuffer stringBuffer = new StringBuffer("FROM t_company_bind tcb,t_company_openid tco,t_weixin_openid two WHERE tco.is_bind=1 And tcb.id="+id+" And tco.company_id="+id+" And tco.openid=two.openid ");
		     return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	public Enterprise checkEnterpriseUserByUserName(String enterpriseusername) {
		return findFirst("select * from t_company_bind where company_user= ?", enterpriseusername);
	}

	public boolean removeBind(String openids) {
		/**
		 * 查询用户组C的id
		 */
		WeChatRole weChatRole=wechatdao.findFirst("select * from t_frontend_role WHERE NAME='C'");
		/*String sql="update t_company_openid tco,t_weixin_openid two "
				+ "set tco.is_bind=0,two.role_name='C',two.role_id="+weChatRole.getLong(WeChatRole.ID)+""
						+ " WHERE two.openid='"+openids+"' and tco.openid='"+openids+"'";*/
		String upsql="update t_weixin_openid set role_name='C',role_id="+weChatRole.getLong(WeChatRole.ID)+" WHERE openid='"+openids+"'";
		String delsql="DELETE FROM t_company_openid WHERE openid='"+openids+"'";
		int up=Db.update(upsql);
		int de=Db.update(delsql);
		return (up==0 && de==0)?false:true;
	}

	public void updatePW(String pw,Long id) {
		String sql="UPDATE t_company_bind SET company_pwd=? WHERE id=?";
		Db.update(sql,pw,id);
	}

	public boolean isExitBindUser(String id) {
		String sql="select * from t_company_openid where company_id=?";
		return Db.query(sql,id).size()>0?true:false;
	}

}
