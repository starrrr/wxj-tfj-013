package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;


@TableBind(tableName = "t_plant_cate", pkName = "id")
public class PlantCate extends Model<PlantCate> {
    public static final PlantCate dao = new PlantCate();

    public static final String ID = "id";
    public static final String PLANT_ID = "plant_id"; // unit_id
    public static final String CATEGORY_ID = "category_id"; // category_id
    
    public boolean savePlantCateRelation(String[] categoryIds,Long id) {
		int k=0;
		String sql="INSERT INTO t_plant_cate(plantbase_id,category_id) VALUE(?,?)";
		for(int i=0;i<categoryIds.length;i++){
			Db.update(sql,id,categoryIds[i]);
			k++;
		}
		return k==categoryIds.length;
	}

	public boolean deleteByPlantId(Long plant_id) {
		String sql="DELETE FROM t_plant_cate WHERE plantbase_id=?";
		Db.update(sql,plant_id);
		return true;
	}

	public List<PlantCate> getPlantCate(String plantbase_id) {
		return this.find("SELECT * FROM t_plant_cate WHERE plantbase_id=?",plantbase_id);
	}
}
