package com.tfj.beta.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class IndexTemplate extends Template{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final ProductValue productValuedao = new ProductValue();
	public List<Template> getTemplateListByPid(String processType,String category_id) {
		StringBuffer sql = new StringBuffer(
				"select * FROM t_template WHERE processType ='" + processType+ "' AND category_id=" + category_id + " AND pid=0").append(" order by sort");// AND index_show = 1
			
		List<Template> templateList = dao.find(sql.toString());
		return templateList;
	}
	
	// 根据pid获取主页的child
	public List<Template> getChildByPidIndex(long pid,String role_name) {
				StringBuffer sql = null;
				if(FrontEndRole.CUSTOMER.equals(role_name)){//当为customer时，首页销售环节是不显示的  processType <> 'SALE_PROCESS'
					sql = new StringBuffer(
							"select * FROM t_template WHERE index_show ="+1+" AND pid="+pid)
					//下面注释的：主页显示和环境显示    字段控制首页显示   
					//.append(" AND ").append("process_show").append(" like '%").append(role_name+"_1").append("%' ").append(" And processType <> 'SALE_PROCESS'").append(" order by sort");
					.append(" And processType <> 'SALE_PROCESS'").append(" order by sort");
				}else {
					sql = new StringBuffer(
							"select * FROM t_template WHERE index_show ="+1+" AND pid="+pid).append(" order by sort");
				}
				return dao.find(sql.toString());
				
	}
	// 根据pid获取child
	public List<Template> getChildByPid(long pid,String role_name,String product_id) {
			List<Template> templates=new ArrayList<Template>();
			StringBuffer sql = new StringBuffer(
					"select * FROM t_template WHERE pid ='" + pid + "'")
			.append(" AND ").append("process_show").append(" like '%").append(role_name+"_1").append("%' ").append(" order by sort");
			List<Template> templateList = dao.find(sql.toString());
			//判断，如果值为空，模板不显示
			for (Template child : templateList){
				//if(!"空白".equals(child.getStr(Template.FIELD_TYPE))&&!"下拉菜单".equals(child.getStr(Template.FIELD_TYPE))&&!"图片类型".equals(child.getStr(Template.FIELD_TYPE))){
				if(!"空白".equals(child.getStr(Template.FIELD_TYPE))&&!"图片类型".equals(child.getStr(Template.FIELD_TYPE))){
//					ProductValue prv=productValuedao.getProductValueByTemplateId(child.getLong(Template.ID));
					ProductValue prv=productValuedao.getProductValueByTemplateId(child.getLong(Template.ID),product_id);
					//模板多值  模板id是相同的
					if(prv!=null&&prv.getStr("product_value")!=null&&!"".equals(prv.getStr("product_value"))){//
						templates.add(child);
					}
				}else {
					templates.add(child);
				}
			}
			return templates;
		}
	
	// 根据pid获取child
		public List<Template> getChildByPidAndIndex(long pid) {
				StringBuffer sql = new StringBuffer(
						"select * FROM t_template WHERE index_show=1 AND levelNum=2 and pid ='" + pid + "'").append(" order by sort");
				List<Template> templateList = dao.find(sql.toString());
				return templateList;
			}
	
	public List<Template> getTemplateListByProcessType(String processType,
			String categoryId,Boolean ISINDEX,String role_name) {
		StringBuffer sql=null;
		if(ISINDEX){
			sql = new StringBuffer(
					"select * FROM t_template WHERE processType ='" + processType
							+ "'AND category_id=" + categoryId + " AND pid=0  AND index_show ="+1+"");//AND levelNum=0
		}else {
			if(StringUtils.isNotBlank(role_name)){
				//select * FROM t_template where process_show like '%a_1%'
				sql = new StringBuffer(
						"select * FROM t_template WHERE processType ='" + processType
								+ "' AND category_id=" + categoryId + " AND levelNum=1");
				sql.append(" AND").append(" process_show").append(" like '%").append(role_name+"_1").append("%' ").append(" order by sort");
			}else {//缓存加载，开始role_name=null把所以数据都加载
				sql = new StringBuffer(
						"select * FROM t_template WHERE processType ='" + processType
								+ "'AND category_id=" + categoryId + " AND levelNum=1 order by sort");
			}
		}
		
		List<Template> templateList = dao.find(sql.toString());
		return templateList;
	}
	
	

}
