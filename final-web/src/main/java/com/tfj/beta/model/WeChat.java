package com.tfj.beta.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "t_weixin_log", pkName = "id")
public class WeChat extends Model<WeChat>{
	
	public static final WeChat dao = new WeChat();
	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";
	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subcribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";
	public static final String REMARK = "remark";
	public static final String GROUP_ID = "group_id";
	public static final String SCAN_TIME = "scan_time";
	
	public Page<WeChat> searchPaginate(int pageNumber, int pageSize,
			WeChat weChat) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
				"FROM t_weixin_log WHERE 1=1 order by scan_time DESC");
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}
}
