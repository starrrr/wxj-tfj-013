package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;

/**
 * 系统配置
 * Created by Administrator on 2015/1/7.
 */
@TableBind(tableName = "s_webconfig")
public class WebConfig extends Model<WebConfig> {

    private static final long serialVersionUID = -4888296493766000643L;

    public static final WebConfig dao = new WebConfig();

    public static final String ID = "id";
    public static final String KEY = "key"; // key键
    public static final String VALUE = "value"; // value值


    /**
     * 根据isSystem查找所有webConfig
     */
    public List<WebConfig> searchByIsSystem(int isSystem) {
        String sql = "select * from s_webconfig where is_system = ?";
        return find(sql, isSystem);
    }
}
