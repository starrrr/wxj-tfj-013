package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

@TableBind(tableName = "t_county", pkName = "id")
public class County  extends Model<County>{
	
	public static final County dao = new County();
	public static final String ID = "id";
	public static final String CODE = "code";// 省代码
	public static final String NAME = "name";// 省名称
	public static final String NUMS = "nums";//基地数量 
	
	public List<County> findAll(){
		return this.find("SELECT * FROM t_county");
	}
	
	public boolean saveNums(boolean flag,String code){
		String sql="UPDATE t_county set nums=nums-1 where code='"+code+"'";
		if(flag){
			sql="UPDATE t_county set nums=nums+1 where code='"+code+"'";
		}
		return 0==Db.update(sql)?false:true;
	}
	
	/**
	 * 基地数量大于0，表示此省有基地
	 * @return
	 */
	public List<County> findListByNums(){
		String sql="SELECT * FROM t_county where nums>0";
		return this.find(sql);
	}
	 
}
