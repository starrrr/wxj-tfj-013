package com.tfj.beta.model.view;


public class TreeBean implements java.io.Serializable {

	private static final long serialVersionUID = 7736849476360588890L;

	private long id;
	// 字段
	private String name;

	// 父类id
	private long pId;
	
	// 环节名称
	private String processType;

	// 层级
	private long levelNum;
	
	// 排序
	private long sort;

	//
	private String iconPath;

	// 是否展开
	private Boolean open;

	// 是否根节点
	private Boolean isParent;
	
	private String fieldType;
	
	private String fieldDevelop;
	
	private Boolean indexShow;
	
	private String processShow;
	
	private String defaultValue;
	
	private String notes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public long getPId() {
		return pId;
	}

	public void setPId(long pId) {
		this.pId = pId;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	

	public long getLevelNum() {
		return levelNum;
	}

	public void setLevelNum(long levelNum) {
		this.levelNum = levelNum;
	}

	public long getSort() {
		return sort;
	}

	public void setSort(long sort) {
		this.sort = sort;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	public Boolean getIndexShow() {
		return indexShow;
	}

	public void setIndexShow(Boolean indexShow) {
		this.indexShow = indexShow;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldDevelop() {
		return fieldDevelop;
	}

	public void setFieldDevelop(String fieldDevelop) {
		this.fieldDevelop = fieldDevelop;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getProcessShow() {
		return processShow;
	}

	public void setProcessShow(String processShow) {
		this.processShow = processShow;
	}



	
	
	

}
