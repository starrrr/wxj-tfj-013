package com.tfj.beta.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by jxx on 2016/3/17.
 */
@TableBind(tableName = "t_index_count", pkName = "id")
public class IndexCount extends Model<IndexCount>{
	public static final IndexCount dao = new IndexCount();
	
    public static final String ID = "id";
    public static final String COUNT_OPENID = "count_openid";
    public static final String COUNT_G_OPENID = "count_G_openid";
    public static final String COUNT_C_OPENID = "count_C_openid";
    public static final String COUNT_B_OPENID = "count_B_openid";
    public static final String COUNT_CATEGORY = "count_category";
    public static final String COUNT_CATEGORYRECORD = "count_categoryrecord";
    public static final String COUNT_PLANTBASE = "count_plantbase";
    
    public IndexCount find(){
    	return this.findById("1");
    }
    
    /**
     * 
     * @param flag(true为+，false为－)
     * @return
     */
    public boolean updatePlantBase(boolean flag){
    	String sql = "UPDATE t_index_count SET count_plantbase=count_plantbase-1";
    	if(flag){
    		sql ="UPDATE t_index_count SET count_plantbase=count_plantbase+1";
    	}
    	return 0==Db.update(sql)?false:true;
    }
    
    public Boolean updateCountCategory(boolean flag){
    	String sql = "UPDATE t_index_count SET count_category=count_category-1";
    	if(flag){
    		sql ="UPDATE t_index_count SET count_category=count_category+1";
    	}
    	return 0==Db.update(sql)?false:true;
    }
}
