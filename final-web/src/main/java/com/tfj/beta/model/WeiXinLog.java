package com.tfj.beta.model;

import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.JsonUtil;

@TableBind(tableName = "t_weixin_log", pkName = "id")
public class WeiXinLog extends Model<WeiXinLog> {
	private static final Logger log = LoggerFactory.getLogger(WeiXinLog.class);
	private static final long serialVersionUID = 5701398753533478081L;
	public static final WeiXinLog dao = new WeiXinLog();
	public static final String ID = "id";
	public static final String SUBSCRIBE = "subscribe";
	public static final String OPENID = "openid";
	public static final String NICKNAME = "nickname";
	public static final String SEX = "sex";

	public static final String LANGUAGE = "language";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String COUNTRY = "country";
	public static final String SUBSCRIBE_TIME = "subscribe_time";
	public static final String HEADIMGURL = "headimgurl";
	public static final String UNIONID = "unionid";

	public static final String REMARK = "remark";
	public static final String GROUPID = "groupid";
	public static final String SCAN_TIME = "scan_time";

	public void saveWeChatLog(String json) {

		WeChatBean bean = (WeChatBean) JsonUtil.parseJavaObject(json,
				WeChatBean.class);
		if (null != bean) {
			WeiXinLog wechatlog = constructWeiXinLog(bean);
			log.error("wechatlog...." + wechatlog + "wechatlog."
					+ wechatlog.getStr(CITY));
			//保证取到openId信息才入库
			if(StringUtils.isNotBlank(bean.getOpenid())){
				wechatlog.save();
			}

			
		}
	}

	public WeiXinLog constructWeiXinLog(WeChatBean bean) {

		WeiXinLog log = new WeiXinLog();
		String currentDate = DateUtils.getCurrentTime();
		log.set("subscribe", bean.getSubscribe());
		log.set("openid", bean.getOpenid());
		String nickName = " ";
		if(StringUtils.isNotBlank(bean.getNickname())){
			 nickName = StringFilter(bean.getNickname()).replaceAll("\"", "").trim();
		}else{
			 nickName ="";
		}

		// StringEscapeUtils.e
		log.set("nickname", nickName);
		log.set("sex", bean.getSex());
		log.set("language", bean.getLanguage());
		log.set("city", bean.getCity());
		log.set("province", bean.getProvince());
		log.set("country", bean.getCountry());
		log.set("subscribe_time", bean.getSubscribeTime());
		log.set("headimgurl", bean.getHeadimgurl());
		log.set("unionid", bean.getUnionid());
		log.set("remark", bean.getRemark());
		log.set("groupid", bean.getGroupid());
		log.set("scan_time", currentDate);
		return log;
	}

	public static String StringFilter(String str) throws PatternSyntaxException {
		if(null== str){
			return "";
		}
		String reg = "[^0-9a-zA-Z\u4e00-\u9fa5]+";
		return str.replaceAll(reg,"").trim();
	}

	public static void main(String[] args) {
	
	}

}
