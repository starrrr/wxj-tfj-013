package com.tfj.beta.model;

import java.lang.reflect.Constructor;

import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.service.value.DateRangeWidget;
import com.tfj.beta.service.value.DateWidget;
import com.tfj.beta.service.value.FileWidget;
import com.tfj.beta.service.value.ImgWidget;
import com.tfj.beta.service.value.NoWidget;
import com.tfj.beta.service.value.SelectWidget;
import com.tfj.beta.service.value.TextWidget;
import com.tfj.beta.service.value.Widget;

public enum FieldType {
	
	BLANK("blank", "空白", NoWidget.class),
	TEXT("text", "文本类型", TextWidget.class), 
	FILE("file", "文件上传", FileWidget.class),
	SELECT("combox", "下拉菜单", SelectWidget.class),
	DATE("date", "日期类型", DateWidget.class),
	DATE_RANGE("date_range", "日期范围", DateRangeWidget.class),
	IMG("img", "图片类型", ImgWidget.class);

	private String key;

	private String name;
	
	private Class<? extends Widget> clazz;

	FieldType(String key, String name, Class<? extends Widget> clazz) {
		this.key = key;
		this.name = name;
		this.clazz = clazz;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean nameEquals(String str)  {
		return getName().equals(str);
	}

	public static String getNameByKey(String key) {
		String name = null;
		FieldType[] fieldTypes = FieldType.values();
		for (FieldType fieldType : fieldTypes) {
			if (fieldType.getKey().equals(key)) {
				name = fieldType.getName();
				return name;
			}
		}
		return null;
	}

	public static String getKeyByName(String name) {
		String key = null;
		FieldType[] fieldTypes = FieldType.values();
		for (FieldType fieldType : fieldTypes) {
			if (fieldType.getName().equals(name)) {
				key = fieldType.getKey();
				return key;
			}
		}
		return null;
	}
	
	/**
	 * 创建不为空的Widget，如果没找到合适的 Widget, 则创建 NoWidget
	 * @param bean
	 * @return
	 */
	public static Widget createNotNullWidget(PvBean bean) {
		String fieldType = bean.getFileType();
		FieldType[] allTypes = FieldType.values();
		for (FieldType aType : allTypes) {
			if (aType.nameEquals(fieldType)) {
				return aType.createWidget(bean);
			}
		}
		return new NoWidget(bean);
	}
	
	public Widget createWidget(PvBean bean) {
		try {
			if (clazz != null) {
				Constructor<? extends Widget> constr = clazz.getConstructor(PvBean.class);
				return constr.newInstance(bean);
			}
		} catch (Exception e) {
			// TODO logger
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.err.println(FieldType.BLANK.getName());
	}
}
