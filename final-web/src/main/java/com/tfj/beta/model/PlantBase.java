package com.tfj.beta.model;

import java.util.List;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.core.Constant;

@TableBind(tableName = "t_plantbase", pkName = "id")
public class PlantBase extends Model<PlantBase> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3906027751316680779L;

	public static final PlantBase dao = new PlantBase();
	public static final String ID = "id";
	public static final String CODE = "base_code";// 基地代码
	public static final String NAME = "base_name";// 基地名称
	public static final String COUNTY_CODE = "base_county_code";// 基地所在省编码
	public static final String BASE_LOCATION = "base_location";// 基地地点
	public static final String BASE_AREA = "base_area";// 基地面积
	public static final String BASE_HEIGHT = "base_height";// 海拔高度
	public static final String LONGITUDE = "longitude"; // 经度
	public static final String LATITUDE = "latitude";// 纬度
	public static final String SOIL = "soil";// 土壤性质
	public static final String BASE_IN_CHARGE = "base_in_charge";// 基地负责人
	public static final String OPERATOR = "operator_id";// 操作人
	public static final String OPERATOR_TIME = "operator_time";// 操作时间
	public static final String HERBAL_TYPE = "category_id";// 中草药种类id
	public static final String HERBAL_NAME = "category_name";// 中草药名称
	public static final String ENV_TEST_REPORT = "env_test_report";// 环境测评报告
																	// 是1否0，选择是需要上传电子附件
	public static final String NOTE = "note";
	public static final String BASE_MAP = "base_map";// 环境地图
	public static final String AUDITFLAG = "auditflag";//审核状态
	public static final String AUDITORID = "auditorid";//审核ID
	public static final String AUDITOR = "auditor";//审核人
	public static final String AUDIT_TIME = "audit_time";//审核时间
	public static final String STLX="STLX";//数字字典土质
	

	public Page<PlantBase> searchPaginate(int pageNumber, int pageSize,
			PlantBase plantBase,List<Unit> units,String admin) {
		String select = "SELECT * ";
		StringBuffer stringBuffer = new StringBuffer(
				"FROM t_plantbase WHERE 1=1 ");
		
		if (null != plantBase.getStr(CODE) && !"".equals(plantBase.getStr(CODE))) {
			stringBuffer.append("AND ").append(CODE).append(" like '%")
					.append(plantBase.getStr(CODE).trim().equals("'")?"''":plantBase.getStr(CODE).trim()).append("%' "); // 用户名向后模糊查找
		}
		if (null != plantBase.getStr(NAME) && !"".equals(plantBase.getStr(NAME))) {
			stringBuffer.append("AND ").append(NAME).append(" like '%")
					.append(plantBase.getStr(NAME).trim().equals("'")?"''":plantBase.getStr(NAME).trim()).append("%' "); // 用户名向后模糊查找
		}
		if (null != plantBase.getStr(BASE_LOCATION) && !"".equals(plantBase.getStr(BASE_LOCATION))) {
			stringBuffer.append("AND ").append(BASE_LOCATION).append(" like '%")
					.append(plantBase.getStr(BASE_LOCATION).trim().equals("'")?"''":plantBase.getStr(BASE_LOCATION).trim()).append("%' "); // 用户名向后模糊查找
		}
		if (null != plantBase.getStr(HERBAL_NAME) && !"".equals(plantBase.getStr(HERBAL_NAME))) {
			stringBuffer.append("AND ").append(HERBAL_NAME).append(" like '%")
					.append(plantBase.getStr(HERBAL_NAME)).append("%' "); // 用户名向后模糊查找
		}
		if (null != plantBase.getStr(HERBAL_TYPE) && !"".equals(plantBase.getStr(HERBAL_TYPE))) {
			//select * from t_plantbase where FIND_IN_SET(83,category_id)
			stringBuffer.append("AND ").append("FIND_IN_SET(")
			.append(plantBase.getStr(HERBAL_TYPE)).append(",")
			.append(HERBAL_TYPE+")"); // 用户名向后模糊查找
		}
		if (null != plantBase.getInt(AUDITFLAG) && !"".equals(plantBase.getInt(AUDITFLAG))) {
			stringBuffer.append(" AND ").append(AUDITFLAG).append("=")
					.append(plantBase.getInt(AUDITFLAG)); // 用户名向后模糊查找
		}
		if(!"true".equals(admin)){
			String str=null;//获取所有unitId  组成字符串
			StringBuffer sb=new StringBuffer();
			if(units.size()>0){
				for(Unit unit:units){
					sb.append(unit.getLong(Unit.ID)+",");
				}
				int endIndex=sb.toString().lastIndexOf(",");
				str=sb.toString().substring(0, endIndex);
			}
			stringBuffer.append(" AND unit_id in(")
			.append(str)
			.append(")");
		}
		stringBuffer.append(" order by id Desc");
		System.err.println(stringBuffer.toString());
		return paginate(pageNumber, pageSize, select, stringBuffer.toString());
	}

	public boolean deleteByIds(String[] ids) {
		StringBuffer stringBuffer = new StringBuffer(
				"delete from `t_plantbase` where ");
		if (null != ids && 1 == ids.length) {
			stringBuffer.append("id = ?");
			return 0 == Db.update(stringBuffer.toString(), ids[0]) ? false
					: true;
		} else if (null != ids && ids.length > 1) {
			stringBuffer.append("id in('");
			for (String id : ids) {
				stringBuffer.append(id).append("','");
			}
			stringBuffer.delete(stringBuffer.length() - 2,
					stringBuffer.length()); // 去掉最后一个,和'
			stringBuffer.append(")");
			return 0 == Db.update(stringBuffer.toString()) ? false : true;
		}
		return false;
	}

	public List<PlantBase> getPlantBaseList() {
		StringBuffer stringBuffer = new StringBuffer("SELECT * FROM `t_plantbase` where auditflag = 1");
		return this.find(stringBuffer.toString());
	}

	public PlantBase getPlantById(int plantBaseId) {
		return this.findById(plantBaseId);
	}
	
	public boolean queryAssociatedById(String id) {
		String sql="SELECT * from t_plantbase,t_product where t_product.plantbase_id="+id+" LIMIT 1";
		return this.findFirst(sql)==null?false:true;
	}
	
	public List<PlantBase> getPlantBaseByCountyCode(String county_code){
		String sql="SELECT * FROM t_plantbase where base_county_code='"+county_code+"'";
		return this.find(sql);
	}

	public List<PlantBase> findAllPlantBase() {
		return this.find("SELECT * FROM t_plantbase");
	}

	public List<PlantBase> getPlantBaseByUnitId(Long unitId) {
		return this.find("SELECT * FROM t_plantbase WHERE auditflag = 1 AND unit_id=?",unitId);
	}

	public List<PlantBase> findPlantBaseByCategoryIdAndUnitId(String unit_id,String category_id) {
		return this.find("SELECT * FROM t_plantbase WHERE auditflag = 1 AND unit_id=? AND id in(SELECT plantbase_id FROM t_plant_cate WHERE category_id=?)",unit_id,category_id);
	}

	public List<PlantBase> findPlantBaseByMap(String picture) {
		return this.find("SELECT * FROM t_plantbase WHERE base_map=?",picture);
	}

	public boolean dulePlantCateUpdate(PlantBase plantBase, String categoryIds) {
		boolean isFlag=true;
		int i=0;
		Db.update("DELETE FROM t_plant_cate WHERE plantbase_id=?",plantBase.getLong(ID));
		if(categoryIds!=null){
			String[] categoryarr=categoryIds.split(",");
			for(String categoryId:categoryarr){
				Db.update("INSERT INTO t_plant_cate(plantbase_id,category_id) values("+plantBase.getLong(ID)+","+categoryId+")");
				i++;
			}
			if(categoryarr.length!=i){
				isFlag=false; 
			}
		}
		return isFlag;
	}

}
