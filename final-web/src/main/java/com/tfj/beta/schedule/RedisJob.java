package com.tfj.beta.schedule;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.tfj.beta.core.Constant;
import com.tfj.beta.model.FrontEndRole;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.service.IndexService;
import com.tfj.beta.utils.PropertiesUtil;
import com.tfj.beta.utils.RedisClusterUtil;
import com.tfj.beta.utils.RedisUtil;

public class RedisJob implements Job{

	private IndexService indexService=new IndexService();
	Boolean flag = PropertiesUtil.getRedisClusterMode();
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// 查询复核打印二维码条件的结果表
				List<ProductResult> list = ProductResult.dao.findQRCodeList();
				if (null != list && list.size() > 0) {
					for (ProductResult productResult : list) {
						Long categoryId = productResult.getLong("category_id");
						Long productId = productResult.getLong("product_id");
						//缓存json
						//this.cacheJson(flag, categoryId, productId);
						this.cacheJson(flag, categoryId, productId,FrontEndRole.GUEST);//缓存三种不同的角色  （G:游客；B：企业用户；C：关注用户）
						this.cacheJson(flag, categoryId, productId,FrontEndRole.BUSINESS);
						this.cacheJson(flag, categoryId, productId,FrontEndRole.CUSTOMER);
					}

				}
	}

	public boolean fushCacheByCIdAndPId(long categoryId,long productId){
		boolean isFlag=false;
		try {
			this.cacheJson(flag, categoryId, productId,FrontEndRole.GUEST);//缓存三种不同的角色  （G:游客；B：企业用户；C：关注用户）
			this.cacheJson(flag, categoryId, productId,FrontEndRole.BUSINESS);
			this.cacheJson(flag, categoryId, productId,FrontEndRole.CUSTOMER);
		} catch (Exception e) {
			isFlag=true;
		}
		return isFlag;
	}
	public void cacheJson(boolean flag,Long categoryId,Long productId,String role_name){
		String indexJson=indexService.getIndexJson(String.valueOf(categoryId),String.valueOf(productId),role_name);
		String plantJson=indexService.getbranchJson(Constant.PLANT_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String superviseJson=indexService.getbranchJson(Constant.SUPERVISE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String warehouseJson=indexService.getbranchJson(Constant.WAREHOUSE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String saleJson=indexService.getbranchJson(Constant.SALE_PROCESS,String.valueOf(categoryId),String.valueOf(productId),role_name);
		String indexKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+role_name;
		String plantKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.PLANT_PROCESS+"+"+role_name;
		String superviseKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.SUPERVISE_PROCESS+"+"+role_name;
		String wareHouseKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.WAREHOUSE_PROCESS+"+"+role_name;
		String saleKey = String.valueOf(categoryId)+"+"+String.valueOf(productId)+"+"+Constant.SALE_PROCESS+"+"+role_name;

		if(flag){
			//Y
			
				//System.out.println("---->" + RedisClusterUtil.getJedis().info());
				//缓存前先删除原来的key
				RedisClusterUtil.delCacheKey(indexKey);
				RedisClusterUtil.cacheStrByKey(indexKey, indexJson);
				
				RedisClusterUtil.delCacheKey(plantKey);
				RedisClusterUtil.cacheStrByKey(plantKey, plantJson);
				
				RedisClusterUtil.delCacheKey(superviseKey);
				RedisClusterUtil.cacheStrByKey(superviseKey, superviseJson);
				
				RedisClusterUtil.delCacheKey(wareHouseKey);
				RedisClusterUtil.cacheStrByKey(wareHouseKey, warehouseJson);
				
				RedisClusterUtil.delCacheKey(saleKey);
				RedisClusterUtil.cacheStrByKey(saleKey, saleJson);
		}else {
			//N
				RedisUtil.delCacheKey(indexKey);
				RedisUtil.cacheStrByKey(indexKey, indexJson);
				
				RedisUtil.delCacheKey(plantKey);
				RedisUtil.cacheStrByKey(plantKey, plantJson);
				
				RedisUtil.delCacheKey(superviseKey);
				RedisUtil.cacheStrByKey(superviseKey, superviseJson);
				
				RedisUtil.delCacheKey(wareHouseKey);
				RedisUtil.cacheStrByKey(wareHouseKey, warehouseJson);
				
				RedisUtil.delCacheKey(saleKey);
				RedisUtil.cacheStrByKey(saleKey, saleJson);
			
			
		}
		
	}
}
