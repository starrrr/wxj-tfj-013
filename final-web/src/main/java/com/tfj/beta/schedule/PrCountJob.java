package com.tfj.beta.schedule;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jfinal.plugin.activerecord.Db;

public class PrCountJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Db.update("call pr_count");
	}

}
