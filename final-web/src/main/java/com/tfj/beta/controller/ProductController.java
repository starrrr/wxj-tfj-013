package com.tfj.beta.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.model.WeChatRole;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.PlantBaseService;
import com.tfj.beta.service.ProductService;
import com.tfj.beta.service.ProductValueService;
import com.tfj.beta.service.RedisCacheService;
import com.tfj.beta.service.RoleService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.service.WeChatService;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.JsonUtil;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/product", viewPath = "admin/product")
public class ProductController extends BaseController<ProductService> {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	private RoleService roleService = new RoleService();
	private CategoryService categoryService = getService(CategoryService.class);
	private OperationLogService operationLogService = new OperationLogService();
	private ProductValueService pvService = getService(ProductValueService.class);
	private RedisCacheService redisCacheService=getService(RedisCacheService.class);
	private ProductService productService=getService(ProductService.class);
	private WeChatService weChatService=getService(WeChatService.class);
	private UnitService unitService = new UnitService();
	private PlantBaseService plantBaseService = new PlantBaseService();
	
	private final static String ATTR_PAGELIST = "pageList";
	private final static String ATTR_PRODUCT = "product";
	private final static String ATTR_CATEGORY_LIST = "categoryList";
	
	private final static String ATTR_SORT_NAME = "sort_name";
	private final static String ATTR_SORT_TYPE = "sort_type";
	
	private final static String PAGE_PRODUCT_LIST = "productList.jsp";
	private final static String PAGE_PRODUCT_FORM = "productForm.jsp";
	
	private final static String PRODUCT_ID = "product_id";

	/**
	 * 种类数据列表
	 */
	public void index() {
		keepModel(Product.class);
		Product product = getModel(Product.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);
		String admin=getSessionAttr("adminType");
		String tag = getPara("tag");
		String page = getPara("page");
		setAttr("page", page);
		setSortAttr(tag);
		setAttr(ATTR_CATEGORY_LIST,  Category.dao.searchCategoryByUnits(units,admin));
		// operation Log
		setAttr(ATTR_PAGELIST, service.getProductPage(pageNumber, pageSize, product, tag,units,admin));
		if(StringUtils.isNotBlank(getPara("recordlog"))&&product.toJson().length()==2){//判断是否从导航sidebar进入
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render(PAGE_PRODUCT_LIST);
	}
	
	private void setSortAttr(String tag) {
		if (StringUtils.isNotBlank(tag)) {
			String[] sortInfo = tag.split(" ");
			// TODO 需要确保 length > 1 么?
			String attr_sort_name = sortInfo.length > 0 ? 
					sortInfo[0].trim() : StringUtils.EMPTY;
			setAttr(ATTR_SORT_NAME, attr_sort_name);
			
			String attr_sort_type = sortInfo.length > 1 ? 
					sortInfo[1].trim() : StringUtils.EMPTY;
			setAttr(ATTR_SORT_TYPE, attr_sort_type);
		} else {
			setAttr(ATTR_SORT_NAME, StringUtils.EMPTY);
			setAttr(ATTR_SORT_TYPE, StringUtils.EMPTY);
		}
	}
	
	public void view() {
		Long id = getParaToLong(Product.ID);
		Product product=service.findById(id);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units="true".equals(admin)?unitService.getUnitList():unitService.getUnitByUser(unitUsers);
		setAttr(ATTR_PRODUCT, null != id ? product : "");
		setAttr(ATTR_CATEGORY_LIST, Category.dao.searchCategoryByUnits(units,admin));
		//获取选择中菜单（在添加菜单显示）
		//List<PlantBase> plantBaseList = PlantBase.dao.getPlantBaseList();
		//获取当前单位的基地
		List<PlantBase> plantBaseList = PlantBase.dao.getPlantBaseByUnitId(null != id ?product.getLong("unit_id"):null);
		setAttr("plantBaseList", plantBaseList);
		//获取选择中字典（在添加菜单显示）
		List<Dict> dictList = Dict.dao.getDictListByType(Product.DICTYPE);
		setAttr("dictList", dictList);
		//获取选择中字典（在添加菜单显示）
		List<Dict> dictListdj = Dict.dao.getDictListByType(Product.DICTYPE_DJ);
		setAttr("dictListdj", dictListdj);
		
		//处理产品回显问题
		setAttr("plantBaseId",product==null?"":product.getLong(Product.PLANTBASE_ID));//找出当前中了   与前端做对比
		setAttr("categoryId",product==null?"":product.getLong(Product.CATEGORY_ID));//找出当前中了   与前端做对比

		setAttr("hasPV", String.valueOf(pvService.hasProductValue(id)));
		
		setAttr("unitList",units);
		render(PAGE_PRODUCT_FORM);
	}
	
	public void save() {
		Product product = getModel(Product.class);
		product.set("unit_id","".equals(getPara("unitid"))?null:getPara("unitid"));//单位
		PlantBase plantBase=PlantBase.dao.getPlantById(Integer.parseInt(product.getStr(Product.PLANTBASE_NAME)));
		if(plantBase==null){
			product.set(Product.PLANTBASE_NAME,"");
		}else{
			product.set(Product.PLANTBASE_NAME,plantBase.get(PlantBase.NAME));
			product.set(Product.PLANTBASE_ID, plantBase.getLong(PlantBase.ID));
		}
		Long oldid=product.getLong(PlantBase.ID);//判断原来是否存在ID，存在：修改；不存在：添加
		boolean isFlag=service.modifyProduct(product);
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			if(oldid!=null){//修改
				//刷新缓存
				Product pr=productService.findById(product.getLong(Product.ID));
				List<WeChatRole> weChatRoles=weChatService.findAllWeChatRose();
				for(WeChatRole weChatRole:weChatRoles){
					redisCacheService.cacheJson(pr.getLong(Product.CATEGORY_ID),pr.getLong(Product.ID), null,weChatRole.getStr(WeChatRole.NAME), true);
				}
				//operation log
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
				OperationType.PRODUCT_UPDATE.getCode(), OperationType.PRODUCT_UPDATE.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				//operation log
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
				OperationType.PRODUCT_ADD.getCode(), OperationType.PRODUCT_ADD.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
			
		}		
		redirect("/admin/product");
	}
	
	/**
	 * 复制功能
	 */
	public void copy() {
		Long product_id = getParaToLong(PRODUCT_ID);
		String product_name = getPara(Product.PRODUCT_NAME);
		String types = getPara("processType");
		String batches = getPara("batches");
		
		boolean result = service.copyProductAndValues(product_id, product_name, batches, types);
		
		Map<String, String> msg = new HashMap<String, String>();
		if (result) {
			msg.put("result", SUCCESS);
			msg.put("msg", "复制成功!");
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product_name,
			OperationType.PRODUCT_COPY.getCode(), OperationType.PRODUCT_COPY.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			
		} else {
			msg.put("result", FAILED);
			msg.put("msg", "复制失败!");
		}
		
		renderText(JsonUtil.parseObjectToJson(msg));
	}
	
	/**
	 * 复制功能页面跳转
	 */
	public void copyView() {
		Long id = getParaToLong(Product.ID);
		setAttr(ATTR_PRODUCT, null != id ? service.findById(id) : StringUtils.EMPTY);
		render("productCopyForm.jsp");
	}
	
	public void delete() {
		String id = getPara("ids");
		boolean isflag=false;
		if(service.queryAssociated(id)){//false:不存在关联	 true:存在关联
			isflag=true;
		}
		if(isflag){
			renderText("true");
		}else {
			Product product = Product.dao.findById(id);//项目没批量删除，固定写一个ID
			boolean isFlag=service.deleteProduct(product);
			//operation log
			if(isFlag){
				User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
						OperationType.PRODUCT_DELETE.getCode(), OperationType.PRODUCT_DELETE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
			
			renderText("false");
		}
	}
	
	public void audit() {

		String rid = this.getPara("rid");
		if (!StrKit.isBlank(rid)) {
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String currentDate = DateUtils.getCurrentTime();
			String[] ids = rid.split(",");
			for (String id : ids) {
				Product product = Product.dao.findById(id);
				if(product.getInt(Product.AUDITFLAG) == 0){
					//没有审核通过的
//					product.set(Product.NOTE, "audit");
					// 审核通过
					product.set(Product.AUDITFLAG, 1);
					// 审核者信息
					product.set(Product.AUDITORID, loginUser.getLong(User.ID));
					product.set(Product.AUDITOR, loginUser.getStr(User.NAME));
					product.set(Product.AUDIT_TIME, currentDate);
					boolean isFlag=service.modifyProduct(product);
					//operation log
					if(isFlag){
						operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
								OperationType.PRODUCT_AUDTI.getCode(), OperationType.PRODUCT_AUDTI.getName(),
								loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
					}
				}
			}
			
			redirect("/admin/product");
		}

	}
	
	public void release(){
		String rid = this.getPara("ids");
		if (!StrKit.isBlank(rid)) {
			String[] ids = rid.split(",");
			for (String id : ids) {
				/*Product product = Product.dao.findById(id);
				//释放审核
				product.set(Product.AUDITFLAG, 0);
				boolean isFlag=service.modifyProduct(product);*/
				//释放审核
				Product product = Product.dao.findById(id);
				boolean isFlag=service.releaseAudit(product);
				// opeartion log 
				if(isFlag){
					User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
					operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
							OperationType.RELEASE.getCode(), OperationType.RELEASE.getName(),
							loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
				}
			}
		}
		redirect("/admin/product");
	}
	
	/**
	 * 强制删除
	 * @throws SQLException 
	 */
	public void mandatorydelete() throws SQLException {
		String rid = this.getPara("ids");
		Product product=Product.dao.findById(rid);
		if (!StrKit.isBlank(rid)) {
			boolean isFlag=service.mandatoryDeleteProduct(rid);
			// opeartion log 
			if(isFlag){
				User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCT.getCode(), ModuleType.PRODUCT.getName(),product.getStr(Product.PRODUCT_NAME),
						OperationType.MANDATORY_DELETE.getCode(), OperationType.MANDATORY_DELETE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
		redirect("/admin/product");
	}

	//单位--->种类
	public void duleunitcategory(){
		String unit_id=getPara("unit_id");
		List<Category> categories=unitService.getCategoryByUnitId(unit_id);
		renderJson(categories);
	}
	//(单位&&种类)--->基地
	public void dulecategoryplantbase(){
		String unit_id=getPara("unit_id");
		String category_id=getPara("category_id");
		List<PlantBase> plantBases=plantBaseService.getPlantBaseByCategoryIdAndUnitId(unit_id,category_id);
		renderJson(plantBases);
	}
}
