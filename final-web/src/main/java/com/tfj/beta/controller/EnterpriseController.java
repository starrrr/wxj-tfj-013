package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import cn.dreampie.encription.EncriptionKit;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Enterprise;
import com.tfj.beta.model.User;
import com.tfj.beta.model.WeChatOpenId;
import com.tfj.beta.service.EnterpriseService;
import com.tfj.beta.service.OperationLogService;

@ControllerBind(controllerKey = "/admin/enterprise", viewPath = "admin/enterprise")
public class EnterpriseController extends BaseController {
	
	private EnterpriseService service=new EnterpriseService();
	private OperationLogService operationLogService = new OperationLogService();
	 
	/**
	 * 数据列表
	 */

	public void index() {
		keepModel(Enterprise.class);
		Enterprise enterprise = getModel(Enterprise.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		setAttr("pageList", service.getEnterprisePage(pageNumber, pageSize, enterprise));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&enterprise.toJson().length()==2){//判断是否从导航sidebar进入
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.ENTERPRISE.getCode(), ModuleType.ENTERPRISE.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("enterpriseList.jsp");

	}
	
	public void view() {
		Long id = getParaToLong(Enterprise.ID);
		setAttr("enterprise", null != id ? Enterprise.dao.findById(id) : "");
		
		render("enterpriseListForm.jsp");
	}
	
	public void save() {
		Enterprise enterprise = getModel(Enterprise.class);
		Long oldid=enterprise.getLong(Enterprise.ID);//判断原来是否存在ID，存在：修改；不存在：添加
		enterprise.put(Enterprise.COMPANY_PWD,EncriptionKit.encrypt(enterprise.getStr(Enterprise.COMPANY_PWD)).toUpperCase());
		boolean isFlag=service.modifyEnterprise(enterprise);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			if(oldid!=null){//修改
				operationLogService.recordOperationLog(loginUser,ModuleType.ENTERPRISE.getCode(), ModuleType.ENTERPRISE.getName(),enterprise.getStr(Enterprise.COMPANY_USER),
				OperationType.ENTERPRISE_UPDATE.getCode(), OperationType.ENTERPRISE_UPDATE.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				operationLogService.recordOperationLog(loginUser,ModuleType.ENTERPRISE.getCode(), ModuleType.ENTERPRISE.getName(),enterprise.getStr(Enterprise.COMPANY_USER),
				OperationType.ENTERPRISE_ADD.getCode(), OperationType.ENTERPRISE_ADD.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
		redirect("/admin/enterprise");
	}
	
	public void manage() {
		setAttr("user", getPara("company_user"));//把企业列表用户名携带到列表标题
		String nickname=getPara("bind_user");
		if(nickname!=null&&!"".equals(nickname)){//绑定用户名（微信nickname）
			dataPage(getPara("bind_user"));
		}else{
			Long id = getParaToLong(Enterprise.ID);
			int pageNumber = getParaToInt(Constant.PAGENUM, 1);
			int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
			Page<Enterprise> enterpriselist=Enterprise.dao.findManagePageById(pageNumber, pageSize, id);//查找绑定的微信用户信息
			setAttr("pageList", enterpriselist);
			render("enterpriseManageListForm.jsp");
		}
		
	}
	
	public void removeBind(){
		String openids = getPara("openid");//openid
		//String id = getPara("id");//id
		WeChatOpenId weChatOpenId=WeChatOpenId.dao.getMessByOpenid(openids);
		boolean isFlag=service.removeBind(openids);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.ENTERPRISE.getCode(), ModuleType.ENTERPRISE.getName(),weChatOpenId.getStr(WeChatOpenId.NICKNAME),
					OperationType.ENTERPRISE_REMOVEBIND.getCode(), OperationType.ENTERPRISE_REMOVEBIND.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		redirect("/admin/enterprise");
	} 
	
	
	/*public void deleteCheck() {
		String[] ids = getParaValues("ids");
		//判断企业是否绑定用户
		boolean hasBindUser=service.checkHasBindUser(ids[0]);
		if(hasBindUser){//true:存在
			renderText("true");
		}else{
			render("false");
		}
	}*/
	
	
	public void delete() {
		String[] ids = getParaValues("ids");
		//判断企业是否绑定用户
		Enterprise enterprise = Enterprise.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
		boolean isFlag=service.deleteEnterprise(ids);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.ENTERPRISE.getCode(), ModuleType.ENTERPRISE.getName(),enterprise.getStr(Enterprise.COMPANY_USER),
					OperationType.ENTERPRISE_DELETE.getCode(), OperationType.ENTERPRISE_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		redirect("/admin/enterprise");
	}
	 public void companyUserName(){
	    	String enterpriseusername=getPara(Enterprise.COMPANY_USER);
	    	Enterprise enterprise=service.checkEnterpriseUserIsExist(enterpriseusername);
	    	if(enterprise!=null){
	    		 if(enterpriseusername.equals(enterprise.getStr(Enterprise.COMPANY_USER))){
	            	renderText("true");
	            }else {
	            	renderText("false");
	    		}
	    	}else {
	    		renderText("false");
			}
	 }
	 
	 private void dataPage(String bind_user){
			Long id = getParaToLong("id");
			int pageNumber = getParaToInt(Constant.PAGENUM, 1);
			int pageSize = getParaToInt(Constant.PAGESIZE, 5);
			setAttr("pageList",service.getEnterprisePage(pageNumber, pageSize, id, bind_user));
			render("enterpriseManageListForm.jsp");
	}
	 
	 public void resetpasswordview(){
		 setAttr("id",getPara("id"));
		 render("enterpriseRSPWForm.jsp");
	 }
	 
	 public void resetpassword(){
		 keepModel(Enterprise.class);
		 Enterprise enterprise = getModel(Enterprise.class);
		 System.err.println(enterprise);
		 service.resetPW(enterprise);
		 redirect("/admin/enterprise");
	 }

}

