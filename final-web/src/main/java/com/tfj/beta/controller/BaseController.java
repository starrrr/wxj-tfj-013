package com.tfj.beta.controller;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import com.jfinal.core.Controller;
import com.tfj.beta.service.ServiceFactory;
import com.tfj.beta.utils.StaticFactory;

/**
 * 最基础的controller 用于controller以后的拓展
 * Created by Administrator on 2015/6/9.
 */

public class BaseController<V> extends Controller {
	
	protected Class<V> serviceClass;
	
	protected V service;
	
	protected final static String SUCCESS = "SUCCESS";
	
	protected final static String FAILED = "FAILED";
	
    @SuppressWarnings("unchecked")
	public BaseController() {
	    Type type = this.getClass().getGenericSuperclass();
	    if (type instanceof ParameterizedType) {
	        ParameterizedType parameterizedType = (ParameterizedType) type;
	        Type[] types = parameterizedType.getActualTypeArguments();
	        serviceClass = (Class<V>) types[0];
	        service = ServiceFactory.getStaticService(serviceClass);
	    }
	    
	}
    
    public V getService() {
    	return service;
    }
    
    public <E> E getService(Class<E> clazz) {
    	return ServiceFactory.getStaticService(clazz);
    }


    @Override
    public void render(String view) {
        setAttr("systemConfig", StaticFactory.getSystemConfigMap());
        setAttr("contextPath", getRequest().getContextPath());

        super.render(view);
    }
}
