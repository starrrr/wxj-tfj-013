package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.User;
import com.tfj.beta.model.WeChatRole;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.WeChatService;
import com.tfj.beta.utils.DateUtils;

@ControllerBind(controllerKey = "/admin/wechatrole", viewPath = "admin/wechat")
public class WeChatRoleController extends BaseController{
	
	private WeChatService service = new WeChatService();
	private OperationLogService operationLogService = new OperationLogService();
	/**
	 * 微信角色列表
	 */
	public void index() {
		keepModel(WeChatRole.class);
		WeChatRole weChatRole = getModel(WeChatRole.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		setAttr("pageList",
				service.getWeChatRolePage(pageNumber, pageSize, weChatRole));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&weChatRole.toJson().length()==2){//判断是否从导航sidebar进入
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.WECHATROLE.getCode(), ModuleType.WECHATROLE.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("wechatroleList.jsp");
	}
	
    public void view() {
        Long id = getParaToLong(WeChatRole.ID);
        setAttr("weChatRole", null != id ? WeChatRole.dao.findById(id) : "");
        render("wechatroleForm.jsp");
    }
   
    public void save() {
    	WeChatRole weChatRole = getModel(WeChatRole.class);
    	weChatRole.set(weChatRole.UPDATE_TIME, DateUtils.getCurrentTime());
    	Long oldid=weChatRole.getLong(WeChatRole.ID);//判断原来是否存在ID，存在：修改；不存在：添加
    	boolean isFlag=service.modifyModel(weChatRole);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			if(oldid!=null){//修改
				operationLogService.recordOperationLog(loginUser,ModuleType.WECHATROLE.getCode(), ModuleType.WECHATROLE.getName(),weChatRole.getStr(WeChatRole.NAME),
						OperationType.WECHATROLE_UPDATE.getCode(), OperationType.WECHATROLE_UPDATE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				operationLogService.recordOperationLog(loginUser,ModuleType.WECHATROLE.getCode(), ModuleType.WECHATROLE.getName(),weChatRole.getStr(WeChatRole.NAME),
						OperationType.WECHATROLE_ADD.getCode(), OperationType.WECHATROLE_ADD.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
        redirect("/admin/wechatrole");
    }
    public void delete() {
        String[] ids = getParaValues("ids");
        WeChatRole weChatRole = WeChatRole.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
        boolean isFlag=service.deleteRoles(ids);
        //operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.WECHATROLE.getCode(), ModuleType.WECHATROLE.getName(),weChatRole.getStr(WeChatRole.NAME),
					OperationType.WECHATROLE_DELETE.getCode(), OperationType.WECHATROLE_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
        redirect("/admin/wechatrole");
    }


}
