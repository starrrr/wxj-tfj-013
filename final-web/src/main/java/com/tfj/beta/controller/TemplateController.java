package com.tfj.beta.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.JFinal;
import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.model.view.TreeBean;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.ProductService;
import com.tfj.beta.service.RedisCacheService;
import com.tfj.beta.service.RoleService;
import com.tfj.beta.service.TemplateService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.utils.JsonUtil;
import com.tfj.beta.utils.ProcessTypeUtils;
import com.tfj.beta.utils.PropertiesUtil;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/template", viewPath = "admin/template")
public class TemplateController extends BaseController<TemplateService> {
	
	private final static String FIELDS_TYPE="FormFieldType";

	private RedisCacheService redisCacheService = new RedisCacheService();
	private OperationLogService operationLogService = new OperationLogService();
	private ProductService productService=new ProductService();
	private CategoryService categoryService=new CategoryService();
	private UnitService unitService = new UnitService();
	/**
	 * 种类数据列表
	 * 
	 */

	public void index() {
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);
		// 获取选择中草药种类菜单
		List<Category> categoryList = Category.dao.searchCategoryByUnits(units,admin);
		setAttr("categoryList", categoryList);
		
		//获取 form fieldType combox
		List<Dict> dictList = Dict.dao.getDictListByType(FIELDS_TYPE);
		setAttr("dictList", dictList);
		setAttr("path", PropertiesUtil.getBase());
		
		setAttr("Image_Pattern", PropertiesUtil.getImagePattern());
		
		// operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))){//判断是否从导航sidebar进入
     		operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
		
		render("templateTab.jsp");
	}

	public void getTemplateTree() {

		String processType = this.getPara("processType");
		String categoryId = this.getPara("category_id");

		// 树 TODO 暂时是 参数hardcode
		// TODO getTreeBean 方法为何是 static ?
		List<TreeBean> beanList = TemplateService.getTreeBean(processType,
				categoryId);
		String json = JsonUtil.parseObjectToJson(beanList);
		//System.err.println(json);
		this.renderJson(json);
	}

	/**
	 * 编辑后保存
	 * 
	 * @throws FileUploadException
	 */
	public void save() {
		String pid=null;
		String sort=null;
		String oldsort=null;//修改模板记录修改前的sort
		String template_id=null;//记录id 标记添加、修改   添加：null  
		boolean isFlag=false;//记录是否存icon;默认false：不存在
		boolean isIMG=false;//图片类型,图片不添加小图标
		String fileName = null;
		Template template = new Template();
		File file = new File(JFinal
				.me()
				.getServletContext()
				.getRealPath(
						"/upload/template/"
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date())));
		if(!file.isDirectory()){
			file.mkdirs();
		}
		try {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// DiskFileItemFactory中DEFAULT_SIZE_THRESHOLD=10240表示如果上传文件大于10K则会产生上传临时文件
			// 上传临时文件的默认目录为java.io.tmpdir中保存的路径，根据操作系统的不同会有区别

			ServletFileUpload upload = new ServletFileUpload(factory);
			// 设置文件上传的大小限制 100k
			upload.setFileSizeMax(102400);

			// 设置文件上传的头编码，如果需要正确接收中文文件路径或者文件名
			// 这里需要设置对应的字符编码，为了通用这里设置为UTF-8
			upload.setHeaderEncoding("UTF-8");

			// 解析请求数据包
			List<FileItem> fileItems = upload.parseRequest(this.getRequest());
			// 遍历解析完成后的Form数据和上传文件数据
			for (Iterator<FileItem> iterator = fileItems.iterator(); iterator
					.hasNext();) {
				FileItem fileItem = iterator.next();
				String fieldName = fileItem.getFieldName();
				String fieldValue = fileItem.getString("UTF-8");
				//System.out.println(fieldName+" ::::: "+fieldValue);
				if("template.icon".equals(fieldName)&&!"".equals(fieldValue)){//存在上传图标
					isFlag=true;
				}
				if("image".equals(fieldValue)){
					isIMG=true;
				}
				if("pid".equals(fieldName)){
					pid=fieldValue;
				}
				if("sort".equals(fieldName)){
					sort=fieldValue;
				}
				// 如果为上传文件数据
				if (!fileItem.isFormField()) {
					if(fileItem.getSize() > 0) {
						String suffix = fieldName.substring(fieldName.indexOf("."));
						fileName = String.valueOf(System.currentTimeMillis())+suffix;
						
						File savedFile = new File(file, fileName);
						FileUtils.copyInputStreamToFile(fileItem.getInputStream(),
								savedFile);
						if (null != fileName && StringUtils.isNoneBlank(fileName)) {// 如果不修改图片，则不用修改
							template.set(
									Template.ICON, "upload/template/"+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "/"+ fileName);
						}
					}
					
				}else{
				
					// id
					if (fieldName.equals("id")) {
						template_id=fieldValue;
						if(StringUtils.isNotBlank(fieldValue)){ //新增特殊考虑
							template.set("id", Long.parseLong(fieldValue));
						}
					} 
					else if (fieldName.equals("pid") && StringUtils.isNotBlank(fieldValue)) {
						template.set("pid", Long.parseLong(fieldValue));
					}else if (fieldName.equals("category_id") && StringUtils.isNotBlank(fieldValue)) {
						template.set("category_id", Long.parseLong(fieldValue));
					} else if (fieldName.equals("levelNum") && StringUtils.isNotBlank(fieldValue)) {
						template.set("levelNum", Long.parseLong(fieldValue));
					} else if (fieldName.equals("sort") && StringUtils.isNotBlank(fieldValue)) {
						template.set(Template.SORT, Integer.parseInt(fieldValue));
					} else if (fieldName.equals("index_show")&& StringUtils.isNotBlank(fieldValue)) {
						template.set("index_show", fieldValue.equals("0") ? false : true);
					} else {
						if(fieldName.equals("remark_note")){
							template.set("remark", fieldValue);
						}else{
							template.set(fieldName, fieldValue);
						}
					}
				}
				
			}
		} catch (Exception e) {

		}
		//System.err.println("savedFilePath===========>" + fileName);
		
		if(!isIMG&&!isFlag&&StringUtils.isBlank(template_id)){//判断是不是添加操作  如果是：图片为空时给出一个默认图标
			template.set(
					Template.ICON, "upload/template/example.icon");
		}
		if(template.getLong(Template.ID)!=null){
			oldsort=String.valueOf(service.dao.findById(template_id).getLong(Template.SORT));
		}
		boolean flag = service.modifyTemplate(template);
		templatesort(template,pid,sort,template_id,oldsort);
		if (flag) {
			//operate log
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			Category category=Category.dao.findById(template.getLong("category_id"));
			if(StringUtils.isBlank(template_id)){//添加：null
				operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),
						category.getStr(Category.NAME)+":"+ProcessTypeUtils.getProcessTypeName(template.getStr(Template.PROCESSTYPE))+":"+template.getStr(Template.KEY_NAME),
				OperationType.TEMPLATE_ADD.getCode(), OperationType.TEMPLATE_ADD.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {//修改：not null
				operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),
						category.getStr(Category.NAME)+":"+ProcessTypeUtils.getProcessTypeName(template.getStr(Template.PROCESSTYPE))+":"+template.getStr(Template.KEY_NAME),
				OperationType.TEMPLATE_UPDATE.getCode(), OperationType.TEMPLATE_UPDATE.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
			this.getResponse().setStatus(200);
			//System.err.println(JsonUtil.parseObjectToJson("{\"message\":\"SUCCESS\"}"));
			renderJson(JsonUtil.parseObjectToJson("{\"message\":\"SUCCESS\"}"));
		} else {
			renderJson(JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}"));
		}

	}
	
	public void getParentMaxCount() {
		Long pid = getParaToLong("pid");
		int max = service.setTemplatePidMaxCount(pid);
		Map<String, Object> retInfo = new HashMap<String, Object>();
		retInfo.put("result", SUCCESS);
		retInfo.put("max", max);
		renderText(JsonUtil.parseObjectToJson(retInfo));
	}

	public void deleteNode() {
		String id = this.getPara("id");
		Template template=Template.dao.findById(id);
		Category category=Category.dao.findById(template.getLong("category_id"));
		boolean flag = service.deleteNode(id);
		if (flag) {
			deleteAfterSort(template.getLong(Template.PID));
			//operate log
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),
					category.getStr(Category.NAME)+":"+ProcessTypeUtils.getProcessTypeName(template.getStr(Template.PROCESSTYPE))+":"+template.getStr(Template.KEY_NAME),
			OperationType.TEMPLATE_DELETE.getCode(), OperationType.TEMPLATE_DELETE.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			//renderJson(JsonUtil.parseObjectToJson("SUCCESS"));
			renderJson(JsonUtil.parseObjectToJson("SUCCESS"));
		} else {
			renderJson(JsonUtil.parseObjectToJson("FAIL"));
		}
	}
	
	public void dicmenushow(){
		List<Dict> dictmenulist = Dict.dao.getDictListGroupByNotes();
		renderJson(dictmenulist);
	}
	
	public void recordlog(){
		Category category=Category.dao.findById(getPara("category_id"));
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),category.getStr(Category.NAME),
		OperationType.CATEGORY_SELECT.getCode(), OperationType.CATEGORY_SELECT.getName(),
		loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		renderText("success");
	}
	
	/**
	 * 刷新所以种类的缓存，模板更新
	 */
	public void reflushrediscache(){
		/**
		 * true:出现异常，返回error
		 */
		
		String categoryId=getPara("category_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		Category category=categoryService.findCategoryById(categoryId);
		List<Product> products=productService.findProductsByCategoryId(categoryId);
		for(Product product:products){
			redisCacheService.fushCacheByCIdAndPId(Long.parseLong(categoryId), product.getLong(Product.ID));
		}
		operationLogService.recordOperationLog(loginUser,ModuleType.TEMPLATE.getCode(), ModuleType.TEMPLATE.getName(),category.getStr(Category.NAME),
		OperationType.TEMPLATE_CU.getCode(), OperationType.TEMPLATE_CU.getName(),
		loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		renderText("success");
	}
	
	public void templatesort(Template template,String pid,String sort,String id,String oldsort){
		List<Template> childs=service.getChildsByPid(Long.valueOf(pid));
		long template_id=template.getLong(Template.ID);//刚添加保存的模板数据
		int oldTemp=0;//标记原来数据的位置
		int newTemp=0;//标记新数据位置
		//1 4 3 4 5
		if("".equals(id)||id==null){//增加
			if(Long.valueOf(sort)>=childs.size()){//当输入的sort大于child时   sort=child.size()+1
				template.set(Template.SORT,childs.size()).update();
			}
			for(int i = 0; i < childs.size(); i++){
				long childSort=childs.get(i).getLong(Template.SORT);
				long childId=childs.get(i).getLong(Template.ID);
				if(template_id!=childId&&childSort>=Long.valueOf(sort)){
					childs.get(i).set(Template.SORT,childs.get(i).getLong(Template.SORT)+1).update();
				}
			}
		}else {//修改
			for(int i=0;i<childs.size();i++){
				if(template_id==childs.get(i).getLong(Template.ID)){
					oldTemp=i;
				}
			}
			for(int i=0;i<childs.size();i++){
				for(int j=1;j<childs.size()-i;j++) {
					if(template_id!=childs.get(j).getLong(Template.ID)){
						if(Long.valueOf(sort)==childs.get(j).getLong(Template.SORT)){
							newTemp=j;
						}
					}
				}
			}
			Template o=childs.get(oldTemp);
			Template n=childs.get(newTemp);
			//下  后面   上  前面
			if(Long.valueOf(sort)>=Long.valueOf(oldsort)){//作用：判断修改后的sort是增大还是减小；增大：新增加的数据sort与相等数据的sort，放在后面，否则，前面
				if(oldTemp<newTemp){//如果修改后的数据在前面，则位置交换后面相同sort的数据
					childs.set(newTemp,o);
					childs.set(oldTemp,n);
				}else {
					childs.set(oldTemp,o);
					childs.set(newTemp,n);
				}
				for(int i=0;i<childs.size();i++){
					Template child=childs.get(i);
					System.err.println(child);
				}
			}else {//上
				if(oldTemp<newTemp){//如果修改后的数据在前面，则位置交换后面相同sort的数据
					childs.set(newTemp,n);
					childs.set(oldTemp,o);
				}else {
					childs.set(oldTemp,n);
					childs.set(newTemp,o);
				}
			}
			for(int i=0;i<childs.size();i++){
				Template child=childs.get(i);
				System.err.println(child);
				child.set(Template.SORT, i+1);
				child.update();
			}
		}
	}
	/**
	 * 删除后排序
	 * @param pid
	 */
	public void deleteAfterSort(long pid){
		List<Template> childs=service.getChildsByPid(pid);
		for(int i=0;i<childs.size();i++){
			Template child=childs.get(i);
			child.set(Template.SORT, i+1);
			child.update();
		}
	}
	/*
	
	*
	*for(int i = 0; i < childs.size(); i++){
				if(Long.valueOf(oldsort)==childs.get(i).getLong(Template.SORT)){
					oldTemp=i;
				}
			}
			for(int i = 0; i < childs.size(); i++){
				if(Long.valueOf(sort)==childs.get(i).getLong(Template.SORT)){
					newTemp=i;
				}
			}
			//1 5 3 4 5 6
			if(newTemp>oldTemp){
				System.err.println(newTemp+"----"+oldTemp);
				for(int i = oldTemp; i <=newTemp+1; i++){
					childs.get(i).set(Template.SORT,childs.get(i).getLong(Template.SORT)).update();
	
				}
			}
	*/
	
}
