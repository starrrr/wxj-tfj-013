package com.tfj.beta.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.EncryptionKit;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitCate;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.RoleService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.service.UserService;

/**
 * Created by Administrator on 2015/6/15.
 */
@ControllerBind(controllerKey = "/admin/user", viewPath = "admin/user")
public class UserController extends BaseController {

    private UserService userService = new UserService();
    private RoleService roleService = new RoleService();
    private UnitService unitService = new UnitService();
    private OperationLogService operationLogService = new OperationLogService();

    public void index() {
        keepModel(User.class);
        User user = getModel(User.class);
        int pageNumber = getParaToInt(Constant.PAGENUM, 1);
        int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
        setAttr("pageList", userService.getUserPage(pageNumber, pageSize, user));
        // operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))&&user.toJson().length()==2){//判断是否从导航sidebar进入
     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
     		operationLogService.recordOperationLog(loginUser,ModuleType.USER.getCode(), ModuleType.USER.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
        render("userList.jsp");
    }

    /**
     * 查看用户
     */
    public void view() {
        Long id = getParaToLong(User.ID);
        setAttr("user", null != id ? User.dao.findById(id) : "");
        setAttr("roleStr", null != id ? userService.getUserRole(id) : "");
        //获取当前登陆用户信息
        Subject subject = SecurityUtils.getSubject();
        User loginUser = (User) subject.getPrincipal();
        Role role=Role.dao.findRoleIdByUserId(loginUser.getLong(User.ID));  
        setAttr("roleType",Role.dao.findById(role.getInt("role_id")).getStr("name"));
        setAttr("roleList", roleService.getRoleList());
        setAttr("unitList", unitService.getUnitList());
        duleUnitUser(id);
        render("userForm.jsp");
    }
    //处理编辑用户回显问题
    private void duleUnitUser(Long id) {
    	if(id!=null){
			List<UnitUser> unitUsers=UnitUser.dao.searchUnitUserByUserId(String.valueOf(id));
			List<Unit> units=unitService.getUnitByUser(unitUsers);
			String unitNames="";
			String unitIds="";
			for(Unit unit:units){
				String ids=String.valueOf(unit.getLong(Unit.ID));
				String names=unit.getStr(Unit.NAME);
				unitIds=unitIds+","+ids;
				unitNames=unitNames+","+names;
			}
			setAttr("unitNames","".equals(unitNames)?"":unitNames.substring(1));
			setAttr("unitIds","".equals(unitIds)?"":unitIds.substring(1));
		}
	}

	/**
     * 修改或保存user
     */
    public void save() {
        User user = getModel(User.class);
        Long oldid=user.getLong(User.ID);//判断原来是否存在ID，存在：修改；不存在：添加
        String[] roleIds = getParaValues("rids");
        String uids=getPara("unitids");//获取单位并处理
		String[] unitIds=null;
		if(uids.indexOf(",")>0){
			unitIds=uids.split(",");
		}else{
			unitIds=new String[1];
			unitIds[0]="".equals(uids)?null:uids;
		}
        boolean isFlag=userService.modifyUser(user, roleIds,unitIds);
        //operation log
      	if(isFlag){
      		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
      		if(oldid!=null){//修改
      			operationLogService.recordOperationLog(loginUser,ModuleType.USER.getCode(), ModuleType.USER.getName(),user.getStr(User.USERNAME),
      					OperationType.USER_UPDATE.getCode(), OperationType.USER_UPDATE.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}else {
      			operationLogService.recordOperationLog(loginUser,ModuleType.USER.getCode(), ModuleType.USER.getName(),user.getStr(User.USERNAME),
      					OperationType.USER_ADD.getCode(), OperationType.USER_ADD.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}
      	}        
        redirect("/admin/user");
    }

    /**
     * 删除用户
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        User user= User.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
        boolean isFlag=userService.deleteUsers(ids);
        //operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.USER.getCode(), ModuleType.USER.getName(),user.getStr(User.USERNAME),
					OperationType.USERCANCELLATION.getCode(), OperationType.USERCANCELLATION.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
        redirect("/admin/user");
    }
    
    public void pwView(){
    	String typeId=getPara("id");
    	Subject subject = SecurityUtils.getSubject();
        User loginUser = (User) subject.getPrincipal();
    	setAttr("typeId", typeId);
        setAttr("user",loginUser);
    	render("pwForm.jsp");
    }
    
    public void pwsave(){
    	keepModel(User.class);
        User loginUser = getModel(User.class);
		String admin=getSessionAttr("adminType");
    	String oldpw=getPara("oldpassword");
    	String newpw=getPara("newpassword");
    	String repeatpw=getPara("repeatpassword");
    	String email=getPara("email");
    	String mobile=getPara("mobile");
    	if(newpw.trim().equals(repeatpw.trim())){
    		//获取当前登陆用户信息
            Subject subject = SecurityUtils.getSubject();
            loginUser = (User) subject.getPrincipal();
            if(EncryptionKit.md5Encrypt(oldpw).equals(loginUser.getStr(User.PASSWORD))){
            	loginUser.set(User.PASSWORD, EncryptionKit.md5Encrypt(newpw));
            	loginUser.set("email",email);
            	loginUser.set("mobile",mobile);
            	boolean isFlag=loginUser.update();
            	if(isFlag){
    				operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),"",
    						OperationType.PWUPDATE.getCode(), OperationType.PWUPDATE.getName(),
    						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
    			}
            }
    	}
    	redirect("/admin");
    	
    }
    public void checkPw(){
    	String oldpw=getPara("pw");
    	//获取当前登陆用户信息
        Subject subject = SecurityUtils.getSubject();
        User loginUser = (User) subject.getPrincipal();
        if(EncryptionKit.md5Encrypt(oldpw).equals(loginUser.getStr(User.PASSWORD))){
        	renderText("true");
        }else {
        	renderText("false");
		}
    }
   
   public void checkUserName(){
    	String username=getPara(User.USERNAME);
    	User user=userService.checkUserIsExist(username);
    	if(user!=null){
    		 if(username.equals(user.getStr(User.USERNAME))){
            	renderText("true");
            }else {
            	renderText("false");
    		}
    	}else {
    		renderText("false");
		}
    }
   public void checkUserRole(){
   	String roleId=getPara("roleid");
   	Role role=Role.dao.findById(roleId);
   	if(role!=null){
   		 if("管理员".equals(role.getStr(Role.NAME))){
           	renderText("true");
           }else {
           	renderText("false");
   		}
   	}else {
   		renderText("false");
		}
   }
}
