package com.tfj.beta.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.jfinal.core.JFinal;
import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.ChartGraphicsService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.ProductResultService;
import com.tfj.beta.service.RoleService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.utils.PropertiesUtil;
import com.tfj.beta.utils.qrcode.MatrixToImageWriter;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/productResult", viewPath = "admin/productResult")
public class ProductResultController extends BaseController {
	
	private static final Logger log = LoggerFactory.getLogger(ProductResultController.class);

	private ProductResultService service = new ProductResultService();
	private OperationLogService operationLogService = new OperationLogService();
	private ProductResult productResultdao=new ProductResult();
	private RoleService roleService = new RoleService();
	private UnitService unitService = new UnitService();
	private ChartGraphicsService chartGraphicsService=new ChartGraphicsService();
	private volatile static BufferedImage logo;

	/**
	 * 
	 * 产品整体详细信息
	 */

	public void index() {

		// 判断是否有权限，如果没有权限 转到 没有权限的页面
		// 获取选择中草药种类菜单
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);
		String admin=getSessionAttr("adminType");
		List<Category> categoryList = Category.dao.searchCategoryByUnits(units,admin);
		setAttr("categoryList", categoryList);
		String productName = this.getPara("product_name");
		if(StringUtils.isNotBlank(productName)){
			setAttr("productName", productName);
		}
		String flowBatchFlag = this.getPara("flow_batch_flag");
		if(StringUtils.isNotBlank(flowBatchFlag)){
			setAttr("flowBatchFlag", flowBatchFlag);
		}
		keepModel(ProductResult.class);
		ProductResult productResult = getModel(ProductResult.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		String tag=getPara("tag");
		String page=getPara("page");
		setSortAttr(tag);
		setAttr("tag", tag);
		setAttr("page", page);
		setAttr("pageList", service.getProductResultPage(admin,units,pageNumber, pageSize,
				productResult,productName,flowBatchFlag,tag));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&productResult.toJson().length()==2){//判断是否从导航sidebar进入
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTRESULT.getCode(), ModuleType.PRODUCTRESULT.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("productResultList.jsp");
	}
	private void setSortAttr(String tag) {
		if (StringUtils.isNotBlank(tag)) {
			String[] sortInfo = tag.split(" ");
			// TODO 需要确保 length > 1 么?
			if (sortInfo.length > 0) {
				setAttr("sort_name", sortInfo[0].trim());
			} else {
				setAttr("sort_name", StringUtils.EMPTY);
			}
			if (sortInfo.length > 1) {
				setAttr("sort_type", sortInfo[1].trim());
			} else {
				setAttr("sort_type", StringUtils.EMPTY);
			}
		} else {
			setAttr("sort_name", StringUtils.EMPTY);
			setAttr("sort_type", StringUtils.EMPTY);
		}
	}
	
	public void view() {
		Long id = getParaToLong(ProductResult.ID);
		setAttr("productResult", null != id ? ProductResult.dao.findById(id)
				: "");
		render("productResultForm.jsp");
	}
	
	public void showQRcode(){
		String flow_batch_num=this.getPara(ProductResult.FLOW_BATCH_NUM);
		ProductResult productResult=productResultdao.findDataByflownum(flow_batch_num);
		Product product=Product.dao.findById(productResult.getLong(productResult.PRODUCT_ID));
		//生成二维码
		String url=null;
		String smallurl=null;//展示全屏图片的URL
		String path=JFinal.me().getServletContext().getRealPath("/upload/QRCode/");
		File f=new File(path);
		if(!f.exists()){
			f.mkdirs();
		}
		if(productResult.getStr("remark")!=null&&productResult.getStr("remark").indexOf(",")>-1){
			url=PropertiesUtil.getBase()+productResult.getStr("remark").split(",")[0];
			smallurl=PropertiesUtil.getBase()+productResult.getStr("remark").split(",")[1];
			File file=new File(path+'/'+url.split("upload/QRCode/")[1]);//只要判断一个就可以了
			if(!file.exists()){
				url=PropertiesUtil.getBase()+createQRcode(flow_batch_num,product,productResult).get("image");
				smallurl=PropertiesUtil.getBase()+createQRcode(flow_batch_num,product,productResult).get("smallimage");
			}
		}else {
			url=PropertiesUtil.getBase()+createQRcode(flow_batch_num,product,productResult).get("image");
			smallurl=PropertiesUtil.getBase()+createQRcode(flow_batch_num,product,productResult).get("smallimage");
		}
		setAttr("url", url);
		setAttr("smallurl", smallurl);
        // operation Log
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
     	operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTRESULT.getCode(), ModuleType.PRODUCTRESULT.getName(),product.getStr(Product.PRODUCT_NAME),
     			OperationType.SHOW.getCode(), OperationType.SHOW.getName(),
     			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		render("showQRcode.jsp");
	}

	public Map<String, String> createQRcode(String flow_batch_num,Product product,ProductResult productResult){
		Map<String, String> imagemap=new HashMap<String, String>();//储存对应两张图片的路径返回
		String text=PropertiesUtil.getBase()+"admin/index/redirectWeChat?flow_batch_num="+flow_batch_num;//访问前端主页路径
		/*try {
			text=PropertiesUtil.getBase()+"admin/index/redirectWeChat?flow_batch_num="+flow_batch_num;//访问前端主页路径
			text="https://open.weixin.qq.com/connect/oauth2/authorize?appid="+PropertiesUtil.getAppId()+"&redirect_uri="+URLEncoder.encode(text,"UTF-8")
					+"&response_type=code&scope=snsapi_userinfo&state=STATE";
		} catch (UnsupportedEncodingException e1) {
		}*/
		log.debug("URL:"+text);
		System.err.println("url:"+text);
		int width = 299;
		int height = 299;
		// 二维码的图片格式
		String format = "png";
		/**
		 * 设置二维码的参数
		 */
		HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		// 内容所使用编码
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
			// 生成二维码(要创建两个，一个显示，一个放大);
			Map<String, String> filePath = getOutputFilePath(flow_batch_num);
			File smallfile=new File(filePath.get("fileNameSmall"));
			File file = new File(filePath.get("fileName"));
			if (getLogo() != null) {
				MatrixToImageWriter.writeToFile(bitMatrix, format, file, getLogo());
				MatrixToImageWriter.writeToFile(bitMatrix, format, smallfile, getLogo());
				chartGraphicsService.graphicsGeneration(file,flow_batch_num,product);
			} else {
				MatrixToImageWriter.writeToFile(bitMatrix, format, file);
				MatrixToImageWriter.writeToFile(bitMatrix, format, smallfile, getLogo());
				chartGraphicsService.graphicsGeneration(file,flow_batch_num,product);
			}
			String image= "upload/QRCode/" + file.getName();
			String smallimage= "upload/QRCode/" + smallfile.getName();
			productResult.set("remark", image+","+smallimage);
			productResult.update();
			imagemap.put("image", image);
			imagemap.put("smallimage", smallimage);
			return imagemap;
		} catch (Exception e) {
			e.printStackTrace();
			renderNull();
		}
		return null;
	}
	
	private Map<String, String> getOutputFilePath(String flow_batch_num) {
		Map<String, String> map=new HashMap<String, String>();
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String dir = getTmpPath();
		String fileName = dir+ "QRCode_" + loginUser.getStr(User.USERNAME) + "_"+ flow_batch_num + ".png";
		String fileNameSmall = dir+ "QRCode_" + loginUser.getStr(User.USERNAME) + "_"+ flow_batch_num + "small.png";
		map.put("fileName", fileName);
		map.put("fileNameSmall", fileNameSmall);
		return map;
	}
	
	private String getTmpPath() {
		String webPath = getSession().getServletContext().getRealPath("/"); 
		return webPath + "/upload/QRCode/";
	}
	private BufferedImage getLogo() {
		try {
			if (logo == null) {
				String webPath = getSession().getServletContext().getRealPath("/"); 
				File file = new File(webPath + "/static/logo/logo.png");
				logo = ImageIO.read(file);
			}
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}
		return logo;
	}
	
	
	public void FullScreenQRcode(){
		setAttr("smallurl", getPara("smallurl"));
		render("showFullScreenQRcode.jsp");
	}
	
	public String doloadImage(){
		InputStream in =null;
		PrintWriter pw =null;
		String imageurl= getPara("smallurl");
        String fileName = imageurl.substring(imageurl.lastIndexOf("/")+1);
        HttpClient client = new HttpClient();  
        GetMethod httpGet = new GetMethod(imageurl);  
         try {  
             client.executeMethod(httpGet);  
             in = httpGet.getResponseBodyAsStream();  
             HttpServletResponse response = getResponse();
             response.setHeader("Content-Type","application/octet-stream");
             response.setHeader("Content-disposition","attachment;filename="+(fileName.split("_")[2]).replaceAll("small",""));
             pw=response.getWriter();
             int len = 0;  
             while((len=in.read())!= -1){  
            	 pw.write(len);  
             }
         }catch(Exception e) { 
         }finally{  
        	 try {
        		 if(in!=null){
        			 in.close();
        		 }
				if(pw!=null){
					pw.close();
				}
				if(httpGet!=null){
					httpGet.releaseConnection();  
				}
			} catch (IOException e) {
			}  
         }
		return fileName;
	}
}