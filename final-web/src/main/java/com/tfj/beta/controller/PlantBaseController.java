package com.tfj.beta.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.JFinal;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.County;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.PlantCate;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.PlantBaseService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.PropertiesUtil;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/plantbase", viewPath = "admin/plantbase")
public class PlantBaseController extends BaseController {

	private PlantBaseService service = new PlantBaseService();
	private OperationLogService operationLogService = new OperationLogService();
	private UnitService unitService = new UnitService();
	private CategoryService categoryService=new CategoryService();
	/**
	 * 基地数据列表
	 */

	public void index() {
		keepModel(PlantBase.class);
		PlantBase plantBase = getModel(PlantBase.class);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);
		String admin=getSessionAttr("adminType");
		//获取选择中草药种类菜单
		List<Category> categoryList = Category.dao.searchCategoryByUnits(units,admin);
		setAttr("categoryList", categoryList);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		setAttr("pc_id", (getPara("plantBase.category_id")==null||"".equals( getPara("plantBase.category_id")))?null:Long.parseLong(getPara("plantBase.category_id")));//种类回显标记
		setAttr("pageList", service.getPlantBasePage(pageNumber, pageSize, plantBase,units,admin));
		// operation Log
		
		if(StringUtils.isNotBlank(getPara("recordlog"))&&plantBase.toJson().length()==2){//判断是否从导航sidebar进入
			operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("plantBaseList.jsp");
	}

	public void view() {
		
		Long id = getParaToLong(Dict.ID);
		setAttr("plantBase", null != id ? PlantBase.dao.findById(id) : "");
		
		//获取选择中草药种类菜单（在添加菜单显示）
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units="true".equals(admin)?unitService.getUnitList():unitService.getUnitByUser(unitUsers);
		List<Category> categoryList = Category.dao.searchCategoryByUnits(units,admin);
		setAttr("categoryList", categoryList);
		
		//获取选择中字典（在添加菜单显示）
		List<Dict> dictList=Dict.dao.getDictListByType(PlantBase.STLX);
		setAttr("dictList", dictList);
		
		//单位
		setAttr("unitList",units);
		
		setAttr("Image_Pattern", PropertiesUtil.getImagePattern());
		//省
		setAttr("countyList",County.dao.findAll());
		
		//处理基地种类
		if(id!=null){
			List<PlantCate> plantCates=PlantCate.dao.getPlantCate(String.valueOf(id));
			List<Category> categories=categoryService.getCategoryPlantCate(plantCates);
			String unitIds="";
			if(categories.size()>0){
				for(Category category:categories){
					String ids=String.valueOf(category.getLong(Category.ID));
					unitIds=unitIds+","+ids;
				}
				setAttr("categoryIds",unitIds.substring(1));
			}else {
				setAttr("categoryIds","");
			}
			
		}
		//读取properties
		String host = PropertiesUtil.getBase();
		setAttr("host",host);
		render("plantBaseForm.jsp");
	}

	public void save() {
		//当前工作目录
		PlantBase plantBase = getModel(PlantBase.class);
		//以日期的格式创建存储图片的文件夹（yyyymmdd）
		File file=new File(JFinal.me().getServletContext().getRealPath("/upload/plantbase/"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())));
		file.mkdirs();
		String newname="";
		String host = PropertiesUtil.getBase();
		UploadFile uploadFile=getFile("plantBase.base_map",file.getPath());//保存的图片
		if(uploadFile!=null){
			newname=System.currentTimeMillis()+uploadFile.getFileName();//修改后的名字
			uploadFile.getFile().renameTo(new File(file.getPath(),newname));//修改文件夹中的名字
		}
		
		//因为通过了multipart/form-data方式提交form，所以要重新封装实体信息获取数据，否则会抛空
		plantBase=createEntity(plantBase);
		
		if(uploadFile!=null){//如果不修改图片，则不用修改
			plantBase.set(plantBase.BASE_MAP,"upload/plantbase/"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+"/"+newname);
		}
		//operation log
		String oldid=getPara("plantBase.id");
		duleunit(plantBase);//处理单位
		String categoryIds=getPara("plantBase.category_id");
		boolean isFlag=service.modifyPlantBase(plantBase,uploadFile,categoryIds);
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			if(oldid==null||"".equals(oldid)){//添加
				operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),plantBase.getStr(PlantBase.NAME),
						OperationType.PLANTBASE_ADD.getCode(), OperationType.PLANTBASE_ADD.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),plantBase.getStr(PlantBase.NAME),
						OperationType.PLANTBASE_UPDATE.getCode(), OperationType.PLANTBASE_UPDATE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
		
		if(uploadFile!=null){
			plantBase.set(plantBase.BASE_MAP, host+"upload/plantbase/"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+"/"+newname);
		}
		redirect("/admin/plantbase");
	}
	public void delete() {
		boolean isflag=false;
        String[] ids = getParaValues("ids");
        for(String id:ids){//删除数据时，把图片删除
        	String picture="";
        	PlantBase plantBase=PlantBase.dao.findById(id);
        	if(plantBase.get(plantBase.BASE_MAP)!=null){//有可能数据库里面数据没图片，需要判断是否为空，防止抛空指针
        		picture=plantBase.get(plantBase.BASE_MAP).toString();
        		//判断picture是否唯一，唯一：删除
        		List<PlantBase> plantBases=PlantBase.dao.findPlantBaseByMap(picture);
            	if(plantBases.size()==1){//copy基地会把图片路径copy  共用同一图片
            		picture=picture.substring(picture.lastIndexOf("/")+1);
                	service.deletepicture(picture);
            	}
        	}
        }
        
		for(String id:ids){
			if(service.queryAssociated(id)==true){//false:不存在关联	 true:存在关联
				isflag=true;
				break;
			}
		}
		if(isflag){
			renderText("true");
		}else {
			PlantBase plantBase = PlantBase.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
			boolean isFlag=service.deletePlantBase(ids,plantBase);
			//operation log
			if(isFlag){
				User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
				operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),plantBase.getStr(PlantBase.NAME),
						OperationType.PLANTBASE_DELETE.getCode(), OperationType.PLANTBASE_DELETE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
			
			
			renderText("false");
		}
	}
	
	public void audit() {
		String rid = this.getPara("rid");
		if (!StrKit.isBlank(rid)) {
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String currentDate = DateUtils.getCurrentTime();
			String[] ids = rid.split(",");
			for (String id : ids) {
				PlantBase plantBase = PlantBase.dao.findById(id);
				if(plantBase.getInt(PlantBase.AUDITFLAG)==0){
					//没有审核通过的
					//plantBase.set(PlantBase.NOTE, "audit");
					// 审核通过
					plantBase.set(PlantBase.AUDITFLAG, 1);
					// 审核者信息
					plantBase.set(PlantBase.AUDITORID, loginUser.getLong(User.ID));
					plantBase.set(PlantBase.AUDITOR, loginUser.getStr(User.NAME));
					plantBase.set(PlantBase.AUDIT_TIME, currentDate);
					boolean isFlag=service.modifyCategoryaudit(plantBase);
					//operation log
					if(isFlag){
						operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),plantBase.getStr(PlantBase.NAME),
								OperationType.PLANTBASE_AUDTI.getCode(), OperationType.PLANTBASE_AUDTI.getName(),
								loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
					}
				}
				
			}
			
		}
		redirect("/admin/plantbase");
	}
	
	public void checkMap(){
    	String rid=getPara("rid");
    	if (!StrKit.isBlank(rid)) {
			String[] ids = rid.split(",");
			for (String id : ids) {
				PlantBase plantBase = PlantBase.dao.findById(id);
				if(StringUtils.isEmpty(plantBase.getStr(PlantBase.BASE_MAP))){
					renderText(plantBase.getStr(PlantBase.NAME));//返回不上传图的基地
					break;
				}else {
					renderText("true");
				}
			}
		}
    }
	
	private PlantBase createEntity(PlantBase plantBase) {
		// TODO Auto-generated method stub
		if(getPara("plantBase.id")==null||"".equals(getPara("plantBase.id"))){//添加
			plantBase.set(plantBase.ID,null);
		}else {//修改
			plantBase.set(plantBase.ID,Long.parseLong(getPara("plantBase.id")));
		}
		plantBase.set(plantBase.CODE,getPara("plantBase.base_code"));
		plantBase.set(plantBase.NAME,getPara("plantBase.base_name"));
		plantBase.set(plantBase.COUNTY_CODE,getPara("plantBase.county_code"));
		plantBase.set(plantBase.BASE_LOCATION,getPara("plantBase.base_location"));
		plantBase.set(plantBase.BASE_AREA,getPara("plantBase.base_area"));
		plantBase.set(plantBase.LONGITUDE,getPara("plantBase.longitude"));
		plantBase.set(plantBase.LATITUDE,getPara("plantBase.latitude"));
		String s=getPara("plantBase.soil");
		if(getPara("plantBase.soil")==null||"".equals(getPara("plantBase.soil"))){
			plantBase.set(plantBase.SOIL,null);
		}else {
			plantBase.set(plantBase.SOIL,getPara("plantBase.soil"));
		}
		//plantBase.set(plantBase.SOIL,getPara("plantBase.soil"));
		
		plantBase.set(plantBase.BASE_IN_CHARGE,getPara("plantBase.base_in_charge"));
		plantBase.set(plantBase.BASE_HEIGHT,getPara("plantBase.base_height"));
		plantBase.set(plantBase.HERBAL_TYPE,getPara("plantBase.category_id"));
		plantBase.set(plantBase.HERBAL_NAME,getCategoryNamesById(getPara("plantBase.category_id")));
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		plantBase.set(plantBase.OPERATOR, loginUser.getStr(User.USERNAME));//记录操作人员
		plantBase.set(plantBase.OPERATOR_TIME,DateUtils.getCurrentTime());//记录操作时间
		if("copy".equals(getPara("copy"))){//判断是否为copy地图
			plantBase.set(plantBase.BASE_MAP,getPara("plantBase.base_map"));
		}
		return plantBase;
	}
	private String getCategoryNamesById(String ids) {
		String Names=Category.dao.findCategoryNameByIds(ids);
		return Names;
	}
	public void release(){
		String rid = this.getPara("ids");
		if (!StrKit.isBlank(rid)) {
			String[] ids = rid.split(",");
			for (String id : ids) {
				PlantBase plantBase = PlantBase.dao.findById(id);
				//释放审核
				plantBase.set(PlantBase.AUDITFLAG, 0);
				boolean isFlag=service.modifyCategoryaudit(plantBase);
				// opeartion log 
				if(isFlag){
					User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
					operationLogService.recordOperationLog(loginUser,ModuleType.PLANT_BASE.getCode(), ModuleType.PLANT_BASE.getName(),plantBase.getStr(PlantBase.NAME),
							OperationType.RELEASE.getCode(), OperationType.RELEASE.getName(),
							loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
				}
			}
		}
		redirect("/admin/plantbase");
	}
	
	public void duleunit(PlantBase plantBase){
		plantBase.set("unit_id","".equals(getPara("unitid"))?null:getPara("unitid"));
		/*String admin=getSessionAttr("adminType");
		if(admin=="true"||"true".equals(admin)){//管理员
			plantBase.set("unit_id","".equals(getPara("unitid"))?null:getPara("unitid"));
		}else{
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			plantBase.set("unit_id",loginUser.getLong("unit_id"));
		}*/
	}
	
	//单位--->种类
	public void duleunitcategory(){
			String unit_id=getPara("unit_id");
			String plantbase_id=getPara("plantbase_id");
			List<Category> categories=unitService.getCategoryByUnitId(unit_id);
			List<PlantCate> plantCates=PlantCate.dao.getPlantCate(plantbase_id);
			StringBuilder sb = new StringBuilder();
			if(categories.size()>0){
				sb.append("[");
		        for (Category category : categories) {
		            sb.append("{\"id\":");
		            sb.append(category.getLong(Category.ID));
		            sb.append(",\"name\":\"");
		            sb.append(category.getStr(Category.NAME));
		            sb.append("\"");
		            sb.append(",\"open\":true");
		            for(PlantCate plantCate : plantCates){
		                if(plantCate.getLong(PlantCate.CATEGORY_ID) == category.getLong(Category.ID))
		                    sb.append(",\"checked\":true");
		            }
		            sb.append("}");
		            sb.append(",");
		        }
		        sb.deleteCharAt(sb.length() - 1);
		        sb.append("]");
			}else{
				sb.append("[]");
			}
	        renderText(sb.toString());
		}
}
