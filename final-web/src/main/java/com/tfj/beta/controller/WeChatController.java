package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.User;
import com.tfj.beta.model.WeChat;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.WeChatService;

@ControllerBind(controllerKey = "/admin/wechat", viewPath = "admin/wechat")
public class WeChatController extends BaseController{
	
	private WeChatService service = new WeChatService();
	private OperationLogService operationLogService = new OperationLogService();
	/**
	 * 微信扫描记录
	 */
	public void index() {

		keepModel(WeChat.class);
		WeChat weChat = getModel(WeChat.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		setAttr("pageList",
				service.getWeChatPage(pageNumber, pageSize, weChat));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&weChat.toJson().length()==2){//判断是否从导航sidebar进入
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.WECHAT.getCode(), ModuleType.WECHAT.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("wechatList.jsp");
	}


}
