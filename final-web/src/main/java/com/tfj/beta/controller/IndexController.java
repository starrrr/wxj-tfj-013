package com.tfj.beta.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.HttpKit;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.FrontEndRole;
import com.tfj.beta.model.PlantBase;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.WeChatOpenIdMS;
import com.tfj.beta.model.WeiXinLog;
import com.tfj.beta.model.view.JsonBean;
import com.tfj.beta.model.view.ProductBean;
import com.tfj.beta.service.IndexService;
import com.tfj.beta.service.RedisCacheService;
import com.tfj.beta.utils.JsonUtil;
import com.tfj.beta.utils.PropertiesUtil;

@ControllerBind(controllerKey = "/admin/index", viewPath = "index")
public class IndexController extends BaseController {
	private static final Logger log = LoggerFactory
			.getLogger(IndexController.class);
	private static final Boolean ISINDEX = true;// 标记，是否首页 是：true 否：false

	String appId = PropertiesUtil.getAppId();
	String appSecret = PropertiesUtil.getAppSecret();

	/**
	 * 二维码扫描传递过来的id： product_id category_id processType
	 */
	private IndexService indexService = new IndexService();
	private ProductResult productResultdao = new ProductResult();
	private WeChatOpenIdMS weChatOpenIdMSdao = new WeChatOpenIdMS();
	private RedisCacheService redisCacheService = new RedisCacheService();

	public void redirectWeChat() {
		String flow_batch_num = getPara(ProductResult.FLOW_BATCH_NUM);
		
		boolean weChatAuth = PropertiesUtil.getWechatAuth();
		//默认配置是游客
		String role_name = PropertiesUtil.getOpenIdRole();
		if (weChatAuth) {
			String url = "";
			try {
				// getPara flow_batch_num

				String text = PropertiesUtil.getBase()
						+ "admin/index?flow_batch_num=" + flow_batch_num;
				// 访问前端主页路径
				url="https://open.weixin.qq.com/connect/oauth2/authorize?appid="+PropertiesUtil.getAppId().trim()+"&redirect_uri="+URLEncoder.encode(text,"UTF-8") +"&response_type=code&scope=snsapi_userinfo&state=STATE";
			
				// System.out.println(url);
				log.error("wechat auto url..." + url);
				redirect(url);
			} catch (Exception e) {
				log.error("wechat auth error:" + e.getMessage());
				// 不进微信后台
				this.redirect("/admin/index?flow_batch_num=" + flow_batch_num+"&role_name="+role_name);
			}

		} else {
			// 不进微信后台
			this.redirect("/admin/index?flow_batch_num=" + flow_batch_num+"&role_name="+role_name);
		}

	}

	public void index() {// 主页
		String code = this.getPara("code");
		String roleName=getPara(WeChatOpenIdMS.ROLE_NAME);
		if(StringUtils.isBlank(roleName)){
			try {
				log.debug("code is---->"+code);
				// 记录微信扫描登陆日志
				if (StringUtils.isNotBlank(code)) {
					roleName = recordWeChatUserInfo(code).toUpperCase();
				}

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}else {
			roleName=roleName.toUpperCase();
		}
		setAttr("role_name", roleName);
		setAttr("flow_batch_num", getPara(ProductResult.FLOW_BATCH_NUM));
		setAttr("host", PropertiesUtil.getBase());
		render("../index/index.jsp");
	}

	/**
	 * 记录微信扫描日志 并返回 当前openid 角色名
	 * 
	 * @param code
	 * @return
	 */
	public String recordWeChatUserInfo(String code) {

		String roleName = FrontEndRole.GUEST;

		// 从Redis获取 accessToken OpenId
		// Boolean flag = PropertiesUtil.getRedisClusterMode();
		String accessToken = null;
		String openId = null;

		String openIdUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
				+ appId+ "&secret="+ appSecret+ "&code="+ code+ "&grant_type=authorization_code";
		// Map<String, String> queryParas = ParaMap.create("appid",
		// appId).put("secret", appSecret).getData();
		String json = HttpKit.get(openIdUrl);
		log.debug("URL...." + openIdUrl);
		//System.out.println("json...." + json);
		log.debug("json...." + json);
		Map<String, String> accessTokenMap = JsonUtil.parseJSON2MapString(json);
		accessToken = accessTokenMap.get("access_token");
		openId = accessTokenMap.get("openid");

		String infoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token="
				+ accessToken + "&openid=" + openId + "&lang=zh_CN";
		String userInfo = HttpKit.get(infoUrl);

		try {
			userInfo = new String(userInfo.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("userInfo.." + userInfo);

		log.error("infoUrl...." + infoUrl);
		log.error("json...." + json);
		WeiXinLog dao = new WeiXinLog();
		dao.saveWeChatLog(userInfo);

		// 通过openid获取角色t_weixin_openid ====> role_name
		WeChatOpenIdMS weChatOpenId = weChatOpenIdMSdao
				.findWeChatMSByOpenId(openId);
		if (null == weChatOpenId) {
			
			// 保存微信openid 并保存角色为游客
			weChatOpenIdMSdao.saveWeChatOpenIdMs(userInfo, FrontEndRole.GUEST);
			roleName = FrontEndRole.GUEST;
		} else {
			//目前只有3类用户 先hardcode
			if (weChatOpenId.getStr(WeChatOpenIdMS.ROLE_NAME).equalsIgnoreCase(
					FrontEndRole.GUEST)) {
				roleName = FrontEndRole.GUEST;
			}
			if (weChatOpenId.getStr(WeChatOpenIdMS.ROLE_NAME).equalsIgnoreCase(
					FrontEndRole.CUSTOMER)) {
				roleName = FrontEndRole.CUSTOMER;
			}
			if (weChatOpenId.getStr(WeChatOpenIdMS.ROLE_NAME).equalsIgnoreCase(
					FrontEndRole.BUSINESS)) {
				roleName = FrontEndRole.BUSINESS;
			}

		}

		return roleName;

	}

	public void plantLink() {// 种植环节
		setAttr("role_name", getPara(WeChatOpenIdMS.ROLE_NAME));
		setAttr("material_batch_num", getPara(ProductResult.MATERIAL_BATCH_NUM));
		setAttr("flow_batch_num", getPara(ProductResult.FLOW_BATCH_NUM));
		setAttr("host", PropertiesUtil.getBase());
		setAttr("processType", getPara("processType"));
		render("../index/plantLink.jsp");
	}

	public void processLink() {// 加工环节
		setAttr("role_name", getPara(WeChatOpenIdMS.ROLE_NAME));
		setAttr("flow_batch_num", getPara(ProductResult.FLOW_BATCH_NUM));
		setAttr("host", PropertiesUtil.getBase());
		setAttr("processType", getPara("processType"));
		render("../index/processLink.jsp");
	}

	public void circulationLink() {// 流通环节
		setAttr("role_name", getPara(WeChatOpenIdMS.ROLE_NAME));
		setAttr("flow_batch_num", getPara(ProductResult.FLOW_BATCH_NUM));
		setAttr("host", PropertiesUtil.getBase());
		setAttr("processType", getPara("processType"));
		render("../index/circulationLink.jsp");
	}

	public void marketLink() {// 销售环节
		setAttr("role_name", getPara(WeChatOpenIdMS.ROLE_NAME));
		setAttr("flow_batch_num", getPara(ProductResult.FLOW_BATCH_NUM));
		setAttr("host", PropertiesUtil.getBase());
		setAttr("processType", getPara("processType"));
		render("../index/marketLink.jsp");
	}

	public void getJson() {
		// admin/index/getJson?flow_batch_num=number&processType=type&role_name=roleName
		String flow_num = getPara(ProductResult.FLOW_BATCH_NUM);
		String role_name = getPara(WeChatOpenIdMS.ROLE_NAME);
		log.warn("flow_num:" + flow_num);
		log.warn("role_name:" + role_name);
		ProductResult productResult = productResultdao
				.findDataByflownum(flow_num);
		if (productResult != null) {// 流通批号判空
			String product_id = String.valueOf(productResult
					.getLong(ProductResult.PRODUCT_ID));
			String category_id = String.valueOf(productResult
					.getLong(ProductResult.CATEGORY_ID));
			String processType = getPara("processType");
			if (product_id != null && category_id != null
					&& product_id.matches("^[1-9]\\d*$")
					&& category_id.matches("^[1-9]\\d*$")) {// 判断传过了的参数是否为空
				if (processType != null) {
					getbranchJson(processType, category_id, product_id,
							role_name);
				} else {
					getIndex(category_id, product_id, role_name);

				}
			} else {
				renderJson(JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}"));
			}
		} else {
			renderJson(JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}"));
		}

	}

	// 获取主页的数据
	public String getIndexJson(String category_id, String product_id,
			String role_name) {
		String[] strs = { Constant.PLANT_PROCESS, Constant.SUPERVISE_PROCESS,
				Constant.WAREHOUSE_PROCESS, Constant.SALE_PROCESS };
		Map<String, Object> map = new HashMap<String, Object>();
		ProductBean productBean = new ProductBean();// 首页头部
		Product product = indexService.getProduct(product_id);
		String json = null;
		if (product != null) {
			// 主页面头部
			productBean.setProductName(product.getStr(product.PRODUCT_NAME));
			productBean.setPlantType(product.getStr(product.PLANT_TYPE));// 类型
			PlantBase plantBase = indexService.getPlantById(product
					.getLong(product.PLANTBASE_ID));
			productBean.setProductLocation(plantBase
					.getStr(PlantBase.BASE_LOCATION));// 地点
			productBean.setYear(product.getStr(product.PRODUCT_YEAR));// 年份
			productBean.setProductLevel(product.getStr(product.PRODUCT_LEVEL));
			productBean
					.setImgPath(plantBase.getStr(PlantBase.BASE_MAP) == null ? ""
							: PropertiesUtil.getBase()
									+ plantBase.getStr(PlantBase.BASE_MAP));// 图片
			map.put("product", productBean);

			for (int i = 0; i < strs.length; i++) {
				if (strs[i].equals(Constant.PLANT_PROCESS)) {
					if (indexService.getJsonBean(strs[i], category_id,
							product_id, ISINDEX,role_name).size() > 0) {
						JsonBean jsonBean = indexService.getJsonBean(strs[i],
								category_id, product_id, ISINDEX,role_name).get(0);
						map.put(Constant.PLANT_PROCESS,
								indexService.getJsonBean(strs[i], category_id,
										product_id, ISINDEX,role_name).get(0));
					} else {
						map.put(Constant.PLANT_PROCESS, "");
					}
				}
				if (strs[i].equals(Constant.SUPERVISE_PROCESS)) {
					if (indexService.getJsonBean(strs[i], category_id,
							product_id, ISINDEX,role_name).size() > 0) {
						map.put(Constant.SUPERVISE_PROCESS,
								indexService.getJsonBean(strs[i], category_id,
										product_id, ISINDEX,role_name).get(0));
					} else {
						map.put(Constant.SUPERVISE_PROCESS, "");
					}
				}
				if (strs[i].equals(Constant.WAREHOUSE_PROCESS)) {
					if (indexService.getJsonBean(strs[i], category_id,
							product_id, ISINDEX,role_name).size() > 0) {
						map.put(Constant.WAREHOUSE_PROCESS,
								indexService.getJsonBean(strs[i], category_id,
										product_id, ISINDEX,role_name).get(0));
					} else {
						map.put(Constant.WAREHOUSE_PROCESS, "");
					}
				}
				if (strs[i].equals(Constant.SALE_PROCESS)) {
					if (indexService.getJsonBean(strs[i], category_id,
							product_id, ISINDEX,role_name).size() > 0) {
						map.put(Constant.SALE_PROCESS,
								indexService.getJsonBean(strs[i], category_id,
										product_id, ISINDEX,role_name).get(0));
					} else {
						map.put(Constant.SALE_PROCESS, "");
					}
				}
			}
			json = JsonUtil.parseObjectToJson(map);
			//renderJson(JsonUtil.parseObjectToJson(map));
		} else {
			json = JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}");
		}
		return json;
	}

	public void getbranchJson(String processType, String category_id,
			String product_id, String role_name) {
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		String json=null;
		try {
			json=redisCacheService.getCacheJson(processType, category_id, product_id, role_name);
			if(json==null){
				List<JsonBean> list = indexService.getJsonBean(processType,
						category_id, product_id, false,role_name);
				json = JsonUtil.parseObjectToJson(list);
				if (flag) {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), processType, role_name, false, json);
					//renderJson(json);
				} else {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), processType, role_name, false, json);
					//renderJson(json);
				}
			}
			renderJson(json);
		} catch (Exception e) {
				List<JsonBean> list = indexService.getJsonBean(processType,
						category_id, product_id, false,role_name);
				renderJson(JsonUtil.parseObjectToJson(list));
		}
	}
	public void getIndex(String category_id, String product_id, String role_name) {
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		String json = null;
		try {
			json=redisCacheService.getCacheJson(null, category_id, product_id, role_name);
			if (json == null) {
				// get null get from database ...string --> cache
				json = getIndexJson(category_id, product_id, role_name);
				if (flag) {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), null, role_name, true, json);
					//renderJson(JsonUtil.parseObjectToJson(json));
				} else {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), null, role_name, true, json);
					//renderJson(JsonUtil.parseObjectToJson(json));
				}
			}
			renderJson(json);
		} catch (Exception e) {
			json = getIndexJson(category_id, product_id, role_name);
			renderJson(json);
		}
	}

	// 原料批号查询 YLZ150827A01 Mixed batch of
	// processType=PLANT_PROCESS
	// processType=SUPERVISE_PROCESS
	public void getJsonByMBNum() {
		String material_batch_num = getPara("material_batch_num");
		String processType = getPara("processType");
		String role_name = getPara("role_name");
		ProductResult productResult = productResultdao
				.findDataByMaterialNum(material_batch_num);
		if (productResult != null) {// 流通批号判空
			String product_id = String.valueOf(productResult
					.getLong(ProductResult.PRODUCT_ID));
			String category_id = String.valueOf(productResult
					.getLong(ProductResult.CATEGORY_ID));
			if (product_id != null && category_id != null
					&& product_id.matches("^[1-9]\\d*$")
					&& category_id.matches("^[1-9]\\d*$")) {// 判断传过了的参数是否为空
				if (processType != null) {
					List<JsonBean> list = indexService.getJsonBean(processType,
							category_id, product_id, false,role_name);
					renderJson(JsonUtil.parseObjectToJson(list));
				}
			} else {
				renderJson(JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}"));
			}
		} else {
			renderJson(JsonUtil.parseObjectToJson("{\"message\":\"FAIL\"}"));
		}

	}

	public void getJsonByMBNumRedis() {
		String material_batch_num = getPara("material_batch_num");
		String processType = getPara("processType");
		String role_name = getPara("role_name");
		ProductResult productResult = productResultdao
				.findDataByMaterialNum(material_batch_num);
		String product_id = String.valueOf(productResult
				.getLong(ProductResult.PRODUCT_ID));
		String category_id = String.valueOf(productResult
				.getLong(ProductResult.CATEGORY_ID));
		
		Boolean flag = PropertiesUtil.getRedisClusterMode();
		String json = null;
		try {
			json=redisCacheService.getCacheJson(processType, category_id, product_id, role_name);
			if(json==null){
				List<JsonBean> list = indexService.getJsonBean(processType,
						category_id, product_id, false,role_name);
				json = JsonUtil.parseObjectToJson(list);
				if (flag) {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), processType, role_name, false, json);
				} else {
					redisCacheService.cacheJson(Long.valueOf(category_id), Long.valueOf(product_id), processType, role_name, false, json);
				}
			}
			renderJson(json);
		}catch (Exception e) {
			List<JsonBean> list = indexService.getJsonBean(processType,
					category_id, product_id, false,role_name);
			renderJson(JsonUtil.parseObjectToJson(list));
		}

	}

}
