package com.tfj.beta.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.MenuResc;
import com.tfj.beta.model.User;
import com.tfj.beta.service.MenuRescService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/6/18.
 */
@ControllerBind(controllerKey = "/admin/menuresc", viewPath = "admin/system")
public class MenuRescController extends BaseController {

    private MenuRescService menuRescService = new MenuRescService();
    private OperationLogService operationLogService = new OperationLogService();

    public void index() {
        setAttr("rescList", menuRescService.getSortMenuRescList());
        // operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))){//判断是否从导航sidebar进入
     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
     		operationLogService.recordOperationLog(loginUser,ModuleType.MENURESC.getCode(), ModuleType.MENURESC.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
        render("menurescList.jsp");
    }

    /**
     * 查看资源
     */
    public void view() {
        Integer id = getParaToInt(MenuResc.ID);

        setAttr("menuResc", null != id ? MenuResc.dao.find(id) : "");
        render("menurescForm.jsp");
    }

    /**
     * 修改或保存resc
     */
    public void save() {
        MenuResc resc = getModel(MenuResc.class);
        
        if(null == resc.getInt(MenuResc.PID)){
        	resc.set(MenuResc.PID, 0);
        }
        Integer oldid=resc.getInt(MenuResc.ID);//判断原来是否存在ID，存在：修改；不存在：添加
        boolean isFlag=menuRescService.modifyModel(resc.set(MenuResc.UPDATE_TIME, DateUtils.getCurrentTime()));
        //operation log
      	if(isFlag){
      		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
      		if(oldid!=null){//修改
      			operationLogService.recordOperationLog(loginUser,ModuleType.MENURESC.getCode(), ModuleType.MENURESC.getName(),resc.getStr(MenuResc.NAME),
      					OperationType.MENURESC_UPDATE.getCode(), OperationType.MENURESC_UPDATE.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}else {
      			operationLogService.recordOperationLog(loginUser,ModuleType.MENURESC.getCode(), ModuleType.MENURESC.getName(),resc.getStr(MenuResc.NAME),
      					OperationType.MENURESC_ADD.getCode(), OperationType.MENURESC_ADD.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}
      	}   
        redirect("/admin/menuresc");
    }

    /**
     * 菜单树
     */
    public void getMenuTree() {
        List<MenuResc> rescs = menuRescService.getMenuRescList();

        StringBuilder sb = new StringBuilder();
        sb.append("[");

        for (MenuResc resc : rescs) {
            if (resc.getStr(MenuResc.TYPE).equals("0")) {
                sb.append("{\"id\":");
                sb.append(resc.getInt(MenuResc.ID));
                sb.append(",\"pId\":");
                sb.append(resc.getInt(MenuResc.PID));
                sb.append(",\"name\":\"");
                sb.append(resc.getStr(MenuResc.NAME));
                sb.append("\"");
                sb.append(",\"open\":true");
                sb.append("}");
                sb.append(",");
            }
        }

        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        renderText(sb.toString());
    }
}
