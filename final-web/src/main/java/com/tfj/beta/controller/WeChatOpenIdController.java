package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.User;
import com.tfj.beta.model.WeChat;
import com.tfj.beta.model.WeChatOpenId;
import com.tfj.beta.model.WeChatRole;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.WeChatService;

@ControllerBind(controllerKey = "/admin/wechatopenid", viewPath = "admin/wechat")
public class WeChatOpenIdController extends BaseController{
	
	private WeChatService service = new WeChatService();
	private OperationLogService operationLogService = new OperationLogService();
	/**
	 * 微信用户管理
	 */
	public void index() {
		keepModel(WeChat.class);
		WeChatOpenId weChatOpenId = getModel(WeChatOpenId.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		Page<WeChatOpenId> page=service.getWeChatOpenidPage(pageNumber, pageSize, weChatOpenId);
		setAttr("pageList",page);
		setAttr("roleList",service.findAllWeChatRose());
		if(weChatOpenId.hashCode()!=0){//不等于0：对象不存在任何值；
			//查询条件回显
			setAttr("nickname",weChatOpenId.getStr(WeChatOpenId.NICKNAME)==null?"":weChatOpenId.getStr(WeChatOpenId.NICKNAME));
			setAttr("rolename",weChatOpenId.getStr(WeChatRole.REMARK)==null?"":weChatOpenId.getStr(WeChatRole.REMARK));
		}
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&weChatOpenId.toJson().length()==2){//判断是否从导航sidebar进入
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.WECHATOPENID.getCode(), ModuleType.WECHATOPENID.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("wechatopenIdList.jsp");
	}
}
