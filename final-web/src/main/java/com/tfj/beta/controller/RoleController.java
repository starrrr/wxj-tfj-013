package com.tfj.beta.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.MenuResc;
import com.tfj.beta.model.Role;
import com.tfj.beta.model.RoleResc;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.MenuRescService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.RoleService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/6/18.
 */
@ControllerBind(controllerKey = "/admin/role", viewPath = "admin/user")
public class RoleController extends BaseController {

    private RoleService roleService = new RoleService();

    private MenuRescService menuRescService = new MenuRescService();
    private UnitService unitService = new UnitService();
    private OperationLogService operationLogService = new OperationLogService();

    public void index() {
        setAttr("roleList", roleService.getRoleList());
        // operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))){//判断是否从导航sidebar进入
     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
     		operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
        render("roleList.jsp");
    }

    /**
     * 查看角色
     */
    public void view() {
        Integer id = getParaToInt(Role.ID);

        setAttr("role", null != id ? Role.dao.findById(id) : "");
        render("roleForm.jsp");
    }

    /**
     * 修改或保存role
     */
    public void save() {
        Role role = getModel(Role.class);
        Integer oldid=role.getInt(Role.ID);//判断原来是否存在ID，存在：修改；不存在：添加
        //role.setAttrs(Role.UPDATE_TIME, DateUtils.getCurrentTime());
        role.set(Role.UPDATE_TIME, DateUtils.getCurrentTime());
        boolean isFlag=roleService.modifyModel(role);
        //operation log
      	if(isFlag){
      		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
      		if(oldid!=null){//修改
      			operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),role.getStr(Role.NAME),
      					OperationType.ROLE_UPDATE.getCode(), OperationType.ROLE_UPDATE.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}else {
      			operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),role.getStr(Role.NAME),
      					OperationType.ROLE_ADD.getCode(), OperationType.ROLE_ADD.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}
      	}       
        redirect("/admin/role");
    }

    /**
     * 删除角色
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        Role role= Role.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
        boolean isFlag=roleService.deleteRoles(ids);
        //operation log
        if(isFlag){
        	User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),role.getStr(Role.NAME),
					OperationType.ROLE_DELETE.getCode(), OperationType.ROLE_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
        redirect("/admin/role");
    }

    /**
     * 角色权限树
     */
    public void getRescTree(){
        Role role = getModel(Role.class);
        List<RoleResc> roleRescs = roleService.getRoleRescList(role);
        List<MenuResc> rescs = menuRescService.getMenuRescList();

        StringBuilder sb = new StringBuilder();
        sb.append("[");

        for (MenuResc resc : rescs) {
            sb.append("{\"id\":");
            sb.append(resc.getInt(MenuResc.ID));
            sb.append(",\"pId\":");
            sb.append(resc.getInt(MenuResc.PID));
            sb.append(",\"name\":\"");
            sb.append(resc.getStr(MenuResc.NAME));
            sb.append("\"");
            sb.append(",\"roleId\":");
            sb.append(role.getInt(Role.ID));
            sb.append(",\"open\":true");
            sb.append(",\"tag\":\"menutag\"");
            for(RoleResc roleResc : roleRescs){
                if(roleResc.getInt(RoleResc.RESC_ID) == resc.getInt(MenuResc.ID))
                    sb.append(",\"checked\":true");
            }
            sb.append("}");
            sb.append(",");
        }

        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        renderText(sb.toString());
    }

    /**
     * 角色增加权限
     */
    public void addResc() {
        RoleResc roleResc = getModel(RoleResc.class);
        boolean ExistRescId=RoleResc.dao.isExistRescId(roleResc.getInt(RoleResc.RESC_ID),roleResc.getInt(RoleResc.ROLE_ID));//需要对role_resc表作出判断：是否存在resc_id
        boolean isFlag=roleService.modifyModel(roleResc);
        //operation log
        int role_id=roleResc.getInt(RoleResc.ROLE_ID);//角色id
        int resc_id=roleResc.getInt(RoleResc.RESC_ID);//资源id
        Role role= Role.dao.findById(role_id);
        MenuResc menuResc=MenuResc.dao.findById(resc_id);
        if(isFlag&&ExistRescId){
        	User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),role.getStr(Role.NAME)+":"+menuResc.getStr(MenuResc.NAME),
					OperationType.ROLE_AUTHORIZE.getCode(), OperationType.ROLE_AUTHORIZE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
        renderNull();
    }

    /**
     * 角色删除权限
     */
    public void deleteResc() {
        RoleResc roleResc = getModel(RoleResc.class);
        int role_id=roleResc.getInt(RoleResc.ROLE_ID);
        int resc_id=roleResc.getInt(RoleResc.RESC_ID);
        boolean ExistRescId=RoleResc.dao.isExistRescId(roleResc.getInt(RoleResc.RESC_ID),roleResc.getInt(RoleResc.ROLE_ID));//需要对role_resc表作出判断：是否存在resc_id
        Role role= Role.dao.findById(role_id);//查出删除前的角色
        MenuResc menuResc= MenuResc.dao.findById(resc_id);//查出删除前的权限
        boolean isFlag=roleService.deleteResc(roleResc);
        //operation log
        if(isFlag&&!ExistRescId){
        	User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			operationLogService.recordOperationLog(loginUser,ModuleType.ROLE.getCode(), ModuleType.ROLE.getName(),role.getStr(Role.NAME)+":"+menuResc.getStr(MenuResc.NAME),
					OperationType.ROLE_AUTHORIZE_DELETE.getCode(), OperationType.ROLE_AUTHORIZE_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
        renderNull();
    }
    
    /**
     * 角色树
     */
    public void getRoleTree(){
    	User user = (User) getSessionAttr(Constant.SHIRO_USER);
        List<RoleUser> roleUsers = roleService.getRoleByUserId(user);
        List<Role> roles = roleService.getRoleList();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Role role : roles) {
            sb.append("{\"id\":");
            sb.append(role.getInt(Role.ID));
            sb.append(",\"name\":\"");
            sb.append(role.getStr(Role.NAME));
            sb.append("\"");
            sb.append(",\"open\":true");
            for(RoleUser roleUser : roleUsers){
                if(roleUser.getInt(RoleUser.ROLE_ID) == role.getInt(Role.ID))
                    sb.append(",\"checked\":true");
            }
            sb.append("}");
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        renderText(sb.toString());
    }
}
