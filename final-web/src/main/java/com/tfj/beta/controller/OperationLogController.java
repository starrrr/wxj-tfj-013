package com.tfj.beta.controller;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.OperationLog;
import com.tfj.beta.model.User;
import com.tfj.beta.service.OperationLogService;

@ControllerBind(controllerKey = "/admin/operationlog", viewPath = "admin/system")
public class OperationLogController extends BaseController {
	
	private OperationLogService service = new OperationLogService();
	private OperationLogService operationLogService = new OperationLogService();
	public void index() {
		keepModel(OperationLog.class);
		keepPara("startTime", "endTime");
		OperationLog operationLog = getModel(OperationLog.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		Page<OperationLog> page=service.getOperationLogPage(loginUser,admin,pageNumber, pageSize, operationLog, getPara("startTime"), getPara("endTime"));
		setAttr("pageList",page);
		//获取所有模块名称
		setAttr("modulelist", ModuleType.getModulesName());
		if(operationLog.hashCode()!=0){//不等于0：对象不存在任何值；
			//查询条件回显
			setAttr("modulename",operationLog.getStr(OperationLog.MODULE_NAME)==null?"":operationLog.getStr(OperationLog.MODULE_NAME));
		}
		render("operationLogList.jsp");
	}
	
	public void delete() {
		String[] ids = getParaValues("ids");
		boolean isFlag=service.deleteOperationLog(ids);
		if(isFlag){
			renderText("true");
		}else {
			renderText("false");
		}
	}
	
	public void batchdeleteview(){
		render("batchDeleteOperationForm.jsp");
	}
	/**
	 * 清理日志
	 */
	public void batchdelete(){
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		//String module_name=getPara("module_name");
		//String operation_name=getPara("operation_name");
		String startTime=getPara("startTime");
		String endTime=getPara("endTime");
		//String timestr="";//startTime+" 至 "+endTime;
		/*if("".equals(startTime)&&!"".equals(endTime)){
			timestr="开始  至 "+endTime;
		}
		if(!"".equals(startTime)&&"".equals(endTime)){
			timestr=startTime+" 至  "+new SimpleDateFormat("YYYY-MM-DD HH:mm:ss").format(new Date());
		}
		if(!"".equals(startTime)&&!"".equals(endTime)){
			timestr=startTime+" 至  "+endTime;
		}*/
		if(service.batchDeleteOperationLog(startTime,endTime)){
			operationLogService.recordOperationLog(loginUser,ModuleType.OPERATIONLOG.getCode(),ModuleType.OPERATIONLOG.getName(),
					startTime+" 至  "+endTime,
					OperationType.OPERATION_DELETE.getCode(), OperationType.OPERATION_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			renderText("true");
		}else {
			renderText("false");
		}
	}
	
}
