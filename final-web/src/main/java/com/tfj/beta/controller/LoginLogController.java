package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.User;
import com.tfj.beta.service.LoginLogService;
import com.tfj.beta.service.OperationLogService;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/loginLog", viewPath = "admin/system")
public class LoginLogController extends BaseController {

    private LoginLogService service = new LoginLogService();
	private OperationLogService operationLogService = new OperationLogService();

    public void index() {
        keepModel(User.class);
        keepPara("startTime", "endTime");
        User user = getModel(User.class);
        int pageNumber = getParaToInt(Constant.PAGENUM, 1);
        int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
        setAttr("pageList", service.getLoginLogPage(pageNumber, pageSize, user, getPara("startTime"), getPara("endTime")));
        // operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))&&user.toJson().length()==2){//判断是否从导航sidebar进入
     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
     		operationLogService.recordOperationLog(loginUser,ModuleType.LOGINLOG.getCode(), ModuleType.LOGINLOG.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
        render("logList.jsp");
    }
}
