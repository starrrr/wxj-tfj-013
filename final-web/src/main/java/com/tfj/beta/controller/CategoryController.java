package com.tfj.beta.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitCate;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.utils.DateUtils;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/category", viewPath = "admin/category")
public class CategoryController extends BaseController {

	private CategoryService service = new CategoryService();
	private UnitService unitService = new UnitService();
	private OperationLogService operationLogService = new OperationLogService();

	/**
	 * 种类数据列表
	 */

	public void index() {
		keepModel(Category.class);
		Category category = getModel(Category.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);//当前用户
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);//当前用户所在的单位
		setAttr("pageList",
				service.getCategoryPage(pageNumber, pageSize, category,units,admin));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&category.toJson().length()==2){//判断是否从导航sidebar进入  
			//写操作信息
			operationLogService.recordOperationLog(loginUser, ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),"",
					OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("categoryList.jsp");
	}

	public void view() {
		Long id = getParaToLong(Category.ID);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);//当前用户
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units="true".equals(admin)?unitService.getUnitList():unitService.getUnitByUser(unitUsers);
		if(id!=null){
			List<UnitCate> unitCates=UnitCate.dao.searchCategoryUnitByCategoryId(id);
			List<Unit> unitsCates=unitService.getUnitByCategory(unitCates);
			String unitNames="";
			String unitIds="";
			if(unitsCates.size()>0){
				for(Unit unit:unitsCates){
					String ids=String.valueOf(unit.getLong(Unit.ID));
					String names=unit.getStr(Unit.NAME);
					unitIds=unitIds+","+ids;
					unitNames=unitNames+","+names;
				}
				setAttr("unitNames",unitNames.substring(1));
				setAttr("unitIds",unitIds.substring(1));
			}else {
				setAttr("unitNames","");
				setAttr("unitIds","");
			}
			
		}
		setAttr("category", null != id ? Category.dao.findById(id) : "");
		setAttr("unitList", units);
		setAttr("unitStr", null != id ? unitService.getCategoryUnit(id) : "");
		render("categoryForm.jsp");
	}

	public void save() {
		Category category = getModel(Category.class);
		setAttr("recordlog","recordlog");//log标记
		Long oldid=category.getLong(Category.ID);//判断原来是否存在ID，存在：修改；不存在：添加
		String uids=getPara("unitids");
		String[] unitIds=null;
		if(uids.indexOf(",")>0){
			unitIds=uids.split(",");
		}else{
			unitIds=new String[1];
			unitIds[0]=uids;
		}
		boolean isFlag=service.modifyCategory(category,unitIds);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			if(oldid!=null){//修改
				operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),category.getStr(Category.NAME),
						OperationType.CATEGORY_UPDATE.getCode(), OperationType.CATEGORY_UPDATE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),category.getStr(Category.NAME),
						OperationType.CATEGORY_ADD.getCode(), OperationType.CATEGORY_ADD.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
		redirect("/admin/category");
	}

	public void delete() {
		boolean isflag=false;
		String[] ids = getParaValues("ids");
		for(String id:ids){
			if(service.queryAssociated(id)){// false:不存在关联	 true:存在关联
				isflag=true;
				break;
			}
		}
		if(isflag){
			renderText("true");
		}else {
			Category category = Category.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
			boolean isFlag=service.deleteCategory(ids);
			//operation log
			if(isFlag){
				User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
				operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),category.getStr(Category.NAME),
						OperationType.CATEGORY_DELETE.getCode(), OperationType.CATEGORY_DELETE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
			
			renderText("false");
		}
	}
	public void audit() {
		String rid = this.getPara("rid");
		if (!StrKit.isBlank(rid)) {
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String currentDate = DateUtils.getCurrentTime();
			String[] ids = rid.split(",");
			for (String id : ids) {
				Category category = Category.dao.findById(id);
				if(category.getInt(Category.AUDITFLAG)==0){
					//没有审核通过的
					//category.set(Category.NOTE, "audit");
					// 审核通过
					category.set(Category.AUDITFLAG, 1);
					// 审核者信息
					category.set(Category.AUDITORID, loginUser.getLong(User.ID));
					category.set(Category.AUDITOR, loginUser.getStr(User.NAME));
					category.set(Category.AUDIT_TIME, currentDate);
					boolean isFlag=service.modifyCategory(category);
					//operation log
					if(isFlag){
						operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),category.getStr(Category.NAME),
								OperationType.CATEGORY_AUDIT.getCode(), OperationType.CATEGORY_AUDIT.getName(),
								loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
					}
				}
				
			}
			redirect("/admin/category");
		}

	}
	
	public void release(){
		String rid = this.getPara("ids");
		if (!StrKit.isBlank(rid)) {
			String[] ids = rid.split(",");
			for (String id : ids) {
				Category category = Category.dao.findById(id);
				//释放审核
				category.set(Category.AUDITFLAG, 0);
				boolean isFlag=service.modifyCategory(category);
				// opeartion log 
				if(isFlag){
					User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
					operationLogService.recordOperationLog(loginUser,ModuleType.CATEGORY.getCode(), ModuleType.CATEGORY.getName(),category.getStr(Category.NAME),
							OperationType.RELEASE.getCode(), OperationType.RELEASE.getName(),
							loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
				}
			}
		}
		redirect("/admin/category");
	}
	
	public void checkCategoryName(){
    	String categoryName=getPara(Category.NAME);
    	Category category=service.checkCategoryIsExist(categoryName);
    	if(category!=null){
    		 if(categoryName.equals(category.getStr(Category.NAME))){
            	renderText("true");
            }else {
            	renderText("false");
    		}
    	}else {
    		renderText("false");
		}
    }

}
