package com.tfj.beta.controller;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Dict;
import com.tfj.beta.model.User;
import com.tfj.beta.service.DictService;
import com.tfj.beta.service.OperationLogService;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/dict", viewPath = "admin/system")
public class DictController extends BaseController {

	private DictService service = new DictService();
	private OperationLogService operationLogService = new OperationLogService();

	/**
	 * 字典数据列表
	 */

	public void index() {

		// 判断是否有权限，如果没有权限 转到 没有权限的页面
		// Subject currentUser = SecurityUtils.getSubject();
		// String currentPath = this.getRequest().getContextPath();
		//
		// if(currentUser.isPermitted("1:123")){
		// System.out.println(" have pession");
		// }else{
		// System.out.println(" no pession");
		// render("dictList.html");
		// }

		keepModel(Dict.class);
		Dict dict = getModel(Dict.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		setAttr("pageList", service.getDictPage(pageNumber, pageSize, dict));
        // operation Log
     	if(StringUtils.isNotBlank(getPara("recordlog"))&&dict.toJson().length()==2){//判断是否从导航sidebar进入
     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
     		operationLogService.recordOperationLog(loginUser,ModuleType.DICT.getCode(), ModuleType.DICT.getName(),"",
     				OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
     				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
     	}
		render("dictList.jsp");
	}

	public void view() {
		
		Long id = getParaToLong(Dict.ID);
		setAttr("dict", null != id ? Dict.dao.findById(id) : "");

		render("dictForm.jsp");
	}

	public void save() {
		Dict dict = getModel(Dict.class);
		Long oldid=dict.getLong(User.ID);//判断原来是否存在ID，存在：修改；不存在：添加
		boolean isFlag=service.modifyDict(dict);
		//operation log
      	if(isFlag){
      		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
      		if(oldid!=null){//修改
      			operationLogService.recordOperationLog(loginUser,ModuleType.DICT.getCode(), ModuleType.DICT.getName(),dict.getStr(Dict.NAME),
      					OperationType.DICT_UPDATE.getCode(), OperationType.DICT_UPDATE.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}else {
      			operationLogService.recordOperationLog(loginUser,ModuleType.DICT.getCode(), ModuleType.DICT.getName(),dict.getStr(Dict.NAME),
      					OperationType.DICT_ADD.getCode(), OperationType.DICT_ADD.getName(),
      					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
      		}
      	}       
		redirect("/admin/dict");
	}
	
	public void delete() {
        String[] ids = getParaValues("ids");
        Dict dict= Dict.dao.findById(ids[0]);//项目没批量删除，固定写一个ID
        boolean isFlag=service.deleteDicts(ids);
        //operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			operationLogService.recordOperationLog(loginUser,ModuleType.DICT.getCode(), ModuleType.DICT.getName(),dict.getStr(Dict.NAME),
					OperationType.DICT_DELETE.getCode(), OperationType.DICT_DELETE.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		redirect("/admin/dict");
	}

}
