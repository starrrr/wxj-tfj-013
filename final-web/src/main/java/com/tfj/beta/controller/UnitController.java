package com.tfj.beta.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Category;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitCate;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.UnitService;

@ControllerBind(controllerKey = "/admin/unit", viewPath = "admin/unit")
public class UnitController extends BaseController {

	private UnitService service = new UnitService();
	private OperationLogService operationLogService = new OperationLogService();
	CategoryService categoryService=new CategoryService();
	private UnitService unitService = new UnitService();
	/**
	 * 单位列表
	 */
	
	public void index() {
		keepModel(Unit.class);
		Unit unit = getModel(Unit.class);
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, Constant.DEFAULTPAGESIZE);
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);//当前用户
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);//当前用户所在的单位
		setAttr("pageList",
				unitService.getUnitPage(pageNumber, pageSize,unit));
		// operation Log
		if(StringUtils.isNotBlank(getPara("recordlog"))&&unit.toJson().length()==2){//判断是否从导航sidebar进入  
			//写操作信息
			operationLogService.recordOperationLog(loginUser, ModuleType.UNIT.getCode(), ModuleType.UNIT.getName(),"",
			OperationType.SELECT.getCode(), OperationType.SELECT.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
		}
		render("unitList.jsp");
	}

	public void view() {
		Long id = getParaToLong(Unit.ID);
		setAttr("unit", null != id ? Unit.dao.findById(id) : "");
		render("unitForm.jsp");
	}

	public void save() {
		Unit unit = getModel(Unit.class);
		Long oldid=unit.getLong(Unit.ID);//判断原来是否存在ID，存在：修改；不存在：添加
		boolean isFlag=service.modifyUnit(unit);
		//operation log
		if(isFlag){
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			if(oldid!=null){//修改
				operationLogService.recordOperationLog(loginUser,ModuleType.UNIT.getCode(), ModuleType.UNIT.getName(),unit.getStr(Unit.NAME),
						OperationType.USER_UPDATE.getCode(), OperationType.USER_UPDATE.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}else {
				operationLogService.recordOperationLog(loginUser,ModuleType.UNIT.getCode(), ModuleType.UNIT.getName(),unit.getStr(Unit.NAME),
						OperationType.UNIT_ADD.getCode(), OperationType.UNIT_ADD.getName(),
						loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			}
		}
		redirect("/admin/unit");
	}

	public void delete() {
		String[] ids = getParaValues("ids");
		service.deleteUnit(ids);
		redirect("/admin/unit");
	}
	
	/**
	 * 种类单位权限树
	 */
	public void getUnitTreeByCaId() {
		String category_id=getPara("category_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);//当前用户
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=service.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units="true".equals(admin)?service.getUnitList():service.getUnitByUser(unitUsers);
		List<UnitCate> unitCates=UnitCate.dao.searchCategoryUnitByCategoryId(category_id);
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Unit unit : units) {
			sb.append("{\"id\":");
			sb.append(unit.getLong(Unit.ID));
			sb.append(",\"name\":\"");
			sb.append(unit.getStr(Unit.NAME));
			sb.append("\"");
			sb.append(",\"open\":true");
			sb.append(",\"tag\":\"unittag\"");
			for (UnitCate unitCate : unitCates) {
				if (unitCate.getLong(UnitCate.UNIT_ID) == unit
						.getLong(Unit.ID))
					sb.append(",\"checked\":true");
			}
			sb.append("}");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		renderText(sb.toString());
	}
	
	/**
	 * 用户单位权限
	 */
	public void getUnitTree(){
		String user_id=getPara("user_id");
		List<UnitUser> unitUsers = service.getUnitUserByUserId(user_id);
		List<Unit> units = service.getUnitList();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Unit unit : units) {
			sb.append("{\"id\":");
			sb.append(unit.getLong(Unit.ID));
			sb.append(",\"pId\":");
            sb.append(0);
			sb.append(",\"name\":\"");
			sb.append(unit.getStr(Unit.NAME));
			sb.append("\"");
			sb.append(",\"open\":true");
			for (UnitUser unitUser : unitUsers) {
				if (unitUser.getLong(UnitUser.UNIT_ID) == unit
						.getLong(Unit.ID))
					sb.append(",\"checked\":true");
			}
			sb.append("}");
			sb.append(",");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		renderText(sb.toString());
	}
	/**
	 * 用户单位权限所有打钩check
	 */
	public void getUnitTreeCheck(){
		List<Unit> units = service.getUnitList();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Unit unit : units) {
			sb.append("{\"id\":");
			sb.append(unit.getLong(Unit.ID));
			sb.append(",\"pId\":");
            sb.append(0);
			sb.append(",\"name\":\"");
			sb.append(unit.getStr(Unit.NAME));
			sb.append("\"");
			sb.append(",\"open\":true");
			sb.append(",\"checked\":true");
			sb.append("}");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		renderText(sb.toString());
	}
	/**
	 * 用户单位权限去除所有打钩check
	 */
	public void getUnitTreeRemoveCheck(){
		List<Unit> units = service.getUnitList();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Unit unit : units) {
			sb.append("{\"id\":");
			sb.append(unit.getLong(Unit.ID));
			sb.append(",\"pId\":");
            sb.append(0);
			sb.append(",\"name\":\"");
			sb.append(unit.getStr(Unit.NAME));
			sb.append("\"");
			sb.append(",\"open\":true");
			sb.append(",\"checked\":false");
			sb.append("}");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		renderText(sb.toString());
	}
	//父节点
	private void parentId(StringBuilder sb) {
		sb.append("{\"id\":");
		sb.append(0);
		sb.append(",\"name\":\"");
		sb.append("全显");
		sb.append("\"");
		sb.append(",\"open\":true");
		sb.append(",\"tag\":\"unittag\"");
		sb.append("}");
		sb.append(",");
	}
}
