package com.tfj.beta.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.ext.route.ControllerBind;

/**
 * Created by Administrator on 2015/6/12.
 */
@ControllerBind(controllerKey = "/admin/error", viewPath = "admin")
public class ErrorController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(ErrorController.class);
	public void index() {
		//String errorCode = getPara("UrlPara");
		//log.info("------------------------------->"+errorCode);
		//setAttr("errorCode", errorCode);
		setAttr("msg", "未授权错误！请联系管理员。");
		render("index.jsp");

	}

	

}
