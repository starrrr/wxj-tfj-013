package com.tfj.beta.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.LoginLog;
import com.tfj.beta.model.ModelEnum;
import com.tfj.beta.model.MyCaptchaRender;
import com.tfj.beta.model.User;
import com.tfj.beta.service.CategoryService;
import com.tfj.beta.service.IndexService;
import com.tfj.beta.service.PlantBaseService;
import com.tfj.beta.service.UserService;
import com.tfj.beta.utils.DateUtils;
import com.tfj.beta.utils.StringUtils;

/**
 * Created by Administrator on 2015/6/9.
 */

@ControllerBind(controllerKey = "/admin", viewPath = "admin")
@ClearInterceptor(ClearLayer.ALL)
public class LoginController extends BaseController {

	UserService userService = new UserService();
	private CategoryService categoryService = new CategoryService();
	private IndexService indexService = new IndexService();
	private PlantBaseService plantBaseService = new PlantBaseService();
	@Before({ SessionInViewInterceptor.class })
	public void index() {
		//清空登陆错误信息
		this.setAttr(Constant.LOGIN_ERROR_MSG, null);
		if (SecurityUtils.getSubject().isAuthenticated()) {
			//统计数据
			setAttr("indexCount",indexService.findCount());
			//种类信息
			setAttr("categoryList",
					categoryService.getIndexCategory());
			//基地
			
			setAttr("plantBaseData", JSON.toJSONString(plantBaseService.getAllPlantBase()));
			render("index.jsp"); // 已登录成功，跳转主页面
		} else {
			render("login.jsp");
		}
	}

	/**
	 * 验证登陆
	 */

	public void doLogin() {
		//清空登陆错误信息
		this.setAttr("loginErrorMsg", null);
		
		String userName = getPara("username", null);
		String passWord = getPara("password", null);
		String inputRandomCode = getPara("captcha");
		boolean validate = MyCaptchaRender.validate(this, inputRandomCode);

		if (validate) {// 验证码通过
			UsernamePasswordToken token = new UsernamePasswordToken(userName,
					passWord, true, getRequest().getRemoteAddr());
			// 这里可以调用 subject 做判断
			Subject subject = SecurityUtils.getSubject();
			try {
				
				if (!subject.isAuthenticated()) {
					subject.login(token);
				}
				// 设置日期格式，获得当前时间
				String currentDate = DateUtils.getCurrentTime();

				User loginUser = (User) subject.getPrincipal();
				//判断是否是管理员组的成员 
				boolean adminType = userService.checkUserIsAdminitor(loginUser.getLong("id"));
				this.setSessionAttr("adminType", String.valueOf(adminType));
				

				// 设置登陆时间，登陆ip，登陆状态
				loginUser
						.set(User.LAST_LOGIN_TIME, currentDate)
						.set(User.LAST_LOGIN_IP,
								StringUtils.getRemoteAddr(getRequest()))
						.set(User.LOGIN_STATUS,
								ModelEnum.OnlineOrOffline.ONLINE.ordinal())
						.set(User.LOGIN_COUNT,
								loginUser.getInt(User.LOGIN_COUNT) + 1)
						.update();

				// 创建本次登陆日志
				LoginLog loginLog = new LoginLog();
				loginLog.set(LoginLog.USER_ID, loginUser.get(User.ID))
						.set(LoginLog.LOGIN_TIME, currentDate)
						.set(LoginLog.LOGIN_IP, getRequest().getRemoteAddr())
						.save();

				redirect("/admin"); // 登录成功，跳转主页面
				return;
				
			} catch (AuthenticationException e) {
				//setAttr("error", Constant.LOGINFAIL);
				setAttr(Constant.LOGIN_ERROR_MSG, "用户名或者密码错误，请检查");
				// 登录失败，跳转登录
				//redirect("/admin");
				render("login.jsp");
			}
		} else {
			this.setAttr(Constant.LOGIN_ERROR_MSG, "验证码错误");
			// 登录失败，跳转登录
			//redirect("/admin");
			render("login.jsp");
		}

	}

	/**
	 * 用户登出
	 */
	public void logout() {
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		loginUser.set(User.LOGIN_STATUS,
				ModelEnum.OnlineOrOffline.OFFLINE.ordinal()).update();
		// 设置登录日志下线时间
		LoginLog.dao.updateLogoutTime(loginUser.getLong(User.ID),
				loginUser.getStr(User.LAST_LOGIN_TIME));

		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		redirect("/admin");
	}

	/**
	 * 验证码
	 */
	public void captcha() {
		render(new MyCaptchaRender(60, 22, 4, true));
	}
}
