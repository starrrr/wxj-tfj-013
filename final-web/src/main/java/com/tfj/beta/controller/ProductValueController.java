package com.tfj.beta.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.jfinal.core.JFinal;
import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.constant.ModuleType;
import com.tfj.beta.constant.OperationType;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.Product;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.model.ProductValue;
import com.tfj.beta.model.Template;
import com.tfj.beta.model.Unit;
import com.tfj.beta.model.UnitUser;
import com.tfj.beta.model.User;
import com.tfj.beta.model.view.PvBean;
import com.tfj.beta.service.OperationLogService;
import com.tfj.beta.service.ProductResultService;
import com.tfj.beta.service.ProductService;
import com.tfj.beta.service.ProductValueService;
import com.tfj.beta.service.TemplateService;
import com.tfj.beta.service.UnitService;
import com.tfj.beta.service.value.ImgWidget;
import com.tfj.beta.service.value.Widget;
import com.tfj.beta.utils.JsonUtil;
import com.tfj.beta.utils.ProcessTypeUtils;
import com.tfj.beta.utils.PropertiesUtil;
import com.tfj.beta.utils.StringUtils;

@ControllerBind(controllerKey = "/admin/productValue", viewPath = "admin/productValue")
public class ProductValueController extends BaseController<ProductValueService> {
	private UnitService unitService = new UnitService();
	private ProductResultService resultService = getService(ProductResultService.class);
	private OperationLogService operationLogService = new OperationLogService();
	private TemplateService templateService=new TemplateService();
	private ProductValueService productValueService=new ProductValueService();
	private ProductResultService productResultService=new ProductResultService();
	private ProductService productService=new ProductService();
	private static final Logger logger = LoggerFactory.getLogger(ProductValueController.class);
	
	private final static String PAGE_PRODUCT_VALUE = "productValueTab.jsp";
	
	private final static String FILE = "File";
	private final static String UPLOADIMG = "uploadImg";
	
	private final static String PRODUCT_ID = "product_id";
	private final static String CATEGORY_ID = "category_id";
	
	private final static String PROCESSTYPE = "processType";
	
	// 定位
	private final static String Locate_Process_Type = "Locate_Process_Type";
	private final static String Locate_Template_Name = "Locate_Template_Name";
	
	
	public final static String FILE_FILENAME = "file-%s-%s%s";
	
	/**
	 * 种类数据列表
	 */
	public void index() {
		Long product_id = getParaToLong(PRODUCT_ID);
		Long category_id = getParaToLong("category_id");

		//判断 产品 对应的模板 如果是 除 空白类型外的数据 全部创建 到t_product_value表
		if((product_id!=null) && "0".equals(Product.dao.findById(product_id).getStr(Product.REMARK))){//判断t_product表remark字段。0：位初始化；1：已初始化
			initProductValue(product_id,category_id);
		}
		ProductService productService = getService(ProductService.class);
		Product p = null;
		
		if (product_id == null) {
			product_id = getSessionAttr(PRODUCT_ID);
			if (product_id != null) {
				p = productService.findById(product_id);
			}
		} else {
			p = productService.findById(product_id);
			// clear session
		}
		
		if (p != null) {
			setAttr("Image_Pattern", PropertiesUtil.getImagePattern());
			
			// 设置定位信息
			ProductResult pr = resultService.getProductResultByProductId(product_id);
			if (pr != null) {
				String processType = pr.getStr(ProductResult.PROCESSTYPE);
				String template_name = pr.getStr(ProductResult.TEMPLATE_NAME);
				
				if (StringUtils.isNotBlank(processType) && StringUtils.isNotBlank(template_name)) {
					setAttr(Locate_Process_Type, processType);
					setAttr(Locate_Template_Name, template_name);
				} else {
					setAttr(Locate_Process_Type, StringUtils.EMPTY);
					setAttr(Locate_Template_Name, StringUtils.EMPTY);	
				}
			} else {
				setAttr(Locate_Process_Type, StringUtils.EMPTY);
				setAttr(Locate_Template_Name, StringUtils.EMPTY);	
			}
			
			setSessionAttr(PRODUCT_ID, product_id);
			setSessionAttr(CATEGORY_ID, p.getLong(Product.CATEGORY_ID));
			setAttr("product", p);
			setAttr("Full_Context_Path", PropertiesUtil.getBase());
			
			 // operation Log
	     	if(StringUtils.isNotBlank(getPara("recordlog"))){//判断是否从导航sidebar进入
	     		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),p.getStr(Product.PRODUCT_NAME),
				OperationType.PRODUCTVALUE_Entry.getCode(), OperationType.PRODUCTVALUE_Entry.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
	     	}
			render(PAGE_PRODUCT_VALUE);
		} else {
			redirect("/admin/product");
		}
	}
	

	public void dataPage() {
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);//当前用户
		String admin=getSessionAttr("adminType");
		List<UnitUser> unitUsers=unitService.getUnitUserByUserId(String.valueOf(loginUser.getLong(User.ID)));
		List<Unit> units=unitService.getUnitByUser(unitUsers);
		Long category_id = getParaToLong("category_id");
		String product_name = getPara("product_name");
		
		keepModel(ProductResult.class);
		ProductResult productResult = getModel(ProductResult.class);
		productResult.set(ProductResult.CATEGORY_ID, category_id);
		
		int pageNumber = getParaToInt(Constant.PAGENUM, 1);
		int pageSize = getParaToInt(Constant.PAGESIZE, 5);
		setAttr("pageList", getService(ProductResultService.class).getProductResultPage(admin,units,
				pageNumber, pageSize, productResult, product_name, null, null));

		render("productValueForm.jsp");
	}
	
	/**
	 * 创建页面树 XML
	 */
	public void createProductValuePage() {
		Long product_id = getSessionAttr(PRODUCT_ID);
		Long category_id = getSessionAttr(CATEGORY_ID);
		String processType = getPara(PROCESSTYPE);
		
		User user = (User) getSessionAttr(Constant.SHIRO_USER);
		
		List<PvBean> tree = service.getPvBean(user, product_id, processType, category_id);
//		printTree(tree);
		StringBuffer html = new StringBuffer();
		for (int i = 0; i < tree.size(); i++) {
			PvBean node = tree.get(i);
//			try {
				html.append(node.generateHTML());
//			} catch (Exception e) {
//				logger.error("createProductValuePage has errors!", e);
//				renderText("PV_ERROR");
//			}
		}
		Map<String, String> result = new HashMap<String, String>();
		result.put("result", SUCCESS);
		result.put("html", html.toString());
		renderText(JsonUtil.parseObjectToJson(result));
	}
	
	public void delMultiProductValue() {
		String data = getPara("data");
		logger.debug("data: " +data);
		
		Long pv_id = Long.valueOf(data);
		//TODO 这里update 原来的值为空 图片换成默认图片
		boolean result = service.delMultiProductValue(pv_id);
		
		if (result) {
			this.getResponse().setStatus(200);
			renderText(SUCCESS);
		} else {
			renderText(FAILED);
		}
	}
	
	public void releaseProductValue() {
		Long pv_id = getParaToLong("pv_id");
		Long template_id = getParaToLong("template_id");
		Long product_id = getParaToLong("product_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		boolean ret = service.unauditProductValue(loginUser, product_id, template_id, pv_id);
		
//		Map<String, String> retInfo = new HashMap<String, String>();
		if (ret) {
//			retInfo.put("html", ImgWidget.EXAMPLE_PNG);
//			retInfo.put("result", ret);
//			renderText(JsonUtil.parseObjectToJson(retInfo));
			Template template=Template.dao.findById(template_id);
			String processName=ProcessTypeUtils.getProcessTypeName(template.getStr(Template.PROCESSTYPE));
			String templateName=template.getStr(Template.KEY_NAME);
			if(templateName.equals("image")){
				templateName="图片";
			}
			Product product=Product.dao.findById(product_id);
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),product.getStr(Product.PRODUCT_NAME)
					+":"+processName+":"+templateName,
			OperationType.PRODUCTVALUE_RELEASE.getCode(), OperationType.PRODUCTVALUE_RELEASE.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			renderText(SUCCESS);
		} else {
			renderText(FAILED);
		}
	}
	
	/**
	 * 新增节点值
	 */
	public void addMultiProductValue() {
		Long product_id = getSessionAttr(PRODUCT_ID);
		Long category_id = getSessionAttr(CATEGORY_ID);
		String para_json = getPara("data");
		JSONArray array = JSON.parseArray(para_json);
		
		boolean result = service.addMultiProductValue(product_id, category_id, array);
		
		if (result) {
			this.getResponse().setStatus(200);
			renderText(SUCCESS);
		} else {
			renderText(FAILED);
		}
	}
	
	/**
	 * 保存节点值
	 */
	public void saveProductValue() {
		Long product_id = getSessionAttr(PRODUCT_ID);
		Long category_id = getSessionAttr(CATEGORY_ID);
		String processType = getPara(PROCESSTYPE);
		String para_json = getPara("data");
		Long focusTemplate_id = getParaToLong("focusInfo", -1L);
		if (StringUtils.isNotBlank(processType) && 
				StringUtils.isNotBlank(para_json)) {
			JSONArray array = JSON.parseArray(para_json);
			
			User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
			String admin=getSessionAttr("adminType");
			
			boolean result = service.saveProductValue(product_id, category_id, 
					processType, focusTemplate_id, array, loginUser);
			
			if (result) {
				this.getResponse().setStatus(200);
				// operation Log
				Product product=Product.dao.findById(product_id);
				operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),product.getStr(Product.PRODUCT_NAME)+":"+ProcessTypeUtils.getProcessTypeName(processType),
				OperationType.PRODUCTVALUE_SAVE.getCode(), OperationType.PRODUCTVALUE_SAVE.getName(),
				loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
				
				renderText(SUCCESS);
			} else {
				renderText(FAILED);
			}
		} else {
			renderText(FAILED);
		}
	}
	
	/**
	 * 环节审核
	 */
	public void auditProductValue() {
		Long product_id = getSessionAttr(PRODUCT_ID);
		Long category_id=getSessionAttr(CATEGORY_ID);
		String processType = getPara("processType");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		boolean result = service.auditProductValue(admin,loginUser, product_id,category_id, processType);
		 
		if (result) {
			this.getResponse().setStatus(200);
			//审核成功，把二维码路径赋空，因为有可能流通批号修改，造成批号图片为历史数据
			if(Constant.WAREHOUSE_PROCESS.equals(processType)){//流通批号
				deleteQRCode(product_id);
			}
			// operation Log
			Product product=Product.dao.findById(product_id);
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),product.getStr(Product.PRODUCT_NAME)+":"+ProcessTypeUtils.getProcessTypeName(processType),
			OperationType.PRODUCTVALUE_AUDIT.getCode(), OperationType.PRODUCTVALUE_AUDIT.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			renderText(SUCCESS);
		} else {
			renderText(FAILED);
		}
	}
	/**
	 * 检查产品是否分配了单位----针对管理
	 */
	public void checkUnitExist(){
		String isFlag="true";
		if(getSessionAttr("adminType")=="true"){
			Long product_id = getSessionAttr(PRODUCT_ID);
			Product product=Product.dao.findById(product_id);
			if(product.getLong("unit_id")==null){
				isFlag="false";
			}
		}
		renderText(isFlag);
	}
	/**
	 * 
	 */
	public void uploadDemoPage() {
		render("newUploadDemo.jsp");
	}
	
	public void uploadDemo() {
		try {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// DiskFileItemFactory中DEFAULT_SIZE_THRESHOLD=10240表示如果上传文件大于10K则会产生上传临时文件
			// 上传临时文件的默认目录为java.io.tmpdir中保存的路径，根据操作系统的不同会有区别

			ServletFileUpload upload = new ServletFileUpload(factory);
			// 设置文件上传的大小限制 1000k
			upload.setFileSizeMax(-1);

			// 设置文件上传的头编码，如果需要正确接收中文文件路径或者文件名
			// 这里需要设置对应的字符编码，为了通用这里设置为UTF-8
			upload.setHeaderEncoding("UTF-8");

			// 解析请求数据包
			List<FileItem> fileItems = upload.parseRequest(this.getRequest());
			
			@SuppressWarnings("unused")
			Map<String, FileItem> itemMap = getUploadFileMap(fileItems);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		renderNull();
	}
	
	/**
	 * XXX Product_Value 先save，后 update 需要优化???
	 * 文件上传
	 */
	public void uploadFile() {
		
		Long product_id = getSessionAttr(PRODUCT_ID);


		String pvPath = ImgWidget.PRODUCT_VALUE_FOLDER + "product_" + product_id;
		
		File file = new File(JFinal.me().getServletContext().getRealPath(pvPath));
		if(!file.exists()){
			file.mkdirs();
		}
		try {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// DiskFileItemFactory中DEFAULT_SIZE_THRESHOLD=10240表示如果上传文件大于10K则会产生上传临时文件
			// 上传临时文件的默认目录为java.io.tmpdir中保存的路径，根据操作系统的不同会有区别
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			// 设置文件上传的大小限制 1000k
			upload.setFileSizeMax(PropertiesUtil.getImageSize());
			
			// 设置文件上传的头编码，如果需要正确接收中文文件路径或者文件名
			// 这里需要设置对应的字符编码，为了通用这里设置为UTF-8
			upload.setHeaderEncoding("UTF-8");
			
			// 解析请求数据包
			List<FileItem> fileItems = upload.parseRequest(this.getRequest());
			Map<String, FileItem> itemMap = getUploadFileMap(fileItems);
			
			// 文件上传 Item
			FileItem uploadItem = itemMap.get(FILE);
			// 上传节点信息
			String id = getItemStrValue(itemMap, "id");
			String uploadType = getItemStrValue(itemMap, "type");
			
			if (StringUtils.isNotBlank(id) && uploadItem != null) {
				
				// 0 : template_id, 1 : pv_id
				String[] info = id.split(Widget.SPLIT);
				String str_template_id = info[0];
				Long template_id = Long.valueOf(str_template_id);
				ProductValue vo = null;
				if (info.length > 1) {
					// 不需要
					vo = service.findById(Long.valueOf(info[1]));
				} else {
					vo = new ProductValue();
					vo.set(ProductValue.PRODUCT_ID, product_id);
					vo.set(ProductValue.TEMPLATE_ID, template_id);
					vo.set(ProductValue.PRODUCT_VALUE, StringUtils.EMPTY);
					vo.set(ProductValue.AUDITFLAG, null);
					vo.set(ProductValue.AUDITORID, null);
					vo.set(ProductValue.AUDITOR, null);
					vo.set(ProductValue.RECORD_ID, null);
					vo.set(ProductValue.RECORD_NAME, null);
					vo.set(ProductValue.RECORD_TIME, null);
					@SuppressWarnings("unused")
					boolean result = vo.save();
					// TODO 用logger
//					System.out.println("save result: " + result + ", " + vo.getLong(ProductValue.ID));
				}
				
				// 上传文件数据
				String itemName = uploadItem.getName();
				// 后缀
				String suffix = itemName.substring(itemName.indexOf(".")).toLowerCase();
				
				String fileName = StringUtils.EMPTY;
				String str_pv_id = String.valueOf(vo.getLong(ProductValue.ID));
				if (UPLOADIMG.equals(uploadType)) {
					fileName = ImgWidget.getFileName(str_template_id, str_pv_id, suffix);
				} else {
					fileName = String.format(FILE_FILENAME, str_template_id, str_pv_id, suffix);
				}
				
				boolean flag = false;
				if (StringUtils.isNotBlank(fileName)) {
					// 保存 到 ProductValue 的 product_value 属性
					String pv = pvPath + "/" + fileName;
					
					File savedFile = new File(file, fileName);
					FileUtils.copyInputStreamToFile(uploadItem.getInputStream(), savedFile);
					
					// 如果不修改图片，则不用修改
					vo.set(ProductValue.PRODUCT_VALUE, pv);
					flag = vo.update();
				}
				
				if (flag) {
					this.getResponse().setStatus(200);
					
					Map<String, String> result = new HashMap<String, String>();
//					result.put("result", SUCCESS);
					
					Long pv_id = vo.getLong(ProductValue.ID);
					result.put("name", template_id + "-" + pv_id);
					
					if (UPLOADIMG.equals(uploadType)) {
						result.put("html", service.generateImgShowHTML(pv_id));
						renderText(JsonUtil.parseObjectToJson(result));
					} else {
						renderText(SUCCESS);
					}
				} else {
					renderText(FAILED);
				}
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderText(FAILED);
	}
	
	/**
	 * 移除节点图片值
	 */
	public void removeImgValue() {
		Long pv_id = getParaToLong("pv_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		
		String ret = service.removeImgValue(loginUser, pv_id);
		
		Map<String, String> retInfo = new HashMap<String, String>();
		if (!FAILED.equals(ret)) {
//			retInfo.put("html", ImgWidget.EXAMPLE_PNG);
			retInfo.put("html", ret);
			renderText(JsonUtil.parseObjectToJson(retInfo));
		} else {
			renderText(FAILED);
		}
	}
	/**
	 * 完成录入
	 */
	public void finishEntry() {
		List<String> processTypelist=new ArrayList<String>();
		processTypelist.add("PLANT_PROCESS");//种植
		processTypelist.add("SUPERVISE_PROCESS");//加工   成品批号
		processTypelist.add("WAREHOUSE_PROCESS");//流通   流通批号
		
		Long product_id = getParaToLong("product_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		String msg = service.checkProductPVImagesHasVale(product_id);
		Map<String, String> retInfo = new HashMap<String, String>();
		if (StringUtils.SUCCESS.equals(msg)) {//代码可优化
			msg=service.checkProductPvHasValueORIsAudit(product_id);
			if(StringUtils.SUCCESS.equals(msg)){
				boolean ret = service.finishEntry(loginUser, product_id);
				if (ret) {
					retInfo.put("result", SUCCESS);
					//普通完成 录入批号
					for (String processType :processTypelist) {
						List<ProductValue> list = ProductValue.dao.getResultDataSource(product_id, processType);
						ProductResult.dao.modifyProductResult("",null,loginUser,list, processType);
					}
					// operation Log
					Product product=Product.dao.findById(product_id);
					operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),product.getStr(Product.PRODUCT_NAME),
					OperationType.PRODUCTVALUE_FINISH.getCode(), OperationType.PRODUCTVALUE_FINISH.getName(),
					loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
				} else {
					retInfo.put("result", FAILED);
				}
			}else {
				retInfo.put("result", FAILED);
				retInfo.put("msg", msg);
				//强制完成 录入批号
				for (String processType : processTypelist) {
					List<ProductValue> list = ProductValue.dao.getResultDataSource(product_id, processType);
					ProductResult.dao.modifyProductResult("",null,loginUser,list, processType);
				}
			}
		} else {
			retInfo.put("result", FAILED);
			retInfo.put("msg", msg);
		}
		renderText(JsonUtil.parseObjectToJson(retInfo));
	}
	
	/**
	 * 注意默认编码 UTF-8
	 * @param data
	 * @param key
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getItemStrValue(Map<String, FileItem> data, 
			String key) throws UnsupportedEncodingException {
		FileItem item = data.get(key);
		if (item != null) {
			return item.getString("UTF-8");
		}
		return null;
	}
	
	/**
	 * 只用于单个文件上传
	 * @param fileItems
	 * @return
	 */
	private Map<String, FileItem> getUploadFileMap(List<FileItem> fileItems) {
		Map<String, FileItem> data = new HashMap<String, FileItem>();
		for (FileItem item : fileItems) {
			if (item.isFormField()) {
				data.put(item.getFieldName(), item);
			} else {
				data.put(FILE, item);
			}
			// XXX 工具类-->动态打印对象属性
//			System.out.println("ItemName: " + item.getName() + ", FieldName: " + item.getFieldName() + ", isFormField: " + item.isFormField());
		}
		return data;
	}
	
	@SuppressWarnings("unused")
	private void printTree(List<PvBean> tree) {
		for (PvBean node : tree) {
			if (node.hasChildren()) {
				printNode(node);
			} else {
				System.out.println(node.getName() + ", " + node.getLevel());
			}
		}
	}
	
	private void printNode(PvBean node) {
		System.out.println(node.getName() + ", " + node.getLevel());
		List<PvBean> children = node.getChildren();
		for (PvBean child : children) {
			if (child.hasChildren()) {
				printNode(child);
			} else {
				System.out.println(child.getName() + ", " + child.getLevel());
			}
		}
	}
	/**
	 * 初始化模板值
	 * @param category_id
	 */
	private void initProductValue(final Long product_id,final Long category_id) {
		List<Template> templates=templateService.getTemplateByCtId(category_id);//根据种类ID获取模板
		boolean isFlag=productValueService.saveProductValue(templates,product_id,category_id);//保存数据	
		if(isFlag){//初始化成功
			productService.finishInit(product_id);//完成初始化 
		}
	}
	/**
	 * 强制完成
	 */
	public void mandatoryfinish(){
		Long product_id = getParaToLong(PRODUCT_ID);
		Long category_id = getParaToLong("category_id");
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String admin=getSessionAttr("adminType");
		boolean isFlag=service.mdfinish(product_id,loginUser);
		if(isFlag){
			//审核成功，把二维码路径赋空，因为有可能流通批号修改，造成批号图片为历史数据
			deleteQRCode(product_id);
			// operation Log
			Product product=Product.dao.findById(product_id);
			operationLogService.recordOperationLog(loginUser,ModuleType.PRODUCTVALUE.getCode(), ModuleType.PRODUCTVALUE.getName(),product.getStr(Product.PRODUCT_NAME),
			OperationType.PRODUCTVALUE_FINISH.getCode(), OperationType.PRODUCTVALUE_FINISH.getName(),
			loginUser.getLong(User.ID), loginUser.getStr(User.USERNAME));
			renderText("true");
		}else {
			renderText("false");
		}
		//redirect("/admin/productValue?product_id="+product_id+"&category_id="+category_id);
	}
	
	/**
	 *更新批号，删除原来的批号
	 */
	private void deleteQRCode(Long product_id) {
		try {
			ProductResult productResult=productResultService.getPResultByProductId(product_id);
			String remark=productResult.getStr("remark");
			if(StringUtils.isNotBlank(remark)&&remark.indexOf(",")>-1){
				String img1=productResult.getStr("remark").split(",")[0];//获取路径;
				String img2=productResult.getStr("remark").split(",")[1];
				String img1Name=img1.substring(img1.lastIndexOf("/")+1);//截取名称
				String img2Name=img2.substring(img1.lastIndexOf("/")+1);
				deletepicture(img1Name);//截取图片名称，删除图片
				deletepicture(img2Name);//
			}
			if(productResult!=null){
				productResult.set("remark", "");
				productResult.update();
			}
		} catch (Exception e) {
		}
	}
	private void deletepicture(String picture) {
		File file=new File(JFinal.me().getServletContext().getRealPath("/upload/QRCode"));
		delete(file,picture);
	}
	private void delete(File file,String picture) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File file2 : files) {
				delete(file2,picture);
			}
		} else {
			String name = file.getName();
			if (picture.equals(name)) {
				file.delete();
			}
		}
	}

}
