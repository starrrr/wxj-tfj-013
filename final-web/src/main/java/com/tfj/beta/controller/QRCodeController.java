package com.tfj.beta.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.jfinal.ext.route.ControllerBind;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.User;
import com.tfj.beta.service.ProductService;
import com.tfj.beta.utils.qrcode.BufferedImageLuminanceSource;
import com.tfj.beta.utils.qrcode.MatrixToImageWriter;

@ControllerBind(controllerKey = "/admin/qrcode", viewPath = "admin/qrcode")
public class QRCodeController extends BaseController<ProductService> {
	
	private final static String PAGE_QRCODE = "qrcode.jsp";
	
	private volatile static BufferedImage logo;

	public void index() {
		render(PAGE_QRCODE);
	}
	
	public void createCode(){
		String text = getPara("text");
//		String hiddenInfo = getPara("hiddenInfo");
//		if (text == null) text = hiddenInfo;
		
		int width = 150;
		int height = 150;
		// 二维码的图片格式
		String format = "png";
		/**
		 * 设置二维码的参数
		 */
		HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		// 内容所使用编码
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
			// 生成二维码
			
			String filePath = getOutputFilePath();
			
			File file = new File(filePath);
			
			if (getLogo() != null) {
				MatrixToImageWriter.writeToFile(bitMatrix, format, file, getLogo());
			} else {
				MatrixToImageWriter.writeToFile(bitMatrix, format, file);
			}
			
			renderHtml(getRequest().getContextPath() + "/tmp/" + file.getName());
			
//			getResponse().setContentType("image/png");
//			MatrixToImageWriter.writeToStream(bitMatrix, format, getResponse().getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			renderNull();
		}
	}
	
	/**
	 * 二维码的解析
	 * 
	 * @param file
	 */
	public void parseCode(File file) {
		try {
			MultiFormatReader formatReader = new MultiFormatReader();

			if (!file.exists()) {
				return;
			}

			BufferedImage image = ImageIO.read(file);

			LuminanceSource source = new BufferedImageLuminanceSource(image);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);

			Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
			hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");

			Result result = formatReader.decode(binaryBitmap, hints);

//			System.out.println("解析结果 = " + result.toString());
//			System.out.println("二维码格式类型 = " + result.getBarcodeFormat());
//			System.out.println("二维码文本内容 = " + result.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private String getOutputFilePath() {
		User loginUser = (User) getSessionAttr(Constant.SHIRO_USER);
		String dir = getTmpPath();
		String fileName =  "QRCode_" + loginUser.getStr(User.USERNAME) +  System.currentTimeMillis() + ".png";
		return dir + fileName;
	}
	
	private String getTmpPath() {
		String webPath = getSession().getServletContext().getRealPath("/"); 
		return webPath + "/tmp/";
	}
	
	private BufferedImage getLogo() {
		try {
			if (logo == null) {
				String webPath = getSession().getServletContext().getRealPath("/"); 
				File file = new File(webPath + "/static/logo/logo.png");
				logo = ImageIO.read(file);
			}
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}
		return logo;
	}
}
