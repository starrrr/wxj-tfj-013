package com.tfj.beta.listener;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * session 过期处理
 * Created by Administrator on 2015/6/18.
 */
public class WebSessionListener extends SessionListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSessionListener.class);

    @Override
    public void onStop(Session session) {
        super.onStop(session);
        setOffLineStatus(session);
    }

    @Override
    public void onExpiration(Session session) {
        super.onExpiration(session);
        setOffLineStatus(session);
    }

    private void setOffLineStatus(Session session) {
//        User user = (User)session.getAttribute(Constant.SHIRO_USER);
//        if(null != user) {
//            user.set(User.LOGIN_STATUS, ModelEnum.OnlineOrOffline.OFFLINE.ordinal()).update();
//            LOGGER.info("用户[{}]下线", user.getStr(User.USERNAME));
//        }
    }
}
