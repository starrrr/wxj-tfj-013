package com.tfj.beta.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.ext.plugin.shiro.ShiroMethod;

/**
 * 简单的未登陆拦截
 * Created by Administrator on 2015/6/11.
 */
public class ManagerInterceptor implements Interceptor {

	 public void intercept(ActionInvocation ai) {
	        Controller controller = ai.getController();
	        //ai.getActionKey().startsWith("")
	        // 认证通过
	        if (ai.getActionKey().startsWith("/admin/index")||ShiroMethod.authenticated()) {
	            ai.invoke();
	        } else {
	            controller.redirect("/admin/login/index");
	        }
	    }
}
