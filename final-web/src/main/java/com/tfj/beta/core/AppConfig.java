package com.tfj.beta.core;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.dreampie.shiro.freemarker.ShiroTags;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Const;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.plugin.quartz.QuartzPlugin;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.plugin.tablebind.SimpleNameStyles;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.tx.TxByActionMethods;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.IErrorRenderFactory;
import com.jfinal.render.RedirectRender;
import com.jfinal.render.Render;
import com.tfj.beta.interceptor.ManagerInterceptor;
import com.tfj.beta.model.FrontEndRole;
import com.tfj.beta.model.ProductResult;
import com.tfj.beta.service.IndexService;
import com.tfj.beta.service.RedisCacheService;
import com.tfj.beta.utils.PropertiesUtil;

/**
 * 工程最核心入口配置类 主要配置 plugin inteceptor ...
 * 
 * @author Administrator
 *
 */
public class AppConfig extends JFinalConfig {

	private static final Logger log = LoggerFactory.getLogger(AppConfig.class);
	private IndexService indexService = new IndexService();
	private RedisCacheService redisCacheService = new RedisCacheService();
	private Routes routes;

	@Override
	public void configConstant(Constants constants) {

		loadPropertyFile("config.properties");

		// 开发模式
		constants.setDevMode(getPropertyToBoolean("app.devMode", false));
		constants.setEncoding(Const.DEFAULT_ENCODING);
		constants.setBaseViewPath("/WEB-INF/pages");

		// shiro 页面标签
		FreeMarkerRender.getConfiguration().setSharedVariable("shiro",
				new ShiroTags());
		// 全局错误页面跳转拦截器
		constants.setErrorView(401, "error");
		constants.setErrorView(403, "error");
		constants.setErrorRenderFactory(new IErrorRenderFactory() {

			public Render getRender(int errorCode, String view) {
				return new RedirectRender(view);
			}
		});

	}

	@Override
	public void configRoute(Routes routes) {
		this.routes = routes;
		routes.add(new AutoBindRoutes());
	}

	@Override
	public void configPlugin(Plugins plugins) {
		// 缓存插件
		plugins.add(new EhCachePlugin());
		// shiro
		plugins.add(new ShiroPlugin(this.routes));

		// 数据源插件
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbc.url"),
				getProperty("jdbc.username"), getProperty("jdbc.password"),
				getProperty("jdbc.driver"));
		// 数据库监控
		WallFilter wallFilter = new WallFilter();
		wallFilter.setDbType("mysql");
		//监控统计："stat" 防SQL注入："wall"组合使用： "stat,wall"
		druidPlugin.addFilter(wallFilter);
		druidPlugin.addFilter(new StatFilter());
		plugins.add(druidPlugin);

		// 自动绑定
		AutoTableBindPlugin autoTableBindPlugin = new AutoTableBindPlugin(
				druidPlugin, SimpleNameStyles.LOWER_UNDERLINE);
		autoTableBindPlugin.setShowSql(getPropertyToBoolean("app.showSql",
				false));
		autoTableBindPlugin.setDialect(new MysqlDialect());
		plugins.add(autoTableBindPlugin);

		// 任务器
		 QuartzPlugin quartzPlugin = new QuartzPlugin("job.properties");
		 plugins.add(quartzPlugin);

	}

	@Override
	public void configInterceptor(Interceptors interceptors) {

		// 自定义未登陆拦截
		interceptors.add(new ManagerInterceptor());

		// shiro权限拦截器配置
		interceptors.add(new ShiroInterceptor());

		// 解决session在freemarker中不能取得的问题 获取方法：${session["manager"].username}
		interceptors.add(new SessionInViewInterceptor());

		// 添加事务，对add、save、update和delete自动进行拦截
		interceptors.add(new TxByActionMethods("add", "save", "update",
				"delete"));
	}

	@Override
	public void configHandler(Handlers handlers) {

		// 跨域 Rest 配置handler

		handlers.add(new ContextPathHandler());

		DruidStatViewHandler druidStatViewHandler = new DruidStatViewHandler(
				"/admin/druid", new IDruidStatViewAuth() {

					public boolean isPermitted(HttpServletRequest request) {
						HttpSession session = request.getSession(false); // 不存在会话，不新建
						// return (session != null &&
						// ((User)session.getAttribute(Constant.SHIRO_LOGIN_USER)).getStr(User.USERNAME).equals(Constant.SYSTEM_ADMINISTRATOR));
						return true;
					}
				});
		handlers.add(druidStatViewHandler);

	}

	/**
	 * 处理一些初始话的工作 缓存清空，缓存最新的json等数据
	 */
	public void afterJFinalStart() {
		Db.tx(new IAtom(){//事物处理
			public boolean run() throws SQLException {
				int dropFlag = Db.update("DROP TABLE IF EXISTS `t_index_count`");
				int creFlag = Db.update("CREATE TABLE `t_index_count` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',`count_openid` int(11) DEFAULT NULL COMMENT '微信用户总数',`count_G_openid` int(11) DEFAULT NULL COMMENT '游客',`count_C_openid` int(11) DEFAULT NULL COMMENT '关注用户',`count_B_openid` int(11) DEFAULT NULL COMMENT '企业用户',`count_category` int(11) DEFAULT NULL COMMENT '产品种类',`count_categoryrecord` int(11) DEFAULT NULL COMMENT '产品记录数',`count_plantbase` int(11) DEFAULT NULL COMMENT '基地总数',PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8");
				int insFlag = Db.update("insert into t_index_count values('1','0','0','0','0','0','0','0')");
				int prFlag = Db.update("call pr_count()");
				return dropFlag==0&&creFlag==0&&insFlag>0;
			}});
		Boolean loadRedis = PropertiesUtil.getRedisInitialMode();
		
		// 判断是否预加载 redis
		if (loadRedis) {

			// 查询复核打印二维码条件的结果表
			List<ProductResult> list = ProductResult.dao.findQRCodeList();
			if (null != list && list.size() > 0) {
				for (ProductResult productResult : list) {
					Long categoryId = productResult.getLong("category_id");
					Long productId = productResult.getLong("product_id");
					// 缓存json
					redisCacheService.cacheJson(categoryId, productId,null,FrontEndRole.GUEST,true);
					redisCacheService.cacheJson(categoryId, productId,Constant.PLANT_PROCESS,FrontEndRole.GUEST,false);//缓存三种不同的角色  （G:游客；B：企业用户；C：关注用户）
					redisCacheService.cacheJson(categoryId, productId,Constant.SUPERVISE_PROCESS,FrontEndRole.GUEST,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.WAREHOUSE_PROCESS,FrontEndRole.GUEST,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.SALE_PROCESS,FrontEndRole.GUEST,false);
					
					redisCacheService.cacheJson(categoryId, productId,null,FrontEndRole.BUSINESS,true);
					redisCacheService.cacheJson(categoryId, productId,Constant.PLANT_PROCESS,FrontEndRole.BUSINESS,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.SUPERVISE_PROCESS,FrontEndRole.BUSINESS,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.WAREHOUSE_PROCESS,FrontEndRole.BUSINESS,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.SALE_PROCESS,FrontEndRole.BUSINESS,false);
					
					redisCacheService.cacheJson(categoryId, productId,null,FrontEndRole.CUSTOMER,true);
					redisCacheService.cacheJson(categoryId, productId,Constant.PLANT_PROCESS,FrontEndRole.CUSTOMER,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.SUPERVISE_PROCESS,FrontEndRole.CUSTOMER,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.WAREHOUSE_PROCESS,FrontEndRole.CUSTOMER,false);
					redisCacheService.cacheJson(categoryId, productId,Constant.SALE_PROCESS,FrontEndRole.CUSTOMER,false);
				}

			}
		}
	}
	
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 9000, "/", 10);
	}
}