package com.tfj.beta.constant;

import java.util.HashMap;
import java.util.Map;
/**
 * 操作类型
 * @author xiaoqing
 *
 */
public enum OperationType {
	//public 
	SELECT("select", "页面显示"),
	RELEASE("release ","审核释放"),
	PWUPDATE("pwupdate","密码修改"),
	MANDATORY_DELETE("mandatoryDelete","强制删除"),
	USERCANCELLATION("usercancellation","用户注销"),
	SHOW("show","二维码显示"),
	//category
	CATEGORY_SHOW("categoryShow", "种类管理页面显示"),
	CATEGORY_ADD("categoryAdd","种类添加"),
	CATEGORY_DELETE("categoryDelete","种类删除"),
	CATEGORY_AUDIT("categoryAudit","种类审核"),
	CATEGORY_UPDATE("categoryUpdate","种类修改"),
	//plantbase
	PLANTBASE_SHOW("plantbaseShow", "基地建设页面显示"),
	PLANTBASE_ADD("plantbaseAdd","基地添加"),
	PLANTBASE_DELETE("plantbaseDelete","基地删除"),
	PLANTBASE_UPDATE("plantbaseUpdate","基地修改"),
	PLANTBASE_AUDTI("plantbaseAudit","基地审核"),
	//productresult
	PRODUCTRESULT_SHOW("productresultShow", "二维码生成页面显示"),
	//product
	PRODUCT_SHOW("productShow", "产品基础信息管理页面显示"),
	PRODUCT_ADD("productAdd","产品添加"),
	PRODUCT_DELETE("productDelete","产品删除"),
	PRODUCT_UPDATE("productUpdate","产品修改"),
	PRODUCT_AUDTI("productAudit","产品审核"),
	PRODUCT_COPY("productCopy","产品复制"),
	
	//template
	TEMPLATE_SHOW("templateShow", "种类模板管理页面显示"),
	CATEGORY_SELECT("categoryselect","种类选择"),
	TEMPLATE_DELETE("templatedelete","节点删除"),
	TEMPLATE_ADD("templateadd","节点添加"),
	TEMPLATE_UPDATE("templateupdate","节点修改"),
	TEMPLATE_CU("templatecu","更新模板"),

	//productvalue
	PRODUCTVALUE_Entry("productvalueEntry","数据录入"),
	PRODUCTVALUE_SAVE("productvalueSave","保存录入数据"),
	PRODUCTVALUE_AUDIT("productvalueAudit","审核数据录入"),
	PRODUCTVALUE_FINISH("productvalueFinish","完成数据录入"),
	PRODUCTVALUE_RELEASE("productvaluerelease","产品值释放"),
	//wechat
	WECHAT_SHOW("wechatshow","微信扫描记录页面显示"),
	//wechatopenid
	WECHATOPENID_SHOW("wechatopenidshow","微信用户管理页面显示"),
	//wechatrole
	WECHATROLE_SHOW("wechatroleShow","微信角色管理页面显示"),
	WECHATROLE_ADD("wechatroleAdd","微信角色添加"),
	WECHATROLE_DELETE("wechatroleDelete","微信角色删除"),
	WECHATROLE_UPDATE("wechatroleUpdate","微信角色修改"),
	//enterprise
	ENTERPRISE_SHOW("enterpriseSHOW","企业绑定号管理页面显示"),
	ENTERPRISE_ADD("enterpriseAdd","企业用户添加"),
	ENTERPRISE_DELETE("enterpriseDelete","企业用户删除"),
	ENTERPRISE_UPDATE("enterpriseUpdate","企业用户修改"),
	ENTERPRISE_REMOVEBIND("enterpriseRemoveBind","企业用户解绑"),
	//unit
	UNIT_ADD("unitAdd","单位添加"),
	UNIT_UPDATE("unitUpdate","单位修改"),
	//user
	USER_ADD("userAdd","用户添加"),
	USER_DELETE("userDelete","用户删除"),
	USER_UPDATE("userUpdate","用户修改"),
	//role:
	ROLE_ADD("roleAdd","角色添加"),
	ROLE_DELETE("roleDelete","角色删除"),
	ROLE_UPDATE("roleUpdate","角色修改"),
	ROLE_AUTHORIZE("roleAuthorize","角色授权"),
	ROLE_AUTHORIZE_DELETE("roleAuthorize","角色授权删除"),
	//dict:
	DICT_ADD("dictAdd","数据字典添加"),
	DICT_DELETE("dictDelete","数据字典删除"),
	DICT_UPDATE("dictUpdate","数据字典修改"),
	//menuResc:
	MENURESC_ADD("menuRescAdd","菜单资源添加"),
	MENURESC_DELETE("menuRescDelete","菜单资源删除"),
	MENURESC_UPDATE("menuRescUpdate","菜单资源修改"),
	//operation
	OPERATION_DELETE("operation","操作日志清理"),
	
	;
	
	private String code;

	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	private OperationType(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public static String getModuleNameByCode(String code ){
		OperationType[] moduleTypes = OperationType.values();
		String name = null;
		for (int i = 0; i < moduleTypes.length; i++) {
			OperationType type = moduleTypes[i];
			if(code.equalsIgnoreCase(type.getCode())){				
				name = type.getName();
				break;
			}
		}
		return name;
	}
	
	public static String getModuleCodeByName(String name ){
		OperationType[] moduleTypes = OperationType.values();
		String code = null;
		for (int i = 0; i < moduleTypes.length; i++) {
			OperationType type = moduleTypes[i];
			if(name.equalsIgnoreCase(type.getName())){				
				code = type.getCode();
				break;
			}
		}
		return code;
	}
	
	/**
	 * 获取所有的模块名称
	 * @return
	 */
	public static String[] getModulesName() {
		OperationType[] moduleTypes = OperationType.values();
		String[] names = new String[moduleTypes.length];
		for (int i = 0; i < moduleTypes.length; i++) {
			OperationType type = moduleTypes[i];
			names[i] = type.getName();
		}
		return names;
	}

	/**
	 * 获取所有的模块编码
	 * @return
	 */
	public static String[] getModulesCode() {
		OperationType[] moduleTypes = OperationType.values();
		String[] names = new String[moduleTypes.length];
		for (int i = 0; i < moduleTypes.length; i++) {
			OperationType type = moduleTypes[i];
			names[i] = type.getCode();
		}
		return names;
	}

	/**
	 * 获取所有的模块Map
	 * @return
	 */
	public static Map getModulesMap() {
		OperationType[] moduleTypes = OperationType.values();
		Map<String, String> map = new HashMap();
		for (int i = 0; i < moduleTypes.length; i++) {
			OperationType type = moduleTypes[i];
			map.put(type.getCode(), type.getName());
		}
		return map;
	}

	public static void main(String[] args) {
		/*String[] names = getModulesName();
		for (String name : names) {
			System.out.println(name);
		}*/
		//System.out.println(getModuleCodeByName("基地管理"));
	}

}
