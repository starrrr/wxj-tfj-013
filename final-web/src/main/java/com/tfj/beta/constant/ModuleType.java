package com.tfj.beta.constant;

import java.util.HashMap;
import java.util.Map;
/**
 * 操作模块
 * @author xiaoqing
 *
 */
public enum ModuleType {
	
	CATEGORY("category", "种类管理"),
	PLANT_BASE("plantBase", "基地建设"), 
	PRODUCTRESULT("productresult", "二维码生成"), 
	PRODUCT("product", "产品基础信息管理"),
	TEMPLATE("template", "种类模板管理"),
	PRODUCTVALUE("productvalue", "产品数据录入"),
	WECHAT("wechat", "微信扫描记录"),
	WECHATOPENID("wechatopenid", "微信用户管理"),
	WECHATROLE("wechatrole", "微信角色管理"),
	ENTERPRISE("enterprise", "企业绑定号管理"),
	UNIT("unit", "单位管理"),
	
	USER("user", "用户管理"),
	ROLE("role", "角色管理"),
	MENURESC("menuresc", "菜单资源"),
	DICT("dict", "数据字典"),
	LOGINLOG("loginlog", "登录日志"),
	OPERATIONLOG("operationlog", "操作日志");

	private String code;

	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	private ModuleType(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public static String getModuleNameByCode(String code ){
		ModuleType[] moduleTypes = ModuleType.values();
		String name = null;
		for (int i = 0; i < moduleTypes.length; i++) {
			ModuleType type = moduleTypes[i];
			if(code.equalsIgnoreCase(type.getCode())){				
				name = type.getName();
				break;
			}
		}
		return name;
	}
	
	public static String getModuleCodeByName(String name ){
		ModuleType[] moduleTypes = ModuleType.values();
		String code = null;
		for (int i = 0; i < moduleTypes.length; i++) {
			ModuleType type = moduleTypes[i];
			if(name.equalsIgnoreCase(type.getName())){				
				code = type.getCode();
				break;
			}
		}
		return code;
	}
	
	/**
	 * 获取所有的模块名称
	 * @return
	 */
	public static String[] getModulesName() {
		ModuleType[] moduleTypes = ModuleType.values();
		String[] names = new String[moduleTypes.length];
		for (int i = 0; i < moduleTypes.length; i++) {
			ModuleType type = moduleTypes[i];
			names[i] = type.getName();
		}
		return names;
	}

	/**
	 * 获取所有的模块编码
	 * @return
	 */
	public static String[] getModulesCode() {
		ModuleType[] moduleTypes = ModuleType.values();
		String[] names = new String[moduleTypes.length];
		for (int i = 0; i < moduleTypes.length; i++) {
			ModuleType type = moduleTypes[i];
			names[i] = type.getCode();
		}
		return names;
	}

	/**
	 * 获取所有的模块Map
	 * @return
	 */
	public static Map getModulesMap() {
		ModuleType[] moduleTypes = ModuleType.values();
		Map<String, String> map = new HashMap();
		for (int i = 0; i < moduleTypes.length; i++) {
			ModuleType type = moduleTypes[i];
			map.put(type.getCode(), type.getName());
		}
		return map;
	}

	public static void main(String[] args) {
		/*String[] names = getModulesName();
		for (String name : names) {
			System.out.println(name);
		}*/
		//System.out.println(getModuleCodeByName("基地管理"));
	}

}
