package com.tfj.beta.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

/**
 * Created by Administrator on 2015/6/18.
 */
public class SessionHandler extends Handler {

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        int index = target.indexOf(";jsessionid".toUpperCase());

        if (index != -1) {
            target = target.substring(0, index);
        }
        nextHandler.handle(target, request, response, isHandled);
    }
}
