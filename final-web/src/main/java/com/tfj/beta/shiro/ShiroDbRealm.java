package com.tfj.beta.shiro;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.kit.StrKit;
import com.tfj.beta.core.Constant;
import com.tfj.beta.model.MenuResc;
import com.tfj.beta.model.RoleResc;
import com.tfj.beta.model.RoleUser;
import com.tfj.beta.model.User;
import com.tfj.beta.service.MenuRescService;

/**
 * 用于配置验证 和授权 实现类
 * Created by Administrator on 2015/6/9.
 */
public class ShiroDbRealm extends AuthorizingRealm {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShiroDbRealm.class);

    MenuRescService menuService = new MenuRescService();
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;

        if (StrKit.isBlank(usernamePasswordToken.getUsername()))
            throw new AccountException("用户名为空");

        try {
            User loginUser = User.dao.searchActiveByUserName(usernamePasswordToken.getUsername());
            String password = loginUser.getStr(User.PASSWORD);
            if (password != null) {
                Subject subject = SecurityUtils.getSubject();
                // 登陆用户设置在session
                subject.getSession().setAttribute(Constant.SHIRO_USER, loginUser);
                return new SimpleAuthenticationInfo(loginUser, password, getName());
            } else {
                throw new AuthenticationException("密码为空");
            }

        } catch (Exception e) {
            final String message = "There was a SQL error while authenticating user [" + usernamePasswordToken.getUsername() + "]";
            LOGGER.error("Exception:{}", e.getMessage(), e);
            throw new AuthenticationException(message, e);
        }
    }

    @Override
    
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    	
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        User loginUser = (User) getAvailablePrincipal(principals);
        // User loginUser = (User) principals.fromRealm(getName()).iterator().next();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        try {
            // Retrieve roles and permissions from database
            List<RoleUser> roleUsers = RoleUser.dao.searchEnableRoleUserByUserId(loginUser.getLong(User.ID));
            getPermissions(info, loginUser.getStr(User.USERNAME), roleUsers);
        } catch (Exception e) {
            final String message = "There was a SQL error while authorizing user [" + loginUser.getStr(User.USERNAME) + "]";
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(message, e);
            }
            // Rethrow any SQL errors as an authorization exception
            throw new AuthorizationException(message, e);
        }
//        Set<String> strings = info.getStringPermissions();
//        for (String string : strings) {
//        	
//        }
        return info;
    }

    protected void getPermissions(SimpleAuthorizationInfo info, String username, List<RoleUser> roleUsers) {

        for (RoleUser roleUser : roleUsers) {
            if (!username.equals(Constant.SYSTEM_ADMINISTRATOR)) {
                List<RoleResc> roleRescs = RoleResc.dao.searchRoleRescByRoleId(roleUser.getInt(RoleUser.ROLE_ID));
                for (RoleResc roleResc : roleRescs) {
                	
                	MenuResc menuResc =	menuService.findMenuRescById(roleResc.getInt(RoleResc.RESC_ID));
                    info.addStringPermission(menuResc.getStr(MenuResc.PERMISSION));
                   	//LOGGER.error("permission::::"+menuResc.getStr(MenuResc.PERMISSION));
                }
            } else {
                List<MenuResc> rescs = MenuResc.dao.searchAll(); // 管理员默认获取全部权限
                for (MenuResc resc : rescs) {
                	//LOGGER.error("permission::::"+resc.getStr(MenuResc.PERMISSION));
                    info.addStringPermission(resc.getStr(MenuResc.PERMISSION));
                	// info.addStringPermission(String.valueOf(resc.getStr(MenuResc.PERMISSION)));
                }
            }

            info.addRole(String.valueOf(roleUser.getInt(RoleUser.ROLE_ID)));
        }
    }
}
