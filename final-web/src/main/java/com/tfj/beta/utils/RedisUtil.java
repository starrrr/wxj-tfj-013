package com.tfj.beta.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.jfinal.config.JFinalConfig;

public class RedisUtil {

	private static String ADDR = PropertiesUtil.getRedisHost();

	private static int PORT = Integer.parseInt(PropertiesUtil.getRedisPort());

	// private static String AUTH = "admin" ;

	// 可用连接实例的最大数目，默认为8

	// 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)

	private static int MAX_ACTIVE = Integer.parseInt(PropertiesUtil
			.getRedisMaxActive());

	// 控制一个pool最多由个少个空闲的jedis实例，默认也是8个。

	private static int MAX_iDLE = Integer.parseInt(PropertiesUtil
			.getRedisMaxIdle());

	// 等待可用连接的最大时间，单位毫秒，默认为-1，表示永不超时。如果超过，抛出JedisConnectionException

	private static int MAX_WAIT = Integer.parseInt(PropertiesUtil
			.getRedisMaxWait());

	private static int TIMEOUT = Integer.parseInt(PropertiesUtil
			.getRedisTimeout());

	// 在borro一个jedis实例时，是否提前经行validate操作；如果为true则得到的jedis实例均是可用的。

	private static boolean TEST_ON_BORROW = true;

	private static Jedis jedis = null;
	private static JedisPool jedisPool = null;

	/**
	 * 
	 * 初始化Redis连接池
	 */
	public static JedisPool getJedisPool() {
		if (jedisPool != null) {
			return jedisPool;
		} else {
			try {
				JedisPoolConfig config = new JedisPoolConfig();
				config.setMaxTotal(MAX_ACTIVE);
				config.setMaxIdle(MAX_iDLE);
				config.setMaxWaitMillis(MAX_WAIT);
				config.setTestOnBorrow(TEST_ON_BORROW);
				jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT);
				return jedisPool;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

	}

	/**
	 * 
	 * 获取Jedis实例
	 */
	public synchronized static Jedis getJedis() {
		if (jedis != null) {
			return jedis;
		} else {
			try {
				String auth = PropertiesUtil.getRedisAuth();
				JedisPool jedisPool = getJedisPool();

				jedis = jedisPool.getResource();
				if (StringUtils.isNotBlank(auth)) {
					jedis.auth(auth);
				}

				return jedis;

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

	}

	/**
	 * 
	 * 释放Jedis资源
	 */
	public static void returnResource(final Jedis jedis) {
		if (jedis != null) {
			// jedisPool.returnResource(jedis);

			jedisPool.returnResourceObject(jedis);
		}
	}

	/**
	 * 根据缓存string
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public static boolean cacheStrByKey(String key, String value) {
		boolean flag = true;
		try {
			RedisUtil.getJedis().set(key, value);
		} catch (Exception e) {
			flag = false;
		}

		return flag;

	}

	/**
	 * 根据key从 Redis获取缓存值
	 * 
	 * @param key
	 * @return
	 */
	public static Object getCacheValueByKey(String key) {

		return RedisUtil.getJedis().get(key);
	}

	/**
	 * 清空redis缓存池
	 */
	public static void clearDataBase() {
		RedisUtil.getJedis().flushAll();
	}

	/**
	 * 删除 redis key
	 * 
	 * @param key
	 * @return
	 */
	public static boolean delCacheKey(String key) {
		try {
			RedisUtil.getJedis().del(key);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean expire(String key, int value) {
		boolean flag = true;
		try {
			if (null == RedisUtil.getJedis()) {
				return false;
			} else {
				RedisUtil.getJedis().expire(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}

		return flag;

	}

	public static void main(String[] args) {
		RedisUtil.cacheStrByKey("test", "zlh");
		System.out.println(RedisUtil.getCacheValueByKey("test"));
	}

}