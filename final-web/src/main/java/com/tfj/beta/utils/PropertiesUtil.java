package com.tfj.beta.utils;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 获得配置文件对象
 * 
 * @author zlh
 * 
 */
public class PropertiesUtil {

	private static Logger logger = Logger.getLogger(PropertiesUtil.class);

	private static Properties properties = new Properties();

	private PropertiesUtil() {
	}

	static {
		try {
			// 生成输入流
			// 生成文件对象
			
			InputStream inputStream = PropertiesUtil.class.getResourceAsStream("/config.properties");
			properties.load(inputStream);
		} catch (Exception ex) {
			logger.error("加载配置文件失败:" + ex);
			throw new RuntimeException("加载配置文件失败:" + ex);
		}
	}

	public static Properties getInstance() {
		return properties;
	}
	
	public static boolean getAppDevMode(Boolean defaultValue) {
		String value = properties.getProperty("app.devMode").trim();
		return (value != null) ? Boolean.parseBoolean(value) : defaultValue;
	}

	public static String getRedisHost() {
		return properties.getProperty("redis_host");
	}
	
	public static String getRedisPort() {
		return properties.getProperty("redis_port");
	}

	public static String getRedisMaxActive() {
		return properties.getProperty("redis_maxactive");
	}

	public static String getRedisMaxIdle() {
		return properties.getProperty("redis_maxidle");
	}

	public static String getRedisMaxWait() {
		return properties.getProperty("redis_maxwait");
	}
	
	public static String getRedisTimeout() {
		return properties.getProperty("redis_timeout");
	}
	
	
	public static String getBase(){
		return properties.getProperty("base");
	}
	
	public static String getRedisClusterNodes(){
		return properties.getProperty("redis_cluster_node");
	}
	
	public static String getRedisClusterAuth(){
		return properties.getProperty("redis_cluster_auth");
	}
	
	public static String getRedisAuth(){
		return properties.getProperty("redis_auth");
	}
	
	public static String getOpenIdRole(){
		return properties.getProperty("openid_role_name");
	}
	
	
	
	public static boolean getRedisClusterMode(){
		return properties.getProperty("redis_cluster_mode").trim().equalsIgnoreCase("Y")?true:false;
	}
	
	public static boolean getRedisInitialMode(){
		return properties.getProperty("redis_initial_mode").trim().equalsIgnoreCase("Y")?true:false;
	}
	
	public static boolean getWechatAuth(){
		return properties.getProperty("wechatAuth").trim().equalsIgnoreCase("Y")?true:false;
	}
	
	
	public static Integer getImageSize(){
		return Integer.parseInt(properties.getProperty("image_size").trim());
	}
	public static String getImagePattern(){
		return properties.getProperty("image_pattern").trim();
	}
	
	public static String getToken(){
		return properties.getProperty("token");
	}
	
	public static String getAppId(){
		return properties.getProperty("appId");
	}
	
	public static String getAppSecret(){
		return properties.getProperty("appSecret");
	}
	
	public static String getJDBCUrl(){
		return properties.getProperty("jdbc.url");
	}
	
	public static String getJDBCUsername(){
		return properties.getProperty("jdbc.username");
	}
	
	public static String getJDBCPassword(){
		return properties.getProperty("jdbc.password");
	}
	
	public static String getJDBCDriver(){
		return properties.getProperty("jdbc.driver");
	}
	
	public static void main(String[] args) {
		System.out.println("----------------" + PropertiesUtil.getJDBCDriver());
		 //System.out.println("+++"+PropertiesUtil.getToken());

	}

}
