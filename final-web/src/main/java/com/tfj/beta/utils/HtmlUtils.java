package com.tfj.beta.utils;

public class HtmlUtils {
	
	public final static String LI_START = "<li>";
	public final static String LI_END = "</li>";
	
	public final static String LI_WRAP = "<li>%s</li>";

	public final static String EVERY_STEP_START = "<div class=\"everyStep\">";
	public final static String EVERY_STEP_END = "</div>";
	
	public final static String DETAIL_STEP_START = "<ul class=\"detailStep\">";
	public final static String DETAIL_STEP_END = "</ul>";
	
	public final static String STEP_TITLE = "<p class=\"stepTitle\">%s</p>";
	
	public final static String STEP_TITLE_NAME = "<p class=\"stepTitle\">" + 
											     "<span>%s</span>" +
											     "</p>";
	
	public final static String UL_WRAP = "<ul class=\"%s\">%s</ul>";
	
	// TODO
	public final static String PIC_PATH = PropertiesUtil.getBase() + "static/admin/index/img/";
	
	public final static String NODE_NO_CHILDREN = "<p>" + 
											      "<img class=\"leftIcon\" src=\"%s\"/>" + 
											      "<span class=\"leftProperty\">%s</span>" + 
											      "%s" + 
											      "</p>";
	
	public final static String NODE_HAS_CHILDREN = "<p>" + 
											       "<img class=\"leftIcon\" src=\"%s\"/>" + 
                                                   "<span class=\"leftProperty\">%s</span>" + 
                                                   "<img class=\"rightIcon\" src=\"" + PIC_PATH + "icon_up.png\"/>" + 
                                                   "%s" + 
                                                   "</p>";
	
	public final static String TOP = "<span class=\"leftProperty\">%s</span>%s"; 
	
//	public final static String TOP = "<img class=\"leftIcon\" src=\"%s\"/>" + 
//									 "<span class=\"leftProperty\">%s</span>" + 
//                                     "%s"; 
	
	public static String getTemplateHtml(String template, Object... args) {
		return String.format(template, args);
	}
}
