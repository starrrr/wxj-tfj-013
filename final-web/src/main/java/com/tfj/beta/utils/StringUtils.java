package com.tfj.beta.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2015/6/9.
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
	
	public final static String SUCCESS = "SUCCESS";
	
	public final static String FAILED = "FAILED";
	
	/**
	 * 获得用户远程地址
	 */
	public static String getRemoteAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String escapeSql(String str) {
		if (str == null) {
			return null;
		}
		return StringUtils.replace(str, "'", "''");
	}
	
	
    public static String toParentheseString(String[] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "()";

        StringBuilder b = new StringBuilder();
        b.append('(');
        for (int i = 0; ; i++) {
            b.append("'" + String.valueOf(a[i]) + "'");
            if (i == iMax)
		return b.append(')').toString();
	    b.append(", ");
        }
    }
}
