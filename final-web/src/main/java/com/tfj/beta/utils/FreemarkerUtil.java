package com.tfj.beta.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerUtil {
	private static FreemarkerUtil util = null;

	public static FreemarkerUtil getInstantce() {
		if (null != util) {
			return util;
		} else {
			util = new FreemarkerUtil();
			return util;
		}
	}

	public Template getTemplate(String name) {
		try {
			// 通过Freemaker的Configuration读取相应的ftl
			Configuration cfg = new Configuration();
			// 设定去哪里读取相应的ftl模板文件
			cfg.setClassForTemplateLoading(this.getClass(), "/component/");
			// 在模板文件目录中找到名称为name的文件
			Template temp = cfg.getTemplate(name);
			return temp;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 控制台输出
	 * 
	 * @param name
	 * @param root
	 */
	public void print(String name, Map<String, Object> root) {
		try {
			// 通过Template可以将模板文件输出到相应的流
			Template temp = this.getTemplate(name);
			temp.process(root, new PrintWriter(System.out));
		} catch (TemplateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 输出HTML文件
	 * 
	 * @param name
	 * @param root
	 * @param outFile
	 */
	public String fprint(String name, Map<String, Object> root) {
		Writer out = null;
		String html = null;
		try {

			// 通过一个文件输出流，就可以写到相应的文件中，此处用的是绝对路径
			out = new StringWriter(2048);
			Template temp = this.getTemplate(name);
			temp.process(root, out);
			out.flush();
			html = out.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return html;
	}

	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("text_id", "ddddd");
		map.put("text_value", "333333333");

		FreemarkerUtil.getInstantce().fprint("text.ftl", map);
	}

}
