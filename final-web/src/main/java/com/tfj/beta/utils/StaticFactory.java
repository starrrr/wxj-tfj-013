package com.tfj.beta.utils;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tfj.beta.model.ModelEnum;
import com.tfj.beta.model.WebConfig;

/**
 * 静态工厂
 * Created by Administrator on 2015/1/7.
 */
public class StaticFactory {
    private static Map<String, String> webConfig = new TreeMap<String, String>();

    /**
     * 获取网页配置
     */
    public synchronized static Map<String, String> getSystemConfigMap() {
        if (webConfig.isEmpty()) {
            setSystemConfigMap();
        }
        return webConfig;
    }

    /**
     * 保存网页静态配置
     */
    public synchronized static void setSystemConfigMap() {
        List<WebConfig> webConfigs = WebConfig.dao.searchByIsSystem(ModelEnum.YesOrNO.YES.ordinal());
        for (WebConfig wc : webConfigs) {
            webConfig.put(wc.getStr(WebConfig.KEY), wc.getStr(WebConfig.VALUE));
        }
    }

    /**
     * 清空WebConfig
     * 适用于修改网页配置时调用
     */
    public synchronized static void clearWebConfig() {
        webConfig.clear();
    }
}
