package com.tfj.beta.utils;

import java.util.HashMap;
import java.util.Map;

public class ProcessTypeUtils {
	 //种植
    public static final String PLANT_PROCESS = "PLANT_PROCESS";
    //加工
    public static final String SUPERVISE_PROCESS = "SUPERVISE_PROCESS";
    //流通
    public static final String WAREHOUSE_PROCESS = "WAREHOUSE_PROCESS";
    //销售
    public static final String SALE_PROCESS = "SALE_PROCESS";
	public static Map<String, String> processTypeMap=null;
	static{
		processTypeMap=new HashMap<String, String>();
		processTypeMap.put(PLANT_PROCESS, "种植环节");
		processTypeMap.put(SUPERVISE_PROCESS, "加工环节");
		processTypeMap.put(WAREHOUSE_PROCESS, "流通环节");
		processTypeMap.put(SALE_PROCESS, "销售环节");
	}
	public static String getProcessTypeName(String processType){
		return processTypeMap.get(processType);
	}
	

}
