package com.tfj.beta.utils;

import java.util.HashSet;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

public class RedisClusterUtil {
	private static JedisCluster jc = null;

	/**
	 * 
	 * 获取Jedis实例
	 */
	public static JedisCluster getJedis() {
		try {

			if (jc != null) {
				return jc;
			} else {
				String auth = PropertiesUtil.getRedisClusterAuth();
				jc = new JedisCluster(getClusterNodes());
				if (StringUtils.isNotBlank(auth)) {
					jc.auth(auth);
				}
				return jc;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Set<HostAndPort> getClusterNodes() {
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

		String redisClusterNodes = PropertiesUtil.getRedisClusterNodes();
		String[] hostPort = redisClusterNodes.split(",");
		if (null != hostPort && hostPort.length > 0) {
			for (String hp : hostPort) {
				String host = hp.trim().substring(0, hp.indexOf(":"));
				String port = hp.trim().substring(hp.indexOf(":") + 1,
						hp.length());
				jedisClusterNodes.add(new HostAndPort(host, Integer
						.parseInt(port)));
			}
		}
		return jedisClusterNodes;
	}

	/**
	 * 根据缓存string
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public static boolean cacheStrByKey(String key, String value) {
		boolean flag = true;
		try {
			if (null == RedisClusterUtil.getJedis()) {
				return false;
			} else {
				RedisClusterUtil.getJedis().set(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}

		return flag;

	}
	
	
	public static boolean expire(String key, int value) {
		boolean flag = true;
		try {
			if (null == RedisClusterUtil.getJedis()) {
				return false;
			} else {
				RedisClusterUtil.getJedis().expire(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}

		return flag;

	}

	/**
	 * 根据key从 Redis获取缓存值
	 * 
	 * @param key
	 * @return
	 */
	public static Object getCacheValueByKey(String key) {
		return RedisClusterUtil.getJedis().get(key);
	}

	/**
	 * 删除 redis key
	 * @param key
	 * @return
	 */
	public static boolean delCacheKey(String key) {
		try {
			RedisClusterUtil.getJedis().del(key);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 清空redis缓存池
	 */
	public static void clearDataBase() {
		RedisClusterUtil.getJedis().flushAll();
	}

	public static void main(String[] args) {

		System.out.println("---->"
				+ RedisClusterUtil.getJedis().getClusterNodes().size());
		//RedisClusterUtil.getJedis().del("test");
		RedisClusterUtil.cacheStrByKey("test", "test3333");
		System.out.println("=====>"
				+ RedisClusterUtil.getCacheValueByKey("56+31"));

	}

}
