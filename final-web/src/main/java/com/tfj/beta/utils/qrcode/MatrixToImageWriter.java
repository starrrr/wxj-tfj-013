package com.tfj.beta.utils.qrcode;

import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.OutputStream;
import java.io.IOException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public final class MatrixToImageWriter {

	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0xFFFFFFFF;

	private MatrixToImageWriter() {
	}

	public static BufferedImage toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
			}
		}
		return image;
	}

	public static void writeToFile(BitMatrix matrix, String format, File file, BufferedImage logo)
			throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		
		Graphics2D gs = image.createGraphics();
		
		int widthLogo = logo.getWidth();
		int heightLogo = logo.getHeight();
		
		// 居中
		int x = (image.getWidth() - widthLogo) / 2;
		int y = (image.getHeight() - heightLogo) / 2;
		
		//开始绘制图片  
//		gs.drawRoundRect(x, y, widthLogo, heightLogo, 15, 15);  
		gs.setStroke(new BasicStroke(2f));  
		gs.setColor(Color.WHITE);  
//		gs.drawRect(x, y, widthLogo, heightLogo);
		gs.drawOval(x, y, widthLogo, heightLogo);
		gs.fillOval(x, y, widthLogo + 2, heightLogo + 2);
		gs.drawImage(logo, x + 1, y + 1, widthLogo, heightLogo, null);  
		
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format "
					+ format + " to " + file);
		}
	}
	
	public static void writeToFile(BitMatrix matrix, String format, File file)
			throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format "
					+ format + " to " + file);
		}
	}

	public static void writeToStream(BitMatrix matrix, String format,
			OutputStream stream) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, stream)) {
			throw new IOException("Could not write an image of format "
					+ format);
		}
	}

}