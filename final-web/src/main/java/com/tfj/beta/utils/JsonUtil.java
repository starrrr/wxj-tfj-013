package com.tfj.beta.utils;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tfj.beta.model.view.TreeBean;

public class JsonUtil {

	/**
	 * 从json字符串中解析出java对象
	 * 
	 * @author:qiuchen
	 * @createTime:2012-7-8
	 * @param jsonStr
	 *            json格式字符串
	 * @param clazz
	 *            java类
	 * @return clazz的实例
	 */
	public static Object parseJavaObject(String jsonStr, Class clazz) {
		return JSON.toJavaObject(JSON.parseObject(jsonStr), clazz);
	}

	public static String parseObjectToJson(Object obj) {

		String json = JSON.toJSONString(obj, true);
		return json;
	}
	
	
	/**
	 * 函数注释：parseJSON2MapString()<br>
	 * 用途：该方法用于json数据转换为<Map<String, String><br>
	 * 备注：***<br>
	 */
	public static Map<String, String> parseJSON2MapString(String jsonStr) {
		Map<String, String> map = new HashMap<String, String>();
		// 最外层解析
		JSONObject json = JSONObject.parseObject(jsonStr);
		for (Object k : json.keySet()) {
			Object v = json.get(k);
			if (null != v) {
				map.put(k.toString(), v.toString());
			}
		}
		return map;
	}
	
	/**
	 * 测试方法
	 * 
	 * @author:qiuchen
	 * @createTime:2012-7-8
	 * @param args
	 */
	public static void main(String[] args) {
		
//		Map<String,Object> map = new HashMap();
//		
//		ProductBean product = new ProductBean();
//		product.setId("222");
//		product.setProductName("赤灵芝产品1号");
//		product.setPlantType("菌种培育");
//		product.setProductLevel("优");
//		product.setProductLocation("浙江省龙泉市小梅镇黄村");
//		product.setImgPath("http://img1.bdstatic.com/static/home/widget/search_box_home/logo/home_white_logo_0ddf152.png");
//		
//		List<TreeBean> zzhjChilds = new ArrayList<TreeBean>();
		TreeBean zzhj = new TreeBean();
		zzhj.setId(1);
		zzhj.setName("基地选址");

		//zzhj.setVisable(true);
		zzhj.setProcessType("ZZHJ");
		zzhj.setSort(1);
		zzhj.setPId(0);
		zzhj.setOpen(Boolean.TRUE);
//		
//		//zzhj.setChild(zzhjChilds);
//
//		TreeBean zycmc = new TreeBean();
//		zycmc.setId(2);
//		//zycmc.setLevel(1);
//		zycmc.setName("中药材名称");
//		//zycmc.setValue("赤灵芝");
//
//		//zycmc.setVisable(true);
//		zycmc.setProcessType("ZZHJ");
//		zycmc.setSort(1);
//		zycmc.setPid(1);
//		zzhjChilds.add(zycmc);
//	
//		TreeBean jdmc = new TreeBean();
//		jdmc.setId(3);
//		//jdmc.setLevel(1);
//		jdmc.setName("基地名称");
//		//jdmc.setValue("浙江省龙泉市小梅镇黄村");
//
//		//jdmc.setVisable(true);
//		jdmc.setProcessType("ZZHJ");
//		jdmc.setSort(2);
//		jdmc.setPid(1);
//		zzhjChilds.add(jdmc);
//		
//		
//		
//
//		TreeBean jzsc = new TreeBean();
//		jzsc.setId(6);
//		jzsc.setName("菌种生产");
//
//		//jzsc.setVisable(true);
//		jzsc.setProcessType("ZZHJ");
//		jzsc.setSort(2);
//		jzsc.setPid(0);
//		
//		
//		List<TreeBean> jzscChilds = new ArrayList<TreeBean>();
//		
//		//jzsc.setChild(jzscChilds);
//		
//		TreeBean jzly = new TreeBean();
//		jzly.setId(1);
//		jzly.setName("菌种来源");
//
//		//jzly.setVisable(true);
//		jzly.setProcessType("ZZHJ");
//		//jzly.setLevel(1);
//		jzly.setSort(1);
//		jzly.setPid(6);
//		
//		jzscChilds.add(jzly);
//		
//		
//		
//		
//		
//		
//		map.put("product",product);
//		
//		map.put("zzhj",zzhj);
//		
//		
		String json = JsonUtil.parseObjectToJson(zzhj);
		System.out.println(json);
		
		

	}
}
