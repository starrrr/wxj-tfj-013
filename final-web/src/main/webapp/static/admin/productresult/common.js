
/**
 * 列头排序
 */
$(document).on("click", "a.sort-column", function() {
	var name = $(this).attr("name");
	var page=$("#page").val();
	var sortCondition = "tag=";
	var tag_span = $(this).children().first();
	if (tag_span.hasClass("fa-sort")) { // 未排序-->desc//流通批号有空的存在，升序排序asc，空数据会显示前面
		sortCondition = name + " desc";
	} else if (tag_span.hasClass("fa-sort-asc bigger-200")) { // asc-->desc
		sortCondition = name + " desc";
	} else if (tag_span.hasClass("fa-sort-desc bigger-200")){ // desc-->asc
		sortCondition = name + " asc";
	}
	var sortForm = $("#sortForm");
	sortForm.append("<input name='tag' type='hidden' value='" + sortCondition + "'/>");
	sortForm.append("<input name='page' type='hidden' value='" + page + "'/>");
	sortForm[0].submit();
});

function initSort(sort_name, sort_type) {
	sort_name = $.trim(sort_name);
	sort_type = $.trim(sort_type);
	var sort_columns = $("a.sort-column");
	if(sort_name != '' && sort_type != '') {
		sort_columns.each(function(index) {
			var name = $(this).attr("name");
			if (name == sort_name) {
				var tag_span = $(this).children().first();
				if (sort_type == 'asc') {
					tag_span.attr("class", "ace-icon fa fa-sort-asc bigger-200");
				} else if (sort_type == 'desc') {
					tag_span.attr("class", "ace-icon fa fa-sort-desc bigger-200");
				}
				return;
			}
		});
	}
}