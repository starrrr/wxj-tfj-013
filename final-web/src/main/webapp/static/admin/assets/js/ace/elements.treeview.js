/**
 <b>Treeview</b>. A wrapper for FuelUX treeview element.
 It's just a wrapper so you still need to include FuelUX treeview script first.
*/
(function($ , undefined) {

	$.fn.aceTree = $.fn.ace_tree = function(options) {
		var $options = {
			//'open-icon' : ace.vars['icon'] + 'fa fa-folder-open',
			//'close-icon' : ace.vars['icon'] + 'fa fa-folder',
			'selectable' : true,
			//'selected-icon' : ace.vars['icon'] + 'fa fa-check',
			//'unselected-icon' : ace.vars['icon'] + 'fa fa-times',
			'loadingHTML': 'Loading...'
		}
	
		$options = $.extend({}, $options, options)
		this.each(function() {
			var $this = $(this);
			$this.addClass('tree').attr('role', 'tree');

            $this.html(
            '<li class="tree-branch hidden" data-template="treebranch" role="treeitem" aria-expanded="false">\
                <div class="tree-branch-header">\
                    <span>\
                    <i class="glyphicon icon-caret ace-icon tree-plus"></i>\
                    <i class="tree-branch-name">\
                        <span class="glyphicon icon-folder glyphicon-folder-close"></span>\
                        <span class="tree-label"></span>\
                    </i>\
                </div>\
                <ul class="tree-branch-children" role="group"></ul>\
                    <div class="tree-loader" role="alert">'+$options['loadingHTML']+'</div>\
            </li>\
            <li class="tree-item hidden" data-template="treeitem" role="treeitem">\
                <span class="tree-item-name">\
                    <i class="glyphicon icon-item ace-icon fa fa-times"></i>\
                    <span class="tree-label"></span>\
                </span>\
            </li>');
			
			$this.addClass($options['selectable'] == true ? 'tree-selectable' : 'tree-unselectable');
			
			$this.tree($options);
		});

		return this;
	}

})(window.jQuery);
