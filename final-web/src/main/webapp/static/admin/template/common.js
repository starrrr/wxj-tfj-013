$(document).ready(function(){

	// 清除远程modal数据
    $("#editModal").on("hidden.bs.modal", function(){
        $(this).removeData("bs.modal");
    });
    
    //图片处理
    function isInImage_Pattern(obj) {
    	var fileName  = obj[0].files[0].name;
    	var ext = fileName.split(".")[1];
    	 if (ext && $("#Image_Pattern").val().indexOf(ext) >= 0) {
    		return true;
    	}else {
    		pvAlert("非支持上传图片类型！支持上传图片类型：\n"+$("#Image_Pattern").val());
    		return false;
    	}
    }
    function findSize(obj) {
    	var byteSize  = obj[0].files[0].size;
    	return Math.ceil(byteSize / 1024);
    }
    
    //========================plant========================
	$(".plantInputselector").change(function(){
		$("#field_develop_PLANT_PROCESS").val('');//每次选择都要把 字段拓展 赋空
		if($(".plantInputselector").val()=='文本类型'){
			$("#key_name_PLANT_PROCESS").removeAttr("readonly");//去掉readonly
			$("#plantmix").show();//文本类型 ：混批按钮显示
			$("#field_develop_PLANT_PROCESS").show();
			$(".plant_dic_menu").hide();
		}else if ($(".plantInputselector").val()=='下拉菜单'){
			$("#key_name_PLANT_PROCESS").removeAttr("readonly");//去掉readonly
			$(".plant_dic_menu").empty();
			$(".plant_dic_menu").show();
			$("#field_develop_PLANT_PROCESS").hide();
			$("#plantmix").hide();
			requestplant();
		}else if($(".plantInputselector").val()=='日期类型'){
			$("#key_name_PLANT_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_PLANT_PROCESS").val('yyyy-MM-dd');
			$("#field_develop_PLANT_PROCESS").show();
			$(".plant_dic_menu").hide();
			$("#plantmix").hide();
		}else if($(".plantInputselector").val()=='图片类型'){
			$("#key_name_PLANT_PROCESS").val('image');
			$("#key_name_PLANT_PROCESS").attr("readonly","readonly");//图片类型  keyName设置为readonly
			$("plantimg").show();
			$("#field_develop_PLANT_PROCESS").show();
			$(".plant_dic_menu").hide();
			$("#plantmix").hide();
		}else {
			$("#key_name_PLANT_PROCESS").removeAttr("readonly");//去掉readonly
			$("plantimg").hide();
			$("#field_develop_PLANT_PROCESS").show();
			$(".plant_dic_menu").hide();
			$("#plantmix").hide();
		} 
	});
	//混批赋值
	$("input[name='plantmix']").click(function(){
		if($(this).is(':checked')){
			$("#field_develop_PLANT_PROCESS").val('{MIX}');
			$(".field_develop_hidden_plant").hide();//隐藏字段拓展
		}else {
			$("#field_develop_PLANT_PROCESS").val('');
			$(".field_develop_hidden_plant").show();//隐藏字段拓展
		}
	});
	//表单赋值
	/*$(".btn-success").click(function(){
		$(":radio").each(function() {
	        if (this.checked) {
	        	console.log(this);
	           //$("#field_develop_PLANT_PROCESS").val($(this).val());
	           $(".btn-cancel").click();
	        }
	    });
	});*/
	//环境显示  默认值B_0:C_0:G_0 
	$("#plantB").click(function(){
		var plantshow=$("#process_show_PLANT_PROCESS").val();
		var plantB=plantshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_PLANT_PROCESS").val("B_1:"+plantB[1]+":"+plantB[2]);
		}else{
			$("#process_show_PLANT_PROCESS").val("B_0:"+plantB[1]+":"+plantB[2]);
		}
	});
	$("#plantC").click(function(){
		var plantshow=$("#process_show_PLANT_PROCESS").val();
		var plantC=plantshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_PLANT_PROCESS").val(plantC[0]+":C_1:"+plantC[2]);
		}else{
			$("#process_show_PLANT_PROCESS").val(plantC[0]+":C_0:"+plantC[2]);
		}
	});
	$("#plantG").click(function(){
		var plantshow=$("#process_show_PLANT_PROCESS").val();
		var plantG=plantshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_PLANT_PROCESS").val(plantG[0]+":"+plantG[1]+":G_1");
		}else{
			$("#process_show_PLANT_PROCESS").val(plantG[0]+":"+plantG[1]+":G_0");
		}
	});
	//图标上传
    $(document).on("change", ".plantUploadFile", function() {
    	var file = $(".plantUploadFile");
    	if ($(this).hasClass("plantUploadImg")) {
    		// 图片文件类型校验
    		if (isInImage_Pattern($(this))) {
    			// 文件大小校验
    	    	if (findSize($(this)) > 5) {
    	    		pvAlert("文件大小不能超过5K");
    	    		file.after(file.clone().val("")); 
        			file.remove();
        			return;
    	    	}
    		}else {
    			file.after(file.clone().val("")); 
    			file.remove();
    			return;
			}
    	}
    	
    });
	//========================plantend========================
	
	//========================sale========================
	$(".saleInputselector").change(function(){
		$("#field_develop_SALE_PROCESS").val('');//每次选择都要把 字段拓展 赋空
		if($(".saleInputselector").val()=='文本类型'){
			$("#key_name_SALE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#salemix").show();//文本类型 ：混批按钮显示
			$("#field_develop_SALE_PROCESS").show();
			$(".sale_dic_menu").hide();
		}else if ($(".saleInputselector").val()=='下拉菜单'){
			$("#key_name_SALE_PROCESS").removeAttr("readonly");//去掉readonly
			$(".sale_dic_menu").empty();
			$(".sale_dic_menu").show();
			$("#field_develop_SALE_PROCESS").hide();
			$("#salemix").hide();
			requestsale();
		}else if($(".saleInputselector").val()=='日期类型'){
			$("#key_name_SALE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_SALE_PROCESS").val('yyyy-MM-dd');
			$("#field_develop_SALE_PROCESS").show();
			$(".sale_dic_menu").hide();
			$("#salemix").hide();
		}else if($(".saleInputselector").val()=='图片类型'){
			$("#key_name_SALE_PROCESS").val('image');
			$("#key_name_SALE_PROCESS").attr("readonly","readonly");//图片类型  keyName设置为readonly
			$("#field_develop_SALE_PROCESS").show();
			$(".sale_dic_menu").hide();
			$("#salemix").hide();
		}else {
			$("#key_name_SALE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_SALE_PROCESS").show();
			$(".sale_dic_menu").hide();
			$("#salemix").hide();
		}
	});
	
	//混批赋值
	$("input[name='salemix']").click(function(){
		if($(this).is(':checked')){
			$("#field_develop_SALE_PROCESS").val('{MIX}');
			$(".field_develop_hidden_sale").hide();//隐藏字段拓展
		}else {
			$("#field_develop_SALE_PROCESS").val('');
			$(".field_develop_hidden_sale").show();//隐藏字段拓展
		}
	});
	//表单赋值
	/*$(".btn-success").click(function(){
		$(":radio").each(function() {
	        if (this.checked) {
	           $("#field_develop_SALE_PROCESS").val($(this).val());
	           $(".btn-cancel").click();
	        }
	    });
	});*/
	//环境显示  默认值B_0:C_0:G_0
	$("#saleB").click(function(){
		var saleshow=$("#process_show_SALE_PROCESS").val();
		var saleB=saleshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SALE_PROCESS").val("B_1:"+saleB[1]+":"+saleB[2]);
		}else{
			$("#process_show_SALE_PROCESS").val("B_0:"+saleB[1]+":"+saleB[2]);
		}
	});
	$("#saleC").click(function(){
		var saleshow=$("#process_show_SALE_PROCESS").val();
		var saleC=saleshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SALE_PROCESS").val(saleC[0]+":C_1:"+saleC[2]);
		}else{
			$("#process_show_SALE_PROCESS").val(saleC[0]+":C_0:"+saleC[2]);
		}
	});
	$("#saleG").click(function(){
		var saleshow=$("#process_show_SALE_PROCESS").val();
		var saleG=saleshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SALE_PROCESS").val(saleG[0]+":"+saleG[1]+":G_1");
		}else{
			$("#process_show_SALE_PROCESS").val(saleG[0]+":"+saleG[1]+":G_0");
		}
	});
	//图标上传
    $(document).on("change", ".saleUploadFile", function() {
    	var file = $(".saleUploadFile");
    	if ($(this).hasClass("saleUploadImg")) {
    		// 图片文件类型校验
    		if (isInImage_Pattern($(this))) {
    			// 文件大小校验
    	    	if (findSize($(this)) > 5) {
    	    		pvAlert("文件大小不能超过5K");
    	    		file.after(file.clone().val("")); 
        			file.remove();
        			return;
    	    	}
    		}else {
    			file.after(file.clone().val("")); 
    			file.remove();
    			return;
			}
    	}
    	
    });
	//========================saleEnd========================
	
	//========================supervise========================
	
	$(".superviseInputselector").change(function(){
		$("#field_develop_SUPERVISE_PROCESS").val('');//每次选择都要把 字段拓展 赋空
		if($(".superviseInputselector").val()=='文本类型'){
			$("#key_name_SUPERVISE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#supervisemix").show();//文本类型 ：混批按钮显示
			$("#field_develop_SUPERVISE_PROCESS").show();
			$(".supervise_dic_menu").hide();
		}else if ($(".superviseInputselector").val()=='下拉菜单'){
			$("#key_name_SUPERVISE_PROCESS").removeAttr("readonly");//去掉readonly
			$(".supervise_dic_menu").empty();
			$(".supervise_dic_menu").show();
			$("#field_develop_SUPERVISE_PROCESS").hide();
			$("#supervisemix").hide();
			requestsupervise();
		}else if($(".superviseInputselector").val()=='日期类型'){
			$("#key_name_SUPERVISE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_SUPERVISE_PROCESS").val('yyyy-MM-dd');
			$("#field_develop_SUPERVISE_PROCESS").show();
			$(".supervise_dic_menu").hide();
			$("#supervisemix").hide();
		}else if($(".superviseInputselector").val()=='图片类型'){
			$("#key_name_SUPERVISE_PROCESS").val('image');
			$("#key_name_SUPERVISE_PROCESS").attr("readonly","readonly");//图片类型  keyName设置为readonly
			$("#field_develop_SUPERVISE_PROCESS").show();
			$(".supervise_dic_menu").hide();
			$("#supervisemix").hide();
		}else {
			$("#key_name_SUPERVISE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_SUPERVISE_PROCESS").show();
			$(".supervise_dic_menu").hide();
			$("#supervisemix").hide();
		} 
	});

	//混批赋值
	$("input[name='supervisemix']").click(function(){
		if($(this).is(':checked')){
			$("#field_develop_SUPERVISE_PROCESS").val('{MIX}');
			$(".field_develop_hidden_supervise").hide();//隐藏字段拓展
		}else {
			$("#field_develop_SUPERVISE_PROCESS").val('');
			$(".field_develop_hidden_supervise").show();//隐藏字段拓展
		}
	});
	//表单赋值
	/*$(".btn-success").click(function(){
		$(":radio").each(function() {
	        if (this.checked) {
	           $("#field_develop_SUPERVISE_PROCESS").val($(this).val());
	           $(".btn-cancel").click();
	        }
	    });
	});*/
	//环境显示  默认值B_0:C_0:G_0
	$("#superviseB").click(function(){
		var superviseshow=$("#process_show_SUPERVISE_PROCESS").val();
		var superviseB=superviseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SUPERVISE_PROCESS").val("B_1:"+superviseB[1]+":"+superviseB[2]);
		}else{
			$("#process_show_SUPERVISE_PROCESS").val("B_0:"+superviseB[1]+":"+superviseB[2]);
		}
	});
	$("#superviseC").click(function(){
		var superviseshow=$("#process_show_SUPERVISE_PROCESS").val();
		var superviseC=superviseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SUPERVISE_PROCESS").val(superviseC[0]+":C_1:"+superviseC[2]);
		}else{
			$("#process_show_SUPERVISE_PROCESS").val(superviseC[0]+":C_0:"+superviseC[2]);
		}
	});
	$("#superviseG").click(function(){
		var superviseshow=$("#process_show_SUPERVISE_PROCESS").val();
		var superviseG=superviseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_SUPERVISE_PROCESS").val(superviseG[0]+":"+superviseG[1]+":G_1");
		}else{
			$("#process_show_SUPERVISE_PROCESS").val(superviseG[0]+":"+superviseG[1]+":G_0");
		}
	});
	//图标上传
    $(document).on("change", ".superviseUploadFile", function() {
    	var file = $(".superviseUploadFile");
    	if ($(this).hasClass("superviseUploadImg")) {
    		// 图片文件类型校验
    		if (isInImage_Pattern($(this))) {
    			// 文件大小校验
    	    	if (findSize($(this)) > 5) {
    	    		pvAlert("文件大小不能超过5K");
    	    		file.after(file.clone().val("")); 
        			file.remove();
        			return;
    	    	}
    		}else {
    			file.after(file.clone().val("")); 
    			file.remove();
    			return;
			}
    	}
    	
    });
	//========================superviseEnd========================
	
	//========================wareHouse========================
	
	$(".wareHouseInputselector").change(function(){
		$("#field_develop_WAREHOUSE_PROCESS").val('');//每次选择都要把 字段拓展 赋空
		
		
		if($(".wareHouseInputselector").val()=='文本类型'){
			$("#key_name_WAREHOUSE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#wareHousemix").show();//文本类型 ：混批按钮显示
			$("#field_develop_WAREHOUSE_PROCESS").show();
			$(".wareHouse_dic_menu").hide();
		}else if ($(".wareHouseInputselector").val()=='下拉菜单'){
			$("#key_name_WAREHOUSE_PROCESS").removeAttr("readonly");//去掉readonly
			$(".wareHouse_dic_menu").empty();
			$(".wareHouse_dic_menu").show();
			$("#field_develop_WAREHOUSE_PROCESS").hide();
			$("#wareHousemix").hide();
			requestwareHouse();
		}else if($(".wareHouseInputselector").val()=='日期类型'){
			$("#key_name_WAREHOUSE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_WAREHOUSE_PROCESS").val('yyyy-MM-dd');
			$("#field_develop_WAREHOUSE_PROCESS").show();
			$(".wareHouse_dic_menu").hide();
			$("#wareHousemix").hide();
		}else if($(".wareHouseInputselector").val()=='图片类型'){
			$("#key_name_WAREHOUSE_PROCESS").val('image');
			$("#key_name_WAREHOUSE_PROCESS").attr("readonly","readonly");//图片类型  keyName设置为readonly
			$("#field_develop_WAREHOUSE_PROCESS").show();
			$(".wareHouse_dic_menu").hide();
			$("#wareHousemix").hide();
		}else {
			$("#key_name_WAREHOUSE_PROCESS").removeAttr("readonly");//去掉readonly
			$("#field_develop_WAREHOUSE_PROCESS").show();
			$(".wareHouse_dic_menu").hide();
			$("#wareHousemix").hide();
		} 
	});
	
	//混批赋值
	$("input[name='wareHousemix']").click(function(){
		if($(this).is(':checked')){
			$("#field_develop_WAREHOUSE_PROCESS").val('{MIX}');
			$(".field_develop_hidden_wareHouse").hide();//隐藏字段拓展
		}else {
			$("#field_develop_WAREHOUSE_PROCESS").val('');
			$(".field_develop_hidden_wareHouse").show();//隐藏字段拓展
		}
	});
	//表单赋值
	/*$(".btn-success").click(function(){
		$(":radio").each(function() {
	        if (this.checked) {
	           $("#field_develop_WAREHOUSE_PROCESS").val($(this).val());
	           $(".btn-cancel").click();
	        }
	    });
	});*/
	//环境显示  默认值B_0:C_0:G_0
	$("#warehouseB").click(function(){
		var warehouseshow=$("#process_show_WAREHOUSE_PROCESS").val();
		var warehouseB=warehouseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_WAREHOUSE_PROCESS").val("B_1:"+warehouseB[1]+":"+warehouseB[2]);
		}else{
			$("#process_show_WAREHOUSE_PROCESS").val("B_0:"+warehouseB[1]+":"+warehouseB[2]);
		}
	});
	$("#warehouseC").click(function(){
		var warehouseshow=$("#process_show_WAREHOUSE_PROCESS").val();
		var warehouseC=warehouseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_WAREHOUSE_PROCESS").val(warehouseC[0]+":C_1:"+warehouseC[2]);
		}else{
			$("#process_show_WAREHOUSE_PROCESS").val(warehouseC[0]+":C_0:"+warehouseC[2]);
		}
	});
	$("#warehouseG").click(function(){
		var warehouseshow=$("#process_show_WAREHOUSE_PROCESS").val();
		var warehouseG=warehouseshow.split(":");
		if($(this).is(':checked')){
			$("#process_show_WAREHOUSE_PROCESS").val(warehouseG[0]+":"+warehouseG[1]+":G_1");
		}else{
			$("#process_show_WAREHOUSE_PROCESS").val(warehouseG[0]+":"+warehouseG[1]+":G_0");
		}
	});
	//图标上传
    $(document).on("change", ".warehouseUploadFile", function() {
    	var file = $(".warehouseUploadFile");
    	if ($(this).hasClass("warehouseUploadImg")) {
    		// 图片文件类型校验
    		if (isInImage_Pattern($(this))) {
    			// 文件大小校验
    	    	if (findSize($(this)) > 5) {
    	    		pvAlert("文件大小不能超过5K");
    	    		file.after(file.clone().val("")); 
        			file.remove();
        			return;
    	    	}
    		}else {
    			file.after(file.clone().val("")); 
    			file.remove();
    			return;
			}
    	}
    	
    });
	//========================wareHouseEnd========================
	
	function requestplant(){
		var path=$("#contextPath").val();
		$.ajax({
			url:""+path+"/admin/template/dicmenushow",
			type:"post",
			data:null,
			datatype:"text",
			success:function(data){
				$(".plant_dic_menu").show();
				var html = "";//<option value="${dict.dict_name!}">${dict.notes!}</option>
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].dict_type+">"
					+data[i].notes
					+ "</option>"; 
					$(".plant_dic_menu").html(html);
				}
			} 
		});
	}
	
	function requestsale(){
		var path=$("#contextPath").val();
		$.ajax({
			url:""+path+"/admin/template/dicmenushow",
			type:"post",
			data:null,
			datatype:"text",
			success:function(data){
				$(".salet_dic_menu").show();
				var html = "";//<option value="${dict.dict_name!}">${dict.notes!}</option>
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].dict_type+">"
					+data[i].notes
					+ "</option>"; 
					$(".sale_dic_menu").html(html);
				}
			} 
		});
	}
	
	function requestsupervise(){
		var path=$("#contextPath").val();
		$.ajax({
			url:""+path+"/admin/template/dicmenushow",
			type:"post",
			data:null,
			datatype:"text",
			success:function(data){
				$(".supervise_dic_menu").show();
				var html = "";//<option value="${dict.dict_name!}">${dict.notes!}</option>
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].dict_type+">"
					+data[i].notes
					+ "</option>"; 
					$(".supervise_dic_menu").html(html);
				}
			} 
		});
	}
	
	function requestwareHouse(){
		var path=$("#contextPath").val();
		$.ajax({
			url:""+path+"/admin/template/dicmenushow",
			type:"post",
			data:null,
			datatype:"text",
			success:function(data){
				$(".wareHouse_dic_menu").show();
				var html = "";//<option value="${dict.dict_name!}">${dict.notes!}</option>
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].dict_type+">"
					+data[i].notes
					+ "</option>"; 
					$(".wareHouse_dic_menu").html(html);
				}
			} 
		});
	}
});