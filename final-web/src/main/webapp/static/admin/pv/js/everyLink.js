function findReportImg(objImg){
	if(objImg.child){
		var reportChild = objImg.child;		
		var reportImg = JSON.stringify(reportChild[0].imgPath);
		var imgHtml = "";
		if(reportImg != undefined){
			reportImg = reportImg.substring(1,reportImg.length-1);
			if(reportImg.indexOf(";") != -1){
				var reportArr = reportImg.split(';');
				for(var n in reportArr){
					imgHtml += '<img src="' +reportArr[n]+ '">';
					$('.imgContainer').html(imgHtml);
				}
			}else{
				imgHtml = '<img src="' +reportImg+ '">';
			}	
		}
	}
}

function findChildImg(imgChild){
	console.log(imgChild)
	if(imgChild.child){
		var imgChild = imgChild.child;
		var cont = '';
		for(var i in imgChild){
			if(imgChild[i].name==''&&imgChild[i].value==''&&imgChild[i].iconPath==''&&imgChild[i].imgPath!=''){
				cont = '<li class="testReportBtn"><p>';
			}else{
				cont = '<li><p>';
			}
		}
	}else{
		cont = '<li><p>';
	}
	return cont;
}