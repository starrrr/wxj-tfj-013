var setting = {
		view: {
			dblClickExpand: true, 
			showIcon: true
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick: onClick
		}
}

//点击事件 
function onClick(event, treeId, treeNode, clickFlag) {
	
	var id = treeNode.id;
	var pid = treeNode.pid;
	var level = treeNode.levelNum;
	var sort = treeNode.sort;
	var processType = treeNode.processType;
	var indexShow = treeNode.indexShow;
	var key_name = treeNode.name;
	var field_type = treeNode.fieldType;
	var field_develop = treeNode.fieldDevelop;
	
	window.location.hash = "#" + treeNode.name;
	window.location = window.location;
	
	 if(processType == 'PLANT_PROCESS'){
		 //$("#levelNum_PLANT_PROCESS").val(level);
		 //$("#id_PLANT_PROCESS").val(id);
		 //$("#pid_PLANT_PROCESS").val(pid);
		 //$("#sort_PLANT_PROCESS").val(sort);
		 //$("#key_name_PLANT_PROCESS").val(key_name);
		 //$("#field_type_PLANT_PROCESS").val(field_type);
		 //$("#field_develop_PLANT_PROCESS").val(field_develop);
		 
		 if(field_type){
			 //$("input:radio[name='index_show_PLANT_PROCESS']").eq(0).removeAttr("checked");
			 //$("input:radio[name='index_show_PLANT_PROCESS']").eq(1).prop("checked",'true')	
		}else{
			//$("input:radio[name='index_show_PLANT_PROCESS']").eq(1).removeAttr("checked");
			//$("input:radio[name='index_show_PLANT_PROCESS']").eq(0).prop("checked",'true')			
		}
	 } else if(processType == 'SUPERVISE_PROCESS'){
		 
	 } else if(processType == 'WAREHOUSE_PROCESS'){
		 
	 } else if(processType == 'SALE_PROCESS'){
		 
	 }
}

// 用来定位元素
var lastInput = null;
var DateRange = "DateRange";
var Input = "Input";
var DateRangeSplit = " 至 ";
var SPLITTER = "-";

var UNAUDITED = 0; 
var AUDITED = 1; 
var FINISHED = 2;

var windowWidth = $(window).width();
var windowHeight = $(window).height();

$(function(){

	var loadingIcon = $('.loading img');
	$('.loading').css({
		'width' : windowWidth,
		'height' : windowHeight
	})
	loadingIcon.css({
		'left': (windowWidth - loadingIcon.width())/2,
		'top' : (windowHeight - loadingIcon.height())/2
	});

});

$.fn.datepicker.dates['zh'] = {
    days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
    daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
    daysMin: ["日", "一", "二", "三", "四", "五", "六"],
    months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
    monthsShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
    today: "Today",
    clear: "Clear",
    format: "yyyy-mm-dd",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};

// date-picker
$(document).on("mouseover", ".date-picker", function() {
	// datepicker plugin
	// link
	$(this).datepicker({
		language: 'zh', 
		autoclose : true,
		todayHighlight : true
	})
	// show datepicker when clicking on the icon
	.next().on(ace.click_event, function() {
		$(this).prev().focus();
	});
});


// date range 日期控件
$(document).on("mouseover", ".input-daterange", function() {
	// datepicker plugin
	// link
	$(this).datepicker({
		language: 'zh', 
		autoclose : true
	});
});

function pvAlert(msg, callbackFunction) {
	bootbox.alert({
		message: msg,
		buttons: {
			ok: {
	            label: "确定",
	            className: "btn-primary btn-sm"
	        }
		},
        callback: callbackFunction
	});
}

function pvConfirm(msg, callbackFunction) {
	return bootbox.confirm({
		message: msg,
		buttons: {
            confirm: {
                label: "确定",
                className: "btn-primary btn-sm"
            },
            cancel: {
                label: "取消",
                className: "btn-sm btn-cancel"
            }
        },
        callback : callbackFunction
	});
}

function exeConfirm(msg) {
	var exeFlag = null;
	bootbox.confirm({
		message: msg,
		buttons: {
            confirm: {
                label: "确定",
                className: "btn-primary btn-sm"
            },
            cancel: {
                label: "取消",
                className: "btn-sm btn-cancel"
            }
        },
        callback : function(result) {
        	exeFlag = result;
        }
	});
	return exeFlag
}

function Widget(name, type, value) {
	this.name = name;
	this.type = type;
	this.value = value;
}

// 上传
$(document).on("change", ".uploadFile", function() {
	var fileInputId = $(this).attr("id");
	var textbox = $(this).prev();
	var imgShow = $(this).parent().parent().next();
	
	var uploadType = '';
	if ($(this).hasClass("uploadImg")) {
		uploadType = 'uploadImg';
		// 图片文件类型校验
		if (!isInImage_Pattern($(this))) {
			return;
		}
	}
	
	// 文件大小校验
	if (findSize($(this)) > 2048) {
		pvAlert("文件大小不能超过2M。");
		return;
	}
	
	textbox.html("上传中...");
	$.ajaxFileUpload({
		url : tfj_ContextPath + '/admin/productValue/uploadFile',
		secureuri : false,
		fileElementId : fileInputId,
		dataType : 'text',
		data : {
			id : $(this).attr("name"),
			type : uploadType,
		},
		success : function(rdata, status) {
			// console.log(rdata);
			// 获取返回的 text, 因为<pre>text</pre>，需要getUploadText去掉干扰
			var text = getUploadText(rdata);
			if (text != "FAILED") {
				var result = $.parseJSON(text);
				if (uploadType == 'uploadImg') {
					// console.log("[image]: " + result.img);
					// 加参数 ?t 刷新 img, 可能不需要?t参数, 但我不想去测试了/(ㄒoㄒ)/~~
					var html_content = $('<div/>').html(result.html).text();
					imgShow.replaceWith(html_content);
//					imgShow.attr("src", result.img + '?t=' + Math.random());
					$("#" + fileInputId).attr("name", result.name);
					textbox.html("上传成功。");
				} else {
					textbox.html("上传成功。");
					pvAlert("上传成功。");
				}
			} else {
				textbox.html("上传失败。");
				pvAlert("上传发生异常。");
			}
		},
		error : function(rdata, status, e) {
			textbox.html("上传失败。");
			pvAlert("上传发生异常。");
		}
	});
});

$(document).on("focus", "[class*=-widget]", function() {
	lastInput = $(this);
	return false;
});

// 保存 ProductValue
$(document).on("click", "button[name='saveProductValue']", function(){
	if ($("input.product_auditflag").val() == 2) {
		pvAlert("该产品审核状态为“已完成”，无需再保存。");
	} else {
		var textTip = $('.textTip');
		
		var widgets = getInputWidgets(true);
		if (validateSaveData(widgets)) {
			
			pvConfirm("确定保存录入值?", function (result) {
		    	if(result) {
		    		/*if (widgets.length <= 0) {//不必要判断，如果修改图片，这里也会<0
		    			pvAlert("保存成功。");
		    			return;
		    		} else {*/
		    			var focusInfo = "";
		    			try{
		    				focusInfo = "&focusInfo=" + lastInput.attr("name").split(SPLITTER)[0];
		    			} catch (e) {
		    				console.log(e);
		    			}
		    			data = "processType=" + tfj_pv_ProcessType + focusInfo + "&data=" + JSON.stringify(widgets);
		    		//}
//		    		console.log(data);
		    		$('.loading').show();
		    		textTip.css({
						'left': (windowWidth - textTip.width())/2,
						'top' : (windowHeight - textTip.height())/2 + $('.loading img').height()*3/4
					});
		    		var url = tfj_ContextPath + "/admin/productValue/saveProductValue";
					$.post(url, data, function(e) {
		    			// console.log(e);
		    			if (e == "SUCCESS") {
		    				$('.loading').hide();
		    				pvAlert("保存成功。", function() {
	    		            	createProductValuePage(tfj_pv_Container, tfj_pv_ProcessType, "ORG");
	    		            	$('.loading').hide();
	    		            });
		    			} else {
		    				$('.loading').hide();
		    				pvAlert("保存失败。");

		    			}
		    		});
		    	}
		    });
		}
	}
});


// 
/**
 * 审核 ProductValue
 */
$(document).on("click", "button[name='auditProductValue']", function() {
	if ($("input.product_auditflag").val() == 2) {
		pvAlert("该产品审核状态为“已完成”，无需再审核。");
	} else {
		var textTip = $('.textTip');
		textTip.html('审核中，请稍候...');

//		console.log(JSON.stringify(orgWidget));
		pvConfirm("审核后数据不能修改,确定审核录入值?", function (result) {
	    	if(result) {
	    		$('.loading').show();
	    		textTip.css({
					'left': (windowWidth - textTip.width())/2,
					'top' : (windowHeight - textTip.height())/2 + $('.loading img').height()*3/4
				});
	    		var url = tfj_ContextPath + "/admin/productValue/auditProductValue?processType=" + tfj_pv_ProcessType;
	    		$.post(url, null, function(e) {
	    			// console.log(e);
	    			if (e == "SUCCESS") {
	    				$('.loading').hide();
	    				pvAlert("审核成功。", function() {
			            	createProductValuePage(tfj_pv_Container, tfj_pv_ProcessType);
			            	modifyProductState(AUDITED);
			            });
	    			} else {
	    				$('.loading').hide();
	    				pvAlert("审核失败。");
	    			}
	    		});
	    	}
		});
	}

});


$(document).on("click",".rightIcon", function(){
	
	var icon_up = window.tfj_ContextPath + '/static/admin/index/img/icon_up.png';
	var icon_down = window.tfj_ContextPath + '/static/admin/index/img/icon_down.png';
	
	var currentUl = $(this).parent().next();
	// alert("currentUL: " + currentUl.css('display'));
	if(currentUl.css('display') == 'none'){
		currentUl.css('display','block');
//		$(this).parent().parent('li').prevAll('li').find('ul').css('display','none');
//		$(this).parent().parent('li').nextAll('li').find('ul').css('display','none');
//		$(this).parent().parent('li').prevAll('li').find('.rightIcon').attr('src', icon_down);
//		$(this).parent().parent('li').nextAll('li').find('.rightIcon').attr('src', icon_down);
		$(this).attr('src', icon_up);
	}else{
		currentUl.css('display','none');
		$(this).attr('src', icon_down);
	}
});


//底部tab点击效果
$(document).on('click', '.menus ul li', function(){
	changgeLi($(this));	
});


$(document).on('mouseover', '.menus ul li',function(){
	changgeLi($(this));
});


$(document).on('mouseout','.menus ul li',function(){
	var bgSize = parseInt($(this).css('background-size'));
	bgSize -=6;
	$(this).find('a').css('font-size','0.8rem');
	$this.css('background-size',bgSize + 'px');
});


function changgeLi(obj){
	$this = obj;
	var bgSize = parseInt($this.css('background-size'));
	bgSize +=6;
	$this.find('a').css('font-size','1rem');
	$this.css('background-size', bgSize + 'px');
}


// 检测报告弹框
var screenHeight = $('.testReport').height();
var screenWidth  = $('.testReport').width();
$(document).on('click','.testReportBtn',function(e){
	$('html,body').css('overflow','hidden');
	$('.testReport').removeClass('hidden');
	var imgObj = $('.reportDetail img');
//	console.log(imgObj.width());
	$('.arrow').css({
		'top' : (screenHeight - $('.arrow').height())/2,
	});
//	imgObj.css({
//		'padding-left':(screenWidth-imgObj.width())/2,
//		'padding-right':(screenWidth-imgObj.width())/2,
//	});
	$('.imgContainer').width(imgObj.width()*$('.reportDetail').find('img').length);
});


$(document).on('click', '.close', function(){
	$('.testReport').addClass('hidden');
	$('html,body').css('overflow','auto');
});


// 新增节点值 
$(document).on('click','.addPvVal', function(){
	// 用来还原用户未保存输入
	var widgets = getInputWidgets(false, "notEncoded");
	var rv = $(this).parent().next().find("." + tfj_pv_ProcessType + "-widget");
	var all = rv.attr("name").split(SPLITTER);
	var data;
	if (all[1] == "") {
		var tmp = '{name:"' + rv.attr("name") + '", value:""}';
		data = "data=[" + tmp + "," + tmp + "]";
	} else {
		// [template_id]-[product_value_id] : product_value_id 为空的时候新增
		data = 'data=[{name:"' + all[0] + SPLITTER + '", value:""}]';
	}
	var url = tfj_ContextPath + "/admin/productValue/addMultiProductValue";
	$.post(url, data, function(e) {
		// console.log(e);
		if (e == "SUCCESS") {
			createProductValuePage(tfj_pv_Container, tfj_pv_ProcessType, widgets);
//			location.reload();
		} else {
			pvAlert("新增失败。");
		}
	});
});


// 删除节点值
$(document).on('click','.delPvVal', function(){
	// 用来还原用户未保存输入
	var widgets = getInputWidgets(false, "notEncoded");
	var rv = $(this).parent().next().find("." + tfj_pv_ProcessType + "-widget");
	var all = rv.attr("name").split(SPLITTER);
	var data = "data=" + all[1];
	
	pvConfirm("确定删除节点值: " + $(this).parent().prev().html()+"？", function (result) {
    	if(result) {
    		var url = tfj_ContextPath + "/admin/productValue/delMultiProductValue";
    		$.post(url, data, function(e) {
    			// console.log(e);
    			if (e == "SUCCESS") {
    				createProductValuePage(tfj_pv_Container, tfj_pv_ProcessType, widgets);
//    				location.reload();
    			} else {
    				pvAlert("删除失败。");
    			}
    		});
    	}
	});
});

function createProductValuePage(container_id, type, widgets) {
	// 加载XML树
	var url = tfj_ContextPath + "/admin/productValue/createProductValuePage?processType=" + type;
	$.post(url, function(data) {
		var obj = null;
		try {
			obj = $.parseJSON(data);
		} catch (e) {
			console.log(e);
		}
		if (!obj) {
			pvAlert("生成页面发生错误或登录超时，请重新操作。", function() {
				location.reload();
			});
		} else {
			if (obj.result == "SUCCESS") {
				$("#" + container_id).html(obj.html);
				
				// ORG 为获取页面第一次加载值，未修改过，或保存后的。
				if (widgets == "ORG") {
					orgWidgets = getInputWidgets(false);
				// 是否重新赋值, 只有在增删多值的时候进行重新赋值操作
				} else if (widgets) {
					reassignWidgetVal(widgets);
				}
				
				if (l_template_name != '') {
					window.location.hash = "#" + l_template_name;
					window.location = window.location;
					l_template_name = ''; // 考虑做法
				}
			} else {
				pvAlert("生成页面发生错误, 请联系有关人员修复程序缺陷。");
			}
		}
	});
}

function reassignWidgetVal(widgets) {
//	console.log("before: " + JSON.stringify(widgets));
	$("." + tfj_pv_ProcessType + "-widget").each(function(index) {

		var dis = $(this).attr("disabled");
		if (!dis && !$(this).hasClass("uploadFile")) {
			
			var name = $(this).attr("name");
			
			for(var i = 0; i < widgets.length; i++) {
				var widget = widgets[i];
				if (widget && (widget.name == name)) {
					if ($(this).hasClass("input-daterange")) {
						var info = widget.value.split(DateRangeSplit);
						if (info[0]) {
							$("#" + name + "_start").val($.trim(valueDecodeURI(info[0])));
						}
						if (info[1]) {
							$("#" + name + "_end").val($.trim(valueDecodeURI(info[1])));
						}
					} else {
						if (widget.value) {
							$(this).val(valueDecodeURI(widget.value));
						}
					}
					widgets[i] = null;
					break;
				}
			}
		}
	});
	
	// 处理节点新建值情况：
	// 例如  template_id-pv_id, 新建时  pv_id 为 null, 
	// 新增值后，寻找 pv_id 值最小的, 为其赋上 "填写了但未保存的值"
	var left = null;
	for(var i = 0; i < widgets.length; i++) {
		var widget = widgets[i];
		if (widgets[i]) {
			left = widget;
			break;
		}
	}
	
	if (left) {
		var query = "." + tfj_pv_ProcessType + "-widget[name^='" + left.name +  "']";
		var min = null;
		var jqWidget = null;
		$(query).each(function(index) {
			var pv_id = $(this).attr("name").split(SPLITTER)[1];
			if (!min) { // 初始化
				min = pv_id;
				jqWidget = $(this);
			} else {
				if (pv_id < min) {
					min = pv_id;
					jqWidget = $(this);
				}
			}
		});
		if (jqWidget) {
//			alert("Widget: " + left.type + ", " + left.value + "\r\n" + jqWidget.html());
			if (left.type == DateRange) {
				var vals = left.value.split(DateRangeSplit);
				
				var startInput = jqWidget.find("input[name='start']");
				var endInput = jqWidget.find("input[name='end']");
				startInput.val(vals[0]);
				endInput.val(vals[1]);
			} else {
				jqWidget.val(left.value);
			}
		}
	}
//	console.log("after: " + JSON.stringify(widgets));
}

/**
 * 
 * @param chgLastInput 是否改变 lastInput 值
 * @returns {Array}
 */
function getInputWidgets(chgLastInput, encoded) {
	var widgets = new Array();
	var tmpIndex = 0;
	$("." + tfj_pv_ProcessType + "-widget").each(function(index) {
		var dis = $(this).attr("disabled");
		if (!dis && !$(this).hasClass("uploadFile")) {
			 // TODO 需要处理第一个是 date_range
			if (chgLastInput && index == 0) {
				if (lastInput == null) {
					lastInput = $(this);
				}
			}
			
			if ($(this).hasClass("input-daterange")) {
				
				var name = $(this).attr("name");
				var startDate = $("#" + name + "_start").val();
				var endDate = $("#" + name + "_end").val();
				var val = "";
				// TODO 出现 startDate 必须出现 end, 加上日期校验
				if($.trim(startDate) && $.trim(endDate)) {
					val = startDate + DateRangeSplit + endDate;
				}
				widgets[tmpIndex++] = new Widget(name, DateRange, val);
				
			} else {
					var name = $(this).attr("name");
					var val = $(this).val();
					if (!encoded) {
						val = valueEncodeURI(val);
					}
					
					// console.log("name: " + name + ", val: " + val);
					widgets[tmpIndex++] = new Widget(name, Input, val);
			}
		}
	});
	
//	console.log("getInputWidgets: \r\n" + JSON.stringify(widgets));
	return widgets;
}

function valueDecodeURI(val) {
	var valDecoded = '';
	if (val) {
		try {
			valDecoded = decodeURI(val);
		} catch (e) {
			console.log("val: " + val);
			valDecoded = val;
		}
		
//		valDecoded = valDecoded.replace(new RegExp("\\\"", "gm"), "\"");
	}
	return valDecoded;
}

function valueEncodeURI(val) {
	var valEncoded = val.replace(new RegExp("\"", "gm"), "\\\"");
	// console.log("val: " + valEncoded);
	valEncoded = encodeURI(valEncoded);
	
	return valEncoded;
}

/**
 * 文件大小
 * @param obj
 * @returns
 */
function findSize(obj) {
	var byteSize  = obj[0].files[0].size;
	return Math.ceil(byteSize / 1024);
}

/**
 * 图片类型
 * @param obj
 * @returns {Boolean}
 */
function isInImage_Pattern(obj) {
	var fileName  = obj[0].files[0].name;
	var ext = fileName.split(".")[1].toLocaleLowerCase();
//	console.log("isImage>>>" + ext + ", " + Image_Pattern.indexOf(ext));
	
	if (ext && Image_Pattern.indexOf(ext) >= 0) {
		return true;
	} else {
		pvAlert("非支持上传图片类型！支持上传图片类型：\n" + Image_Pattern);
		return false;
	}
}

function getUploadText(data) {
    var start = data.indexOf(">");  
    if(start != -1) {  
      var end = data.indexOf("<", start + 1);  
      if(end != -1) {  
        data = data.substring(start + 1, end);  
       }  
    }
    return data;
}

function reflushTree(processType) {
	 if(processType == P1_NAME){
		 $("#plantProcessMenu").click();
	 } else if(processType == P2_NAME){
		 $("#superviseMenu").click();
	 } else if(processType == P3_NAME){
		 $("#wareHouseMenu").click();
	 } else if(processType == P4_NAME){
		 $("#saleMenu").click();
	 } else {
		 $("#plantProcessMenu").click();
	 }
}

function validateSaveData(widgets) {
	var flag = true;
	$("." + tfj_pv_ProcessType + "-widget").filter(".input-daterange").each(function(index) {
		var dis = $(this).attr("disabled");
		if (!dis) {
			var name = $(this).attr("name");
			var start = $("#" + name + "_start");
			var end = $("#" + name + "_end");
			var startDate = start.val();
			var endDate = end.val();
			if ($.trim(startDate) != '' || $.trim(endDate) != '') {
				if ($.trim(startDate) == '') {
					pvAlert("请输入开始日期。", function() {
						start.focus();
					});
					return flag = false;
				} else if ($.trim(endDate) == ''){
					pvAlert("请输入结束日期。", function() {
						end.focus();
					});
					return flag = false;
				} else {
					// 第一个日期晚于第二个日期
					// str.replace(new RegExp(/(的)/g),'');
					// str.split(replaceStr).join('');
					var sd = parseInt(startDate.split("-").join(""));
					var ed = parseInt(endDate.split("-").join(""));
					if (ed <= sd) {
						pvAlert("第一个日期晚于第二个日期，请重新输入。", function() {
							end.focus();
						});
						return flag = false;
					}
				}
				
			}
		}
	});
	return flag;
}

function compareVals() {
	if (orgWidgets) {
		var newInputs = getInputWidgets(false);
		
		for(var i = 0; i < orgWidgets.length; i++) {
			var breakFlag = false;
//			console.log(i + " orgVal: " + orgWidgets[i].value + ", newVal: " + newInputs[i].value);
			if (orgWidgets[i].value != newInputs[i].value) {
				return true;
			}
		}
	}
	return false;
}

/**
 * 加载页面
 * @param processType
 * @param container
 * @param tree_id
 * @param category_id
 */
function loadPageData(processType, container, tree_id, category_id) {
	 window.tfj_pv_ProcessType = processType;
	 window.tfj_pv_Container = container;
	 createProductValuePage(container, processType, "ORG");
	 
	 var url = tfj_ContextPath + "/admin/template/getTemplateTree?processType=" + 
	 		processType + "&category_id=" + category_id;
	 $.post(url, function(data){
           $.fn.zTree.init($("#" + tree_id), setting, data);
    });
}

//=================混批弹出框 start=================
var pvMark;
$(document).on('click','.toPvForm', function(){
	pvMark = $(this);
	// bootstrap modal 打开 productValueForm.jsp页面
//	console.log("clickPv: " + pvMark.prev().attr("name"));
});

function fillProductValue() {
	// 界面选中值
	var selectedVals = new Array();
	var checked = $("input[name='subBox']:checked");
	if (checked.size() < 1) {
		pvAlert("请选择一条记录。");
		return;
	}
	checked.each(function(index) {
		selectedVals[index] = $(this).parent().nextAll(":eq(2)").html();
	});
//	console.log("vals: " + vals);
	$("#cancel-dialog").click();
	// pvMark 前一个 节点为input
	var inputWidget = pvMark.prev();
	// 已存在值
	var inputVals = inputWidget.val().split(",");
	
	var finalVals = new Array();
	var index = 0;
	for (var i = 0; i < selectedVals.length; i++) {
		var sVal = $.trim(selectedVals[i]);
		var contains = false;
		for (var j = 0; j < inputVals.length; j++) {
			contains = sVal == $.trim(inputVals[j]);
			if (contains) {
				break;
			}
		}
		if(!contains) {
			finalVals[index++] = sVal;
		}
	}

	if (finalVals.length > 0) {
		inputWidget.val(inputVals + "," + finalVals.toString());
	}
	lastInput = inputWidget;
}

//$(document).on('click','.getPvVal', function(){
////alert("U click me! if U don't date me, then UR a complete asshole!");
//});

function page(pageNum) {
	var remote = pvMark.attr("href");
	var queryUrl = pvMark.data("tfj.mix.value");
	if (queryUrl && $.trim(queryUrl) != '') {
		remote = queryUrl;
	}
	$("#editModal").find('.modal-content').load(remote + "&page=" + pageNum);
}

function findPvPage() {
	var remote = pvMark.attr("href");
	var product_name = $("input[name='product_name']").val();
	if (product_name && $.trim(product_name) != '') {
		remote = remote + "&product_name=" + encodeURI($.trim(product_name));
		// console.log("remote: " + remote);
		pvMark.data("tfj.mix.value", remote);
	}
	
	$("#editModal").find('.modal-content').load(remote);
	pvMark.data("tfj.mix.value", remote);
}

function removeImgValue(thisObj) {
	pvConfirm("确定清除图片值?", function(ret) {
		if (ret) {
			var info = $(thisObj).attr("name").split(SPLITTER);
			var pv_id = info[1] ? info[1] : -1;
			var url = tfj_ContextPath + "/admin/productValue/removeImgValue";
			var data = "pv_id=" + pv_id;
			
			var imgShow = $(thisObj).parent().parent();
			
//			var rightValue = $(thisObj).parent().parent().prev();
			$.post(url, data, function(e) {
				if (e != "FAILED") {
					var result = $.parseJSON(e);
//					console.log("[image.html]: " + result.html);
					imgShow.replaceWith(result.html);
				} else {
					pvAlert("清除失败！");
				}
			});
		}
	});
}

// 全选功能  
function selectAll(obj){
	var arrayList=$("input[name='subBox']");
	for(var i=0;i<arrayList.length;i++){
		if(arrayList[i].disabled!=true){
			arrayList[i].checked=obj.checked;
		}
	}
}
//=================混批弹出框 end=================
$(document).on('click', 'a.unlock',function(){
	
	var info = $(this).attr("name").split(SPLITTER);
	var product_id = $("input.pv_product_id").val();
	
	pvConfirm("确定取消此值审核?", function(confirm) {
		if (confirm) {
			// 用来还原用户未保存输入
			var widgets = getInputWidgets(false, "notEncoded");
			
			var url = tfj_ContextPath + "/admin/productValue/releaseProductValue";
			
			var data = "product_id=" + product_id + "&template_id=" + info[0] + "&pv_id=" + $.trim(info[1]);
			$.post(url, data, function(e) {
				// console.log(e);
				if (e == "SUCCESS") {
//					$('.loading').hide();
					pvAlert("释放成功。", function() {
						createProductValuePage(tfj_pv_Container, tfj_pv_ProcessType, widgets);
						modifyProductState(AUDITED);
//						$('.loading').hide();
					});
				} else {
//					$('.loading').hide();
					pvAlert("释放失败。");
				}
			});
		}
	});
});

/**
 * 完成录入, 注意 productValueTab.jsp 页面中，
 * 隐藏的a标签，finishEntry(product_id, auditflag), auditflag 写死为1
 * @param product_id
 * @param auditflag
 */
function finishEntry(product_id, auditflag) {
	if (auditflag == 0) {
		pvAlert("此产品尚未审核，请审核后再完成数据录入。");
	} else if (auditflag == 2){
		pvAlert("此产品已完成录入。");
	} else {
		pvConfirm("确定已完成此产品数据录入？", function(confirm) {
			if (confirm) {
				var url = tfj_ContextPath + "/admin/productValue/finishEntry";
				var data = "product_id=" + product_id;
				var manaryurl=tfj_ContextPath+"/admin/productValue/mandatoryfinish?product_id="+product_id+"&category_id="+$(".pv_category_id").val()+"";
				$.post(url, data, function(e) {
					var jObj = $.parseJSON(e);
					if (jObj.result == "FAILED") {
						if (jObj.msg) {
							if(jObj.msg.indexOf("首张产品图片还没有上传")>0){//是否写死？  首页不上传图片和上传后提示不相同
								pvAlert(jObj.msg);
							}else{
								bootbox.confirm({
		                            message: jObj.msg,
		                            buttons: {
		                                confirm: {
		                                    label: "确定完成",
		                                    className: "btn-primary btn-sm"
		                                },
		                                cancel: {
		                                    label: "取消",
		                                    className: "btn-sm btn-cancel"
		                                }
		                            },
		                            callback: function(result) {
		                            	if(result){
		                            		$.ajax({
		                            			url:manaryurl,
		                            			type:"post",
		                            			data:null,
		                            			datatype:"text",
		                            			success:function(data){
		                            				if(data=='true'){
		                            					pvAlert("操作成功。", function() {
		                            						location.href=location.toString().replace("recordlog=recordlog&", "");//操作完成，去掉“recordlog=recordlog&”，该字段标志着要在操作日志记录
		                        						});
		                            				}else{
		                            					pvAlert("完成失败。", function() {
		                            						location.href=location.toString().replace("recordlog=recordlog&", "");
		                        						});
		                            				}
		                            			} 
		                            		});
		                            	}
		                            }
		                            
		                      });
							}
						} else {
							pvAlert("程序错误！");
						}
					} else {
						pvAlert("操作成功。", function() {
							location.reload();
						});
					}
				});
			}
		});
	}
}

function modifyProductState(state) {
	var name = "";
	if (state == AUDITED) {
		name = "已审核";
	} else if (state == FINISHED) {
		name = "已完成";
	} else if (state == UNAUDITED){
		name = "未审核";
	}
	if (name != "") {
		$("input.product_auditflag").val(state);
		$("#product_auditflagName").html(name);
		if (state == AUDITED) {
			//完成按钮
			$("a[name='CheckProductValue']").attr("style", "display: block;");
			$("a[name='CheckProductValue']").attr("style", "size: 10px;");
			//各个环节保存和审核按钮
			$(".plantInputAudit").attr("style", "display: block;");
			$(".plantInputSave").attr("style", "display: block;");
			$(".superviseInputAudit").attr("style", "display: block;");
			$(".superviseInputSave").attr("style", "display: block;");
			$(".warehouseInputAudit").attr("style", "display: block;");
			$(".warehouseInputSave").attr("style", "display: block;");
			$(".saleInputAudit").attr("style", "display: block;");
			$(".saleInputSave").attr("style", "display: block;");
		}
	}
}

function allPrpos ( obj ) { 
	  // 用来保存所有的属性名称和值 
	  var props = "" ; 
	  // 开始遍历 
	  for ( var p in obj ){ // 方法 
	  if ( typeof ( obj [ p ]) == " function " ){ obj [ p ]() ; 
	  } else { // p 为属性名称，obj[p]为对应属性的值 
	  props += p + " = " + obj [ p ] + " /t " ; 
	  } } // 最后显示所有的属性 
	  console.log( props ) ;
}

var elementScrollTop = $('.wrap').offset().top;
$(document).scroll(function(){
	var viewScrollTop = document.body.scrollTop || document.documentElement.scrpllTop || window.pageYOffset;
	var paddingNum = parseInt($('.borderStyle').css('padding-left')) + 1;
	setInterval(function(){
		var tabContent = $('.tab-content').width();
		$('.wrap').width(tabContent*2/3-paddingNum*3+3);
	},1);
	
	if(viewScrollTop >= elementScrollTop){
		$('.wrap').css({
			'position':'fixed',
			'top' : '0px',
			'z-index':'100'
		});
		
	}else{
		$('.wrap').css({
			'position':'fixed',
			'top' : (elementScrollTop - viewScrollTop)  + 'px',
			'z-index':'100'
		});
	}
	$('.borderStyle').css('margin-top','34px');

});

$(document).on('mouseover','.closeBtn img',function(){
	$(this).attr('src',tfj_Full_ContextPath+'static/admin/index/img/delete_btn_red.png');
});
$(document).on('mouseout','.closeBtn img',function(){
	$(this).attr('src',tfj_Full_ContextPath+'static/admin/index/img/delete_btn_red.png');
});
