/**
 *企业绑定号管理-操作-管理
 */
var pvMark;
$(document).on('click','.manage', function(){
	pvMark = this;
});
function findEMPage() {
	
	var remote = pvMark;
	var company_user = $("input[name='company_user']").val();
	if (company_user && $.trim(company_user) != '') {
		remote = remote + "&bind_user=" + encodeURI($.trim(company_user))+ "&time="+new Date().getTime();
	}else{
		remote = remote + "&bind_user=&time="+new Date().getTime();//一定要携带bind_user，否则不能跳转到action
	}
	$("#editModal").find('.modal-content').load(remote);
}


/**
 * 解绑
 */
$(document).on('click','.removebind', function(){
	$("#editModal").find('.modal-content').load(pvMark);
});
