function pvAlert(msg, callbackFunction) {
	bootbox.alert({
		message: msg,
		buttons: {
			ok: {
	            label: "确定",
	            className: "btn-primary btn-sm"
	        }
		},
        callback: callbackFunction
	});
}

function pvConfirm(msg, callbackFunction) {
	return bootbox.confirm({
		message: msg,
		buttons: {
            confirm: {
                label: "确定",
                className: "btn-primary btn-sm"
            },
            cancel: {
                label: "取消",
                className: "btn-sm btn-cancel"
            }
        },
        callback : callbackFunction
	});
}

function audit() {
	var uids = '';
	var flag = false;
	var auditFlag = false;
	$("input[name='subBox']:checked").each(function() {

		if ($(this).attr('audit') == 1) {
			auditFlag = true;
		}

		if (flag) {
			uids = uids + "," + $(this).val();
		} else {
			uids = uids + $(this).val();
		}
		flag = true;
	});
	if (auditFlag) {
		pvAlert("提示:所选产品存在已审核的产品!");
	} else {
		if (uids != '') {
			bootbox.confirm({
				message : "确定审核？",
				buttons : {
					confirm : {
						label : "审核",
						className : "btn-primary btn-sm"
					},
					cancel : {
						label : "取消",
						className : "btn-sm btn-cancel"
					}
				},
				callback : function(result) {
					if (result) {
						var url = tfj_ContextPath + "/admin/product/audit";
						var form = $("<form></form>");
						form.attr('action', url);
						form.attr('method', 'post');
						var param = $("<input type='hidden' name='rid' value='"
								+ uids + "'/>");
						form.append(param);
						form.submit();
						// $.post("${contextPath}/admin/catagory/audit?rid="+uids,
						// function(data){
						// $.fn.zTree.init($("#rescTree"), setting,
						// JSON.parse(data));
						// });
					}
				}
			});
		} else {
			pvAlert("请选择未审核的产品。");
		}
	}
}

// 判断非法字符
$("#search").click(function(){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
    if($("#productname").val().trim() != ""){
    	if(pattern.test($("#productname").val())){
    		pvAlert("存在非法字符，请重新输入查询条件。", function() {
    			$("#productname").val("");
    		});           			 
        } else {
     	   $("#search").attr("type", "submit");
		}
   } else {
	   $("#search").attr("type", "submit");//输入框为空时，按钮设为sumbit
   }  
});

function toDataInput() {
	var checked = $("input[name='subBox']:checked");
	if (checked && checked.length == 1) {
		var auditFlag = checked.parent().nextAll().find('.product_auditflag');
		var cid=checked.parent().nextAll().find('.cidHidden');
		var pid = checked.val();
		location.href = tfj_ContextPath + "/admin/productValue?recordlog=recordlog&product_id=" + pid+"&category_id="+cid.val()+"";
	} else if (checked.length == 0) {
		pvAlert("请先勾选一个产品。");
	} else {
		pvAlert("请只勾选一个产品。");
	}
}

function page(pageNum) {
    $("#page").val(pageNum);
    $("#productSearchForm").submit();
}

/**
 * 全选功能 
 */
function selectAll(obj){
	var arrayList=$("input[name='subBox']");
	for(var i=0;i<arrayList.length;i++){
		if(arrayList[i].disabled!=true){
			arrayList[i].checked=obj.checked;
		}
	}
}

/**
 * 列头排序
 */
$(document).on("click", "a.sort-column", function() {
	var name = $(this).attr("name");
	var page=$("#page").val();
	var sortCondition = "tag=";
	var tag_span = $(this).children().first();
	if (tag_span.hasClass("fa-sort")) { // 未排序-->asc
		sortCondition = name + " asc";
	} else if (tag_span.hasClass("fa-sort-asc bigger-200")) { // asc-->desc
		sortCondition = name + " desc";
	} else if (tag_span.hasClass("fa-sort-desc bigger-200")){ // desc-->asc
		sortCondition = name + " asc";
	}
	var productSearchForm = $("#productSearchForm");
	$("#sortTag").val(sortCondition);
	productSearchForm.append("<input name='page' type='hidden' value='" + page + "'/>");
	$("#productSearchForm")[0].submit();
});

function initSort(sort_name, sort_type) {
	sort_name = $.trim(sort_name);
	sort_type = $.trim(sort_type);
	var sort_columns = $("a.sort-column");
	if(sort_name != '' && sort_type != '') {
		sort_columns.each(function(index) {
			var name = $(this).attr("name");
			if (name == sort_name) {
				var tag_span = $(this).children().first();
				if (sort_type == 'asc') {
					tag_span.attr("class", "ace-icon fa fa-sort-asc bigger-200");
				} else if (sort_type == 'desc') {
					tag_span.attr("class", "ace-icon fa fa-sort-desc bigger-200");
				}
				return;
			}
		});
	}
}