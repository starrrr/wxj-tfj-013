	//获取工程名
	var host = $("#projectPath").val();
$(function(){
	//本地存储
	var data;
	if(sessionStorage.indexData){ 
//		$(window).load(function(){
//			$('#container').html(sessionStorage.indexData);
//			$('.fakeloader').hide();
//			console.log('yes');
//		});
		$('#container').html(sessionStorage.indexData);
//		$('.loading').hide();
		console.log('yes');
	}else{
		console.log('no');
		
		//没有本地存储就直接根据后台传的url获取data，再进行解析
		$.get(url,'',function(data,status){
			//启动缓存返回的数据为string（需要转json） 关闭缓存返回的数据为json格式，不需要转换json
			if(typeof(data)=='string'){
				data=$.parseJSON(data);
			}
			if(status = true){
				$('.overtimeTip').hide();
	//			console.log("数据加载成功！");
				var  containerCont = "";
				if(data.product){
					var product = data.product;
					containerCont += '<div class="productIntroduce">' +
										'<div class="product">' + 
											'<p>产品名称：' +product.productName+ '</p>' +
											'<p>种植方式：' +product.plantType+ '</p>' +
											'<p>产地：' +product.productLocation+ '</p>' +
											'<p>年份：' +product.year+ '</p>' +
											'<p>等级：' +product.productLevel+ '</p>' +
										'</div>' +
										'<img class="productImg" src="' +product.imgPath + '">'+
									'</div>';
				}
				containerCont += '<div class="wholeGrow">';
				// 解析种植环节
				if(data.PLANT_PROCESS.child){
					containerCont += '<div class="growLink plantLink">';
					containerCont += plantToFind(data.PLANT_PROCESS);
					containerCont += '</li></ul></div>';
					var addDiv = '<p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_01.png"></p>';
				}
				// 加工环节
				if(data.SUPERVISE_PROCESS.child){
					containerCont += '<div class="growLink processLink">';
					containerCont += superviseToFind(data.SUPERVISE_PROCESS);
					containerCont += '</li></ul></div>';
				}
				// 流通环节
				if(data.WAREHOUSE_PROCESS.child){
					containerCont += '<div class="growLink circulationLink">';
					containerCont += warehouseToFind(data);
					// 判断销售环节是否有数据，没有的，加上终点图标
					if(!data.SALE_PROCESS.child){
						containerCont += '<div class="stepsLeft"><img class="lastIcon" src="'+ host +'static/admin/index/img/icon_site_purple.png"></div>';
					}else{
						containerCont += '';
					}
					containerCont += '</li></ul></div>';
				}
				// 销售环节
				if(data.SALE_PROCESS.child){
					containerCont += '<div class="growLink marketLink">';
					containerCont += saleToFind(data.SALE_PROCESS);
					// 最下面的图标
					containerCont += '<div class="stepsLeft"><img class="lastIcon" src="'+ host +'static/admin/index/img/icon_site.png"></div>';
					containerCont += '</li></ul></div>';
				}else{
					containerCont += '';
				}
				containerCont += '</div>';
				sessionStorage.indexData = containerCont;
				// sessionStorage.clear();
				$('#container').html(containerCont);
//				$('.fakeloader').hide();
//				$('.loading').hide();
			}else{
				var timeOut = setTimeout(function(){
					$('.overtimeTip').show();
//					$('.fakeloader').hide();
//					$('.loading').hide();
				},180000);
				/*if(timeOut){
					clearTimeout(timeOut);
					$('.overtimeTip').hide();
					timeOut = null;
				}*/
				console.log("数据加载失败！");
				// alert('数据加载失败！')
			}
		});
	}
});


//种植环节
function plantToFind(obj){
	dataHtml = '<p class="linkTitle">' +
							'<img src="' +obj.iconPath+ '">' +
							'<span>' +obj.name+ '</span>' +
						'</p>';
	if(obj.child){
		var childObj = obj.child;
		dataHtml += '<ul class="linkSteps">';
		// 获取加工环节的节点步骤
		for(var i in childObj){
			dataHtml += '<li>' +
							'<div class="stepsLeft">' + 
								'<p class="stepName"><img src="' +childObj[i].iconPath+ '"><span>' +childObj[i].name+ '</span></p>' +
//								'<p class="stepTime">' +childObj[i].value+ '</p>' +
							'</div>';
			var rightHtml = '';
			if(childObj[i] == childObj[1]){
				rightHtml = getRightData(childObj[i],rightHtml);
				// console.log(rightHtml);
				// 判断查询的数据是否为空
				if(rightHtml != ''){
					dataHtml += '<div class="stepsRight"><p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_01.png"></p></div>';
				}else{
					dataHtml += '';
				}
				dataHtml += rightHtml;
				
			}else if(childObj[i].name == '田间管理'){
				rightHtml = getFieldManageData(childObj[i],rightHtml);
				dataHtml += rightHtml;
			}else if(childObj[i].name == '上架管理'){
				rightHtml = getFieldManageData(childObj[i],rightHtml);
				dataHtml += rightHtml;
			}else if(childObj[i].name == '苓场管理'){
				rightHtml = getFieldManageData(childObj[i],rightHtml);
				dataHtml += rightHtml;
			}else{
				dataHtml += '<div class="stepsRight"><p style="margin-top:10px;"></p></div>';
				rightHtml = getRightData(childObj[i],rightHtml);
				dataHtml += rightHtml;
			}
			dataHtml +='</li>';
		}
		dataHtml += '</ul>';
	}
	return dataHtml;
}


//加工环节
function superviseToFind(obj){
	dataHtml = '<p class="linkTitle">' +
							'<img src="' +obj.iconPath+ '">' +
							'<span>' +obj.name+ '</span>' +
						'</p>';
	if(obj.child){
		var childObj = obj.child;
		dataHtml += '<ul class="linkSteps">';
		// 获取加工环节的节点步骤
		for(var i in childObj){
			dataHtml += '<li>' +
							'<div class="stepsLeft">' + 
								'<p class="stepName"><img src="' +childObj[i].iconPath+ '"><span>' +childObj[i].name+ '</span></p>' +
//								'<p class="stepTime">' +childObj[i].value+ '</p>' +
							'</div>';
			var rightHtml = '';
			// 第一个节点加上图标
			if(childObj[i] == childObj[0]){
				rightHtml = getRightData(childObj[i],rightHtml);
				// console.log(rightHtml);
				// 判断查询的数据是否为空
				if(rightHtml != ''){
					dataHtml += '<div class="stepsRight"><p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_05.png"></p></div>';
				}else{
					dataHtml += '';
				}
				dataHtml += rightHtml;
			}else{
				dataHtml += '<div class="stepsRight"><p style="margin-top:10px;"></p></div>';
				rightHtml = getRightData(childObj[i],rightHtml);
				dataHtml += rightHtml;
			}
			dataHtml +='</li>';
		}
		dataHtml += '</ul>';
	}
	return dataHtml;
}



//流通环节
function warehouseToFind(obj){
	dataHtml = '<p class="linkTitle">' +
							'<img src="' +obj.WAREHOUSE_PROCESS.iconPath+ '">' +
							'<span>' +obj.WAREHOUSE_PROCESS.name+ '</span>' +
						'</p>';
	if(obj.WAREHOUSE_PROCESS.child){
		var childObj = obj.WAREHOUSE_PROCESS.child;
		dataHtml += '<ul class="linkSteps">';
		// 获取加工环节的节点步骤
		for(var i in childObj){
			console.log(childObj.length)
			// 判断销售环节是否有子节点，没有的话，给流通环节最后一个li去掉border
			if(!obj.SALE_PROCESS.child){
				if(i == childObj.length - 1){
					dataHtml += '<li style="border-bottom:none;">';
				}else{
					dataHtml += '<li>';
				}
			}else{
				dataHtml += '<li>';
			}
			dataHtml += '<div class="stepsLeft">' + 
							'<p class="stepName"><img src="' +childObj[i].iconPath+ '"><span>' +childObj[i].name+ '</span></p>' +
//							'<p class="stepTime">' +childObj[i].value+ '</p>' +
						'</div>';
			var rightHtml = '';
			// 获取右边显示的数据
			dataHtml += '<div class="stepsRight"><p style="margin-top:10px;"></p></div>';
			rightHtml = getRightData(childObj[i],rightHtml);
			dataHtml += rightHtml;
			dataHtml +='</li>';
		}
		dataHtml += '</ul>';
	}
	return dataHtml;
}

// 销售环节
function saleToFind(obj){
	dataHtml = '<p class="linkTitle">' +
							'<img src="' +obj.iconPath+ '">' +
							'<span>' +obj.name+ '</span>' +
						'</p>';
	if(obj.child){
		var childObj = obj.child;
		dataHtml += '<ul class="linkSteps">';
		// 获取加工环节的节点步骤
		for(var i in childObj){
			dataHtml += '<li>' +
							'<div class="stepsLeft">' + 
								'<p class="stepName"><img src="' +childObj[i].iconPath+ '"><span>' +childObj[i].name+ '</span></p>' +
//								'<p class="stepTime">' +childObj[i].value+ '</p>' +
							'</div>';
			var rightHtml = '';
			// 获取右边显示的数据
			dataHtml += '<div class="stepsRight"><p style="margin-top:10px;"></p></div>';
			rightHtml = getRightData(childObj[i],rightHtml);
			dataHtml += rightHtml;
			dataHtml +='</li>';
		}
		dataHtml += '</ul>';
	}
	return dataHtml;
	
}

// 查找各个节点，获取有value的数据
function getRightData(childNode,html){
	// level 1的节点值是否为空不需要判断
		if(childNode.child){
			// console.log(childNode)
			var twoChild = childNode.child;
			for(var j in twoChild){
				// console.log(twoChild[j])
				// 判断level 2的节点值是否为空
				if(twoChild[j].value != '' && twoChild[j].value != 'image'){
					html += '<div class="stepsRight">' +
									'<div class="stepDetail">' +
										'<p class="stepCont">' +twoChild[j].name +'：'+twoChild[j].value+ '</p>' +
									'</div>' +
								'</div>';
				}else{
					if(twoChild[j].child){
						var threeChild = twoChild[j].child;
						for(var k in threeChild){
							// 判断level 3的节点值是否为空
							if(threeChild[k].value != '' && threeChild[k].value != 'image'){
								html += '<div class="stepsRight">' +
												'<div class="stepDetail">' +
													'<p class="stepCont">' +threeChild[k].name +'：'+threeChild[k].value+ '</p>' +
												'</div>' +
											'</div>';
								if(threeChild[k].child){
									var fourChild = threeChild[k].child;
									for(var l in fourChild){
										// 判断level 4的节点值是否为空
										if(fourChild[l].value != '' && fourChild[l].value != 'image'){
											html += '<div class="stepsRight">' +
												'<div class="stepDetail">' +
													'<p class="stepCont">' +fourChild[l].name +'：'+fourChild[l].value+ '</p>' +
												'</div>' +
											'</div>';
										}else{
											html += '';
										}
									}
								}
							}else{
								if(threeChild[k].child){
									var fourChild = threeChild[k].child;
									for(var l in fourChild){
										if(fourChild[l].value != '' && fourChild[l].value != 'image'){
											html += '<div class="stepsRight">' +
												'<div class="stepDetail">' +
													'<p class="stepCont">' +fourChild[l].name +'：'+fourChild[l].value+ '</p>' +
												'</div>' +
											'</div>';
										}else{
											html += '';
										}
									}
								}
							}
						}
					}
				}
			}
		}
	return html;
}

function getFieldManageData(Node,html) {
//	判断当前节点的remark字段是否为空
	if(Node.remark != ''){
//		console.log(Node.remark)
		var childHtml = '';
//		判断是否子节点，然后各种管理事项
		if(Node.child){
			var nodeChild = Node.child;
			var dataSplit;
//			console.log(nodeChild)
			for(var i in nodeChild){
				if(nodeChild[i].name.indexOf("管理") != '-1'){
//					console.log(nodeChild[i].name.split('管理')[0]);
					if(i != nodeChild.length - 1){
						dataSplit = nodeChild[i].name.split('管理')[0] + "、";
					}else{
						dataSplit = nodeChild[i].name.split('管理')[0];
					}
				}else{
//					console.log(nodeChild[i].name)
					if(i != nodeChild.length - 1){
						dataSplit = nodeChild[i].name + "、";
					}else{
						dataSplit = nodeChild[i].name;
					}
				}
				childHtml += dataSplit;
			}
		}
//		获取remark中包含“管理内容”的内容
		if(Node.remark.indexOf('管理内容') != -1){
			var dataTxt1 = Node.remark.split("；")[0];
			html += '<div class="stepsRight"><p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_02.png"></p></div>';
			html += '<div class="stepsRight">' +
						'<div class="stepDetail">' +
							'<p class="stepCont">' + dataTxt1  + childHtml + '</p>' +
						'</div>' +
					'</div>';
		}
//		获取remark中包含“技术服务”的内容
		if(Node.remark.indexOf('技术服务') != -1){
			var dataTxt2 = Node.remark.split("；")[1];
			html += '<div class="stepsRight"><p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_03.png"></p></div>';
			html += '<div class="stepsRight">' +
						'<div class="stepDetail">' +
							'<p class="stepCont">' + dataTxt2 + '</p>' +
						'</div>' +
					'</div>';
		}
//		获取remark中包含“生产资料”的内容
		if(Node.remark.indexOf('生产资料') != -1){
			var dataTxt3 = Node.remark.split("；")[2];
			html += '<div class="stepsRight"><p class="tagImg"><img src="'+ host +'static/admin/index/img/unit_04.png"></p></div>';
			html += '<div class="stepsRight">' +
						'<div class="stepDetail">' +
							'<p class="stepCont">' + dataTxt3 + '</p>' +
						'</div>' +
					'</div>';
		}
	}else{
		html = '';
	}
	return html;
}
