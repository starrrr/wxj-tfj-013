	// console.log(url)
	// console.log(url.split("=")[2])
// var  linkParam = url.split("=")[3];
function getLinkParam(){
	var  p1 = url.split("=");
	var leng = p1.length;
	return p1[leng - 1];
}

$(function(){

	// 本地储存
	localStorage(getLinkParam());
});

/**
 * 判断是否有本地存储数据
 * param 各个环节值：PLANT_PROCESS,SUPERVISE_PROCESS,WAREHOUSE_PROCESS,SALE_PROCESS
 */
function localStorage(param){
	if(sessionStorage[param]){
		// console.log(sessionStorage[param])
		/*$(window).load(function(){
			$('#container').html(sessionStorage[param]);
			$('.fakeloader').hide();
			console.log('yes');
		});*/
		$('#container').html(sessionStorage[param]);
//		$('.loading').hide();
		console.log('yes');
		evenClick();
		reportClick();
		mulClick();
	}else{
		console.log('no');
		//如果没有本地存储，则通过后台传的url，解析data数据
		dataParser(url,param);
	}
	
}

//子节点点击显示
function evenClick(){
	$(".detailStep li").click(function(e){
		var path = $("#projectPath").val();
		if(!$(this).hasClass('testReportBtn')){
		var liHasImg = $(this).children('p').find('img.rightIcon');
		
			// 阻止冒泡事件
			e.stopPropagation();
			if($(this).children('ul').css('display') == 'none'){
				$(this).children('ul').css('display','block');
				$(this).prevAll('li').find('ul').css('display','none');
				$(this).nextAll('li').find('ul').css('display','none');
				$(this).prevAll('li').find('.rightIcon').attr('src',path +'static/admin/index/img/icon_down.png');
				$(this).nextAll('li').find('.rightIcon').attr('src',path +'static/admin/index/img/icon_down.png');
				//liHasImg.attr('src','img/icon_up.png');
				liHasImg.attr('src',path +'static/admin/index/img/icon_up.png');
			}else{
				$(this).find('ul').css('display','none');
				//liHasImg.attr('src','img/icon_down.png');
				liHasImg.attr('src',path +'static/admin/index/img/icon_down.png');
			}
		}
	});
	
}

/**
*获取后台传过来的URL，解析页面的json数据
*param 各个环节的本地存储参数
*/
function dataParser(linkUrl,param,callback){
	$.get(linkUrl, '', function(data,status){
		// console.log(data)
		var host = $("#projectPath").val();
		
		//启动缓存返回的数据为string（需要转json） 关闭缓存返回的数据为json格式，不需要转换json
		if(typeof(data)=='string'){
			data=$.parseJSON(data);
		}
		if(status = 'success'){
			$('.overtimeTip').hide();
			var containerCont = "";
			var obj = data;
			for( var i in obj){
				if(obj[i].visable == true){
					var noImgCont;
//					判断是有大图，有的话，拼接大图，没有的话，不拼接
					if(obj[i].imgPath != "" && obj[i].imgPath != undefined){
						containerCont += '<div class="everyStep product" id="imgProduct">' +
													'<p class="stepTitle"><img src="' + obj[i].imgPath + '"></p>' ;
					}else if(obj[i].name != ""){
						
						containerCont += '<div class="everyStep">' +
													'<p class="stepTitle"><span>' + obj[i].name + '</span></p>';
					}
					if(obj[i].child){//这里是第一个child
						//子集存在，循环子集取子集的下一级
						var objchild = obj[i].child;
						containerCont += '<ul class="detailStep">';
						var liImg;
						for ( var j in objchild){
							if(objchild[j].visable == true){
								if(objchild[j].imgPath != "" && objchild[j].iconPath == ''){
									liImg = '<li class="stepImg"><img src="' + objchild[j].imgPath + '"></li>';
								}else{
									liImg = ''; 
								}
								if(objchild[j].iconPath !='' && objchild[j].imgPath == ''){	
									// 获取有两个图片的iconPath值
									var iconPathStr = JSON.stringify(objchild[j].iconPath);
									iconPathStr = iconPathStr.substring(1,iconPathStr.length - 1);
									if(iconPathStr.indexOf(";") != -1){
										var newIconPath = iconPathStr.split(';');
										if(objchild[j].value != ""){
	//												containerCont += '<li class="testReportBtn"><p>' +
											// 处理带有报告图片的li
											containerCont += findChildImg(objchild[j]);
											
											containerCont +='<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
															'<span class="leftProperty">' + objchild[j].name + '</span>';
											//图片的value值为否，则不显示右边的小图标
											if(objchild[j].value != '否'){
												containerCont += '<img class="rightIcon" src="' +host +newIconPath[1] + '">' +
																 '<span class="rightValue">' + objchild[j].value + '</span>' ;
											}else{
												containerCont += '<span class="rightValue">' + objchild[j].value + '</span>' ;;
											}
															
											containerCont += '</p>';
										}else{
											containerCont += '<li><p>' +
															'<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
															'<span class="leftProperty">' + objchild[j].name + '</span>' +
															'<img class="rightIcon" src="' +host + newIconPath[1] + '">' +
															'</p>';
										}
									}else{
										containerCont += '<li><p>' +
														'<img class="leftIcon" src="' + objchild[j].iconPath + '">' +
														'<span class="leftProperty">' + objchild[j].name + '</span>';
										containerCont += getMulLots(objchild[j]);				 
														// '<span class="rightValue">' + objchild[j].value + '</span>' +
										containerCont += '</p>';
									}
									if(objchild[j].child){//这里是第二个子集
										var objtwo = objchild[j].child;
										containerCont += '<ul class="secondStep">';
										for ( var k in objtwo){
											if(objtwo[k].visable == true){
												// 获取有两个图片的iconPath值
												var iconPathThird = JSON.stringify(objtwo[k].iconPath);
												iconPathThird =iconPathThird.substring(1,iconPathThird.length - 1);
												if(iconPathThird.indexOf(";") != -1){
													var newIconPath = iconPathThird.split(';');
													if(objtwo[k].value){
														// 处理带有报告图片的li
														containerCont += findChildImg(objtwo[k]);
	//															containerCont += '<li class="testReportBtn"><p>' +
														containerCont += '<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
																	'<span class="leftProperty">' + objtwo[k].name + '</span>';
														//图片的value值为否，则不显示右边的小图标
														if(objtwo[k].value != '否'){
															containerCont += '<img class="rightIcon" src="' +host + newIconPath[1] + '">' +
															                 '<span class="rightValue">' + objtwo[k].value + '</span>' ;
														}else{
															containerCont += '<span class="rightValue">' + objtwo[k].value + '</span>' ;
														}
																	
														containerCont += '</p>';
													}else{
														containerCont += '<li><p>' +
																	'<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
																	'<span class="leftProperty">' + objtwo[k].name + '</span>' +
																	'<img class="rightIcon" src="' +host + newIconPath[1] + '">' +
																	'</p>';
													}
												}else{
													containerCont += '<li><p>' +
																	'<img class="leftIcon" src="' + objtwo[k].iconPath + '">' +
																	'<span class="leftProperty">' + objtwo[k].name + '</span>' +
																	'<span class="rightValue">' + objtwo[k].value + '</span>' +
																	'</p>';
												}
												if(objtwo[k].child){//这里是第三个子集
													var objthree = objtwo[k].child;
													containerCont += '<ul class="thirdStep">';
													for ( var l in objthree){
														if(objthree[l].visable == true){
															// 获取有两个图片的iconPath值
															var iconPathFourth = JSON.stringify(objthree[l].iconPath);
															iconPathFourth =iconPathFourth.substring(1,iconPathFourth.length - 1);
															if(iconPathFourth.indexOf(';') != -1){
																var newIconPath = iconPathFourth.split(';');
																if(objthree[l].value){
																	// 处理带有报告图片的li
																	containerCont += findChildImg(objthree[l]);
	//																		containerCont += '<li class="testReportBtn"><p>' +
																	containerCont +='<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
																				'<span class="leftProperty">' + objthree[l].name + '：</span>';
																	//图片的value值为否，则不显示右边的小图标
																	if(objthree[l].value != '否'){
																		containerCont +='<img class="rightIcon" src="' +host + newIconPath[1] + '">' +
																						'<span class="rightValue">' + objthree[l].value + '</span>';
																	}else{
																		containerCont += '<span class="rightValue">' + objthree[l].value + '</span>';
																	}
																	containerCont += '</p>';
																}else{
																	containerCont += '<li><p>' +
																					'<img class="leftIcon" src="' + host +newIconPath[0] + '">' +
																					'<span class="leftProperty">' + objthree[l].name + '</span>' +
																					'<img class="rightIcon" src="' +host + newIconPath[1] + '">' +
																					'</p>';
																}
															}else{
																containerCont += '<li><p>' +
																				'<img class="leftIcon" src="' + objthree[l].iconPath + '">' +
																				'<span class="leftProperty">' + objthree[l].name + '：</span>' +
																				'<span class="rightValue">' + objthree[l].value + '</span>' +
																				'</p>';
															}
														}
														if(objthree[l].child){//这里是第四个子集
															var objfour = objthree[l].child;
															containerCont += '<ul class="fourthStep">';
															for(var m in objfour){
																containerCont += '<li><p>' +
																				'<span class="leftProperty">' + objfour[m].name + '：</span>' +
																				'<span class="rightValue">' + objfour[m].value + '</span>' +
																				'</p></li>';
															}
															containerCont += '</ul>';
														}
													}
													containerCont +='</li>';
													containerCont += '</ul>';
												}
											}
										}
										containerCont += '</ul>';
									}
									containerCont +='</li>';
								}
								
								if(objchild[j].iconPath =='' && objchild[j].imgPath == ''){
									containerCont += '';
								}
								containerCont += liImg;
							}
//							containerCont += liImg;
						}
//						containerCont += liImg;
						containerCont += '</ul>';
					}
					containerCont += '</div>';
				}else{
					containerCont = "";
				}	
			}
			sessionStorage[param] = containerCont;
			// sessionStorage.clear();
			$('#container').html(containerCont);
//			$('.fakeloader').hide();
//			$('.loading').hide();
		}else{
			var timeOut = setTimeout(function(){
				$('.overtimeTip').show();
//				$('.fakeloader').hide();
//				$('.loading').hide();
			},180000);
			/*if(timeOut){
				clearTimeout(timeOut);
				$('.overtimeTip').hide();
				timeOut = null;
			}*/
			// alert('页面数据加载出错了！');
		}
		evenClick();
		reportClick();
		mulClick();
		if(typeof(callback) == 'function'){
			callback();
		}
	});
	
	
}

//获取报告一行的子节点图片，并隐藏
function findChildImg(imgChild){
//	 console.log(imgChild)
	if(imgChild.child){
//		 console.log(imgChild)
		var imgChild = imgChild.child;
		var cont = '';
		var imgSrc = '';
		if(imgChild != ''){
			for(var i in imgChild){
//				获取有报告图片的节点
				if(imgChild[i].name=='image'){
//					console.log(imgChild[i]);
					cont = '<li class="testReportBtn">';
					if(imgChild[i].imgPath != '' ){
//						拼接报告图片图片，并隐藏着
						imgSrc += '<p class="hidden">' + imgChild[i].imgPath + '</p>';
					}else{
						imgSrc += '';
					}
				}else{
					cont = '<li>';
				}
			}
			cont += imgSrc;
			cont += '<p>';
		}else{
			cont = '<li><p>';
		}
	}else{
		cont = '<li><p>';
	}
	return cont;
}


// 检测报告弹框
function reportClick(){
$('.testReportBtn').click(function(){
//	$('html,body').css('overflow','hidden');
//	$('.testReport').removeClass('hidden');
	var imgSrc;
	var reportHtml = '';
	var imgParent = $($(this).children('p.hidden'));
	var rightValue = $(this).find('.rightValue').text();
//	console.log(rightValue);
	//判断：“是”显示报告，“否”不显示报告
	if(rightValue == '是'){
		if(imgParent){
			for(var i = 0; i < imgParent.length; i++){
//				获取隐藏着的报告图片
				imgSrc = $(imgParent[i]).text();
				reportHtml += '<li><a href="' + imgSrc + '"><img src="' + imgSrc + '"></a></li>';
			}
			$('.imgContainer').html(reportHtml);
		}else{
			alert('暂时没有数据报告！')
		}
//		配置插件参数
		var options = {allowUserZoom:true};
//		调用插件
		$("#Gallery a").photoSwipe(options);
		//$(".gallery").hide()//隐藏小图页面
//		给第一个li标签绑定click事件
		$(".gallery li").eq(0).find("a").trigger("click")//直接跳转大图页面	
	}else{
		return false;
	}
});
//$(document).on('click','.close',function(){
//	$('.testReport').addClass('hidden');
//	$('html,body').css('overflow','auto');
//});
}


// 获取有多个批号值的原料批号
function getMulLots(currObj){
	// 原料批号的值是由','	（英文状态下）隔开的，截取各个批号值
	sessionStorage.clear();
	var pHtml = '<span class="rightValue">' ;						
	if(currObj.name == '原料批号' && currObj.value.indexOf(',') != -1){
		// console.log(currObj.value)
		var newArrLot = currObj.value.split(',');
		
		for (var i = 0; i <newArrLot.length; i++) {
			if(i == 0){
//				默认第一个批号为主批号
				pHtml += '<a class="block" href="#" id="link'+i+'">' + newArrLot[i] + '</a>';
			}else{
//				其他批号为次批号
				pHtml += '<a class="block anotherId" id="link'+i+'" href="#">' + newArrLot[i] + '</a>';
			}
		};
	}else{
		pHtml += currObj.value;
	}
	pHtml += '</span>' ;
	return pHtml;
}

// 点击蓝色的原料批号，获取加工环节和种植环节的数据
function mulClick(){
	$('.anotherId').click(function(e){
		var roleName = $('#role_name').val();
		// 阻止冒泡事件
		e.stopPropagation();
		//通过流通批号，获取新的json路径
		var newUrl = $('#projectPath').val() + "admin/index/getJsonByMBNum?material_batch_num="+$(this).text()+"&role_name="+roleName+"&processType=" + getLinkParam();
		//获取种植环节原本的链接路径
		var aHref = $('.menus .menusChild div:nth-child(2)').find('a').attr('href');
		var hrefHead = aHref.split("?")[0];
		var hrefFoot = aHref.split("&")[1];
		//给种植环节赋予通过流通批号获取的新路径
		$('.menusChild div:nth-child(2)').find('a').attr('href',hrefHead + "?material_batch_num=" + $(this).text() + "&" + hrefFoot + "&flow_batch_num=" + $("#flow_batch_num").val());
		console.log($('.plant').attr('href'))
		// console.log($('.menus ul li:nth-child(2)').find('a').attr('href'))
		url = newUrl;
		console.log(url);
		//执行页面解析数据
		dataParser(url,getLinkParam(),$.proxy(function(){
			$('.block').addClass('anotherId');
			$('#'+this.id).removeClass('anotherId');
			mulClick();
		},this));
	});
}

