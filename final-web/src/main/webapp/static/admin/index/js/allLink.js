//获取页面的宽高
$(function(){
	var roleName = $("#role_name").val();

	// 给对应的tab加上蓝色图标和蓝色文字
	$('.'+getParam()).addClass('selected');


	//底部tab点击效果
	$(document).on('click','.menusChild div',function(){
		var aLink = $(this).find('a');
		// 游客组(只显示首页)
		if(roleName == 'g' || roleName == 'G'){
			if($(this).hasClass('index')){
				changgeLi($(this));
			}else{
				aLink.removeAttr('href');
				$('.menusChild div').removeClass('selected');
				$(this).addClass('selected');
				$('.focusPage').removeClass('hidden');
			}
		// 关注用户组（屏蔽首页的销售环节，以及销售环节）
		}else if(roleName == 'c' || roleName == 'C'){
			if($(this).hasClass('market')){
				aLink.removeAttr('href');
			}else{
				changgeLi($(this));
			}
		// 企业用户组
		}else{
			changgeLi($(this));
		}
	});

	//点击或者鼠标经过时，添加图标和文字变大的样式class
	function changgeLi(obj){
		$this = obj;
		var spanObj = $this.find('span');
		spanObj.addClass('selected');
	}



})


//用户变化屏幕方向时调用
$(window).bind('orientationchange',function(){
	reportClick();
});

function getParam(){
	var url = window.location.href;
	var p1 = url.split('?')[0];
	var p2 = p1.split('/');
	var len = p2.length;
	return p2[len-1].replace('Link','');
}

$('.focusPage').click(function(){
	if(!$(this).hasClass('hidden')){
		$(this).addClass('hidden');
	}
});

$('.popUp').click(function(){
	return false;
});