<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>${systemConfig['webName']}</title>
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/font-awesome.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ace-fonts.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ace.css" />
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ace-part2.css" />
    <![endif]-->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ace-rtl.css" />
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ace-ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${contextPath}/static/admin/assets/js/html5shiv.js"></script>
    <script src="${contextPath}/static/admin/assets/js/respond.js"></script>
    <![endif]-->

    <!-- basic scripts -->
    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${contextPath}/static/admin/assets/js/jquery.js'>"+"<"+"/script>");
    </script>
    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${contextPath}/static/admin/assets/js/jquery1x.js'>"+"<"+"/script>");
    </script>
    <![endif]-->
    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='${contextPath}/static/admin/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
    </script>

    <script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
    <script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
    <script type="text/javascript">
   
        $(document).ready(function(){
        	//回车登录
        	 $(document).keypress(function (e) {
     	        if (e.keyCode == 13)
     	        $("#loginForm").submit();
     	     });
        	
            $("#subBtn").click(function(){
                $("#loginForm").submit();
            });

            $("#loginForm").validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },
                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },
                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                    form.submit();
                },
                invalidHandler: function (form) {
                }
            });
            $
			
        });
        function changeValidateCode(obj){
        	var timenow = new Date().getTime();
        	obj.src = $("#contextPath").val()+"/admin/captcha?"+timenow;
        }
        function aa(){
        	alert('ss');
        }
    </script>
</head>
<body class="login-layout light-login">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i><img alt="" src="${contextPath}/static/logo/logo.png"></i>
                            <span class="red">天方健</span>
                            <span class="grey" id="id-text2">中药材追溯系统</span>
                        </h1>
                        <h4 class="blue" id="id-company-text">&copy; ${systemConfig['webName']}</h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-coffee green"></i>
                                        <#if login_error_msg??>
                                    	   <font color="red">${login_error_msg! }</font>
                                    	<#else>
                                    	     请输入用户信息 
                                    	</#if>
                                    </h4>

                                    <div class="space-6"></div>

                                    <form id="loginForm" action="${contextPath}/admin/doLogin" method="post">
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="block clearfix">
                                                            <span class="block input-icon input-icon-right">
                                                                <input name="username" type="text" class="form-control required" placeholder="用户名" />
                                                                <i class="ace-icon fa fa-user"></i>
                                                            </span>
                                                </label>
                                            </div>

                                            <div class="form-group">
                                                <label class="block clearfix">
                                                            <span class="block input-icon input-icon-right">
                                                                <input name="password" type="password" class="form-control required" placeholder="密码" />
                                                                <i class="ace-icon fa fa-lock"></i>
                                                            </span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="inline">
                                                <span class="lbl">验证码:</span>
                                                </label>
                                                <input type="hidden" id="contextPath" value="${contextPath}">
                                                <span>
												<img src="${contextPath}/admin/captcha" onclick="changeValidateCode(this)" title="点击刷新">
												&nbsp;
												<input name="captcha" type="text" class="required"  value="" placeholder="验证码"/>
                                           		</span>
                                            </div>
                                            <div style="font-size: 10px;color: red; margin-bottom: 10px">点击图片刷新验证码</div>
                                            <div class="clearfix">
                                                <button type="button" class="width-100 pull-right btn btn-sm btn-primary" id="subBtn">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span class="bigger-110">登录</span>
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>

                                    <div class="space-6"></div>

                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->
                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->
</body>
</html>