<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">系统管理</a></li>
    <li class="active">操作日志</li>
</#assign>

<#assign pageCss>
    <!-- page specific plugin styles -->
    <link rel="stylesheet"
          href="${contextPath}/static/admin/assets/css/bootstrap-datetimepicker.css" />
</#assign>

<#assign pageJavascript>
    <script type="text/javascript">
	    $(document).ready(function(){
	    	// 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
	    	// 删除按钮
	        $("a.delete").on(ace.click_event, function(e){
	            var currentA = this;
	            e.preventDefault();
	            bootbox.confirm({
	                        message: "确定删除"+ $(currentA).attr('name')+"？",
	                        buttons: {
	                            confirm: {
	                                label: "确定",
	                                className: "btn-primary btn-sm"
	                            },
	                            cancel: {
	                                label: "取消",
	                                className: "btn-sm btn-cancel"
	                            }
	                        },
	                        callback: function(result) {
	                            if(result){
	                            	$.ajax({
	                        			url:$(currentA).attr("href"),
	                        			type:"post",
	                        			data:"&&time="+new Date().getTime(),
	                        			datatype:"text",
	                        			success:function(data){
	                        				if(data=='true'){
	                                            window.location.href = window.location.href;
	                        				}
	                        			}
	                        		});
	                            }
	                        	
	                        }
	                    }
	            );
	        });
	      //判断非法字符
            $("#search").click(function(){
        		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
                   if($("#operation_name").val().trim() != ""){  
                       if(pattern.test($("#operation_name").val())){  
                    	 bootbox.alert({  
                               buttons: {  
                                  ok: {  
                                       label: '确定',  
                                       className: 'btn-primary btn-sm'  
                                   }  
                               },  
                               message: '存在非法字符，请重新输入查询条件。',  
                               callback: function() {  
                            		$("#operation_name").val("");
                               },  
                           });  
                       }else {
                       	$("#search").attr("type","submit");
					 }
                  }else {
                	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
				}  
    		});
	      	
	      	//清理日志
	      	$(".batchdelete").click(function(){
	      		$.ajax({
        			url:"${contextPath}/admin/operationlog/batchdeleteview",
        			type:"post",
        			data:null,
        			datatype:"text",
        			success:function(data){
        			} 
        		});                	
	        });
	      		
	      
	      	
	      	//时间
	      	 $("input[name='endTime']").datetimepicker({
        		 format:"yyyy-mm-dd",
        		 showMeridian:true,
        		 autoclose:true,
        		 language:'zh-CN',
        		 pickDate:true,
        		 minView:2,
        		 pickTime:true,
        		 todayBtn:true
        	 }).on('change',function(ev){
        		
             	var startTime=$("input[name='startTime']").val();
           	    var endTime = $("input[name='endTime']").val();
           	    
           		var time1 = new Date(startTime.replace("-", "/").replace("-", "/"));
         	     var time2 = new Date(endTime.replace("-", "/").replace("-", "/"));
         		 
         		if(time1 > time2)
         		{
         			bootbox.alert({
         				message: "提示:开始时间 大于结束时间，请修改。",
         				buttons: {
         					ok: {
         			            label: "确定",
         			            className: "btn-primary btn-sm"
         			        }
         				}
         			});
         		}
           	    
             }); 
	    	 
	    	 $(".date-picker").datetimepicker({
	                language : 'zh-CN',
	                autoclose: true,
	                todayHighlight: true
	            });
	      
	    });
        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>

    <@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="systemFlag-operationLogFlag">

<div class="row">
    <div class="col-xs-12">
        <form id="searchForm" class="form-search form-inline" method="post">
            <input id="page" name="page" type="hidden"/>
            <label>模块名称:</label>
            <select id="module_name" name="operationLog.module_name" class="input-md">
                	<option value="">请选择</option>
                	<#list modulelist as module>
                    	<option value="${module}" <#if (module?? && modulename??) && module = modulename>selected="selected"</#if>>${module}</option>
                    </#list>
            </select>&nbsp;
            <label>操作类型:</label><input id="operation_name" type="text" name="operationLog.operation_name" class="input-sm" value="${operationLog.operation_name!}"/>&nbsp;
            <label>操作时间:</label>
            <input class="date-picker input-sm" name="startTime" value="${startTime!}"
                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />-<input class="date-picker input-sm" name="endTime" value="${endTime!}"
                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />
            <input type="button" id="search"  class="btn btn-sm btn-primary" value="查询"/>
             <div style="float: right;">
            	 
            	<#if session['adminType'] == 'true'>
                <a href="${contextPath}/admin/operationlog/batchdeleteview" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-trash-o"></i>清理日志</a>
          		</#if>
          		
          		
           	</div>
        </form>
        </br>
        <table id="contentTable" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th width="50"  align="center" style="text-align: center;"  >序号</th>
                    <th width="150" align="center" style="text-align: center;"  >模块名称</th>  
                    <!--
                    <th width="100" align="center" style="text-align: center;"  >模块编码</th>
                    <th width="150" align="center" style="text-align: center;"  >操作类型编码</th>
                    -->
                    <th width="100" align="center" style="text-align: center;"  >操作类型</th>
                    <th width="200" align="center" style="text-align: center;"  >操作对象</th>
                    <th width="100" align="center" style="text-align: center;"  >操作用户</th>
                    <th width="100" align="center" style="text-align: center;"  >操作时间</th>
                   <!-- 
                    <#if session['adminType'] == 'true'>
                    <th width="150" align="center" style="text-align: center;"  >操作</th>
                    </#if>
                    -->
                </tr>
            </thead>
			
            <tbody>
            	<#list pageList.list as operationLog>
                <tr>
                    <td>${operationLog_index+1}</td>
                    <td>${operationLog.module_name!}</td>
                    <!--
                    <td>${operationLog.module_code!}</td>
                    <td>${operationLog.operation_code!}</td>
                     -->
                    <td>${operationLog.operation_name!}</td>
                    <td>
                    	<#if operationLog.base_name?length lt 45>
	                      	 <a  title="${operationLog.base_name!}">${operationLog.base_name!}</a> 
	                        <#else>
	                         <a title="${operationLog.base_name!}">${(operationLog.base_name[0..45])?default("")}......</a> 
						</#if>
                    </td>
                    <td>${operationLog.operation_user!}</td>
                    <td>${operationLog.operation_datetime!}</td>
                    <!-- 
                    <#if session['adminType'] == 'true'>
                    <td><a class="red tooltip-error delete" href="${contextPath}/admin/operationlog/delete?ids=${operationLog.id!}" name="${operationLog.operation_name!}" data-rel="tooltip" title="删除"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></td>
               		</#if>
               		 -->
                </tr>
            </#list>
            </tbody>
        </table>
        <@pagination pageList!/>
    </div>
</div>


</@mainlayout>