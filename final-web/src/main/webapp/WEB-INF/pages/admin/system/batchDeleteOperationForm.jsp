<form id="categoryForm" class="form-horizontal" action="${contextPath}/admin/operationlog/batchdelete" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="blue">日志清理</h5>
    </div>

    <div class="modal-body">
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="category_code">操作时间:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
	                <input class="date-picker input-sm" name="startTime" value="${startTime!}"
	                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />-<input class="date-picker input-sm" name="endTime" value="${endTime!}"
	                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer center">
        <button type="button" class="btn btn-sm btn-success batchdelete"><i class="ace-icon fa fa-check"></i>清理</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
    	// 清除远程modal数据
        $("#editModal").on("hidden.bs.modal", function(){
            $(this).removeData("bs.modal");
        });
    	$(".batchdelete").click(function(){
    		
    		
    		
         bootbox.confirm({
                message: "确定清理日志？",
                buttons: {
                    confirm: {
                        label: "确定",
                        className: "btn-primary btn-sm"
                    },
                    cancel: {
                        label: "取消",
                        className: "btn-sm btn-cancel"
                    }
                },
                callback: function(result) {
                   	if(result){
                   		if($("input[name='startTime']").val()!=""&&$("input[name='endTime']").val()!=""){
        					$.ajax({
                    			url:"${contextPath}/admin/operationlog/batchdelete",
                    			type:"post",
                    			data:getdata(),
                    			datatype:"text",
                    			success:function(data){
                    				if(data=='true'){
                    					bootbox.alert({  
                    		       	           buttons: {  
                    		                         ok: {  
                    		                              label: '确定',  
                    		                              className: 'btn-primary btn-sm'  
                    		                          }  
                    		                      },  
                    		                      message: '清理成功。', 
                    		                      callback: function(result) {
                    		                    	  location.reload();
                    		                      }
                    		    		}); 
                    				}else{
                    					bootbox.alert({  
                 		       	           buttons: {  
                 		                         ok: {  
                 		                              label: '确定',  
                 		                              className: 'btn-primary btn-sm'  
                 		                          }  
                 		                      },  
                 		                      message: '清理失败。', 
                 		                      callback: function(result) {
                 		                    	  location.reload();
                 		                      }
                 		    		}); 
                    				}
                    			} 
                    		});
        				}else{
        					bootbox.alert({  
      		       	           buttons: {  
      		                         ok: {  
      		                              label: '确定',  
      		                              className: 'btn-primary btn-sm'  
      		                          }  
      		                      },  
      		                      message: '操作日期不能为空，请选择', 
      		                      callback: function(result) {
      		                      }
      		    			}); 
        				}
                   	}
                }
            }
    	  );
    	});
    	function getdata(){
    		var params="time="+new Date().getTime();
    		var startTime=$("input[name='startTime']").val();
    		var endTime=$("input[name='endTime']").val();
    		if(startTime){
    			params+="&&startTime="+startTime;
    		}
    		if(endTime){
    			params+="&&endTime="+endTime;
    		}
    		return params;
    	}
	    $("input[name='endTime']").datetimepicker({
	   		 format:"yyyy-mm-dd",
	   		 showMeridian:true,
	   		 autoclose:true,
	   		 language:'zh-CN',
	   		 pickDate:true,
	   		 minView:2,
	   		 pickTime:true,
	   		 todayBtn:true
	   	 }).on('change',function(ev){
	   		
	        	var startTime=$("input[name='startTime']").val();
	      	    var endTime = $("input[name='endTime']").val();
	      	    
	      		var time1 = new Date(startTime.replace("-", "/").replace("-", "/"));
	    	     var time2 = new Date(endTime.replace("-", "/").replace("-", "/"));
	    		 
	    		if(time1 > time2)
	    		{
	    			bootbox.alert({
	    				message: "提示:开始时间 大于结束时间，请修改。",
	    				buttons: {
	    					ok: {
	    			            label: "确定",
	    			            className: "btn-primary btn-sm"
	    			        }
	    				}
	    			});
	    		}
	      	    
	        }); 
	   	 $(".date-picker").datetimepicker({
	               language : 'zh-CN',
	               autoclose: true,
	               todayHighlight: true
	      }); 
    });
</script>
