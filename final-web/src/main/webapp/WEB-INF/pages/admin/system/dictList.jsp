<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">系统管理</li>
    <li class="active">数据字典</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
    <!-- 
    <link rel="stylesheet" href="${contextPath}/admin/assets/css/ui.jqgrid.css" />
     -->
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
        	 // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定删除"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
            
        });
		
        //判断非法字符
        $("#search").click(function(){
   		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
              if($("#dicttype").val().trim() != ""||$("#dictcode").val().trim() != ""||$("#dictname").val().trim() != ""){  
                  if(pattern.test($("#dicttype").val())||pattern.test($("#dictcode").val())||pattern.test($("#dictname").val())){  
               	 bootbox.alert({  
                          buttons: {  
                             ok: {  
                                  label: '确定',  
                                  className: 'btn-primary btn-sm'  
                              }  
                          },  
                          message: '存在非法字符，请重新输入查询条件。',  
                          callback: function() {  
                       		$("#dicttype").val("");
                       		$("#dictcode").val("");
                       		$("#dictname").val("");
                          },  
                      });  
          			 
                  }else {
                  	$("#search").attr("type","submit");
				 }
             }else {
           	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		}); 
        
        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="systemFlag-dictFlag">
<div class="row">
    <div class="col-xs-12">

			<form id="searchForm" class="form-search form-inline" method="post">
                <input id="page" name="page" type="hidden"/>
                <label>类型:</label><input type="text" id="dicttype" name="dict.dict_type" class="input-sm input-small" value="${dict.dict_type! }"/>&nbsp;
                <label>编码:</label><input type="text" id="dictcode" name="dict.dict_code" class="input-sm input-small" value="${dict.dict_code! }"/>&nbsp;
                <label>名称:</label><input type="text" id="dictname" name="dict.dict_name" class="input-sm input-small" value="${dict.dict_name! }"/>&nbsp;
    
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>
                <div style="float: right;">
                <a href="${contextPath}/admin/dict/view" class="btn btn-sm btn-warning " data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
           		</div>
            </form>
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="40" align="right" style="text-align: center;">序号</th>
     				<th width="100" align="left" style="text-align: center;">类型</th>
                    <th width="100" align="left" style="text-align: center;">编码</th>
                    <th width="100" align="left" style="text-align: center;">名称</th>
                    <th width="100" align="left" style="text-align: center;">备注</th>
                    <th width="100" align="left" style="text-align: center;">操作</th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as dict>
                    <tr>
                        <td>${dict_index+1}</td>
                        <td>${dict.dict_type!}</td>
						<td>${dict.dict_code!}</td>
                        <td>${dict.dict_name!}</td> 
                        <td>${dict.notes!}</td> 
                        <td>
                        	<div class="hidden-sm hidden-xs action-buttons">
                                <a class="green tooltip-success edit" href="${contextPath}/admin/dict/view?id=${dict.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                <a class="red tooltip-error delete" href="${contextPath}/admin/dict/delete?ids=${dict.id!}" name="${dict.dict_name!}" data-rel="tooltip" title="删除"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                            </div>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>

</@mainlayout>