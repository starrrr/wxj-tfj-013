<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">系统管理</a></li>
    <li class="active">登录日志</li>
</#assign>

<#assign pageCss>
    <!-- page specific plugin styles -->
    <link rel="stylesheet"
          href="${contextPath}/static/admin/assets/css/bootstrap-datetimepicker.css" />
</#assign>

<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	 $("input[name='endTime']").datetimepicker({
        		 format:"yyyy-mm-dd",
        		 showMeridian:true,
        		 autoclose:true,
        		 language:'zh-CN',
        		 pickDate:true,
        		 minView:2,
        		 pickTime:true,
        		 todayBtn:true
        	 }).on('change',function(ev){
        		
             	var startTime=$("input[name='startTime']").val();
           	    var endTime = $("input[name='endTime']").val();
           	    
           		var time1 = new Date(startTime.replace("-", "/").replace("-", "/"));
         	     var time2 = new Date(endTime.replace("-", "/").replace("-", "/"));
         		 
         		if(time1 > time2)
         		{
         			bootbox.alert({
         				message: "提示:登录开始时间 大于登录结束时间，请修改。",
         				buttons: {
         					ok: {
         			            label: "确定",
         			            className: "btn-primary btn-sm"
         			        }
         				}
         			});
         		}
           	    
             }); 
        	
        
        	
            $(".date-picker").datetimepicker({
                language : 'zh-CN',
                autoclose: true,
                todayHighlight: true
            });
        	  
        });
      	
        //判断非法字符
        $("#search").click(function(){
   		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
              if($("#username").val().trim() != ""){  
                if(pattern.test($("#username").val())){  
               	bootbox.alert({  
        	           buttons: {  
                          ok: {  
                               label: '确定',  
                               className: 'btn-primary btn-sm'  
                           }  
                       },  
                       message: '存在非法字符，请重新输入查询条件。', 
                       callback: function(result) {
                    	   $("#username").val("");
                       }
     			  });   
                  }else {
                  	$("#search").attr("type","submit");
				 }
             }else {
           	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		}); 

        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>

    <@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="systemFlag-loginLogFlag">

<div class="row">
    <div class="col-xs-12">
        <form id="searchForm" class="form-search form-inline" method="post">
            <input id="page" name="page" type="hidden"/>
            <label>用户名:</label><input id="username" type="text" name="user.username" class="input-sm" value="${user.username!}"/>&nbsp;
            <label>登录时间:</label>
            <input class="date-picker input-sm" name="startTime" value="${startTime!}"
                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />-<input class="date-picker input-sm" name="endTime" value="${endTime!}"
                   type="text" data-date-format="YYYY-MM-DD HH:mm:ss" />
            <input type="button" id="search"  class="btn btn-sm btn-primary" value="查询"/>
        </form>
         </br>
        <table id="contentTable" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th width="40" align="center" style="text-align: center;"  >序号</th>
                    <th width="150" align="left" style="text-align: center;"  >用户名</th>  
                    <th width="150" align="left" style="text-align: center;"  >姓名</th>
                    <th width="150" align="left" style="text-align: center;"  >手机</th>
                    <th width="150" align="left" style="text-align: center;"  >邮箱</th>
                    <th width="150" align="center" style="text-align: center;"  >登陆时间</th>
                    <th width="150" align="center" style="text-align: center;"  >退出时间</th>
                    <th width="150" align="center" style="text-align: center;"  >登陆IP</th>
                </tr>
            </thead>

            <tbody>
            <#list pageList.list as loginLog>
                <tr>
                    <td>${loginLog_index+1}</td>
                    <td>${loginLog.username!}</td>
                    <td>${loginLog.name!}</td>
                    <td>${loginLog.mobile!}</td>
                    <td>${loginLog.email!}</td>
                    <td>${loginLog.login_time!}</td>
                    <td>${loginLog.logout_time!}</td>
                    <td>${loginLog.login_ip!}</td>
                </tr>
            </#list>
            </tbody>
        </table>
        <@pagination pageList!/>
    </div>
</div>

</@mainlayout>