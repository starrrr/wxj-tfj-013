<form id="dictForm" class="form-horizontal" action="${contextPath}/admin/dict/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if dict.id??>
        <h5 class="blue">编辑数据字典</h5>
        <#else>
        <h5 class="blue">添加数据字典</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="dict.id" id="dict.id" value="${dict.id!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="dict_type"><font style="color:red;padding-right: 4px">*</font>字典类型:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="dict.dict_type" id="dict_type" class="col-xs-12 col-sm-8 required" value="${dict.dict_type!}"/>
                </div>
                
            </div>
            
        </div>

 
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="dict_code"><font style="color:red;padding-right: 4px">*</font>字典编码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="dict.dict_code" id="dict_code" class="col-xs-12 col-sm-8 required" value="${dict.dict_code!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="dict_name"><font style="color:red;padding-right: 4px">*</font>字典名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="dict.dict_name" id="dict_name" class="col-xs-12 col-sm-8 required" value="${dict.dict_name!}"/>
                </div>
            </div>
        </div>
        
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="dict_name"><font style="color:red;padding-right: 4px">*</font>备注:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="dict.notes" id="dict_name" class="col-xs-12 col-sm-8 required" value="${dict.notes!}"/>
                </div>
            </div>
        </div>

       

    </div>

    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $("#dictForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
               
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });
	 });
</script>
