<form id="rescForm" class="form-horizontal" action="${contextPath}/admin/menuresc/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if menuResc.id??>
        <h5 class="blue">编辑菜单资源</h5>
        <#else>
        <h5 class="blue">添加菜单资源</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="menuResc.id" id="id" value="${menuResc.id!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="pid">上级资源:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="hidden" name="menuResc.pid" id="pid" class="col-xs-12 col-sm-4" value="${menuResc.pid!}" readonly/>
                    <input type="text" id="parentName" class="col-xs-12 col-sm-4" value="${menuResc.pname!}" readonly/>
                    <div id="treeContent"style="display:none; position: absolute;z-index: 9999;">
                        <ul id="rescTree" class="ztree" style="margin-top:0;"></ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"><font style="color:red;padding-right: 4px">*</font>名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="menuResc.name" id="name" class="col-xs-12 col-sm-4 required" value="${menuResc.name!}"/>
                </div>
            </div>
        </div>
 		<div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="permission">资源路径:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="menuResc.url" id="url" class="col-xs-12 col-sm-4" value="${menuResc.url!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="permission">权限名:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="menuResc.permission" id="permission" class="col-xs-12 col-sm-4" value="${menuResc.permission!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="sort"><font style="color:red;padding-right: 4px">*</font>排序:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text"  name="menuResc.sort" id="sort" class="col-xs-12 col-sm-4 required" value="${menuResc.sort!}" min="0"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">类型:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <select class="input-medium" id="type" name="menuResc.type">
                        <option value="0" <#if menuResc.type?? && menuResc.type = '0'>selected="selected"</#if>>菜单</option>
                        <option value="1" <#if menuResc.type?? && menuResc.type = '1'>selected="selected"</#if>>功能</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">目标:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <select class="input-medium" id="target" name="menuResc.target">
                        <option value="" <#if !menuResc.target?? || menuResc.target = ''>selected="selected"</#if>>当前</option>
                        <option value="_blank" <#if menuResc.target?? && menuResc.target = '_blank'>selected="selected"</#if>>新窗口</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">状态:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <label class="radio-inline blue"><input name="menuResc.enable" value="0" type="radio" class="ace" <#if !menuResc.enable?? || (menuResc.enable?? && menuResc.enable = '0')>checked="checked"</#if>/><span class="lbl">启用</span></label>
                    <label class="radio-inline blue"><input name="menuResc.enable" value="1" type="radio" class="ace" <#if menuResc.enable?? && menuResc.enable = '1'>checked="checked"</#if>/><span class="lbl">禁用</span></label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="permission">图标:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="menuResc.icon" id="icon" class="col-xs-12 col-sm-8" value="${menuResc.icon!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="remark">备注:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <textarea name="menuResc.remark" maxlength="90" id="remark" class="col-xs-12 col-sm-8" placeholder="资源备注">${menuResc.remark!}</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> 保存</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> 取消</button>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#sort').ace_spinner({min:0,max:99,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})

        $("#rescForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });

        // 菜单树
        var setting = {
            view: {
                dblClickExpand: false,
                showIcon: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onClick: onClick
            }
        };

        $.post("${contextPath}/admin/menuresc/getMenuTree", function(data){
            $.fn.zTree.init($("#rescTree"), setting, JSON.parse(data));
        });

        $("#parentName").focus(function(){
            var parent = $("#parentName");
//            var parentOffset = $("#pid").offset();
//            $("#treeContent").css({left:parentOffset.left + "px", top:parentOffset.top + parent.outerHeight() + "px"}).slideDown("fast");
            $("#treeContent").css({left:"12px", top:parent.outerHeight() + "px"}).slideDown("fast");

            if(!jQuery.isEmptyObject($("#pid").val())){
                var zTree = $.fn.zTree.getZTreeObj("rescTree");
                var selectNode = zTree.getNodeByParam("id", $("#pid").val());
                zTree.selectNode(selectNode, true);
            }

            $("#rescForm").bind("mousedown", onBodyDown);
        }); 


    });

    function onClick(e, treeId, treeNode){
        $("#pid").val(treeNode.id);
        $("#parentName").val(treeNode.name);
        hideMenu();
    }
    
    function hideMenu() {
        $("#treeContent").fadeOut("fast");
        $("#rescForm").unbind("mousedown", onBodyDown);
    }
    function onBodyDown(event) {
        if (!(event.target.id == "parentName" || event.target.id == "treeContent" || $(event.target).parents("#treeContent").length>0)) {
            hideMenu();
        }
    } 
</script>
<style type="text/css">
    .ztree{
        margin-top: 10px;
        border: 1px solid #617775;
        background: #f0f6e4;
        width: 150px;
        height: 300px;
        overflow-y: scroll;
        overflow-x: auto;
    }
</style>
