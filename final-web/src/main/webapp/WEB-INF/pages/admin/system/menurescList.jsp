<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">系统管理</li>
    <li class="active">菜单资源</li>
</#assign>

<#assign pageCss>
	<!--
    <link href="${contextPath}/static/admin/assets/treeTable/themes/vsStyle/treeTable.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
	  -->
</#assign>

<#assign pageJavascript>
    <script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
    <!--
    <script src="${contextPath}/static/admin/assets/treeTable/jquery.treeTable.min.js" type="text/javascript"></script>
     -->
    <script type="text/javascript">
        $(document).ready(function(){
           
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
            
          
           
        });
    </script>
</#assign>

<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="systemFlag-menurescFlag">
    <div class="row">
        <div class="widget-box widget-color-blue">
            <div class="widget-header widget-header-small">
                <h5 class="widget-title">菜单资源列表</h5>
                
                <div class="widget-toolbar">
                	<#if session['adminType'] == 'true'>
                    <a href="${contextPath}/admin/menuresc/view" data-rel="tooltip" title="添加" data-toggle="modal" data-target="#editModal"> <i class="ace-icon fa fa-plus-circle white">添加</i></a>
                    </#if>
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i></a>
                </div>
                
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <table id="contentTable" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="120" style="text-align: center;">资源名称</th>
                            <th width="60" style="text-align: center;">资源路径</th>
                            <th width="50" style="text-align: center;">权限名</th>
                            <th width="50" style="text-align: center;">资源类型</th>
                            <!--  
                            <th width="40">资源目标</th>
                            -->
                            <th width="40" style="text-align: center;">排序</th>
                            <th width="40" style="text-align: center;">状态</th>
                            <th width="60" style="text-align: center;">更新时间</th>
                            <th width="40" style="text-align: center;">操作</th>
                            <!--  
                            <th width="100">备注</th>
                             -->
                        </tr>
                        </thead>
                        <tbody>
                        <#list rescList as resc>
                            <tr id="${resc.id}" <#if resc.pid??>pId="${resc.pid}"</#if>>
                                <td><i class="menu-icon fa ${resc.icon!}"></i>${resc.name!}</td>
                                <td>${resc.url!}</td>
                                <td>${resc.permission!}</td>
                                <td>${resc.type!}</td>
                                 <!-- 
                                <td>${resc.target!}</td>
                                 -->
                                <td>${resc.sort!}</td>
                                <td><#if resc.enable == '0'> <div>启用</div> <#else> <div style="color:red">停用</div></#if></td>
                                <td>${resc.update_time?string("yyyy-MM-dd HH:mm:ss")!}</td>
                                <td>
                                	<#if session['adminType'] == 'true'>
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green tooltip-success edit" href="${contextPath}/admin/menuresc/view?id=${resc.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                    </div>
                                    </#if>
                                </td>
                                <!--
                                <td>${resc.remark!}</td>
                                -->
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--编辑对话框-->
    <div id="editModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
</@mainlayout>