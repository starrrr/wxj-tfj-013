<#include "../common/__pagination.jsp">

<form id="productForm" class="form-horizontal" action="#" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="blue">产品列表</h5>
    </div>


    <div class="modal-body">
        
        	 <input id="page" name="page" type="hidden"/>
                
             <label>产品名称:</label>
             <input type="text" name="product_name" class="input-sm input-large" value="${productName!}" style="width:190px;margin-bottom:10px;"/>&nbsp;
            
             <input type="button" class="btn btn-sm btn-primary" onclick="findPvPage()" value="查询"/>&nbsp;
              
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
               		<th width="30" align="right" ><input type="checkbox" onclick="selectAll(this)"/></th>
                    <th width="200" align="left" >产品名称</th>
                    <th width="200" align="left" >种子批号</th>
                    <th width="200" align="left" >原料批号</th>
                    <th width="200" align="left" >成品批号</th>
                </tr>
                </thead>

                <tbody>
               	<#list pageList.list as productResult>
                    <tr>
                        <td>
                    		<input name="subBox" value="${productResult.id!}" type="checkbox"/>
                    	</td>
						<td>${productResult.product_name!}</td>
                        <td>${productResult.seed_batch_num!}</td> 
                        <td>${productResult.material_batch_num!}</td>
						<td>${productResult.product_batch_num!}</td>
						<!-- 
                        <td>${productResult.flow_batch_num!}</td>
                        -->
                    </tr>
            	 </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
    </div>
    
   	<div class="modal-footer" style="margin-top:10px;">
        <button type="button" class="btn btn-sm btn-success" onclick="fillProductValue()"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" id="cancel-dialog" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>

</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	
    	if($("#isaudit").val()==1||$("#isaudit").val()=='1'){
    		$(".isaudit").attr("disabled","disabled");
    	}
    	
    	 $("#category_id").change(function(){
     		$("#product_name").attr("value",$("#category_id").find("option:selected").text());
     	});

        $("#productForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
            	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });
        
        $("#category_id").change(function(){
        	$("#category_name").val($(this).find("option:selected").text());
        });
    });
</script>
