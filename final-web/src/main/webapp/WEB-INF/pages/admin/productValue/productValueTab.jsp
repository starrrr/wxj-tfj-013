<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageCss>
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/chosen.css" />
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/datepicker.css">
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.custom.css" />
<link rel="stylesheet" href="${contextPath}/static/admin/pv/css/allLink.css" type="text/css">
<link rel="stylesheet" href="${contextPath}/static/admin/pv/css/public.css" type="text/css">
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
</#assign>

<#assign pageBreadCrumbs>
<li>产品管理</li>
<li class="active">产品数据录入</li>
</#assign> 

<#assign pageJavascript>
    <script src="${contextPath}/static/admin/assets/js/fileupload/ajaxfileupload.js"></script>
    <script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
	<script src="${contextPath}/static/admin/pv/js/allLink.js"></script>
	<script type="text/javascript">
	var P1_NAME = "PLANT_PROCESS";
	var P2_NAME = "SUPERVISE_PROCESS";
	var P3_NAME = "WAREHOUSE_PROCESS";
	var P4_NAME = "SALE_PROCESS";
	
	var P1_Container = "PlantContainer";
	var P2_Container = "SuperviseContainer";
	var P3_Container = "WarehouseContainer";
	var P4_Container = "SaleContainer";
	
	var l_proces_type = '${Locate_Process_Type}';
	var l_template_name = '${Locate_Template_Name}';
	var Image_Pattern = '${Image_Pattern}';
	
	var orgWidgets;
	var inited;
	
			$(document).ready(function(){
				
				
				window.tfj_Full_ContextPath = '${Full_Context_Path}';
				window.tfj_ContextPath = '${contextPath}';
				window.tfj_pv_ProcessType = P1_NAME;
				window.tfj_pv_Container = P1_Container;
				
	            // 清除远程modal数据
	            $("#editModal").on("hidden.bs.modal", function(){
	                $(this).removeData("bs.modal");
	            });
				
				//初始化页面 tab不允许 点击 
				$("#plantProcessMenu").attr("href", "#plantProcess");
				$("#superviseMenu").attr("href", "#superviseProcess");
				$("#wareHouseMenu").attr("href", "#wareHouseProcess");
				$("#saleMenu").attr("href","#saleProcess");
				
				// 种植环节 
				$("#plantProcessMenu").bind("click", function(){
					 var category_id = ${session["category_id"]};
					 loadPageData(P1_NAME, P1_Container, "plantTree", category_id);
				});
				
				// 加工环节 
				$("#superviseMenu").bind("click", function(event){
					var category_id = ${session["category_id"]};
					loadPageData(P2_NAME, P2_Container, "superviseTree", category_id);
					
				});
				
				// 仓管流通环节 
				$("#wareHouseMenu").bind("click", function(){
					var category_id = ${session["category_id"]};
					loadPageData(P3_NAME, P3_Container, "wareHouseTree", category_id);
				});
				
				// 销售环节 
				$("#saleMenu").bind("click", function(){
					var category_id = ${session["category_id"]};
					loadPageData(P4_NAME, P4_Container, "saleTree", category_id);
				});
				
				jQuery(function($){
					$('.dd-handle a').on('mousedown', function(e){
						// e.stopPropagation();
					});
					
					$('[data-rel="tooltip"]').tooltip();
				});
				
				//var l_proces_type = '${Locate_Process_Type}';
				//var l_template_name = '${Locate_Template_Name}';
				reflushTree(l_proces_type);
	        });
	</script>
</#assign>


<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-productValueFlag">
             <input type="hidden" class="pv_product_id" value="${product.id!}"/>
             <input type="hidden" class="pv_category_id" value="${product.category_id!}"/>
             <table id="contentTable" class="table table-striped table-bordered">
                <thead>
                <tr>
                	<!-- 
                    <th width="40" align="right" >ID</th>
                     -->
                    <th width="150" align="left" style="text-align: center;">名称</th>
                    <th width="150" align="left" style="text-align: center;">种类名称</th>
                    <th width="150" align="left" style="text-align: center;">种植方式</th>
                    <th width="150" align="left" style="text-align: center;">等级</th>
                    <th width="150" align="left" style="text-align: center;">年份</th>
                    <th width="150" align="left" style="text-align: center;">基地</th>
                    <th width="150" align="left" style="text-align: center;">审核状态</th>
                    <#if session['adminType'] == 'true'>
                    <th width="200" align="center" style="text-align: center;">操作</th>
                    </#if>
                </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>${product.product_name!}</td>
                        <td>${product.category_name!}</td>
                        <td>${product.plant_type!}</td>
                        <td>${product.product_level!}</td>
                        <td>${product.product_year!}</td>
                        <td>${product.plantbase_name!}</td>
                        <td id="product_auditflagName">
                        	${product.auditflagName!}
                        </td>
                        <#if session['adminType'] == 'true'>
                        <td align="center">
                        <input type="hidden" class="product_auditflag" value="${product.auditflag!}"/>
                      	
	                        <#if (product.auditflag?? && product.auditflag = 1)>
	                        	<a style="size: 10px;" class="btn btn-sm btn-primary red tooltip-success" href="#" name="CheckProductValue" onclick="finishEntry(${product.id!}, ${product.auditflag!})"  data-rel="tooltip" ><font style="font-weight:bold;">√</font>&nbsp;完成</a>
	                        <#else><!-- 为了释放值所用，注意 finishEntry(id, auditflag) 中参数auditflag 为 1 -->
	                            <a style="display: none;" class="btn btn-sm btn-primary red tooltip-success finish" href="#" name="CheckProductValue" onclick="finishEntry(${product.id!}, 1)"  data-rel="tooltip" ><font style="font-weight:bold;">√</font>&nbsp;完成</a>
	                        </#if>
                        </td>
                        </#if>
                    </tr>
                </tbody>
            </table>

<div class="row">
	<div class="col-xs-12">
		<div >
			<div >
				<div class="tabbable">
					<ul class="nav nav-tabs padding-18">
						<li class="active">
							<a data-toggle="tab" href="#plantProcess" id="plantProcessMenu"> <i class="green ace-icon fa fa-leaf bigger-120"></i> 种植环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#superviseProcess" id="superviseMenu"> <i class="orange ace-icon fa fa-rss bigger-120"></i> 加工环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#wareHouseProcess" id="wareHouseMenu"> <i class="blue ace-icon fa fa-users bigger-120"></i> 流通环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#saleProcess" id="saleMenu"> <i class="pink ace-icon fa fa-picture-o bigger-120"></i> 销售环节</a></li>
					</ul>

					<div class="tab-content no-border padding-24">

						<div id="plantProcess" class="tab-pane tab-pane in active">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">种植环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="plantTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								     <!--
								     <button type="button" name="release" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>释放</button>
								       -->
								</div>
								
								<#include "./plantInput.jsp">
							</div>
						</div>

						<div id="superviseProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">加工环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="superviseTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								<#include "./superviseInput.jsp">
							
							</div>
						</div>

						<div id="wareHouseProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">流通环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="wareHouseTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								
								<#include "./warehouseInput.jsp">
							</div>
						</div>

						<div id="saleProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">销售环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="saleTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
							<#include "./saleInput.jsp">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div>
</div>

<div id="editModal" class="modal fade" style="overflow-y:hidden;">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div>

<p>
  <span style="background-color: #fafafa;"><!-- 右键菜单div -->
    <div id="rMenu" style="position:absolute; display:none;">
	  <li>
	    <ul id="m_add" onclick="addPrivilege();"><li>增加</li></ul>
		<ul id="m_del" onclick="delPrivilege();"><li>删除</li></ul>
		<ul id="m_del" onclick="editPrivilege();"><li>编辑</li></ul>
		<ul id="m_del" onclick="queryPrivilege();"><li>查看</li></ul>
	  </li>
	</div>
  </span>
</p>

<!-- 动画加载 -->
<div class="loading">
	<img src="${contextPath}/static/admin/assets/img/loading_blue.GIF">
	<span class="textTip"></span>
</div>

</@mainlayout>
