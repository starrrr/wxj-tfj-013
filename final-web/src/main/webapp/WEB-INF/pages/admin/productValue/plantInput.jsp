<div class="col-xs-8 col-sm-8 widget-color-green">
	<div class="wrap widget-header widget-header-small">
		<#if (product.auditflag?? && product.auditflag != 2)>
		<@shiro.hasPermission name="product_admin_plantAudit">
		<button type="button" name="auditProductValue" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>审核</button>
		</@shiro.hasPermission>   
		<button type="button" name="saveProductValue" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>保存</button>
		<#else><!-- 为了释放值所用-->
		<@shiro.hasPermission name="product_admin_plantAudit">
		<button style="display: none;" type="button" name="auditProductValue" class="btn btn-sm btn-success plantInputAudit"><i class="ace-icon fa fa-check"></i>审核</button>
		</@shiro.hasPermission>   
		<button style="display: none;" type="button" name="saveProductValue" class="btn btn-sm btn-success plantInputSave"><i class="ace-icon fa fa-check"></i>保存</button>
		
		</#if>
	</div>
	
	
	<div id="PlantContainer" class="container borderStyle">
	</div>
</div>