<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageCss>
<style>
	body{font-size: 12px;}
	body,p,div{ padding: 0; margin: 0;}
	.wraper{ padding: 30px 0;}
	.btn-wraper{ text-align: center;}
	.btn-wraper input{ margin: 0 10px;}
	#file-list{ width: 350px; margin: 20px auto;}
	#file-list li{ margin-bottom: 10px;}
	.file-name{ line-height: 30px;}
	.progress{ height: 4px; font-size: 0; line-height: 4px; background: orange; width: 0;}
	.tip1{text-align: center; font-size:14px; padding-top:10px;}
    .tip2{text-align: center; font-size:12px; padding-top:10px; color:#b00}
</style>
</#assign>

<#assign pageJavascript>
	<script src="${contextPath}/static/admin/pv/plupload/js/plupload.full.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		var uploader = new plupload.Uploader({ //实例化一个plupload上传对象
			browse_button : 'browse',
			url : '${contextPath}/admin/productValue/uploadDemo',
			flash_swf_url : '${contextPath}/static/admin/pv/plupload/js/plupload.flash.swf',
			silverlight_xap_url : '${contextPath}/static/admin/pv/plupload/js/plupload.silverlight.xap',
			multi_selection : false,
		});
		uploader.init(); //初始化

		//绑定文件添加进队列事件
		uploader.bind('FilesAdded',function(uploader, files){
			// allPrpos(uploader.total);
			console.log("File Size: >>>" + uploader.total.queued);
			
			var file_name = files[0].name; //文件名
			//构造html来更新UI
			var html = '<li id="file-' + files[0].id +'"><p class="file-name">' + file_name + '</p><p class="progress"></p></li>';
			$('#file-list').html(html);
			
			
			 $.each(uploader.files, function (i, file) {
				 if (uploader.files.length <= 1) {
					 return;
			     }
				 uploader.removeFile(file);
			 });
		});
		 
		//绑定文件上传进度事件
		uploader.bind('UploadProgress',function(uploader,file){
			$('#file-'+file.id+' .progress').css('width',file.percent + '%');//控制进度条
		});

		//上传按钮
		$('#upload-btn').click(function(){
			uploader.start(); //开始上传
		});
		
		function allPrpos ( obj ) { 
			  // 用来保存所有的属性名称和值 
			  var props = "" ; 
			  // 开始遍历 
			  for ( var p in obj ){ // 方法 
			  if ( typeof ( obj [ p ]) == " function " ){ obj [ p ]() ; 
			  } else { // p 为属性名称，obj[p]为对应属性的值 
			  props += p + " = " + obj [ p ] + " /t " ; 
			  } } // 最后显示所有的属性 
			  console.log( props ) ;
		}
	});
	</script>
</#assign>

<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-productValueFlag">
	<p class="tip2">注意：该demo把上传的地址设为了一个静态的html页面，所以文件并不会真正的上传到服务器，但这不会影响上传功能的演示！</p>
	<div class="wraper">
		<div class="btn-wraper">
			<input type="button" value="选择文件..." id="browse" />
			<input type="button" value="开始上传" id="upload-btn" />
		</div>
		<ul id="file-list">

		</ul>
	</div>
</@mainlayout>