<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</a>
    <li class="active">二维码生成</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
    <!-- 
    <link rel="stylesheet" href="${contextPath}/admin/assets/css/ui.jqgrid.css" />
     -->
</#assign>


<#assign pageJavascript>
	<script src="${contextPath}/static/admin/productresult/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        	// 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
           
            initSort("${sort_name}", "${sort_type}");
            
        });

        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-qrCodeFlag">
<div class="row">
    <div class="col-xs-12">
			<form id="sortForm" method="post"></form>
			<form id="searchForm" class="form-search form-inline" method="post">
                <input id="page" name="page" type="hidden"/>
                <input id="page " name="pagesort" type="hidden" value="${page! }"/><!--排序时需要停留在当前页面  -->
                <input id="tag" name="tag" type="hidden" value="${tag! }"/>
                <label>中药种类:</label>
                 <select class="input-medium" id="type" name="productResult.category_id">
                	  	<option value="">---请选择---</option>
                    <#list categoryList as category>
                        <option value="${category.id}" <#if (category.id?? && productResult.category_id??) && category.id=productResult.category_id>selected="selected"</#if>>${category.category_name!}</option>
                    </#list>
                </select>
                
                &nbsp;
                <label>产品名称:</label><input type="text" name="product_name" class="input-sm input-large" value="${productName! }"/>&nbsp;
                <label>是否存在二维码:</label>
                <select class="input-medium" id="type" name="flow_batch_flag">
                	  	<option value="">---请选择---</option>
                   		<option value="0" <#if flowBatchFlag?? && flowBatchFlag='0' >selected="selected"</#if>>否</option>
                        <option value="1" <#if flowBatchFlag?? && flowBatchFlag='1' >selected="selected"</#if>>是</option>
                   		
                 </select>
    
                <input type="submit" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
            </form>
           </br>
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>

     				<th width="150" align="left" style="text-align: center;"  >
     					<a href="#" class="sort-column" name="category_name"><span class="ace-icon fa fa-sort fa-lg bigger-200"></span></a>中草药种类
     				</th>
                    <th width="150" align="left" style="text-align: center;"  >
                   		<a href="#" class="sort-column" name="product_name"><span class="ace-icon fa fa-sort fa-lg bigger-200"></span></a>产品名称
                    </th>
                    <th width="220" align="left" style="text-align: center;"  >种子批号</th>
                    <th width="220" align="left" style="text-align: center;"  >原料批号</th>
                    <th width="250" align="left" style="text-align: center;"  >成品批号</th>
                    <th width="250" align="left" style="text-align: center;"  >
                    	<a href="#" class="sort-column" name="flow_batch_num"><span class="ace-icon fa fa-sort fa-lg bigger-200"></span></a>流通追溯码
                    </th>
                    <th width="100" align="left" style="text-align: center;"  >操作</th>
                </tr>
                </thead>

                <tbody>
               	<#list pageList.list as productResult>
                    <tr>
                        <td>${productResult.category_name!}</td>
						<td>${productResult.product_name!}</td>
                        <td>${productResult.seed_batch_num!}</td> 
                        <td>${productResult.material_batch_num!}</td>
						<td>${productResult.product_batch_num!}</td>
                        <td>${productResult.flow_batch_num!}</td>
                        <td>
                        	<div class="hidden-sm hidden-xs action-buttons">
                        		<#if productResult.flow_batch_num?? && productResult.flow_batch_num != ''>
                                <a class="green tooltip-success edit" href="${contextPath}/admin/productResult/showQRcode?flow_batch_num=${productResult.flow_batch_num!}" data-rel="tooltip" title="二维码" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-key bigger-130"></i>二维码</a>
                               </#if>
                            </div>
                        </td>
                    </tr>
            	 </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>

</@mainlayout>