<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<form id="dictForm" class="form-horizontal" action="" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="blue">二维码打印</h5>
    </div>
    <div class="modal-body" >
         <div  class="center">
         	<div class="center"  id="showQRcode">
            <img  height="432" width="270" src="${url}"/>
            </div>
            <input type="hidden" value="${smallurl}" id="smallurl">
		    <div class="modal-footer center">
		        <button id="btn" type="button" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>打印</button>
		    	<!-- #section:pages/gallery -->
				<a href="" class="fancybox btn btn-sm" data-target="#editModal" style="background-color:#32B16C!important;border-color:#32B16C;" title="" role="button">
					全屏显示
				</a>
				
				<a href="${contextPath}/admin/productResult/doloadImage?smallurl=${smallurl}" class="btn download btn-sm" style="background-color:#32B16C!important;border-color:#32B16C;" role="button">
					二维码保存
				</a>
				  
				  <!--
				  <a href="" class="btn download btn-sm" style="background-color:#32B16C!important;border-color:#32B16C;" role="button">
					二维码保存
				</a>
				-->
				<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
		    </div>
	    </div>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery-migrate-1.2.1.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.jqprint-0.3.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="${contextPath}/static/admin/assets/js/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/assets/css/jquery.fancybox.css" media="screen" />


<script type="text/javascript">
   $(document).ready(function(){
	   
	// 清除远程modal数据
       $("#showQRcode").on("hidden.bs.modal", function(){
           $(this).removeData("bs.modal");
       });
	   var windowHeight = $(window).height();
	   //var htmlstr="<img height="+windowHeight+"' width='"+windowHeight+"' src='${smallurl}'/>";
	   $(".fancybox").fancybox({
			openEffect  : 'none',
			closeEffect	: 'none',
			scrolling :'no',
			href :'${contextPath}/admin/productResult/FullScreenQRcode?smallurl='+$("#smallurl").val(),
			width:windowHeight-(windowHeight*0.10),
			height:windowHeight-(windowHeight*0.10),
			//width:windowHeight,
			//height:windowHeight,
			autoScale:true,
			type:'iframe',
			//禁止点击背景关闭
			/* closeClick  : false, // prevents closing when clicking INSIDE fancybox
		    helpers     : { 
		        overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
		    }, */
			
			afterClose:function(){
			     //window.location.href=window.location.href;
			     window.location.href="${contextPath}/admin/productResult";
			},
		});
	    $("#dictForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
               
            },
            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });
        $("#btn").click(function(){
        	 $("#showQRcode").jqprint();
        });
        
        /* //图片保存可能出现异常找不到，用ajax返回异常信息
        $(".download").click(function(){
    		if($("#oldpassword").val()!=''){
    			$.ajax({
        			url:"${contextPath}/admin/productResult/doloadImage",
        			type:"post",
        			data:getdata(),
        			datatype:"text",
        			success:function(data){
        				alert(data);
        				if(data=='true'){
        					alert("图片不存在");
        				}else{
        					
        				}
        			} 
        		});
    		}
    	}); 
        
        function getdata(){
    		var params="time="+new Date().getTime();
    		var smallurl=$("#smallurl").val();
    		if(smallurl){
    			params+="&&smallurl="+smallurl;
    		}
    		return params;
    	}
         */
    });
</script>

<div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
<!-- 
<style>
body {TEXT-ALIGN: center;}
#center { MARGIN-RIGHT: auto; MARGIN-LEFT: auto; }
</style>
 -->