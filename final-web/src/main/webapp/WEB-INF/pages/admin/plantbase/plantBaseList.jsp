<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">基地建设</li>
</#assign>
<#assign pageCss>
	
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
   	<!-- 
    <link rel="stylesheet" href="${contextPath}/admin/assets/css/ui.jqgrid.css" />
     -->
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
        	 // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定删除"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                //if(result) window.location.href = $(currentA).attr("href");
                                if(result){
                                	$.ajax({
                            			url:$(currentA).attr("href"),
                            			type:"post",
                            			data:"&&time="+new Date().getTime(),
                            			datatype:"text",
                            			success:function(data){
                            				if(data=='true'){
                            					bootbox.alert({  
                                                    buttons: {  
                                                       ok: {  
                                                            label: '确定',  
                                                            className: 'btn-primary btn-sm'  
                                                        }  
                                                    },  
                                                    message: '该基地存在关联，不能删除',  
                                                    callback: function() {  
                                                    	window.location.href=window.location.href;
                                                    },  
                                                });  
                            				}else{
                            					window.location.href=window.location.href;
                            				}
                            			}
                            		});
                                }
                            	
                            }
                        }
                );
            });
        	
            // 释放按钮
            $("a.release").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定释放"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "释放",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
        	 
        	 
         // 审核按钮
            $("a.audit").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
                //href="${contextPath}/admin/plantbase/audit?rid=${plantBase.id!}" 
                $.ajax({
        			url:"${contextPath}/admin/plantbase/checkMap",
        			type:"post",
        			data:$(currentA).attr("href").split("?")[1]+"&&time="+new Date().getTime(),
        			datatype:"text",
        			success:function(data){
        				if(data=='true'){
	        				bootbox.confirm({
	                               message: "审核后数据不能修改，确定审核"+$(currentA).attr('name')+"？",
	                               buttons: {
	                                   confirm: {
	                                       label: "审核",
	                                       className: "btn-primary btn-sm"
	                                   },
	                                   cancel: {
	                                       label: "取消",
	                                       className: "btn-sm btn-cancel"
	                                   }
	                               },
	                               callback: function(result) {
	                                   if(result) window.location.href = $(currentA).attr("href");
	                               }
	                           }
	                    	); 
        				}else{
        					bootbox.alert({  
        				           buttons: {  
        			                  ok: {  
        			                       label: '确定',  
        			                       className: 'btn-primary btn-sm'  
        			                   }  
        			               },  
        			               message: "该基地还没有上传图片，请先上传图片再审核",    
        				          
        			             callback: function(result) {
        			                
        			             }
        					 });    
        				}
        			} 
        		});
            });    
        });
        
        
      //判断非法字符
        $("#search").click(function(){
    		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
               if($("#plantcode").val().trim() != ""||$("#plantname").val().trim() != ""||$("#plantlocation").val().trim() != ""){  
                   if(pattern.test($("#plantcode").val())||pattern.test($("#plantname").val())||pattern.test($("#plantlocation").val())){  
                	 bootbox.alert({  
                           buttons: {  
                              ok: {  
                                   label: '确定',  
                                   className: 'btn-primary btn-sm'  
                               }  
                           },  
                           message: '存在非法字符，请重新输入查询条件。',  
                           callback: function() {  
                        		$("#plantcode").val("");
                        		$("#plantname").val("");
                        		$("#plantlocation").val("");
                           },  
                       });  
           			 
                   }else {
                   	$("#search").attr("type","submit");
				 }
              }else {
            	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		});
        

        function page(pageNum) {
            $("#page").val(pageNum);
            $("#plantBaseSearchForm").submit();
        }
        
        //全选功能  
		function selectAll(obj){
			var arrayList=$("input[name='subBox']");
			for(var i=0;i<arrayList.length;i++){
				if(arrayList[i].disabled!=true){
					arrayList[i].checked=obj.checked;
				}
			}
		}
		
        function audit(){
     		var uids = '';
    	 	var flag = false;
    		$("input[name='subBox']:checked").each(function(){
    				if(flag){
    					uids = uids+ "," + $(this).val() ;
    				}else{
    					uids = uids + $(this).val();
    				}
    				flag = true;
    		});
    		if(uids!=''){
    			 $.ajax({
         			url:"${contextPath}/admin/plantbase/checkMap",
         			type:"post",
         			data:"rid="+uids+"&&time="+new Date().getTime(),
         			datatype:"text",
         			success:function(data){
         				if(data=='true'){
         					bootbox.confirm({
         	                     message: "审核后数据不能修改，确定审核？",
         	                     buttons: {
         	                         confirm: {
         	                             label: "审核",
         	                             className: "btn-primary btn-sm"
         	                         },
         	                         cancel: {
         	                             label: "取消",
         	                             className: "btn-sm btn-cancel"
         	                         }
         	                     },
         	                     callback: function(result) {
         	                    	if(result){
         	                    		var url = "${contextPath}/admin/plantbase/audit";
         	                 			var form = $("<form></form>");
         	                 			form.attr('action',url);
         	                 			form.attr('method','post');
         	                 			var param = $("<input type='hidden' name='rid' value='"+uids+"'/>");
         	                 			form.append(param);
         	                 			form.submit();
         	                    	}
         	                     }
         	               	});
        					}else{
        						bootbox.alert({  
         				           buttons: {  
         			                  ok: {  
         			                       label: '确定',  
         			                       className: 'btn-primary btn-sm'  
         			                   }  
         			               },  
         			               message: "【"+data+"】还没有上传图片，请先上传图片再审核",    
         				          
         			             callback: function(result) {
         			                
         			             }
         					 });   
        					}
         				}
         			});
    		}else{
    			  bootbox.alert({
                      message: "请选择未审核的基地。",
                      buttons: {
       	                ok: {
       	                    label: "确定",
       	                    className: "btn-primary btn-sm"
       	                }
       	            }
    			  });    		  
    			
    		}
        }
        
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-planBaseFlag">
<div class="row">
    <div class="col-xs-12">
			<form id="plantBaseSearchForm" class="form-search form-inline" method="post">
               <input id="page" name="page" type="hidden"/>
               
               <label>中草药名称:</label>
                 <select class="input-medium" id="type" name="plantBase.category_id">
                	  	<option value="">---请选择---</option>
                    <#list categoryList as category>
                         <option value="${category.id}" <#if (category.id?? && pc_id??) && category.id=pc_id>selected="selected"</#if>>${category.category_name!}</option>
                    </#list>
                </select>
                <label>审核状态:</label>
                <select class="input-medium" id="type" name="plantBase.auditflag">
                	  	<option value="">---请选择---</option>
                   		<option value="0" <#if plantBase.auditflag?? && plantBase.auditflag = 0>selected="selected"</#if>>未审核</option>
                        <option value="1" <#if plantBase.auditflag?? && plantBase.auditflag = 1>selected="selected"</#if>>已审核</option>
                 </select>
                
                <label>基地编码:</label><input type="text" id="plantcode" name="plantBase.base_code" class="input-sm input-small" value="${plantBase.base_code! }"/>&nbsp;
                <label>基地名称:</label><input type="text" id="plantname" name="plantBase.base_name" class="input-sm input-small" value="${plantBase.base_name! }"/>&nbsp;
                <label>基地地点:</label><input type="text" id="plantlocation" name="plantBase.base_location" class="input-sm input-small" value="${plantBase.base_location! }"/>&nbsp;
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
                <div style="float: right;">
                <a href="${contextPath}/admin/plantbase/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
                <@shiro.hasPermission name="admin_plantbase_audit">
           		<a href="javascript:audit();" class="btn btn-sm btn-warning"><i class="ace-icon fa fa-key"></i>审核</a>
           		</@shiro.hasPermission>
           		</div>
            </form>
            
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
					<th width="30" align="right" ><input  type="checkbox" onclick="selectAll(this)"/> </th>
					<th width="60" align="right" style="text-align: center;" >序号</th>
                    <th width="100" align="left" style="text-align: center;" >基地编码</th>
                    <th width="150" align="left" style="text-align: center;" >基地名称</th>
                    <th width="180" align="left" style="text-align: center;" >基地地点</th>
                     <th width="100" align="left" style="text-align: center;" >基地面积</th>
                      <th width="100" align="left" style="text-align: center;" >基地海拔</th>
                    <th width="200" align="left" style="text-align: center;" >中草药名称</th>
                    <th width="75" align="left" style="text-align: center;" >审核状态</th>
                    <th width="100" align="left" style="text-align: center;" >操作</th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as plantBase>
                    <tr>
                    	<td>
                    		<#if (plantBase.auditflag?? && plantBase.auditflag=0)>
                    		<input name="subBox"  value="${plantBase.id!}" type="checkbox"/>
                    		</#if>
                    	</td>
                    	<td>${plantBase_index+1}	</td>
						<td>${plantBase.base_code!}</td>
                        <td>
                        	<#if plantBase.base_name?length lt 10>
	                      	 <a  title="${plantBase.base_name!}">${plantBase.base_name!}</a> 
	                        <#else>
	                         	 <a title="${plantBase.base_name!}">${(plantBase.base_name[0..9])?default("")}......</a> 
							</#if> 
                        </td> 
                        <td >
	                        <#if plantBase.base_location?length lt 10>
	                      	 <a  title="${plantBase.base_location!}">${plantBase.base_location!}</a> 
	                        <#else>
	                         	 <a title="${plantBase.base_location!}">${(plantBase.base_location[0..9])?default("")}......</a> 
							</#if> 
                        </td> 
                        <td>${plantBase.base_area! }</td> 
                        <td>${plantBase.base_height }</td> 
                        <td>
                        	<#if plantBase.category_name??>
                        		<#if plantBase.category_name?length lt 10>
	                      			 <a  title="${plantBase.category_name!}">${plantBase.category_name!}</a> 
	                       		<#else>
	                         		 <a title="${plantBase.category_name!}">${(plantBase.category_name[0..9])?default("")}......</a> 
								</#if>
							</#if>
                        </td> 
                        <td> 
                        <#if (plantBase.auditflag?? && plantBase.auditflag=0)> 未审核</#if>
                        <#if (plantBase.auditflag?? && plantBase.auditflag=1)> 已审核</#if>
                        </td> 
                        <td>
                        	<div class="hidden-sm hidden-xs action-buttons">
	                            <a class="green tooltip-success edit" href="${contextPath}/admin/plantbase/view?id=${plantBase.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
	                            <@shiro.hasPermission name="admin_plantbase_audit">
	                            <#if (plantBase.auditflag?? && plantBase.auditflag=0)>
	                            <a class="red tooltip-error audit" href="${contextPath}/admin/plantbase/audit?rid=${plantBase.id!}" name="${plantBase.base_name!}" data-rel="tooltip" title="审核"> <i class="ace-icon fa fa-key bigger-130"></i></a>
                                </#if>
                                <!-- 
                                <#if (plantBase.auditflag?? && plantBase.auditflag=1)>
	                            <a> <i class="ace-icon fa fa-key bigger-130"></i></a>
                                </#if>
                                 -->
                                <#if (plantBase.auditflag?? && plantBase.auditflag=0)>
                                <a class="red tooltip-error delete" href="${contextPath}/admin/plantbase/delete?ids=${plantBase.id!}" name="${plantBase.base_name!}" data-rel="tooltip" title="删除"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                           		</#if>
                           		</@shiro.hasPermission>
                           		
                           		<#if session['adminType'] == 'true'>
                            	<a class="orange tooltip-error release" href="${contextPath}/admin/plantbase/release?ids=${plantBase.id!}" name="${plantBase.base_name!}" data-rel="tooltip" title="释放"> <i class="ace-icon fa fa-leaf bigger-130"></i></a>
                            	</#if>
                            	
                           	</div>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>

</@mainlayout>
