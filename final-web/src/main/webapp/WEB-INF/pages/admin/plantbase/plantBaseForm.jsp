<form id="plantBaseForm" class="form-horizontal" action="${contextPath}/admin/plantbase/save" method="post" enctype="multipart/form-data">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if plantBase.id??>
        <h5 class="blue">编辑基地</h5>
        <#else>
        <h5 class="blue">添加基地</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="plantBase.id" id="plantBase.id" value="${plantBase.id!}"/>
        
        <input type="hidden" name="categoryId" id="categoryId" value="${categoryId!}"/><!--标识作用  -->
        <input type="hidden" name="categoryName" id="categoryName" value="${categoryName!}"/><!--标识作用  -->
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_code"><font style="color:red;padding-right: 4px">*</font>基地编码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="plantBase.base_code" id="base_code" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.base_code!}"/>
                </div>
                
            </div>
            
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_name"><font style="color:red;padding-right: 4px">*</font>基地名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="plantBase.base_name" id="base_name" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.base_name!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="plantBase_name"><font style="color:red;padding-right: 4px">*</font>基地地点:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100"  name="plantBase.base_location" id="base_location" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.base_location!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
	        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_in_charge"><font style="color:red;padding-right: 4px">*</font>基地所在省份:</label>
	         <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
	                <select class="col-xs-12 col-sm-8 required isaudit" id="county" name="plantBase.county_code">
				        	<option value="">---------------------请选择----------------------</option>
				        	<#list countyList as county>
				        	<option value="${county.code!}" <#if (county.code?? && plantBase.base_county_code??) &&  county.code = plantBase.base_county_code>selected="selected"</#if>>${county.name}</option>
				            </#list>
				    </select>
			   </div>
		   </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_area"><font style="color:red;padding-right: 4px">*</font>基地面积:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="number" min="0" maxlength="10" name="plantBase.base_area" id="base_area" class="col-xs-12 col-sm-7 required isaudit" value="${plantBase.base_area!}"/><font style="font-weight: 1;margin-left: 10px;" size="5">亩</font>
                </div>
            </div>
        </div>

		<div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="longitude"><font style="color:red;padding-right: 4px">*</font>经度:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="number" min="0" max="180" name="plantBase.longitude" id="longitude" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.longitude!}"/>
                </div>
            </div>
        </div>
        
        <div class="form-group" >
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="longitude"><font style="color:red;padding-right: 4px">*</font>纬度:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="number"  min="0" max="90" name="plantBase.latitude" id="latitude" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.latitude!}"/>
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" ></label>
            <div class="col-xs-12 col-sm-9">
                	<font style="color:red;" size="1px">提示：纬度的范围是南北纬0-90°,经度的范围是东西经0-180°</font>
            </div>
        </div>
         <div class="form-group">
	        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_in_charge"><font style="color:red;padding-right: 4px">*</font>土壤性质:</label>
	         <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
	               
	                <select required class="col-xs-12 col-sm-4 required isaudit soilselect" id="soil" name="plantBase.soil">
				        	<option value="">----请选择----</option>
				        	<#list dictList as dic>
				              	<option value="${dic.dict_code!}" <#if (dic.dict_code?? && plantBase.soil??) &&  dic.dict_code = plantBase.soil>selected="selected"</#if>>${dic.dict_name }</option>
				            </#list>
				    </select>
				    <input type="hidden" id="soil" class="col-xs-12 col-sm-4 required isaudit soilinput" value="${plantBase.soil!}"/>
				    <input type="checkbox"  id="soilchebox" class="col-xs-12 col-sm-4" />
			   </div>
		   </div>
        </div>
        
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_in_charge"><font style="color:red;padding-right: 4px">*</font>基地负责人:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="plantBase.base_in_charge" id="base_in_charge" class="col-xs-12 col-sm-8 required isaudit" value="${plantBase.base_in_charge!}"/>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_in_charge"><font style="color:red;padding-right: 4px">*</font>基地海拔:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="number" maxlength="10" name="plantBase.base_height" id="base_height" class="col-xs-12 col-sm-7 required isaudit" value="${plantBase.base_height!}"/><font style="font-weight: 1;margin-left: 10px;" size="5">米</font>
                </div>
            </div>
        </div>
        <div class="form-group" id="unitdiv">
	            <label class="control-label col-xs-12 col-sm-3 no-padding-right"><font style="color:red;padding-right: 4px">*</font>单位:</label>
	            <div class="col-xs-12 col-sm-9">
	                <div class="clearfix">
	                        <select required class="unit col-xs-12 col-sm-8 required isaudit" name="unitid" id="unitid">
	                          <option value="">---------------------请选择----------------------</option>
	                          <#list unitList as unit>  
	                        	<option value="${unit.id}" <#if (unit.id?? &&plantBase.unit_id??) &&unit.id=plantBase.unit_id>selected="selected"</#if>>${unit.name}</option>
	                       	  </#list>
						    </select>
	                </div>
	            </div>
	      </div>
        <div class="form-group" style="height: 100px">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right"><font style="color:red;padding-right: 4px">*</font>中草药名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
               		<input type="hidden" readonly="readonly" name="plantBase.category_id" id="category_id" class="col-xs-12 col-sm-8 required isaudit" value="${categoryIds!}"/> 
                    <div id="treeContent" style="position: absolute;z-index: 9999;size: 200px">
                        <ul id="cateTree" class="ztree" style="margin-top:0;"></ul>
                    </div>
                </div>
            </div>
       	</div>
       	 <div class="form-group" style="display: none;" id="plantCheckbox">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">种类不能为空</font>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="base_map"><font style="color:red;padding-right: 4px">*</font>环境地图:</label>
            <div class="col-xs-12 col-sm-9" id="fileupload">
                <div class="clearfix">
                	 <input type="hidden" name="plantBase.base_map" readonly="readonly" type="text" id="base_map" class="col-xs-12 col-sm-4 required isaudit" value="${plantBase.base_map!}"/>
               		 <input type="file" name="plantBase.base_map" id="base_map" class="col-xs-12 col-sm-4 required isaudit base_map uploadFile uploadImg" value="${plantBase.base_map!}" />
                	   勾选不处理图片<input type="checkbox"  name="plantBase.env_test_report" id="env_test_report" class="col-xs-12 col-sm-4 isaudit" />
                </div>
            </div>
         </div>
    </div>
	
    <div class="modal-footer center">
    	<#if (plantBase.auditflag??&&plantBase.auditflag=0)>
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    	</#if>
    	<#if plantBase.id??>
    	<#else>
    	<button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    	</#if>
    </div>
    <input type="hidden" name="isaudit" id="isaudit" value="${plantBase.auditflag!}"/>
    <input type="hidden" name="Image_Pattern" id="Image_Pattern" value="${Image_Pattern!}"/>
     <input type="hidden" name="url" id="url" value="${contextPath}"/>
</form>
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	//=======================tree===========================
     	var cateSetting = {
         		 view: {
                      dblClickExpand: true,
                      showIcon: false
                  },
                  data: {
                      simpleData: {
                          enable: true
                      }
                  },
                  check:{
                      enable: true,
                      chkStyle: "checkbox",
                      chkboxType: { "Y": "", "N": "" },
                      autoCheckTrigger: true
                  },
                 callback:{
                     onCheck: onClick
                 }
         };
     	//选择单位
    	var unit_cate_url=$("#url").val()+"/admin/plantbase/duleunitcategory";
    	$("#unitid").change(function(){
    		 var unit_id=$(this).children('option:selected').val(); 
    		 $.post(unit_cate_url,{"unit_id":unit_id,"plantbase_id":$("input[name='plantBase.id']").val()},function(data){
    			 $.fn.zTree.init($("#cateTree"),cateSetting,JSON.parse(data));
             });
    		 $("#treeContent").css({left:"11px"}).slideDown("fast");
     	});
    	//修改页面
    	if($("input[name='plantBase.id']").val()!=''){
    		var unit_id=$("#unitid").children('option:selected').val(); 
   		 	$.post(unit_cate_url,{"unit_id":unit_id,"plantbase_id":$("input[name='plantBase.id']").val()},function(data){
                $.fn.zTree.init($("#cateTree"),cateSetting, JSON.parse(data));
                if($("#isaudit").val()==1||$("#isaudit").val()=='1'){//已审核种类，把树节点disabled
                 	Disabled();
             	}
            });
   			 $("#treeContent").css({left:"11px"}).slideDown("fast");
    	}
        //=======================treeEnd===========================
    	//勾选输入土壤
    	$("#soilchebox").click(function(){
    		if($(this).is(':checked')){
    			$(".soilselect").hide();
    			//对select和input进行操作
    			$(".soilselect").removeAttr("name");
    			$(".soilinput").attr("name","plantBase.soil");
    			$(".soilinput").attr("type","text");
    			$(".soilinput").val("");
    		}else{
    			$(".soilselect").show();
    			//对select和input进行操作
    			$(".soilselect").attr("name","plantBase.soil");
    			$(".soilinput").removeAttr("name");
    			$(".soilinput").attr("type","hidden");
    		}
    	}); 
    	//编辑时，把地图勾上
    	if($("input[name='plantBase.id'][type='hidden']").val()!=''){
    		$("input[name='plantBase.env_test_report']:checkbox").attr("checked", true);
    		$("input[name='plantBase.base_map'][type='file']").hide();
			$("input[name='plantBase.base_map'][type='hidden']").attr("type","text");
    	}
    	if($("#isaudit").val()==1||$("#isaudit").val()=='1'){
    		$(".isaudit").attr("disabled","disabled");
    	}
    	$("#env_test_report").click(function(){
    		if($(this).is(':checked')){
    			$("input[name='plantBase.base_map'][type='file']").hide();
    			$("input[name='plantBase.base_map'][type='hidden']").attr("type","text");
    			$("#base_map-error").hide();
    		}else{
    			$("input[name='plantBase.base_map'][type='text']").attr("type","hidden");
    			$("input[name='plantBase.base_map'][type='file']").show();;
    		}
    	});
    	
    	 $("#category_id").change(function(){
    		$("#category_name").attr("value",$("#category_id").find("option:selected").text());
    	}); 
    	
    	
    	$('input[id=lefile]').change(function() {
    		$('#photoCover').val($(this).val());
    		});

        $("#plantBaseForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
            	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            	if($("#category_id").val()==""){
            		$("#plantCheckbox").show();
            	}else{
            		$("#plantCheckbox").hide();
            	}
            	if($("#plantCheckbox").is(":hidden")==true){
            		form.submit();
            	}
            },
            invalidHandler: function (form) {
            } 
        });
       
    });
    
    
 // 上传
    $(document).on("change", ".uploadFile", function() {
    	var file = $(".uploadFile");
    	if ($(this).hasClass("uploadImg")) {
    		// 图片文件类型校验
    		if (isInImage_Pattern($(this))) {
    			// 文件大小校验
    	    	if (findSize($(this)) > 2048) {
    	    		pvAlert("文件大小不能超过2M");
    	    		file.after(file.clone().val("")); 
        			file.remove();
        			return;
    	    	}
    		}else {
    			file.after(file.clone().val("")); 
    			file.remove();
    			return;
			}
    	}
    	
    });
	
    function isInImage_Pattern(obj) {
    	var fileName  = obj[0].files[0].name;
    	var ext = fileName.split(".")[1];
    	//alert(fileName+">>"+ext+">>"+$("#Image_Pattern").val());
    	 if (ext && $("#Image_Pattern").val().indexOf(ext) >= 0) {
    		return true;
    	}else {
    		pvAlert("非支持上传图片类型！支持上传图片类型：\n"+$("#Image_Pattern").val());
    		return false;
    	}
    }
    
    function findSize(obj) {
    	var byteSize  = obj[0].files[0].size;
    	return Math.ceil(byteSize / 1024);
    }
    
    function pvAlert(msg, callbackFunction) {
    	bootbox.alert({  
	           buttons: {  
                  ok: {  
                       label: '确定',  
                       className: 'btn-primary btn-sm'  
                   }  
               },  
               message: msg,    
	          
             callback: function(result) {
                
             }
		  });    
    }
    function onClick(event, treeId, treeNode){plantCheckbox
	   	 var id=$("#category_id").val();
	   	 if(treeNode.checked){
	   		$("#category_id").val(id==""?treeNode.id:id+","+treeNode.id);
	   	 }else{
	   		if(id.indexOf(treeNode.id)==0){//第一个
	   			id=id.replace(treeNode.id+",","");
	   		}else{
	   			id=id.replace(","+treeNode.id,"");
	   		}
	   		if(id.indexOf(",")<0){//最后一个，不存在,
	   			id=id.replace(treeNode.id,"");
	   		}
	   		$("#category_id").val(id);
	   	 }
 	}
    function Disabled(){//所有节点disabled
   	 var treeObj = $.fn.zTree.getZTreeObj("cateTree");
        var nodes = treeObj.getNodes();//获取所有节点
        if(nodes.length>0){
 			for(var i=0;i<nodes.length;i++){//拼接字符串
 				treeObj.setChkDisabled(nodes[i], true);
	 		}
 		} 
   }
</script>
<style type="text/css">
    .ztree{
        margin-top: 10px;
        border: 1px solid #617775;
        background: #ffffff;
        width: 280px;
        height: 100px;
        overflow-y: scroll;
        overflow-x: auto;
    }
</style>