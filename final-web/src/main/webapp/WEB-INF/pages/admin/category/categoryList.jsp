<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">种类管理</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
    <!-- 
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ui.jqgrid.css" />
     -->
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
        	 // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
            	
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定删除"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                //if(result) window.location.href = $(currentA).attr("href");
                               	if(result){
                               		$.ajax({
                            			url:$(currentA).attr("href"),
                            			type:"post",
                            			data:"&&time="+new Date().getTime(),
                            			datatype:"text",
                            			success:function(data){
                            				if(data=='true'){
                            					bootbox.alert({  
                                                    buttons: {  
                                                       ok: {  
                                                            label: '确定',  
                                                            className: 'btn-primary btn-sm'  
                                                        }  
                                                    },  
                                                    message: '该种类存在关联，不能删除',  
                                                    callback: function() {  
                                                    	window.location.href=window.location.href;
                                                    },  
                                                });  
                            				}else{
                            					window.location.href=window.location.href;
                            				}
                            			}
                            		});
                               	}
                            }
                        }
                );
            });
        	 
            // 释放按钮
            $("a.release").on(ace.click_event, function(e){
            	
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定释放"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                            	if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
        	 
            // 审核按钮
            $("a.audit").click(ace.click_event, function(e){
               		var currentA = this;
                    e.preventDefault();
//                    bootbox.setDefaults({locale:"zh_CN"});
                    bootbox.confirm({
                                message: "审核后数据不能修改，确定审核"+ $(currentA).attr('name')+"？",
                                buttons: {
                                    confirm: {
                                        label: "审核",
                                        className: "btn-primary btn-sm"
                                    },
                                    cancel: {
                                        label: "取消",
                                        className: "btn-sm btn-cancel"
                                    }
                                },
                                callback: function(result) {
                                    if(result) window.location.href = $(currentA).attr("href");
                                }
                            }
                    );
            }); 
			
            
            //判断非法字符
            $("#search").click(function(){
        		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
                   if($("#catecode").val().trim() != ""||$("#catename").val().trim() != ""){  
                       if(pattern.test($("#catecode").val())||pattern.test($("#catename").val())){  
                    	 bootbox.alert({  
                               buttons: {  
                                  ok: {  
                                       label: '确定',  
                                       className: 'btn-primary btn-sm'  
                                   }  
                               },  
                               message: '存在非法字符，请重新输入查询条件。',  
                               callback: function() {  
                            		$("#catecode").val("");
                               		$("#catename").val("");
                               },  
                           });  
               			 
                       }else {
                       	$("#search").attr("type","submit");
					 }
                  }else {
                	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
				}  
    		});
        });
        
        function page(pageNum) {
        	$("#page").val(pageNum);
            $("#categorySearchForm").submit(); 
        }
        
        
     	 //全选功能  
		function selectAll(obj){
			var arrayList=$("input[name='subBox']");
			for(var i=0;i<arrayList.length;i++){
				if(arrayList[i].disabled!=true){
					arrayList[i].checked=obj.checked;
				}
			}
		}
     	 
     	function audit(){
     		var uids = '';
    	 	var flag = false;
    		$("input[name='subBox']:checked").each(function(){
    				if(flag){
    					uids = uids+ "," + $(this).val() ;
    				}else{
    					uids = uids + $(this).val();
    				}
    				flag = true;
    		});
    		if(uids!=''){
    			bootbox.confirm({
                    message: "审核后数据不能修改，确定审核？",
                    buttons: {
                        confirm: {
                            label: "审核",
                            className: "btn-primary btn-sm"
                        },
                        cancel: {
                            label: "取消",
                            className: "btn-sm btn-cancel"
                        }
                    },
                    callback: function(result) {
                    	if(result){
                    		var url = "${contextPath}/admin/category/audit";
                			var form = $("<form></form>");
                			form.attr('action',url);
                			form.attr('method','post');
                			var param = $("<input type='hidden' name='rid' value='"+uids+"'/>");
                			form.append(param);
                			form.submit();
                    	}
                    }
                }
       		);
    		}else{
    			 bootbox.alert({  
       	           buttons: {  
                         ok: {  
                              label: '确定',  
                              className: 'btn-primary btn-sm'  
                          }  
                      },  
                      message: '请选择未审核的种类。', 
                      callback: function(result) {
                         
                      }
    			  });    		  
    			
    		}
    		
     	}
    </script>
</#assign>


<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-categoryFlag">
<div class="row">
    <div class="col-xs-12">

			<form id="categorySearchForm" class="form-search form-inline" method="post">
			
				<input id="recordlog" name="recordlog" value="ccc" type="hidden"/>
                <input id="page" name="page" type="hidden"/>
                <label>种类编码:</label><input type="text" id="catecode" name="category.category_code" class="input-sm input-medium" value="${category.category_code! }"/>&nbsp;
                <label>种类名称:</label><input type="text" id="catename" name="category.category_name" class="input-sm input-medium" value="${category.category_name! }"/>&nbsp;
       			<label>审核状态:</label>  
       			  
       			  <select class="input-medium" id="type" name="category.auditflag">
                	  	<option value="-1">---请选择---</option>
                   		<option value="0" <#if category.auditflag?? && category.auditflag = 0>selected="selected"</#if>>未审核</option>
                        <option value="1" <#if category.auditflag?? && category.auditflag = 1>selected="selected"</#if>>已审核</option>
                   		
                 </select>
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
                <#if session['adminType'] == 'true'> 
                <div style="float: right;">
                <a href="${contextPath}/admin/category/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
               
               
               <@shiro.hasPermission name="admin_category_audit"> 
                <a href="javascript:audit();" class="btn btn-sm btn-warning"><i class="ace-icon fa fa-key"></i>审核</a>
           		</@shiro.hasPermission>
           		</div>
           		<#else>
           		<div style="height: 20px"/>
           		</#if>
            </form>
            
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                	<th width="30" align="right" ><input  type="checkbox" onclick="selectAll(this)"/> </th>
                	<th width="40" align="right" style="text-align: center;" >序号</th>
                    <th width="150" align="left" style="text-align: center;" >种类编码</th>
                    <th width="250" align="left" style="text-align: center;" >种类名称</th>
                    <th width="250" align="left" style="text-align: center;" >审核状态</th>
                     <#if session['adminType'] == 'true'> 
                    <th width="100" align="left" style="text-align: center;" >操作</th>
                    </#if>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as category>
                    <tr>
                    	<td>
                    		<#if (category.auditflag?? && category.auditflag=0)>
                    			<input name="subBox"  value="${category.id!}" type="checkbox"/>
                    		</#if>
                    	</td>
                    	<td>${category_index+1}	</td>
						<td>
							<#if category.category_code?length lt 10>
	                      	 <a  title="${category.category_code!}">${category.category_code!}</a> 
	                        <#else>
	                         	 <a title="${category.category_code!}">${(category.category_code[0..9])?default("")}......</a> 
							</#if>
						</td>
                        <td>
                        	<#if category.category_name?length lt 10>
	                      	 <a  title="${category.category_name!}">${category.category_name!}</a> 
	                        <#else>
	                         	 <a title="${category.category_name!}">${(category.category_name[0..9])?default("")}......</a> 
							</#if>
                        </td> 
                        <td> 
                        <#if (category.auditflag?? && category.auditflag=0)> 未审核</#if>
                        <#if (category.auditflag?? && category.auditflag=1)> 已审核</#if>
                        </td> 
                         <#if session['adminType'] == 'true'> 
                        <td>
                        	<div class="hidden-sm hidden-xs action-buttons">
                                <a class="green tooltip-success edit" href="${contextPath}/admin/category/view?id=${category.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                <@shiro.hasPermission name="admin_category_audit"> 
                                <#if (category.auditflag?? && category.auditflag=0)>
                                <a class="red tooltip-error audit"   href="${contextPath}/admin/category/audit?rid=${category.id!}" name="${category.category_name!}" data-rel="tooltip" title="审核"><i class="ace-icon fa fa-key bigger-130"></i></a>
                                </#if>
                                <!--  
                                <#if (category.auditflag?? && category.auditflag=1)>
                                <a><i class="ace-icon fa fa-key bigger-130"></i></a>
                                </#if>
                                -->
                                <#if (category.auditflag?? && category.auditflag=0)>
                                <a class="red tooltip-error delete" href="${contextPath}/admin/category/delete?ids=${category.id!}" name="${category.category_name!}" data-rel="tooltip" title="删除"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                            	</#if>
                            	</@shiro.hasPermission>
                            	
                            	<#if session['adminType'] == 'true'>
                            	<a class="orange tooltip-error release" href="${contextPath}/admin/category/release?ids=${category.id!}" name="${category.category_name!}" data-rel="tooltip" title="释放"> <i class="ace-icon fa fa-leaf bigger-130"></i></a>
                            	</#if>
                            	
                            </div>
                        </td>
                        </#if> 
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
</@mainlayout>
