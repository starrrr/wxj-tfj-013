<form id="categoryForm" class="form-horizontal" action="${contextPath}/admin/category/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if category.id??>
        <h5 class="blue">编辑种类</h5>
        <#else>
        <h5 class="blue">添加种类</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="category.id" id="category.id" value="${category.id!}"/>
		<input type="hidden" name="isaudit" id="isaudit" value="${category.auditflag!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="category_code"><font style="color:red;padding-right: 4px">*</font>种类编号:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text"  maxlength="100" name="category.category_code" id="category_code" class="col-xs-12 col-sm-8 required isaudit" value="${category.category_code!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="category_name"><font style="color:red;padding-right: 4px">*</font>种类名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="category.category_name" id="category_name" class="col-xs-12 col-sm-8 required isaudit" value="${category.category_name!}"/>
                </div>
            </div>
        </div>
        <div class="form-group" style="display: none;" id="categorycheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">该种类已存在</font>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="category_type">备注:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                	  <textarea name="category.note" maxlength="150" id="note" class="col-xs-12 col-sm-8 isaudit" style="padding:0 4px;">${category.note!}</textarea>
                </div>
        	</div>
        </div>
       	 <div class="form-group" style="height: 150px">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right"><font style="color:red;padding-right: 4px">*</font>单位:</label>
            <div class="col-xs-12 col-sm-9">
                 <div class="clearfix">
                  	<input required="required" name="unitids" type="hidden" class="col-xs-12 col-sm-8 unit required" id="unitIds" class="col-xs-12 col-sm-4" value="${unitIds! }" readonly/>
                    <input required="required" type="hidden" class="col-xs-12 col-sm-8 unit required" id="unitNames" class="col-xs-12 col-sm-4" value="${unitNames! }" readonly/>
                 	<div id="treeContent" style="display:none; position: absolute;z-index: 9999;size: 200px">
                        <ul id="unitTree" class="ztree" style="margin-top:0;"></ul>
                    </div>
                   	
                </div>
            </div>
       	 </div>
       	<div class="form-group" style="display: none;" id="unitcheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">单位不能为空</font>
                </div>
            </div>
        </div>
       	
    </div>
    <div class="modal-footer center">
    	<#if (category.auditflag?? && category.auditflag=0)>
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    	</#if>
    	<#if category.id??>
    	<#else>
    	<button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    	</#if>
    </div>
</form>

<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	//getModel();
    	if($("#isaudit").val()==1||$("#isaudit").val()=='1'){
    		$(".isaudit").attr("disabled","disabled");
    	}
    	//=======================检查种类名称是否存在=======================
    	$("#category_name").blur(function(){
	    	if($("#category_name").val()!=''){
	    		$.ajax({
	        		url:"${contextPath}/admin/category/checkCategoryName",
	        			type:"post",
	        			data:"category_name="+$("#category_name").val(),
	        			datatype:"text",
	        			success:function(data){
	        				if(data=='true'){
	        					$("#categorycheck").show();
	        				}else{
	        					$("#categorycheck").hide();
	        				}
	        			} 
	        	}); 
	    	}
	    }); 
    	//=======================End检查种类名称是否存在=======================

        $("#categoryForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
            	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            	if($("#unitNames").val()==""){
            		$("#unitcheck").show();
            	}else{
            		$("#unitcheck").hide();
            	}
            	if($("#categorycheck").is(":hidden")==true&&$("#unitcheck").is(":hidden")==true){
               		 form.submit();
            	}
            },
            invalidHandler: function (form) {
            }
        });
       
    	//=======================tree===========================
        var setting = {
        		 view: {
                     dblClickExpand: true,
                     showIcon: false
                 },
                 data: {
                     simpleData: {
                         enable: true
                     }
                 },
                 check:{
                     enable: true,
                     chkStyle: "checkbox",
                     chkboxType: { "Y": "", "N": "" },
                     autoCheckTrigger: true
                 },
                callback:{
                    onCheck: onClick
                }
        };
        $.post("${contextPath}/admin/unit/getUnitTreeByCaId",{"category_id":$("input[name='category.id']").val()}, function(data){
            $.fn.zTree.init($("#unitTree"), setting, JSON.parse(data));
            if($("#isaudit").val()==1||$("#isaudit").val()=='1'){//已审核种类，把树节点disabled
            	Disabled();
        	}
        });
        $("#treeContent").css({left:"11px"}).slideDown("fast");
       
    });
    function onClick(event, treeId, treeNode){
	   	 var id=$("#unitIds").val();
	  	 var name=$("#unitNames").val();
	   	 if(treeNode.checked){
	   		$("#unitcheck").hide();//隐藏提示
	   		$("#unitIds").val(id==""?treeNode.id:id+","+treeNode.id);
	   		$("#unitNames").val(name==""?treeNode.name:name+","+treeNode.name);
	   	 }else{
	   		if(id.indexOf(treeNode.id)==0){//第一个
	   			id=id.replace(treeNode.id+",","");
	   			name=name.replace(treeNode.name+",","");
	   		}else{
	   			id=id.replace(","+treeNode.id,"");
	   			name=name.replace(","+treeNode.name,"");
	   		}
	   		if(id.indexOf(",")<0){//最后一个，不存在,
	   			id=id.replace(treeNode.id,"");
	   			name=name.replace(treeNode.name,"");
	   		}
	   		$("#unitIds").val(id);
	   		$("#unitNames").val(name);
	   	 }
    }
    function Disabled(){//所有节点disabled
    	 var treeObj = $.fn.zTree.getZTreeObj("unitTree");
         var nodes = treeObj.getNodes();//获取所有节点
         if(nodes.length>0){
  			for(var i=0;i<nodes.length;i++){//拼接字符串
  				treeObj.setChkDisabled(nodes[i], true);
 	 		}
  		} 
    }
</script>
<style type="text/css">
    .ztree{
        margin-top: 10px;
        border: 1px solid #617775;
        background: #ffffff;
        width: 280px;
        height: 150px;
        overflow-y: scroll;
        overflow-x: auto;
    }
</style>
