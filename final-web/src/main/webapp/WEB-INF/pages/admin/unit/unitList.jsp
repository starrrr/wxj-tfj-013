<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">单位管理</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
            
            
          //判断非法字符
            $("#unitsearch").click(function(){
        		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
                   if($("#unitname").val().trim() != ""){  
                       if(pattern.test($("#unitname").val())){  
                    	 bootbox.alert({  
                               buttons: {  
                                  ok: {  
                                       label: '确定',  
                                       className: 'btn-primary btn-sm'  
                                   }  
                               },  
                               message: '存在非法字符，请重新输入查询条件。',  
                               callback: function() {  
                            		$("#unitname").val("");
                               },  
                           });  
               			 
                       }else {
                       	$("#search").attr("type","submit");
    				 }
                  }else {
                	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
    			}  
    		});
            
            
        });
        
        function page(pageNum) {
        	$("#page").val(pageNum);
            $("#unitsearch").submit(); 
        }
    </script>
</#assign>


<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-unit">
<div class="row">
    <div class="col-xs-12">

			<form id="unitsearch" class="form-search form-inline" method="post">
			
				<input id="recordlog" name="recordlog" value="ccc" type="hidden"/>
                <input id="page" name="page" type="hidden"/>
                <label>名称:</label><input type="text" id="unitname" name="unit.name" class="input-sm input-medium" value="${unit.name!}"/>&nbsp;
       			
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
                <#if session['adminType'] == 'true'> 
                <div style="float: right;">
                <a href="${contextPath}/admin/unit/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
           		</div>
           		<#else>
           		<div style="height: 20px"/>
           		</#if>
            </form>
            
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="50" style="text-align: center;">序号</th>
                    <th width="100" style="text-align: center;">名称</th>
                    <th width="100" style="text-align: center;">创建时间</th>
                    <th width="100" style="text-align: center;">备注</th>
                    <th width="100" align="left" style="text-align: center;" >操作</th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as unit>
                     <tr>
                        <td>${unit_index+1}</td>
                        <td>${unit.name!}</td>
                        <td>${unit.createtime!}</td>
                        <td>${unit.remark!}</td>
                        <td>
                        <div class="hidden-sm hidden-xs action-buttons">
                          <a class="green tooltip-success edit" href="${contextPath}/admin/unit/view?id=${unit.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						</div>
                        </td>
                      </tr>
                 </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
</@mainlayout>
