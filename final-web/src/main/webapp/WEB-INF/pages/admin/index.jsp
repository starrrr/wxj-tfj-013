 <#include "common/layout/__mainlayout.jsp"> 
 <#assign pageBreadCrumbs>
<li class="active">首页</li>
</#assign> 
<#assign pageCss>
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/china-map.css"/>
</#assign> 
<#assign pageJavascript>
<script type="text/javascript"
	src="${contextPath}/static/admin/assets/js/map/lib/raphael-min.js"></script>
<script type="text/javascript"
	src="${contextPath}/static/admin/assets/js/map/res/chinaMapConfig.js"></script>
<script type="text/javascript"
	src="${contextPath}/static/admin/assets/js/map/map-min.js"></script>
<script type="text/javascript">
	$(function() {
		var placeholder = $('#piechart-placeholder').css({
			'width' : '90%',
			'min-height' : '260px'
		});
		var data = [ {
			label : "游客 "+'${indexCount.count_G_openid}'+"人",
			data : '${indexCount.count_G_openid}',
			color : "#68BC31",
			name:"游客"
		}, {
			label : "关注用户 "+'${indexCount.count_C_openid}'+"人",
			data : '${indexCount.count_C_openid}',
			color : "#2091CF",
			name:"关注用户"
		}, {
			label : "企业用户 "+'${indexCount.count_B_openid}'+"人",
			data : '${indexCount.count_B_openid}',
			color : "#AF4E96",
			name:"企业用户"
		} ];
		function drawPieChart(placeholder, data, position) {
			$.plot(placeholder, data, {
				series : {
					pie : {
						show : true,
						tilt : 1,
						highlight : {
							opacity : 0.25
						},
						stroke : {
							color : '#fff',
							width : 2
						},
						startAngle : 2,
						radius: 1, //半径  
						label: {  
				                        show: true,  
				                        radius: 2/3,  
				                        formatter: function(label, series){  
				                            return '<div style="font-size:15px;text-align:center;padding:10px;color:#000000;">'+series.name+'<br/>'+Math.round(series.percent)+'%</div>';  
				                    }
				                  }
					}
				},
				legend : {
					show : true,
					position : position || "ne",
					labelBoxBorderColor : null,
					margin : [ -30, 15 ]
				},
				grid : {
					hoverable : true,
					clickable : true
				}
			})
		}
		drawPieChart(placeholder, data);
		var $tooltip = $(
				"<div class='tooltip top in'><div class='tooltip-inner'></div></div>")
				.hide().appendTo('body');
		var previousPoint = null;

		placeholder.on('plothover', function(event, pos, item) {
			if (item) {
				if (previousPoint != item.seriesIndex) {
					previousPoint = item.seriesIndex;
					var tip = item.series['label'] + " : "
							+ Number(item.series['percent']).toFixed(1) + '%';
					$tooltip.show().children(0).text(tip);
				}
				$tooltip.css({
					top : pos.pageY + 10,
					left : pos.pageX + 10
				});
			} else {
				$tooltip.hide();
				previousPoint = null;
			}

		});
		$('#ChinaMap').SVGMap({
			mapName: 'china',
			mapWidth : 460,
			mapHeight : 330,
			stateData:$.parseJSON('${plantBaseData}'),
			showTip:true,
			stateTipWidth:200,
			stateTipHtml: function(stateData, obj) {
				if((typeof(stateData[obj.id])!= "undefined")){
		                        return stateData[obj.id].name + '<br/>'+stateData[obj.id].plantbase;
				}else{
					$('.stateTip').hide();
					return "";
				}
	                }
		});
	});
</script>

</#assign> 
<@mainlayout pageJavascript=pageJavascript pageCss=pageCss pageBreadCrumbs=pageBreadCrumbs currentMenu="indexFlag-pageFlag">
<!--${msg! }  -->

<div class="row">
	<div class="leftGraph col-sm-6 ">
		<div class="leftTitle">
			<img src="${contextPath}/static/admin/assets/images/titleButton.png" alt=""/>
			<h3 style="font-size:18px;">微信用户总人数&nbsp;&nbsp;&nbsp;<small>共</small><span>${indexCount.count_openid}</span><small>人</small></h3>
		</div>
		<div class="col-sm-12">
			<div class="widget-box" style="border:0">
				<div class="widget-header-small">
					<div class="widget-body">
						<div class="widget-main">
							<div
								style="width: 90%; margin-top:30px;min-height: 200px; padding: 0px; position: relative;"
								id="piechart-placeholder">
								<canvas height="400" width="400"
									style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 384px; height: 150px;"
									class="flot-base"></canvas>
								<canvas height="400" width="400"
									style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 384px; height: 150px;"
									class="flot-overlay"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="centerLine"></div> -->
	<div class="rightMap col-sm-6 ">
		<div class="leftTitle">
			<img src="${contextPath}/static/admin/assets/images/titleButton.png" alt=""/>
			<@shiro.hasPermission name="product_admin_plantbase">
			<a href="${contextPath}/admin/plantbase?recordlog=recordlog">更多基地信息>></a>
			</@shiro.hasPermission>
			<h3 style="font-size:18px;">全国共<span>${indexCount.count_plantbase}</span>个生产基地</h3>
		</div>
		<div class="itemCon" style="float: left;border-left:1px solid #ccc;">
		    <div id="ChinaMap" style="margin: 30px 40px;"></div>
		   <div id="stateTip" style="position: fixed;left: 100%;text-align: left;display: inline;"></div>
		</div>
	</div>
</div>
	</div>
</div>
<div class="bottomList col-sm-12">
	<div class="leftTitle">
		<img src="${contextPath}/static/admin/assets/images/titleButton.png" alt=""/>
		<@shiro.hasPermission name="product_admin_category">
		<a href="${contextPath}/admin/category?recordlog=recordlog">更多种类信息>></a>
		</@shiro.hasPermission>
		<h3 style="font-size:18px;">产品种类&nbsp;&nbsp;&nbsp;<small>共</small><span>${indexCount.count_category}</span>
		<small>种</small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;产品记录数&nbsp;&nbsp;&nbsp;<small>共</small>
		<span>${indexCount.count_categoryrecord}</span><small>种</small></h3>
	</div>
	<ul>
		<#list categoryList as category>
			<li><span class="badge">${category.PY }</span>${category.category_name }&nbsp;<span style="color:#438EB9;">(<small>${category.nums}</small>)</span></li>
		</#list>
	</ul>
</div>
</@mainlayout>