<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/pv/js/allLink.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	$("button.copy-confirm").click(function() {
    		var product_name = $("#product_name").val();
    		if (product_name && $.trim(product_name) != '') {
        		pvConfirm("确定复制此产品?", function(confirm) {
        			if (confirm) {
            				var processType = new Array();
            				$("input[name='process_type']:checked").each(function(index) {
            					processType[index] = $(this).val();
            				});
            				
            				var batches = {
            						"seed_batch" : $("input[name='seed_batch']").val(), 
            				        "material_batch" : $("input[name='material_batch']").val(), 
            				        "product_batch" : $("input[name='product_batch']").val(), 
            				        "flow_batch" : $("input[name='flow_batch']").val()
            				}
            				
            				var url = "${contextPath}/admin/product/copy";
            				
            				var data = "product_id=" + $("#product_id").val() + "&" + 
            						   "product_name=" + product_name + "&" + 
            						   "processType=" + processType + "&" + 
            						   "batches=" + JSON.stringify(batches);
            				
            				$.post(url, data, function(result) {
            					var obj = $.parseJSON(result);
            					if (obj && obj.msg) {
                					pvAlert(obj.msg, function() {
                						location.href = "${contextPath}/admin/product";
                					});
            					} else {
            						pvAlert("程序发生错误.");
            					}
            				});
        			}
        		});
    		} else {
    			pvAlert("产品名称不能为空!");
    		}
    	});
    });
</script>

<form id="productForm" class="form-horizontal" action="${contextPath}/admin/product/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="blue">产品复制</h5>
    </div>

    <div class="modal-body">
        <input type="hidden" name="product_id" id="product_id" value="${product.id!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-1 no-padding-right" for="product_name">
            </label>
            <div class="col-xs-12 col-sm-11 form-group-pfm fs16">
                <div class="clearfix">
                   <font style="color:red;" size="1px">提示：原产品批号若未填写的情况下，以下批号暂不复制。</font>
                </div>
            </div>
        </div>
        <div class="form-group">
        	<label class="control-label col-xs-12 col-sm-2 no-padding-right fs16" for="product_name">
            	<font style="color:red;padding-right: 4px">*</font>产品名称:
            </label>
            <div class="col-xs-12 col-sm-10">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="product.product_name" id="product_name" class="col-xs-12 col-sm-8 required isaudit" value="${product.product_name!}"/>
                </div>
            </div>
        </div>
         		
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-1 no-padding-right" for="product_name">
            	<input type="checkbox" name="process_type" value="PLANT_PROCESS"/>
            </label>
            <div class="col-xs-12 col-sm-11 form-group-pfm fs16">
                <div class="clearfix">
                                                种植环节   
                </div>
            </div>
        </div>
        
        <div class="form-group">
        	<div class="form-group col-sm-6">
            	<label class="control-label col-xs-12 col-sm-4 no-padding-right" for="product_name">
            	   种子批号
            	</label>
            	<div class="col-xs-12 col-sm-8">
                	<div class="clearfix">
                		<input type="text" name="seed_batch" class="col-xs-12 col-sm-12"/>
                	</div>
            	</div>
        	</div>
        
        	<div class="form-group col-sm-6">
            	<label class="control-label col-xs-12 col-sm-4 no-padding-right" for="product_name">
            	  原料批号
            	</label>
            	<div class="col-xs-12 col-sm-8">
                	<div class="clearfix">
                		<input type="text" name="material_batch" class="col-xs-12 col-sm-12"/>
               		</div>
            	</div>
        	</div>
        </div>
        

         
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-1 no-padding-right" for="category_name">
            	<input type="checkbox" name="process_type" value="SUPERVISE_PROCESS"/>
            </label>
            <div class="col-xs-12 col-sm-11 form-group-pfm fs16">
                <div class="clearfix">
               	   加工环节
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="form-group col-sm-6">
                <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="product_name">
                                                  成品批号
                </label>
                <div class="col-xs-12 col-sm-8">
                    <div class="clearfix">
                        <input type="text" name="product_batch" class="col-xs-12 col-sm-12"/>
                    </div>
                </div>
            </div>           
        </div>

        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-1 no-padding-right" for="plant_type">
            	<input type="checkbox" name="process_type" value="WAREHOUSE_PROCESS"/>
            </label>
            <div class="col-xs-12 col-sm-11 form-group-pfm fs16">
                <div class="clearfix">
              	 流通环节
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group col-sm-6">
                <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="product_name">
                                                   流通批号
                </label>
                <div class="col-xs-12 col-sm-8">
                    <div class="clearfix">
                        <input type="text" name="flow_batch" class="col-xs-12 col-sm-12"/>
                    </div>
                </div>
            </div>        
        </div>

        
         <div class="form-group">
            <label class="control-label col-xs-12 col-sm-1 no-padding-right" for="product_level">
            	<input type="checkbox" name="process_type" value="SALE_PROCESS"/>
            </label>
            <div class="col-xs-12 col-sm-11 form-group-pfm fs16">
                <div class="clearfix">
                                                 销售环节
                </div>
            </div>
        </div>
       
    </div>

    <div class="modal-footer center">
        <button type="button" class="btn btn-sm btn-success copy-confirm"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>