<form id="productForm" class="form-horizontal" action="${contextPath}/admin/product/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if product.id??>
        <h5 class="blue">编辑产品</h5>
        <#else>
        <h5 class="blue">添加产品</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="product.id" id="product.id" value="${product.id!}"/>
		<input type="hidden" name="hasPV" id="hasPV" value="${hasPV!}"/>
 		<input type="hidden" name="plantBaseId" id="plantBaseId" value="${plantBaseId!}"/>
 		<input type="hidden" name="categoryId" id="categoryId" value="${categoryId!}"/>
 		<input type="hidden" name="productAuditflag" id="productAuditflag" value="${product.auditflag!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product_name"><font style="color:red;padding-right: 4px">*</font>产品名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="product.product_name" id="product_name" class="col-xs-12 col-sm-8 required" value="${product.product_name!}"/>
                </div>
            </div>
        </div>
        <input type="hidden" maxlength="100" readonly="readonly" name="product.product_name" id="product_name" class="col-xs-12 col-sm-8 required" value="${product.product_name!}"/>
        <div class="form-group"  id="unitdiv">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="unit_id"><font style="color:red;padding-right: 4px">*</font>单位:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                <select required class="col-xs-12 col-sm-8 required" id="unitid" name="unitid">
			               <option value="">---------------------请选择----------------------</option>
			               <#list unitList as unit>  
                        	<option value="${unit.id}" <#if (unit.id?? &&product.unit_id??) &&unit.id=product.unit_id>selected="selected"</#if>>${unit.name}</option>
                       	  </#list>
			        </select>
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="category_name"><font style="color:red;padding-right: 4px">*</font>种类名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <select class="col-xs-12 col-sm-8 required isEditable categoryclass" id="category_id" name="product.category_id">
                    	<option value="">---------------------请选择----------------------</option>
                	  	<#list categoryList as category>
                	  		<option value="${category.id!}" <#if (product.category_id?? && category.id??) && product.category_id = category.id>selected="selected"</#if>>${category.category_name!}</option>
                	  	</#list>
                    </select>
                    <input type="hidden" id="category_name" name="product.category_name" value="${product.category_name!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="plantbase_name"><font style="color:red;padding-right: 4px">*</font>基地:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                <select class="col-xs-12 col-sm-8" id="plantbase_name" name="product.plantbase_name" >
			               <option value="">---------------------请选择----------------------</option>
			               <#list plantBaseList as plantBase>
			              		<option value="${plantBase.id!}" <#if (product.plantbase_name?? && plantBase.base_name??) &&  plantBase.base_name = product.plantbase_name>selected="selected"</#if>>${plantBase.base_name!}</option>
			               </#list>
			    </select>
			    
			    <input type="hidden" id="plantbase_name_hidden" name="product.plantbase_name" value="${product.plantbase_id!}"/>
                 
                </div>
            </div>
        </div>
        <div class="form-group" style="display: none;" id="plantbasecheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">不存在该种类的基地，请先添加</font>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="plant_type"><font style="color:red;padding-right: 4px">*</font>种植方式:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                 <select class="col-xs-12 col-sm-4 required typeselect" id="plant_type" name="product.plant_type">
				        	<option value="-1">----请选择----</option>
				        	<#list dictList as dic>
				              	<option value="${dic.dict_name! }" <#if (dic.dict_name?? && product.plant_type??) &&  dic.dict_name = product.plant_type>selected="selected"</#if>>${dic.dict_name! }</option>
				            </#list>
				 </select> 
				 
				 <input name="product.plant_type" type="hidden" id="product.plant_type" class="col-xs-12 col-sm-4 required isaudit typeinput" value="${product.plant_type!}"/>
				 <input type="checkbox"  id="plant_typechebox" class="col-xs-12 col-sm-4" />
               
                </div>
            </div>
        </div>
        
         <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product_level"><font style="color:red;padding-right: 4px">*</font>等级:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                <select required class="col-xs-12 col-sm-8 required" name="product.product_level" id="product_level">
			               <option value="">---------------------请选择----------------------</option>
			               <#list dictListdj as dic>
			              		<option value="${dic.dict_name! }" <#if (dic.dict_name?? && product.product_level??) &&  dic.dict_name = product.product_level>selected="selected"</#if>>${dic.dict_name!}</option>
			               </#list>
			        </select>
                </div>
            </div>
        </div>
       
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="product_year"><font style="color:red;padding-right: 4px">*</font>年份:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="10" name="product.product_year"  data-date-format="YYYY" id="product_year" class="col-xs-12 col-sm-8 required date-picker" value="${product.product_year!}"/>
                </div>
            </div>
        </div>

    </div>

    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	var unit_cate_url=tfj_ContextPath+"/admin/product/duleunitcategory";
    	var cate_plant_url=tfj_ContextPath+"/admin/product/dulecategoryplantbase";
    	if($("#hasPV").val() == 'true'){
    		$(".isEditable").attr("disabled", "disabled");
    	}
    	//进入添加或修改界面，把单位、基地锁定
    	$("#category_id").prop("disabled","disabled");
    	$("#plantbase_name").prop("disabled","disabled");
    	//选择单位--->种类--->基地
     	$("#unitid").change(function(){
    		var unit_id=$(this).children('option:selected').val(); 
    		$.post(unit_cate_url,"unit_id="+unit_id, function(data){
				$("#category_id").removeAttr("disabled");
				$("#category_id").empty();
				var html = "";
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].id+">"
					+data[i].category_name
					+ "</option>"; 
					$("#category_id").html(html);
				}
				$("#category_name").val(data[0].category_name);//把种类赋值给隐藏域
				var category_id=$("#category_id").children('option:selected').val();
				postUrl(category_id,unit_id,cate_plant_url);
			});
     	});
    	////选择种类--->基地
    	$('#category_id').change(function(){
			var category_id=$(this).children('option:selected').val(); 
			var unit_id=$("#unitid").children('option:selected').val(); 
			postUrl(category_id,unit_id,cate_plant_url);
		});
    	//编辑
    	if($("input[name='product.id']").val()!=''){
    		
    		var unit_id=$("#unitid").children('option:selected').val(); 
    		$.post(unit_cate_url,"unit_id="+unit_id, function(data){
    			if($("#productAuditflag").val()=='0'){//审核、完成，锁定名称和种类
    				$("#category_id").removeAttr("disabled");
	        	}
				$("#category_id").empty();
				var html = "";
				for(var i=0;i<data.length;i++){
					html +="<option value="+data[i].id+">"
					+data[i].category_name
					+ "</option>"; 
					$("#category_id").html(html);
				}
				$("#category_id").find('option[value="'+$("#categoryId").val()+'"]').attr("selected",true);//选择当前种类
				var category_id=$("#category_id").children('option:selected').val();
				postUrl(category_id,unit_id,cate_plant_url);
			}); 
    		if($("#productAuditflag").val()!='0'){//审核、完成，锁定名称和种类
        		$("#product_name").prop("readOnly","readOnly");
        		$("#unitid").prop("disabled","disabled");
        	}
    		
    	}
    	

		
    	
        $("#productForm").validate({
           	errorElement: 'div',
           	errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            }, 
             success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            }, 
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent()); 
            },

            submitHandler: function (form) {
            	if($("#plantbasecheck").is(":hidden")==true){
              		form.submit();
          	 	}
            } ,
            invalidHandler: function (form) {
            } 
        });
        //选择种类，把值赋给种类隐藏域
        $("#category_id").change(function(){
        	$("#category_name").val($(this).find("option:selected").text());
        });
        //选择基地，把值赋给基地隐藏域
        $("#plantbase_name").change(function(){
        	$("#plantbase_name_hidden").val($(this).find("option:selected").val());
        });
        
      //勾选输入种植方式
    	$("#plant_typechebox").click(function(){
    		if($(this).is(':checked')){
    			$(".typeselect").hide();
    			//对select和input进行操作
    			$(".typeselect").removeAttr("name");
    			$(".typeinput").attr("name","product.plant_type");
    			
    			$(".typeinput").attr("type","text");
    		}else{
    			$(".typeselect").show();
    			//对select和input进行操作
    			$(".typeselect").attr("name","product.plant_type");
    			$(".typeinput").removeAttr("name");
    			
    			$(".typeinput").attr("type","hidden");
    		}
    	});
        
    });
    function postUrl(category_id,unit_id,cate_plant_url){
    	$.post(cate_plant_url,"category_id="+category_id+"&unit_id="+unit_id,function(data){
			$("#plantbase_name").removeAttr("disabled");
			$("#plantbase_name").empty();
			var html = "";
			for(var i=0;i<data.length;i++){
				html +="<option value="+data[i].id+">"
				+data[i].base_name
				+ "</option>"; 
			}
			$("#plantbase_name").html(html);
			if(data.length==0){
				$("#plantbasecheck").show();
			}else{
				$("#plantbase_name_hidden").val(data[0].id);//把种类赋值给隐藏域
				$("#plantbasecheck").hide();
			}
			$("#plantbase_name").find('option[value="'+$("#plantBaseId").val()+'"]').attr("selected",true);//选择当前基地
		});
    }
    function Disabled(){//所有节点disabled
      	 var treeObj = $.fn.zTree.getZTreeObj("cateTree");
           var nodes = treeObj.getNodes();//获取所有节点
           if(nodes.length>0){
    			for(var i=0;i<nodes.length;i++){//拼接字符串
    				treeObj.setChkDisabled(nodes[i], true);
   	 		}
    		} 
      }
</script>
