<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">产品基本信息管理</li>
</#assign>

<#assign pageCss>
	<!-- 
    <link href="${contextPath}/static/admin/assets/treeTable/themes/vsStyle/treeTable.min.css" rel="stylesheet" type="text/css" />
     -->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
</#assign>


<#assign pageJavascript>
	<script src="${contextPath}/static/admin/product/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
			window.tfj_ContextPath = '${contextPath}';
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
        	 // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定删除"+ $(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                //if(result) window.location.href = $(currentA).attr("href");
                                if(result){
                                	$.ajax({
                            			url:$(currentA).attr("href"),
                            			type:"post",
                            			data:"&&time="+new Date().getTime(),
                            			datatype:"text",
                            			success:function(data){
                            				if(data=='true'){
                            					pvAlert("该产品存在关联，不能删除", function() {  
                                                	window.location.href = window.location.href;
                                                }); 
                            				}else{
                            					window.location.href = window.location.href;
                            				}
                            			}
                            		});
                                }
                            	
                            }
                        }
                );
            });
        	
            // 释放按钮
            $("a.release").on(ace.click_event, function(e){
            	var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定释放"+ $(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "释放",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: { 
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
            
         // 强制删除按钮
            $("a.mandatory").on(ace.click_event, function(e){
            	var currentA = this;
                e.preventDefault();
                bootbox.confirm({
                            message: "确定强制删除"+ $(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "删除",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: { 
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
            
        	 
         	// 审核按钮
            $("a.audit").on(ace.click_event, function(e){
            	var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "审核后数据不能修改，确定审核"+ $(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "审核",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: { 
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
            
         	initSort("${sort_name}", "${sort_type}");
        });
        //################################## end of '$(document).ready()' ##################################
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-productFlag">
<div class="row">
    <div class="col-xs-12">
			<form id="productSearchForm" class="form-search form-inline" method="post">
			    <input id="sortTag" name="tag" type='hidden' value="${sort_name} ${sort_type}"/>
                <input id="page" name="page" type="hidden"/>
                <input id="page " name="pagesort" type="hidden" value="${page! }"/><!--排序时需要停留在当前页面  -->
                <label>产品名称:</label>
                <input type="text" id="productname" name="product.product_name" class="input-sm input-medium" value="${product.product_name! }"/>&nbsp;
                <label>种类名称:</label>
                <select name="product.category_id" class="input-sm input-medium">
                	<option value="-1">---请选择---</option>
                	<#list categoryList as category>
                		<option value="${category.id!}"<#if (category.id?? && product.category_id??) && category.id=product.category_id>selected="selected"</#if>>${category.category_name!}</option>
                	</#list>
                </select>
       			<label>审核状态:</label>  
       			 <select class="input-medium" id="type" name="product.auditflag">
       			 	<option value="-1">---请选择---</option>
       			 	<option value="0" <#if product.auditflag?? && product.auditflag = 0>selected="selected"</#if>>未审核</option>
       			 	<option value="1" <#if product.auditflag?? && product.auditflag = 1>selected="selected"</#if>>已审核</option>
       			 	<option value="2" <#if product.auditflag?? && product.auditflag = 2>selected="selected"</#if>>已完成</option>
                 </select>
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
                <div style="float: right;">
                <a href="${contextPath}/admin/product/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
                <!--  
                <@shiro.hasPermission name="admin_product_audit">
                <a href="javascript:audit();" class="btn btn-sm btn-warning"><i class="ace-icon fa fa-key"></i>审核</a>
                </@shiro.hasPermission>
                -->
                <a href="javascript:toDataInput();" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-key"></i>数据录入</a>
            	</div>
            </form>
            
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                	<th width="30" align="right" ><input  type="checkbox" onclick="selectAll(this)"/> </th>
                    <!--  <th width="40" align="right" >编号ID</th>-->
                    <th width="50" align="right" style="text-align: center;">
                                                                   序号
                    </th>
                    <th width="200" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="product_name"><span class="ace-icon fa fa-sort fa-lg bigger-200"></span></a>产品名称
                    </th>
                    <th width="250" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="category_name"><span class="ace-icon fa fa-sort bigger-200"></span></a>种类名称
                    </th>
                    <th width="250" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="plant_type"><span class="ace-icon fa fa-sort bigger-200"></span></a>种植方式
                    </th>
                    <th width="100" align="left" style="text-align: center;">
                      	等级
                    </th>
                    <th width="100" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="product_year"><span class="ace-icon fa fa-sort bigger-200"></span></a>年份
                    </th>
                    <th width="250" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="plantbase_name"><span class="ace-icon fa fa-sort bigger-200"></span></a>基地
                    </th>
                    <th width="150" align="left" style="text-align: center;">
                      <a href="#" class="sort-column" name="auditflag"><span class="ace-icon fa fa-sort bigger-200"></span></a>审核状态
                    </th>
                    <th width="180" align="center" style="text-align: center;">
                                                                    操作
                    </th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as product>
                    <tr>
                    	
                    	<td>
                    		<input name="subBox" value="${product.id!}" audit="${product.auditflag!}" type="checkbox"/>
                    	</td>
                    	<!--  
                        <td>${product.id!}</td>-->
                        <td>${product_index+1}
                        </td>
                        <td>
                        	<#if product.product_name?length lt 10>
	                      	 <a  title="${product.product_name!}">${product.product_name!}</a> 
	                        <#else>
	                         	 <a title="${product.product_name!}">${(product.product_name[0..9])?default("")}......</a> 
							</#if>
                        </td>
                        <td><input type="hidden" class="cidHidden" value="${product.category_id!}"/>${product.category_name!}
                        </td>
                        <td>${product.plant_type! }</td>
                        <td>${product.product_level! }</td>
                        <td>${product.product_year! }</td>
                        <td >
	                        <#if product.plantbase_name?length lt 10>
	                      	 <a  title="${product.plantbase_name!}">${product.plantbase_name!}</a> 
	                        <#else>
	                         	 <a title="${product.plantbase_name!}">${(product.plantbase_name[0..9])?default("")}......</a> 
							</#if> 
                        </td> 
                        <td>
                        <input type="hidden" class="product_auditflag" value="${product.auditflag!}"/>
                        ${product.auditflagName!}
                        </td> 
                        <td>                               	
                        	<div class="hidden-sm hidden-xs action-buttons">
                        		
                                <a class="green tooltip-success edit" href="${contextPath}/admin/product/view?id=${product.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                 
                                <@shiro.hasPermission name="admin_product_audit">
                                <!--
                                <#if (product.auditflag?? && product.auditflag = 0)>
                                  <a class="red tooltip-error audit" href="${contextPath}/admin/product/audit?rid=${product.id!}" name="${product.product_name!}"  data-rel="tooltip" title="审核"> <i class="ace-icon fa fa-key bigger-130"></i></a>
                                </#if>
                                 -->
                                <a class="red tooltip-success copy" href="${contextPath}/admin/product/copyView?id=${product.id!}" name="${product.product_name!}" data-rel="tooltip" title="复制" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-copy bigger-130"></i></a>
                                 
                                <#if (product.auditflag?? && product.auditflag = 0)>
                                  <a class="red tooltip-error delete" href="${contextPath}/admin/product/delete?ids=${product.id!}" name="${product.product_name!}" data-rel="tooltip" title="删除"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                                </#if>
                                
                                </@shiro.hasPermission>
                                <#if session['adminType'] == 'true' && product.auditflag?? && product.auditflag != 0>
                            	<a class="orange tooltip-error release" href="${contextPath}/admin/product/release?ids=${product.id!}" name="${product.product_name!}" data-rel="tooltip" title="释放"> <i class="ace-icon fa fa-leaf bigger-130"></i></a>
                            	</#if>
                            	
                            	<#if session['adminType'] == 'true'>
                            	<a class="orange tooltip-error mandatory" href="${contextPath}/admin/product/mandatorydelete?ids=${product.id!}" name="${product.product_name!}" data-rel="tooltip" title="强制删除"> <i class="ace-icon fa fa-bolt bigger-130"></i></a>
                            	</#if>
                            	
                            	
                            </div>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
<div id="jconfirmDemo"></div>
</@mainlayout>
