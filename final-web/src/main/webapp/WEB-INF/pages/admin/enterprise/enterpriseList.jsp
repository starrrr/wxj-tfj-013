<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">企业绑定号管理</li>
</#assign>

<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
</#assign>


<#assign pageJavascript>
	<script src="${contextPath}/static/admin/enbind/common.js"></script>
    <script type="text/javascript">
    
    $(document).ready(function(){
    	
    	 // 清除远程modal数据
        $("#editModal").on("hidden.bs.modal", function(){
            $(this).removeData("bs.modal");
           // window.location.href=window.location.href;
        });
    	 
        // 删除按钮
        $("a.delete").on(ace.click_event, function(e){
            var currentA = this;
            e.preventDefault();
            bootbox.confirm({
                        message: "确定删除"+$(currentA).attr('name')+"？",
                        buttons: {
                            confirm: {
                                label: "确定",
                                className: "btn-primary btn-sm"
                            },
                            cancel: {
                                label: "取消",
                                className: "btn-sm btn-cancel"
                            }
                        },
                        callback: function(result) {
                           	if(result){
                           		window.location.href=currentA;
                           	}
                        }
                    }
            );
        });
    	 
    	 
    	//判断非法字符
        $("#search").click(function(){
    		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
               if($("#company_user").val().trim() != ""){  
                   if(pattern.test($("#company_user").val())){  
                	 bootbox.alert({  
                           buttons: {  
                              ok: {  
                                   label: '确定',  
                                   className: 'btn-primary btn-sm'  
                               }  
                           },  
                           message: '存在非法字符，请重新输入查询条件。',  
                           callback: function() {  
                        		$("#company_user").val("");
                           },  
                       });  
           			 
                   }else {
                   	$("#search").attr("type","submit");
				 }
              }else {
            	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		});
        
    });
	
        
    function page(pageNum) {
        $("#page").val(pageNum);
        $("#enterpriseSearchForm").submit();
    }
        
     	
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-enterprise">
<div class="row">
    <div class="col-xs-12">

			<form id="enterpriseSearchForm" class="form-search form-inline" method="post">
                <input id="page" name="page" type="hidden"/>
                <label>企业用户名:</label>
                <input type="text" id="company_user" name="enterprise.company_user" class="input-sm input-medium" value="${enterprise.company_user! }"/>&nbsp;
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>&nbsp;
                <div style="float: right;">
                <a href="${contextPath}/admin/enterprise/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
            	</div>
            </form>
            <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <!--  <th width="40" align="right" >编号ID</th>-->
                    <th width="50" align="right" style="text-align: center;"  >序号</th>
                    <th width="150" align="left" style="text-align: center;"  >企业用户名</th>
                    <th width="150" align="left" style="text-align: center;"  >账号名</th>
                    <th width="150" align="left" style="text-align: center;"  >电话</th>
                    <th width="200" align="left" style="text-align: center;"  >地址</th>
                    <th width="250" align="left" style="text-align: center;"  >公司或个人主页</th>
                    <th width="100" align="left" style="text-align: center;"  >企业个人</th>
                    <th width="200" align="center" style="text-align: center;"  >操作</th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as enterprise>
                    <tr>
                        <td>${enterprise_index+1}</td>
                       	<td>${enterprise.company_user!}</td>
                        <td>${enterprise.name!}</td>
                        <td>${enterprise.telephone!}</td>
                        <td>${enterprise.address! }</td>
                        <td>${enterprise.website! }</td>
                        <td>
	                        <input type="hidden" value="${enterprise.is_company!}"/>
	                        <#if (enterprise.is_company?? && enterprise.is_company = 0)> 个人</#if>
	                        <#if (enterprise.is_company?? && enterprise.is_company = 1)> 企业</#if>
                        </td>
                        <td>    
                        	<a class="green tooltip-success manage" href="${contextPath}/admin/enterprise/manage?id=${enterprise.id!}&company_user=${enterprise.company_user!}" data-rel="tooltip" title="管理" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-book bigger-130"></i></a>
                            <a class="red tooltip-error delete" href="${contextPath}/admin/enterprise/delete?ids=${enterprise.id!}" name="${enterprise.name!}" data-rel="tooltip" title="删除"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                        	<a class="green tooltip-success edit" href="${contextPath}/admin/enterprise/view?id=${enterprise.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                        	<a class="green tooltip-success reset" href="${contextPath}/admin/enterprise/resetpasswordview?id=${enterprise.id!}" data-rel="tooltip" title="密码重置" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-slideshare light-orange bigger-130"></i></a>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>
 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
<div id="jconfirmDemo"></div>
</@mainlayout>
