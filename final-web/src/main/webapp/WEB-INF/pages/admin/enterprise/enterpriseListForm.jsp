<form id="enterpriseForm" class="form-horizontal" action="${contextPath}/admin/enterprise/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if enterprise.id??>
        <h5 class="blue">编辑企业用户</h5>
        <#else>
        <h5 class="blue">添加企业用户</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="enterprise.id" id="enterprise.id" value="${enterprise.id!}"/>
        <input type="hidden" id="checkId" value="${enterprise.id!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="company_user"><font style="color:red;padding-right: 4px">*</font>企业用户名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <#if enterprise.id??>
       					<input type="text" readonly="readonly" maxlength="100" name="enterprise.company_user" id="company_user" class="col-xs-12 col-sm-8 required isaudit" value="${enterprise.company_user!}"/>
      			 	<#else>
        				<input type="text" maxlength="100" name="enterprise.company_user" id="company_user" class="col-xs-12 col-sm-8 required isaudit" value="${enterprise.company_user!}"/>
       				</#if>
                </div>
            </div>
        </div>
        <div class="form-group" style="display: none;" id="usercheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">该用户名已注册过，请更换用户名</font>
                </div>
            </div>
       </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="company_pwd"><font style="color:red;padding-right: 4px">*</font>登录密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password" maxlength="100" name="enterprise.company_pwd" id="company_pwd" class="col-xs-12 col-sm-8 required isaudit" value="${enterprise.company_pwd!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for=""><font style="color:red;padding-right: 4px">*</font>确定登录密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password" maxlength="100" name="" id="repeatpassword" class="col-xs-12 col-sm-8 required isaudit" value="${enterprise.company_pwd!}"/>
                </div>
            </div>
        </div>
         <div class="form-group" style="display: none;" id="prompt">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">两次输入密码不一致</font>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"><font style="color:red;padding-right: 4px">*</font>名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="enterprise.name" id="name" class="col-xs-12 col-sm-8 required isaudit" value="${enterprise.name!}"/>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="telephone">电话:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="enterprise.telephone" id="telephone" class="col-xs-12 col-sm-8 " value="${enterprise.telephone!}"/>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="address">地址:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="enterprise.address" id="address" class="col-xs-12 col-sm-8 " value="${enterprise.address!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="website">公司或个人主页:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="100" name="enterprise.website" id="website" class="col-xs-12 col-sm-8 " value="${enterprise.website!}"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right"><font style="color:red;padding-right: 4px">*</font>企业个人:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <label class="radio-inline blue"><input name="enterprise.is_company" value="0" type="radio" class="ace" <#if !enterprise.is_company?? || (enterprise.is_company?? && enterprise.is_company == 0)>checked="checked"</#if>/><span class="lbl">个人</span></label>
                    <label class="radio-inline blue"><input name="enterprise.is_company" value="1" type="radio" class="ace" <#if enterprise.is_company?? && enterprise.is_company == 1>checked="checked"</#if>/><span class="lbl">企业</span></label>
                </div>
            </div>
        </div>
    </div>
        
	
    <div class="modal-footer center">
    	<button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>

<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	if($("#checkId").val()==''){//编辑时id不为空
	    	$("#company_user").blur(function(){
	    		if($("#company_user").val()!=''){
	    			$.ajax({
	        			url:"${contextPath}/admin/enterprise/companyUserName",
	        			type:"post",
	        			data:getdata(),
	        			datatype:"text",
	        			success:function(data){
	        				if(data=='true'){
	        					$("#usercheck").show();
	        				}else{
	        					$("#usercheck").hide();
	        				}
	        			} 
	        		});
	    		}
	    	}); 
	    	function getdata(){
	    		var params="time="+new Date().getTime();
	    		var company_user=$("#company_user").val();
	    		if(company_user){
	    			params+="&&company_user="+company_user;
	    		}
	    		return params;
	    	}
    	}
    	$("#repeatpassword").blur(function(){
    		if($("#company_pwd").val()!=''){
        		if($("#company_pwd").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else {
        			$("#prompt").hide();
				}
        	}
		});
    	$("#company_pwd").blur(function(){
    		if($("#repeatpassword").val()!=''){
        		if($("#company_pwd").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else{
        			$("#prompt").hide();
        		}
        	}
		});
    		

        $("#enterpriseForm").validate({
           	errorElement: 'div',
           	errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            }, 
             success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            }, 
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent()); 
            },

            submitHandler: function (form) {
            	if($("#prompt").is(":hidden")==true&&$("#usercheck").is(":hidden")==true){
            		form.submit();
            	}
            } ,
            invalidHandler: function (form) {
            } 
        });
        
    });
</script>
