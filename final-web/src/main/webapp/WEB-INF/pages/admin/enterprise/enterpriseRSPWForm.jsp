<form id="pwForm" class="form-horizontal" action="${contextPath}/admin/enterprise/resetpassword" method="post">
    <div class="modal-header">
		<h5 class="blue">密码重置</h5>
    </div>
    <div class="modal-body">
    	<input hidden="hidden" name="enterprise.id" id="id" value="${id!}">
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="resetpassword"><font style="color:red;padding-right: 4px">*</font>重置密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password"  maxlength="100" name="enterprise.company_pwd" id="resetpassword" class="col-xs-12 col-sm-8 required isaudit"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px">*</font>确认密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password" maxlength="100" name="repeatpassword" id="repeatpassword" class="col-xs-12 col-sm-8 required isaudit" />
                </div>
            </div>
        </div>
         <div class="form-group" style="display: none;" id="prompt">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">两次输入密码不一致</font>
                </div>
            </div>
        </div>
    </div>
     <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	$("#resetpassword").blur(function(){
    		if($("#repeatpassword").val()!=''){
        		if($("#resetpassword").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else {
        			$("#prompt").hide();
				}
        	}
    	}); 
    	$("#repeatpassword").blur(function(){
    		if($("#resetpassword").val()!=''){
        		if($("#resetpassword").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else {
        			$("#prompt").hide();
				}
        	}
		});
        $("#pwForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
            	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            	if($("#prompt").is(":hidden")==true){
            		form.submit();
            	}
            },
            invalidHandler: function (form) {
            }
        });
    });
</script>