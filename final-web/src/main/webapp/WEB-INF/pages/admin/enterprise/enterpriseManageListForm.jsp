<#include "../common/__paginationabove.jsp">
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/enbind/common.js"></script>
<form id="enterpriseSearchForm" class="form-horizontal" action="" method="post"><!--${contextPath}/admin/enterprise  -->
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="blue">企业用户[<font size="5px" color="black">${user }</font>]列表</h5>
    </div>


    <div class="modal-body">
        
        	 <input id="page" name="page" type="hidden"/>
                
             <label>绑定用户:</label>
             <input type="text" id="company_user" name="company_user" class="input-sm input-large" value="" style="width:190px;margin-bottom:10px;"/>&nbsp;
             <input type="button" id="" class="btn btn-sm btn-primary" onclick="findEMPage()" value="查询"/>&nbsp;
              
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
               		<!--
               		<th width="30" align="right" ><input type="checkbox" onclick="selectAll(this)"/></th>
               		-->
                    <th width="100" align="right" style="text-align: center;"  >序号</th>
                	<th width="150" align="left" style="text-align: center;"  >绑定用户</th>
                	<th width="100" align="left" style="text-align: center;"  >性别</th>
                    <th width="100" align="left" style="text-align: center;"  >省</th>
                    <th width="100" align="left" style="text-align: center;" >城市</th>
                    <th width="100" align="left" style="text-align: center;"  >国家</th>
                	<th width="100" align="left" style="text-align: center;"  >角色</th>
                	<th width="100" align="left" style="text-align: center;"  >OpenId</th>
                	<th width="100" align="center" style="text-align: center;"  >操作</th>
                </tr>
                </thead>
                <tbody>
               	<#list pageList.list as enterprise>
                    <tr>
                    	<!--
                        <td>
                    		<input name="subBox" value="${enterprise.id!}" type="checkbox"/>
                    	</td>
                    	-->
						<td>${enterprise_index+1}<input type="hidden" id="companyuser" name="companyuser" value="${enterprise.company_user!}"/></td>
                        <td>${enterprise.nickname!}</td>
                        <td>
                        	 <#if (enterprise.sex?? && enterprise.sex=1)> 男</#if>
                      	 	 <#if (enterprise.sex?? && enterprise.sex=2)> 女</#if>
                        	 <#if (enterprise.sex?? && enterprise.sex=0)> 未知</#if>
                        </td>
                        <td>${enterprise.province!}</td>
                        <td>${enterprise.city!}</td>
                        <td>${enterprise.country!}</td>
                        <td>${enterprise.role_name!}</td>
                        <td>
                        	<#if enterprise.openid?length lt 10>
	                      	 <a  title="${enterprise.openid!}">${enterprise.openid!}</a> 
	                        <#else>
	                         	 <a title="${enterprise.openid!}">${(enterprise.openid[0..9])?default("")}......</a> 
							</#if>
                        </td>
						<td>    
                            <a class="red tooltip-error removebind" href="${contextPath}/admin/enterprise/removeBind?openid=${enterprise.openid!}&id=${enterprise.id!}" name="${enterprise.company_user!}" data-rel="tooltip" title="解绑"> <i class="ace-icon fa fa-key bigger-130"></i></a>
                        </td>
                    </tr>
            	 </#list>
                </tbody>
            </table>
        <@paginationabove pageList!/>
    </div>
    
   	<div class="modal-footer" style="margin-top:10px;">
    </div>

</form>
<script type="text/javascript">
function pageabove(pageNum) {
	var remote = pvMark;
	$("#editModal").find('.modal-content').load(remote + "&page=" + pageNum+ "&time="+new Date().getTime());
} 

    $(document).ready(function(){
    	
    	$("#removebind").on(ace.click_event, function(e){
    		alert('sss');
    		var currentA = this;
            e.preventDefault();
            bootbox.confirm({
                        message: "确定解绑"+ $(currentA).attr('name')+"？",
                        buttons: {
                            confirm: {
                                label: "解绑",
                                className: "btn-primary btn-sm"
                            },
                            cancel: { 
                                label: "取消",
                                className: "btn-sm btn-cancel"
                            }
                        },
                        callback: function(result) {
                            if(result) window.location.href = $(currentA).attr("href");
                        }
                    }
            );
    		
    	});
    	 //判断非法字符
        $("#search").click(function(){
    		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
               if($("#company_user").val().trim() != ""){  
                   if(pattern.test($("#company_user").val())){  
                	 bootbox.alert({  
                           buttons: {  
                              ok: {  
                                   label: '确定',  
                                   className: 'btn-primary btn-sm'  
                               }  
                           },  
                           message: '存在非法字符，请重新输入查询条件。',  
                           callback: function() {  
                        		$("#company_user").val("");
                           },  
                       });  
           			 
                   }else {
                   	$("#search").attr("type","submit");
				 }
              }else {
            	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		});
    });
    
    
</script>
