<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
			<span class="bigger-120"><span class="blue bolder">${pageTitle}</span>
                &copy; 2015-2015</span> &nbsp; &nbsp; <span class="action-buttons">
                <!-- 
                <a target="_blank" href="http://weibo.com"> <i class="ace-icon fa fa-weibo red bigger-120"></i></a>
                <a target="_blank" href="http://im.qq.com"> <i class="ace-icon fa fa-qq red bigger-120"></i></a>
                <a target="_blank" href="http://mp.weixin.qq.com"> <i class="ace-icon fa fa-weixin green bigger-120"></i></a>
				 -->
			</span>
        </div>
        <!-- /section:basics/footer -->
    </div>
</div>
<a href="#" id="btn-scroll-up"
   class="btn-scroll-up btn btn-sm btn-inverse"> <i
        class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>