<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ui.jqgrid.css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/js/jquery.css"/>
</#assign>
<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	// 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
        });
    </script>
</#assign>
<li class="light-blue"><a data-toggle="dropdown" href="#"
                          class="dropdown-toggle"> 
    <span
            class="user-info"> <small>欢迎你,</small> ${session['loginUser'].username}
	</span> <i class="ace-icon fa fa-caret-down"></i>
</a>

    <ul
            class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
        <li><a href="${contextPath}/admin/user/pwView?id=pw_id" class="" data-toggle="modal" data-target="#editModal"> <i class="ace-icon fa fa-cog"></i> 更改密码
        </a></li>

        <li><a href="${contextPath}/admin/user/pwView?id=ms_id" class="" data-toggle="modal" data-target="#editModal"> <i class="ace-icon fa fa-user"></i>
           	 用户信息
        </a></li>
		<!-- 
        <li class="divider"></li>

        <li><a href="${contextPath}/admin/logout"> <i class="ace-icon fa fa-power-off"></i>
            退出
        </a></li>
         -->
    </ul>
</li>
<div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>