<script type="text/javascript">
function toProductValue() {
	var category_id;
	<#if session.category_id??>
		category_id = ${session["category_id"]};
	<#else>
	var category_id = "";
	</#if>
	if (category_id == "") {
		bootbox.confirm({
			message: "请在“产品基础信息管理”勾选一个产品，点击【数据录入】按钮，进入产品数据录入页面进行数据录入。",
			buttons: {
				confirm: {
                    label: "确定",
                    className: "btn-primary btn-sm"
                }, 
                cancel: { 
                    label: "取消",
                    className: "btn-sm btn-cancel"
                }
            },
			callback: function(result) {
				if (result) {
					location.href = "${contextPath}/admin/product";
				}
            }
        });
	} else {
		location.href = "${contextPath}/admin/productValue?recordlog=recordlog";
	}
}
</script>

<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts" style="display: none">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<!-- #section:basics/sidebar.layout.shortcuts -->
						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					
					 <li onclick="location.href='${contextPath}/admin'" class="" id="indexFlag">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-home blue"></i>
                            <span class="menu-text">首页</span>
                        </a>
                        <b class="arrow"></b>
                        <!-- 
                        <ul class="submenu">
                            <li class="" id="pageFlag">
                                <a href="${contextPath}/admin/view">
                                    <i class="menu-icon fa fa-user blue"></i>
                                    	概要图
                                </a>
                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>
					 -->
					<@shiro.hasPermission name="product_admin">
                    <li class="" id="userRoleFlag">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-users green"></i>
                            <span class="menu-text">产品管理</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                        <@shiro.hasPermission name="product_admin_category">
         					<li class="" id="categoryFlag">
                                <a href="${contextPath}/admin/category?recordlog=recordlog">
                                    <i class="menu-icon fa fa-user light-green"></i>
                                    		种类管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                             </@shiro.hasPermission>
                            <@shiro.hasPermission name="product_admin_plantbase">
                            <li class="" id="planBaseFlag">
                                <a href="${contextPath}/admin/plantbase?recordlog=recordlog">
                                    <i class="menu-icon fa fa-leaf light-green"></i>
                                    		基地建设
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission>
                             <@shiro.hasPermission name="product_admin_productResult">
                            <li class="" id="qrCodeFlag">
                                <a href="${contextPath}/admin/productResult?recordlog=recordlog">
                                    <i class="menu-icon fa fa-key light-blue"></i>
                                    		二维码生成
                                </a>
                                <b class="arrow"></b>
                            </li>
                             </@shiro.hasPermission>   
                            <@shiro.hasPermission name="product_admin_product">
                            <li class="" id="productFlag">
                                <a href="${contextPath}/admin/product?recordlog=recordlog">
                                    <i class="menu-icon fa fa-list-alt light-blue"></i>
                                    		产品基础信息管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission>   
                            <@shiro.hasPermission name="product_admin_template">
                            <li class="" id="templateFlag">
                                <a href="${contextPath}/admin/template?recordlog=recordlog">
                                    <i class="menu-icon fa fa-desktop light-red"></i>
                                    		种类模板管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission>   
                            <@shiro.hasPermission name="product_admin_productValue">
                             <li class="" id="productValueFlag">
                                <a href="javascript:toProductValue()">
                                    <i class="menu-icon fa fa-gift light-blue"></i>
                                    		产品数据录入
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission>    
                            <!--  
                            <@shiro.hasPermission name="product_admin_templateAuth">   
                            <li class="" id="authorizationFlag">
                                <a href="#">
                                    <i class="menu-icon fa fa-eye light-orange"></i>
                                    		模板权限管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission> 
                            -->   
                            <@shiro.hasPermission name="product_admin_wechatlog">   
                            <li class="" id="weChatScan">
                                <a href="${contextPath}/admin/wechat?recordlog=recordlog">
                                    <i class="menu-icon fa fa-tachometer light-green"></i>
                                    		微信扫描记录
                                </a>
                                <b class="arrow"></b>
                            </li>
                            </@shiro.hasPermission>   

                            <li class="" id="weChatOpenId">
                                <a href="${contextPath}/admin/wechatopenid?recordlog=recordlog">
                                    <i class="menu-icon fa fa-coffee light-pink"></i>
                                    		微信用户管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                            
                              
                              <!-- 
                              <li class="" id="weChatRole">
                                <a href="${contextPath}/admin/wechatrole?recordlog=recordlog">
                                    <i class="menu-icon fa fa-heart light-red"></i>
                                    		微信角色管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                     		
                            <@shiro.hasPermission name="product_admin_wechatrole">   
                             </@shiro.hasPermission>  
                             <@shiro.hasPermission name="product_admin_wechatopenid">
                              </@shiro.hasPermission>   
                           
                            <li class="" id="enterprise">
                         
                                <a href="${contextPath}/admin/enterprise?recordlog=recordlog">
                                    <i class="menu-icon fa fa-certificate light-green"></i>
                                    		企业绑定号管理
                                </a>
                                <b class="arrow"></b>
                            </li> 
                            
                             -->
                             <@shiro.hasPermission name="product_admin_unit"></@shiro.hasPermission> 
                             <li class="" id="unit">
                                <a href="${contextPath}/admin/unit?recordlog=recordlog">
                                    <i class="menu-icon fa fa-certificate light-green"></i>
                                    		单位管理
                                </a>
                                <b class="arrow"></b>
                            </li>    
                                     
                        </ul>
                    </li>
					</@shiro.hasPermission>
				 <@shiro.hasPermission name="system">
                    <li class="" id="systemFlag">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-cogs grey"></i>
                            <span class="menu-text">系统管理</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                   <@shiro.hasPermission name="auth_admin">     
                            <li class="" id="userFlag">
                                <a href="${contextPath}/admin/user?recordlog=recordlog">
                                    <i class="menu-icon fa fa-user light-blue"></i>
                                    用户管理
                                </a>
                                <b class="arrow"></b>
                            </li>
                   </@shiro.hasPermission>             
                            
			<@shiro.hasPermission name="auth_role">
                            <li class="" id="roleFlag">
                                <a href="${contextPath}/admin/role?recordlog=recordlog">
                                    <i class="menu-icon fa fa-slideshare light-orange"></i>
                                    角色管理
                                </a>
                                <b class="arrow"></b>
                            </li>
             </@shiro.hasPermission>  
                      
                        <@shiro.hasPermission name="system_menu">
                            <li class="" id="menurescFlag">
                                <a href="${contextPath}/admin/menuresc?recordlog=recordlog">
                                    <i class="menu-icon fa fa-list-alt light-blue"></i>
                                    菜单资源
                                </a>
                                <b class="arrow"></b>
                            </li>
		     	</@shiro.hasPermission>
			
			     	
				<@shiro.hasPermission name="system_dict">
				   <li class="" id="dictFlag">
                                <a href="${contextPath}/admin/dict?recordlog=recordlog">
                                    <i class="menu-icon fa fa-book light-green"></i>
                                    数据字典
                                </a>
                                <b class="arrow"></b>
                   </li>   
				</@shiro.hasPermission>
				   
			
			
                            
		<@shiro.hasPermission name="system_loginlog">
                            <li class="" id="loginLogFlag">
                                <a href="${contextPath}/admin/loginLog?recordlog=recordlog">
                                    <i class="menu-icon fa fa-pencil-square-o light-red"></i>
                                    登录日志
                                </a>
                                <b class="arrow"></b>
                            </li>
        </@shiro.hasPermission>  
        <@shiro.hasPermission name="system_operationlog">
                            <li class="" id="operationLogFlag">
                                <a href="${contextPath}/admin/operationlog?recordlog=recordlog">
                                    <i class="menu-icon fa fa-pencil-square-o light-red"></i>
                                    操作日志
                                </a>
                                <b class="arrow"></b>
                            </li>
        </@shiro.hasPermission>  
<!-- 
                            <li class="" id="dbFlag">
                                <a href="${contextPath}/admin/druid/" target="_blank">
                                    <i class="menu-icon fa fa-database dark"></i>
                                    数据库监控
                                </a>
                                <b class="arrow"></b>
                            </li>
-->
                        </ul>
                    </li>
                  </@shiro.hasPermission>        
				</ul><!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div/>
