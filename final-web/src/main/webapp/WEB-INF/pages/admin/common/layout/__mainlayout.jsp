<#macro mainlayout pageTitle="${systemConfig['webName']}" pageCss="" pageJavascript="" pageBreadCrumbs="" currentMenu="">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta charset="utf-8"/>
        <title>${pageTitle}</title>
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
        <!-- Css BEGAIN -->
        <#include "../inc/_css.jsp">
            <!-- Css END -->
    </head>

    <body class="no-skin">

    <!-- navbar BEGAIN -->
    <#include "../inc/_navbar.jsp">
        <!-- navbar END-->


        <!-- main-container BEGAIN-->
        <div class="main-container" id="main-container">

            <!-- sidebar BEGAIN -->
            <#include "../inc/_sidebar.jsp">
                <!-- sidebar END -->

                <!-- maincontent BEGAIN-->
                <div class="main-content">
                    <div class="main-content-inner">

                        <!-- breadcrumbs BEGAIN -->
                        <#include "../inc/_breadcrumbs.jsp">
                            <!-- breadcrumbs END -->

                            <!-- pagecontent  BEGAIN-->
                            <div class="page-content">
                                <!-- settingbox  BEGAIN -->
                                <#comment>
                                <#include "../inc/_settingbox.jsp">
                                    </#comment>
                                    <!-- settingbox END -->

                                    <!-- PAGE CONTENT BEGINS -->
                                    <#nested>
                                        <!-- PAGE CONTENT ENDS -->
                            </div>
                            <!-- pagecontent  END-->
                    </div>
                </div>
                <!-- maincontent END-->

                <!-- maincontent FOOT-->
                <#include "../inc/_footer.jsp">
                    <!-- maincontent END-->
        </div>
        <!-- main-container END-->

        <!-- JavaScript BEGAIN -->
        <#include "../inc/_js.jsp">
            <!-- JavaScript END -->
    </body>
    </html>
</#macro>