
<div class="col-xs-8 col-sm-8">
     <div class="widget-box widget-color-green">
         <div class="widget-header widget-header-small">
         	   <font id="title" name="title" color="black" size="5px"></font>
			   <button type="button" onclick="deleteNode('SUPERVISE_PROCESS')" class="btn btn-sm btn-success" ><i class="ace-icon fa fa-times"></i>删除节点</button>
			   <button type="button" onclick="addNode('SUPERVISE_PROCESS')" class="btn btn-sm btn-success" ><i class="ace-icon fa fa-check"></i>新增节点</button>
         </div>
         
		 <form id="SUPERVISE_PROCESS_templateForm" action="" method="post" enctype="multipart/form-data">
         <div class="widget-body">
         
          		 <input type="hidden" name="template.id" id="id_SUPERVISE_PROCESS" value=""/>
 				 <input type="hidden" name="template.pid" id="pid_SUPERVISE_PROCESS" value=""/>
		         <input type="hidden" name="template.category_id" id="category_id_SUPERVISE_PROCESS" value=""/>
		         <input type="hidden" name="template.levelNum" id="levelNum_SUPERVISE_PROCESS" value=""/>
		         <input type="hidden" name="template.processType" id="processType_SUPERVISE_PROCESS" value=""/>
		         
		         <div class="modal-body left">
	
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="key_name"><font style="color:red;padding-right: 4px">*</font>名称:</label>
					            <div class="clearfix">
					            	 <input type="text" name="template.key_name" maxlength="20" id="key_name_SUPERVISE_PROCESS" value=""/>
					           		 <input type="button" id="superviseimg" class="btn btn-sm btn-primary" value="图片" style="display: none;">
					            </div>
					        </div>
					        
					         <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="sort">排序:</label>
					            <div class="clearfix">
									 <input type="text" name="template.sort" id="sort_SUPERVISE_PROCESS" class="required" value="" min="1"  disabled="disabled"/>
					            </div>
					        </div>
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for=""></label>
					            <div class="clearfix">
					            	<font color="red">提示：图片大小小于5K，格式：gif,icon,png,jpeg,jpg。</font>
					            </div>
					        </div>
							<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="icon">小图标:</label>
					            <div class="clearfix">
					            	 <input id="icon_SUPERVISE_PROCESS" type="file" class="superviseUploadFile superviseUploadImg" name="template.icon" size="50">
					            </div>
					        </div>
							<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="icon">小图标效果图:</label>
					         	<div class="clearfix">
					            	 <img id="icon_SUPERVISE_PROCESS_SHOW" alt="" width="" height="" src="">
					            </div>
					        </div>  
							<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="field_type"><font style="color:red;padding-right: 4px">*</font>字段属性:</label>
					            <div class="clearfix">
					            	<select class="input-medium superviseInputselector" id="field_type_SUPERVISE_PROCESS" name="template.field_type">
					                	  	<option value="-1">---请选择---</option>
					                   		<#list dictList as dict>
					                        <option value="${dict.dict_name!}">${dict.dict_name!}</option>
					                    	</#list>
					                </select>
					            	<input name="supervisemix" type="checkbox" class="ace"/><span id="supervisemix" class="lbl blue" style="display: none;">混批</span>
					            </div>
					        </div>
					        
					        <div class="form-group field_develop_hidden_supervise">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="field_develop_SUPERVISE_PROCESS">字段拓展:</label>
					            <div class="clearfix">
					            	 <input id="field_develop_SUPERVISE_PROCESS" type="text" maxlength="40" name="template.field_develop" size="50">
					           		 <select class="input-medium supervise_dic_menu"  name="template.field_type" style="display: none;">
					                 </select>
					            </div>
					        </div>
					        <!-- 
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="default_value_WAREHOUSE_PROCESS">默认值:</label>
					            <div class="clearfix">
					            	 <input id="default_value_SUPERVISE_PROCESS" type="text" name="template.default_value" size="50">
					            </div>
					        </div>
					         -->
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="index_show">主页显示:</label>
					            <div class="clearfix">
					            		<label class="radio-inline blue"><input name="index_show_SUPERVISE_PROCESS" value="0" type="radio" class="ace"/><span class="lbl">禁用</span></label>
					            	    <label class="radio-inline blue"><input name="index_show_SUPERVISE_PROCESS" value="1" type="radio" class="ace" /><span class="lbl">启用</span></label>
					            </div>
					        </div>
					        
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="process_show_SUPERVISE_PROCESS">环节显示:</label>
					            <div class="clearfix">
					            	 <input  id="process_show_SUPERVISE_PROCESS" type="hidden" readonly="readonly" value="B_0:C_0:G_0" size="15">
					            	 <label class="checkbox inline blue"><input id="superviseB" value="" type="checkbox" class="ace"/><span class="lbl">企业用户</span></label>
					            	 <label class="checkbox inline blue"><input id="superviseC" value="" type="checkbox" class="ace" /><span class="lbl">关注用户</span></label>
					            	 <label class="checkbox inline blue"><input id="superviseG" value="" type="checkbox" class="ace" /><span class="lbl">游客</span></label>
					            </div>
					        </div>
	 						<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="remark_SUPERVISE_PROCESS">备注:</label>
					            <div class="clearfix">
					            		   <input type="text"  id="remark_SUPERVISE_PROCESS" maxlength="100" class="col-xs-12 col-sm-4 required" value=""/>
					            </div>
					        </div>
							 
					    <div class="modal-footer center">
					        <button type="button"  onclick ="saveNode('SUPERVISE_PROCESS')" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
					        <button type="reset"  class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
					        <button onclick="reflushTree('SUPERVISE_PROCESS')" type="button" class="btn btn-sm" ><i class="ace-icon fa fa-leaf"></i>刷新树</button>
					    </div>
		
		         </div>
          </div>
 		  </form>
     </div>
     
</div>
