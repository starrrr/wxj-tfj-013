<#include "../common/layout/__mainlayout.jsp">
<#assign pageCss>
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/template/css/template.css">
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css" />
	
<!--spinner插件样式重写，插件中的<input>与其他字段对齐  -->
<style>
.ui-spinner-input {
	border: none;
	background: none;
	color: inherit;
	padding: 0;
	margin: .2em 0;
	vertical-align: middle;
	margin-right: 22px;
}

</style>
</#assign> <#assign pageBreadCrumbs>
<li>产品管理</li>
<li class="active">种类模板管理</li>
</#assign> 

<#assign pageJavascript >
    <script src="${contextPath}/static/admin/assets/js/jquery.ui.touch-punch.js"></script>
	<script src="${contextPath}/static/admin/assets/js/jquery-ui.js"></script>
    <script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
    <script src="${contextPath}/static/admin/assets/js/fileupload/ajaxfileupload.js"></script>
	<script type="text/javascript">
			//获取页面的宽高
			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			  var setting2 = {
		                view: {
		                    dblClickExpand: true,
		                    showIcon: true
		                },
		                data: {
		                    simpleData: {
		                        enable: true
		                    }
		                },
		    			callback: {
		    				//onClick: onClick
		    			}
		            };
			function pvAlert(msg, callbackFunction) {
				bootbox.alert({
					message: msg,
					buttons: {
						ok: {
				            label: "确定",
				            className: "btn-primary btn-sm"
				        }
					},
			        callback: callbackFunction
				});
			}
			function pvConfirm(msg, callbackFunction) {
				return bootbox.confirm({
					message: msg,
					buttons: {
			            confirm: {
			                label: "确定",
			                className: "btn-primary btn-sm"
			            },
			            cancel: {
			                label: "取消",
			                className: "btn-sm btn-cancel"
			            }
			        },
			        callback : callbackFunction
				});
			}
			function requestEvery(processType,field_develop){
				var path=$("#contextPath").val();
				$.ajax({
					url:""+path+"/admin/template/dicmenushow",
					type:"post",
					data:null,
					datatype:"text",
					success:function(data){
						var html = "";
						for(var i=0;i<data.length;i++){
							html +="<option value="+data[i].dict_type+">"
							+data[i].notes
							+ "</option>"; 
						} 
						if(processType=="PLANT_PROCESS"){
							$(".plant_dic_menu").show();
							$(".plant_dic_menu").html(html);
							$(".plant_dic_menu").each(function() {
						       for(var i=0;i<this.options.length;i++){
								   if(field_develop==this.options[i].value){
									   this.options[i].selected = true;
								   	   return;
								   }
							   }
						    });
						}else if(processType=="SUPERVISE_PROCESS"){
							$(".supervise_dic_menu").show();
							$(".supervise_dic_menu").html(html);
							$(".supervise_dic_menu").each(function() {
						       for(var i=0;i<this.options.length;i++){
								   if(field_develop==this.options[i].value){
									   this.options[i].selected = true;
								   	   return;
								   }
							   }
						    });
						}else if(processType=="WAREHOUSE_PROCESS"){
							$(".wareHouse_dic_menu").show();
							$(".wareHouse_dic_menu").html(html);
							$(".wareHouse_dic_menu").each(function() {
						       for(var i=0;i<this.options.length;i++){
								   if(field_develop==this.options[i].value){
									   this.options[i].selected = true;
								   	   return;
								   }
							   }
						    });
						}else if(processType=="SALE_PROCESS"){
							$(".sale_dic_menu").show();
							$(".sale_dic_menu").html(html);
							$(".sale_dic_menu").each(function() {
						       for(var i=0;i<this.options.length;i++){
								   if(field_develop==this.options[i].value){
									   this.options[i].selected = true;
								   	   return;
								   }
							   }
						    });
						}
						$("#field_develop_"+processType).hide();
					} 
				});
			}
			$(document).ready(function(){
				//$('#sort_PLANT_PROCESS').ace_spinner({value:1,min:1,max:100,step:1, on_sides: true, onSpinUp:false,onSpinDown:false,icon_up:'ace-icon fa fa-plus smaller-75', icon_down:'ace-icon fa fa-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
				//$('#sort_SUPERVISE_PROCESS').ace_spinner({value:1,min:1,max:100,step:1, on_sides: true, icon_up:'ace-icon fa fa-plus smaller-75', icon_down:'ace-icon fa fa-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
				//$('#sort_WAREHOUSE_PROCESS').ace_spinner({value:1,min:1,max:100,step:1, on_sides: true, icon_up:'ace-icon fa fa-plus smaller-75', icon_down:'ace-icon fa fa-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
				//$('#sort_SALE_PROCESS').ace_spinner({value:1,min:1,max:100,step:1, on_sides: true, icon_up:'ace-icon fa fa-plus smaller-75', icon_down:'ace-icon fa fa-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
				//spinner
				$( "#sort_PLANT_PROCESS" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					}//,
					//min:1,
				});
				$( "#sort_SUPERVISE_PROCESS" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					},
					min:1
				});
				$( "#sort_WAREHOUSE_PROCESS" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					},
					min:1
				});
				$( "#sort_SALE_PROCESS" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					},
					min:1
				});
				
				
				//loading
				var loadingIcon = $('.loading img');
				var textTip = $('.textTip');
				//textTip.html('更新中，请稍候...');
				$('.loading').css({
					'width' : windowWidth,
					'height' : windowHeight
				});
				loadingIcon.css({
					'left': (windowWidth - loadingIcon.width())/2,
					'top' : (windowHeight - loadingIcon.height())/2
				});
				textTip.css({
					'left': (windowWidth - textTip.width())/2,
					'top' : (windowHeight - textTip.height())/2 + $('.loading img').height()*3/4
				});
				
				//初始化页面 tab不允许 点击 
				$("#plantProcessMenu").attr("href","#");
				$("#superviseMenu").attr("href","#");
				$("#wareHouseMenu").attr("href","#");
				$("#saleMenu").attr("href","#");
				
				//种植环节 
				$("#plantProcessMenu").bind("click",function(){
					var  category_id = $("#herbal_type  option:selected").val();
					if(category_id=='-1'){
						pvAlert("请先选择中草药种类",function(){
							$("#selectCategoryMenu").click();//跳转回到选择种类界tab
						});
					}else{
						$("#category_id_PLANT_PROCESS").val(category_id);
						$("#processType_PLANT_PROCESS").val('PLANT_PROCESS');
						removedata('PLANT_PROCESS');
						 //加载树 
						 $.post("${contextPath}/admin/template/getTemplateTree?processType=PLANT_PROCESS&category_id="+category_id, function(data){
					           $.fn.zTree.init($("#plantTree"), setting, data);
					    });
						 
					}
					
				});
				
				//加工环节 
				$("#superviseMenu").bind("click",function(){
					var category_id = $("#herbal_type  option:selected").val();
					if(category_id=='-1'){
							pvAlert("请先选择中草药种类",function(){
								$("#selectCategoryMenu").click();//跳转回到选择种类界tab
							});
					}else{
						 $("#category_id_SUPERVISE_PROCESS").val(category_id);
						 $("#processType_SUPERVISE_PROCESS").val('SUPERVISE_PROCESS');
						 $("input:radio[name='index_show_SUPERVISE_PROCESS']").eq(0).prop("checked",true);
						 removedata('SUPERVISE_PROCESS');
						 //加载树 
						 $.post("${contextPath}/admin/template/getTemplateTree?processType=SUPERVISE_PROCESS&category_id="+category_id, function(data){
				            $.fn.zTree.init($("#superviseTree"), setting, data);
				    	 });   
					}
				});
				//仓管流通环节 
				$("#wareHouseMenu").bind("click",function(){
					 
					var category_id = $("#herbal_type  option:selected").val();
					if(category_id=='-1'){
							pvAlert("请先选择中草药种类",function(){
								$("#selectCategoryMenu").click();//跳转回到选择种类界tab
							});
					}else{
						 $("#category_id_WAREHOUSE_PROCESS").val(category_id);
						 $("#processType_WAREHOUSE_PROCESS").val('WAREHOUSE_PROCESS');
						 $("input:radio[name='index_show_WAREHOUSE_PROCESS']").eq(0).prop("checked",true);
						 removedata('WAREHOUSE_PROCESS');
						 //加载树 
						 $.post("${contextPath}/admin/template/getTemplateTree?processType=WAREHOUSE_PROCESS&category_id="+category_id, function(data){
					            $.fn.zTree.init($("#wareHouseTree"), setting, data);
					     });
					}
				});
				//销售环节 
				$("#saleMenu").bind("click",function(){
					
					var category_id = $("#herbal_type  option:selected").val();
					if(category_id=='-1'){
							pvAlert("请先选择中草药种类",function(){
								$("#selectCategoryMenu").click();//跳转回到选择种类界tab
							});
					}else{
						 $("#category_id_SALE_PROCESS").val(category_id);
						 $("#processType_SALE_PROCESS").val('SALE_PROCESS');
						 $("input:radio[name='index_show_SALE_PROCESS']").eq(0).prop("checked",true);
						 removedata('SALE_PROCESS');
						 //加载树 
						 $.post("${contextPath}/admin/template/getTemplateTree?processType=SALE_PROCESS&category_id="+category_id, function(data){
					            $.fn.zTree.init($("#saleTree"), setting, data);
					     });
					}
				});
 
				 var setting = {
	                view: {
	                    dblClickExpand: true,
	                    showIcon: true
	                },
	                data: {
	                    simpleData: {
	                        enable: true
	                    }
	                },
	    			callback: {
	    				onClick: onClick
	    			}
	            } 
			
				 //点击事件 
				 function onClick(event, treeId, treeNode, clickFlag) {
					var id = treeNode.id;
					var pid = treeNode.pId;
					var level = treeNode.levelNum;
					var sort = treeNode.sort;
					var processType = treeNode.processType;
					var indexShow = treeNode.indexShow;
					var key_name = treeNode.name;
					var field_type = treeNode.fieldType;
					var field_develop = treeNode.fieldDevelop;
					var default_value = treeNode.defaultValue;
					var remark_note = treeNode.notes;
					var process_show = treeNode.processShow;
					var icon_note =treeNode.iconPath;//节点不存在图片时undefined
					var path=$("#path").val();
					if(field_type=="图片类型"){
						$("#key_name_"+processType).attr("readonly","readonly");
						 //字段拓展
						 $("#field_develop_"+processType).show();
						 $(".plant_dic_menu").hide();
						 $(".plant_dic_menu").empty();
						 $(".supervise_dic_menu").hide();
						 $(".supervise_dic_menu").empty();
						 $(".wareHouse_dic_menu").hide();
						 $(".wareHouse_dic_menu").empty();
						 $(".sale_dic_menu").hide();
						 $(".sale_dic_menu").empty();
					}else if(field_type=="下拉菜单"){
						 //去掉disable   keyName  有可能节点选择了图片类型，不保存，选择另一个节点修改，这时需要把readonly去掉
						$("#key_name_"+processType).removeAttr("readonly");//去掉readonly
						requestEvery(processType,field_develop);
					}else{
						 //去掉disable   keyName  有可能节点选择了图片类型，不保存，选择另一个节点修改，这时需要把readonly去掉
						 $("#key_name_"+processType).removeAttr("readonly");//去掉readonly
						 //字段拓展
						 $("#field_develop_"+processType).show();
						 $(".plant_dic_menu").hide();
						 $(".plant_dic_menu").empty();
						 $(".supervise_dic_menu").hide();
						 $(".supervise_dic_menu").empty();
						 $(".wareHouse_dic_menu").hide();
						 $(".wareHouse_dic_menu").empty();
						 $(".sale_dic_menu").hide();
						 $(".sale_dic_menu").empty();
					}
					
					
					
					//保证 根节点 不为空 
					if(pid == null){
						pid = 0;
					}
					 if(processType == 'PLANT_PROCESS'){
						 	//处理环节显示那些事勾上的
						 	checkProcessShow(field_type,field_develop,process_show,processType);
						 	
							$("#levelNum_PLANT_PROCESS").val(level);
						 	$("#id_PLANT_PROCESS").val(id);
							$("#pid_PLANT_PROCESS").val(pid);
							$("#sort_PLANT_PROCESS").val(sort);
							$("#key_name_PLANT_PROCESS").val(key_name);
							$("#field_type_PLANT_PROCESS").val(field_type);
							$("#field_develop_PLANT_PROCESS").val(field_develop);
							$("#default_value_PLANT_PROCESS").val(default_value);
							$("#remark_PLANT_PROCESS").val(remark_note);
							if(icon_note==undefined){
								$("#icon_PLANT_PROCESS_SHOW").hide(); 
							}else{
								$("#icon_PLANT_PROCESS_SHOW").show();//不存在时，div隐藏了，为了效果每次都show
								$("#icon_PLANT_PROCESS_SHOW").attr('width',"3.5%");
								$("#icon_PLANT_PROCESS_SHOW").attr('height',"3.5%");
								$("#icon_PLANT_PROCESS_SHOW_ID").val(treeNode.tId);//保留当前ID 为保存数据重新触发该节点
								$("#icon_PLANT_PROCESS_SHOW").attr('src',path+icon_note); 
							}
							
							$("#process_show_PLANT_PROCESS").val(process_show);
							
							if(indexShow){
					
								$("input:radio[name='index_show_PLANT_PROCESS']").eq(0).removeAttr("checked");
								$("input:radio[name='index_show_PLANT_PROCESS']").eq(1).prop("checked",'true');
							}else{
				
								$("input:radio[name='index_show_PLANT_PROCESS']").eq(1).removeAttr("checked");
								$("input:radio[name='index_show_PLANT_PROCESS']").eq(0).prop("checked",'true');			
							}
					 }
					 if(processType == 'SUPERVISE_PROCESS'){
						    //处理环节显示、混批,判断原来打勾的
						 	checkProcessShow(field_type,field_develop,process_show,processType);
							$("#levelNum_SUPERVISE_PROCESS").val(level);
						 	$("#id_SUPERVISE_PROCESS").val(id);
							$("#pid_SUPERVISE_PROCESS").val(pid);
							$("#sort_SUPERVISE_PROCESS").val(sort);
							$("#key_name_SUPERVISE_PROCESS").val(key_name);
							$("#field_type_SUPERVISE_PROCESS").val(field_type);
							$("#field_develop_SUPERVISE_PROCESS").val(field_develop);

							$("#default_value_SUPERVISE_PROCESS").val(default_value);
							$("#remark_SUPERVISE_PROCESS").val(remark_note);
							if(icon_note==undefined){
								$("#icon_SUPERVISE_PROCESS_SHOW").hide(); 
							}else{
								$("#icon_SUPERVISE_PROCESS_SHOW").show();//不存在时，div隐藏了，为了效果每次都show
								$("#icon_SUPERVISE_PROCESS_SHOW").attr('width',"3.5%");
								$("#icon_SUPERVISE_PROCESS_SHOW").attr('height',"3.5%");
								$("#icon_SUPERVISE_PROCESS_SHOW").attr('src',path+icon_note); 
							}
							
							$("#process_show_SUPERVISE_PROCESS").val(process_show);

							if(indexShow){
								$("input:radio[name='index_show_SUPERVISE_PROCESS']").eq(0).removeAttr("checked");
								$("input:radio[name='index_show_SUPERVISE_PROCESS']").eq(1).prop("checked",'true');	
							}else{
								$("input:radio[name='index_show_SUPERVISE_PROCESS']").eq(1).removeAttr("checked");
								$("input:radio[name='index_show_SUPERVISE_PROCESS']").eq(0).prop("checked",'true');			
							}						 
					 }
					 if(processType == 'WAREHOUSE_PROCESS'){
						    //处理环节显示那些事勾上的
						 	checkProcessShow(field_type,field_develop,process_show,processType);
						    $("#levelNum_WAREHOUSE_PROCESS").val(level);
						 	$("#id_WAREHOUSE_PROCESS").val(id);
							$("#pid_WAREHOUSE_PROCESS").val(pid);
							$("#sort_WAREHOUSE_PROCESS").val(sort);
							$("#key_name_WAREHOUSE_PROCESS").val(key_name);
							$("#field_type_WAREHOUSE_PROCESS").val(field_type);
							$("#field_develop_WAREHOUSE_PROCESS").val(field_develop);

							$("#default_value_WAREHOUSE_PROCESS").val(default_value);
							$("#remark_WAREHOUSE_PROCESS").val(remark_note);
							if(icon_note==undefined){
								$("#icon_WAREHOUSE_PROCESS_SHOW").hide(); 
							}else{
								$("#icon_WAREHOUSE_PROCESS_SHOW").show();//不存在时，div隐藏了，为了效果每次都show
								$("#icon_WAREHOUSE_PROCESS_SHOW").attr('width',"3.5%");
								$("#icon_WAREHOUSE_PROCESS_SHOW").attr('height',"3.5%");
								$("#icon_WAREHOUSE_PROCESS_SHOW").attr('src',path+icon_note); 
							}
							
							$("#process_show_WAREHOUSE_PROCESS").val(process_show);

							if(indexShow){
								$("input:radio[name='index_show_WAREHOUSE_PROCESS']").eq(0).removeAttr("checked");
								$("input:radio[name='index_show_WAREHOUSE_PROCESS']").eq(1).prop("checked",'true');	
							}else{
								$("input:radio[name='index_show_WAREHOUSE_PROCESS']").eq(1).removeAttr("checked");
								$("input:radio[name='index_show_WAREHOUSE_PROCESS']").eq(0).prop("checked",'true');			
							}
					 }
					 if(processType == 'SALE_PROCESS'){
							//处理环节显示那些事勾上的
						 	checkProcessShow(field_type,field_develop,process_show,processType);
						 	$("#levelNum_SALE_PROCESS").val(level);
						 	$("#id_SALE_PROCESS").val(id);
							$("#pid_SALE_PROCESS").val(pid);
							$("#sort_SALE_PROCESS").val(sort);
							$("#key_name_SALE_PROCESS").val(key_name);
							$("#field_type_SALE_PROCESS").val(field_type);
							$("#field_develop_SALE_PROCESS").val(field_develop);

							$("#default_value_SALE_PROCESS").val(default_value);
							$("#remark_SALE_PROCESS").val(remark_note);
							if(icon_note==undefined){
								$("#icon_SALE_PROCESS_SHOW").hide(); 
							}else{
								$("#icon_SALE_PROCESS_SHOW").show();//不存在时，div隐藏了，为了效果每次都show
								$("#icon_SALE_PROCESS_SHOW").attr('width',"3.5%");
								$("#icon_SALE_PROCESS_SHOW").attr('height',"3.5%");
								$("#icon_SALE_PROCESS_SHOW").attr('src',path+icon_note); 
							}
							
							$("#process_show_SALE_PROCESS").val(process_show);

							if(indexShow){
								$("input:radio[name='index_show_SALE_PROCESS']").eq(0).removeAttr("checked");
								$("input:radio[name='index_show_SALE_PROCESS']").eq(1).prop("checked",'true');	
							}else{
								$("input:radio[name='index_show_SALE_PROCESS']").eq(1).removeAttr("checked");
								$("input:radio[name='index_show_SALE_PROCESS']").eq(0).prop("checked",'true');			
							}
					 }
				}	
		 
	        });
	
			
			//选择种类 
			function selectCategory(){
				var selectIndex = $("#herbal_type option:selected").val();
				$("font[name='title']").html("当前种类："+$("#herbal_type option:selected").text());//选择种类，把值赋给input  其它环节使用
				if(selectIndex =='-1'){
					pvAlert('提示:请选择中草药种类!',function(){});
				}else{
					$("#herbal_type").attr("disabled","disabled"); 
					$("#plantProcessMenu").attr("href","#plantProcess");
					$("#superviseMenu").attr("href","#superviseProcess");
					$("#wareHouseMenu").attr("href","#wareHouseProcess");
					$("#saleMenu").attr("href","#saleProcess");

					//operate log
					$.ajax({
						url:"${contextPath}/admin/template/recordlog",
	        			type:"post",
	        			data:"category_id="+selectIndex,
	        			datatype:"text",
	        			success:function(data){
	        				pvAlert('已成功选择种类',function(){
	        					$("#plantProcessMenu").click();//种类选择好，自动调整到种植环节
	        				});
	        			} 
	        		});
				}
			}
			
			//切换种类 
			function changeCategory(){
				window.location.reload();
			}
			
			//刷新缓存
			function redisCache(){
				var selectIndex = $("#herbal_type option:selected").val();
				if(selectIndex =='-1'){
					pvAlert('提示:请选择中草药种类!',function(){});
				}else{
					pvConfirm("确定更新模板",function(result){
						$(".loading").show();
						if(result){
							$.ajax({
								url:"${contextPath}/admin/template/reflushrediscache",
			        			type:"post",
			        			data:"category_id="+selectIndex,
			        			datatype:"text",
			        			success:function(data){
			        				if(data=="success"){
			        					pvAlert('更新成功',function(){
			        						$(".loading").hide();
			        					});
			        				}else{
			        					pvAlert('出现异常，更新失败',function(){
			        						$(".loading").hide();
			        					});
			        				}
			        				
			        			} 
			        		});
						}else{
							$(".loading").hide();
						}
					});
				}
			}

			//处理环节显示、混批勾选的事件
			function checkProcessShow(field_type,field_develop,process_show,processType){
				//判断环境显示的值，符合勾上
				var process_shows=process_show.split(":");
				var bUser=process_shows[0];
				var cUser=process_shows[1];
				var gUser=process_shows[2];
				if(processType == 'PLANT_PROCESS'){
					if(bUser=='B_1'){
						$("#plantB:checkbox").prop("checked",true);
					}else{
						$("#plantB:checkbox").prop("checked",false);
					}
					if(cUser=='C_1'){
						$("#plantC:checkbox").prop("checked",true);
					}else{
						$("#plantC:checkbox").prop("checked",false);
					}
					if(gUser=='G_1'){
						$("#plantG:checkbox").prop("checked",true);
					}else{
						$("#plantG:checkbox").prop("checked",false);
					}
					//混批判断
					if(field_type=="文本类型"){
						$("#plantmix").show();
						if(field_develop.trim()=='{MIX}'){
							$("input[name='plantmix']").prop("checked", true);
							$(".field_develop_hidden_plant").hide();//隐藏字段拓展
						}else{
							$("input[name='plantmix']").prop("checked", false);
							$(".field_develop_hidden_plant").show();
						}
					}else{
						$("#plantmix").hide();
						$(".field_develop_hidden_plant").show();
					}
				}
				if(processType == 'SUPERVISE_PROCESS'){
					if(bUser=='B_1'){
						$("#superviseB:checkbox").prop("checked", true);
					}else{
						$("#superviseB:checkbox").prop("checked", false);
					}
					if(cUser=='C_1'){
						$("#superviseC:checkbox").prop("checked", true);
					}else{
						$("#superviseC:checkbox").prop("checked", false);
					}
					if(gUser=='G_1'){
						$("#superviseG:checkbox").prop("checked", true);
					}else{
						$("#superviseG:checkbox").prop("checked", false);
					}
					//混批判断
					if(field_type=="文本类型"){
						$("#supervisemix").show();
						if(field_develop.trim()=='{MIX}'){
							$("input[name='supervisemix']").prop("checked", true);
							$(".field_develop_hidden_supervise").hide();//隐藏字段拓展
						}else{
							$("input[name='supervisemix']").prop("checked", false);
							$(".field_develop_hidden_supervise").show();
						}
					}else{
						$("#supervisemix").hide();
						$(".field_develop_hidden_supervise").show();
					}
				}
				if(processType == 'WAREHOUSE_PROCESS'){
					if(bUser=='B_1'){
						$("#warehouseB:checkbox").prop("checked", true);
					}else{
						$("#warehouseB:checkbox").prop("checked", false);
					}
					if(cUser=='C_1'){
						$("#warehouseC:checkbox").prop("checked", true);
					}else{
						$("#warehouseC:checkbox").prop("checked", false);
					}
					if(gUser=='G_1'){
						$("#warehouseG:checkbox").prop("checked", true);
					}else{
						$("#warehouseG:checkbox").prop("checked", false);
					}
					//混批判断
					if(field_type=="文本类型"){
						$("#wareHousemix").show();
						if(field_develop.trim()=='{MIX}'){
							$("input[name='wareHousemix']").prop("checked", true);
							$(".field_develop_hidden_wareHouse").hide();//隐藏字段拓展
						}else{
							$("input[name='wareHousemix']").prop("checked", false);
							$(".field_develop_hidden_wareHouse").show();
						}
					}else{
						$("#wareHousemix").hide();
						$(".field_develop_hidden_wareHouse").show();
					}
				}
				if(processType == 'SALE_PROCESS'){
					if(bUser=='B_1'){
						$("#saleB:checkbox").prop("checked", true);
					}else{
						$("#saleB:checkbox").prop("checked", false);
					}
					if(cUser=='C_1'){
						$("#saleC:checkbox").prop("checked", true);
					}else{
						$("#saleC:checkbox").prop("checked", false);
					}
					if(gUser=='G_1'){
						$("#saleG:checkbox").prop("checked", true);
					}else{
						$("#saleG:checkbox").prop("checked", false);
					}
					//混批判断
					if(field_type=="文本类型"){
						$("#salemix").show();
						if(field_develop.trim()=='{MIX}'){
							$("input[name='salemix']").prop("checked", true);
							$(".field_develop_hidden_sale").hide();//隐藏字段拓展
						}else{
							$("input[name='salemix']").prop("checked", false);
							$(".field_develop_hidden_sale").show();
						}
					}else{
						$("#salemix").hide();
						$(".field_develop_hidden_sale").show();
					}
				}
				
			}
			
			function addNode(processType){
				if (processType && $.trim(processType) != '') {
					 var id = $("#id_" + processType).val();
					 var level = $("#levelNum_" + processType).val();
					 $("#id_" + processType).val('');
					 $("#pid_" + processType).val(id);
					 $("#levelNum_" + processType).val(parseInt(level) + 1);
					 $("#key_name_" + processType).val("");
					 $("#field_type_" + processType).val(-1);
					 
					 $("#field_develop_" + processType).val("");
					 $("#field_develop_" + processType).show();
					 
					 $(".plant_dic_menu").empty();
					 $(".plant_dic_menu").hide();
					 $(".sale_dic_menu").empty();
					 $(".sale_dic_menu").hide();
					 $(".supervise_dic_menu").empty();
					 $(".supervise_dic_menu").hide();
					 $(".wareHouse_dic_menu").empty();
					 $(".wareHouse_dic_menu").hide();
					 //隐藏混批按钮
					 $(".plantmix").hide();
					 $(".salemix").hide();
					 $(".supervisemix").hide();
					 $(".wareHousemix").hide();
					 //显示字段拓展字段
					 $(".field_develop_hidden_plant").show();
					 $(".field_develop_hidden_supervise").show();
					 $(".field_develop_hidden_wareHouse").show();
					 $(".field_develop_hidden_sale").show();
					 //清空各个环节   环节显示   复选框
					 $("input[type='checkbox']").removeAttr("checked");
					 //主页显示默认值写定为禁用
					 $("input:radio[name='index_show_"+processType+"']").eq(0).prop("checked",true);
					//清空小图标
					 $("#icon_"+processType+"_SHOW").attr("src","").attr("width","").attr("height","");
					 
	 				 if (id && id != '') {
						 var url = "${contextPath}/admin/template/getParentMaxCount";
						 var data = "pid=" + id;
						 $.post(url, data, function(e) {
							 var jObj = $.parseJSON(e);
							 if (jObj.result == "SUCCESS") {
								 $("#sort_" + processType).val(jObj.max);
							 }
						 });
					 }
					 $("#default_value_" + processType).val("");
					 $("#remark_" + processType).val("");
					 $("#process_show_" + processType).val("B_0:C_0:G_0");
					  
					 //判断 节点 是否选中
					 if (id == '') {
						 $("#pid_" + processType).val("0");
						 $("#levelNum_" + processType).val("0");
					}
				}
			}
			
			function deleteNode(processType){
				 var id ;
				 var pid;
				 var keyName="";
				 var level = $("#levelNum_" + processType).val();
				 if(processType == 'PLANT_PROCESS'){
					  id = $("#id_PLANT_PROCESS").val();
					  keyName=$("#key_name_PLANT_PROCESS").val();
				 }
				 if(processType == 'SUPERVISE_PROCESS'){
					  id = $("#id_SUPERVISE_PROCESS").val();
					  keyName=$("#key_name_SUPERVISE_PROCESS").val();
				 }
				 if(processType == 'WAREHOUSE_PROCESS'){
					  id = $("#id_WAREHOUSE_PROCESS").val();
					  keyName=$("#key_name_WAREHOUSE_PROCESS").val();
				 }
				 if(processType == 'SALE_PROCESS'){
					  id = $("#id_SALE_PROCESS").val();
					  keyName=$("#key_name_SALE_PROCESS").val();
				 }
				 if(id==''){
						pvAlert('提示:请选择删除节点，务必保证节点是子节点!',function(){});
				 }else{
						bootbox.dialog({  
			                message: "确定删除["+keyName+"]节点？",  
			                buttons: {  
			                    Cancel: {  
			                        label: "确定",  
			                        className: "btn-primary",  
			                        callback: function () {  
			                        	$.ajax( {  
										     url:'${contextPath}/admin/template/deleteNode?id='+id,// 跳转到 action    
										     type:'post',  
										     cache:false,  
										     dataType:'json',  
										     success:function(data) {  
										         if(data =="SUCCESS" ){  
										        	 reflushTree(processType);
										        	 pvAlert('提示:删除成功!',function(){
										        		 $("#key_name_" + processType).val("");
										        		 $("#process_show_" + processType).val("B_0:C_0:G_0");
										        		 $("#field_develop_" + processType).val("");
										        		 $("#remark_" + processType).val("");
										        	 });
										         }else{  
										        	 pvAlert('提示:删除失败，务必保证节点是子节点!',function(){});
										        }  
										      },  
										      error : function() {  
										    	  //pvAlert('提示:删除异常 请联系管理员!',function(){});
										    	  pvAlert('提示:删除失败，务必保证节点是子节点!',function(){});
										      }  
										 });
			                        }  
			                    }  
			                    , OK: {  
			                        label: "取消",  
			                        className: "btn-default",  
			                        callback: function () {  
			                        }  
			                    }  
			                }  
			         }); 
					}
			
			}

			
			//保存节点
			function saveNode(processType){
				var param;
				var fileInputId;
				 if(processType == 'PLANT_PROCESS'){
					 var plantId=$("#id_PLANT_PROCESS").val();
					 fileInputId =  'icon_PLANT_PROCESS';
					 var field_type_selected = $("#field_type_PLANT_PROCESS option:selected").val();
					 var keyname=$("#key_name_PLANT_PROCESS").val().trim();
					 var fd_PLANT_PROCESS="";
					 if(field_type_selected=='下拉菜单'){
						 fd_PLANT_PROCESS=$(".plant_dic_menu option:selected").val();
					 }else{
						 fd_PLANT_PROCESS=$("#field_develop_PLANT_PROCESS").val();
					 }
					 
					 if(!field_type_selected || field_type_selected =='-1'||keyname==''){
						 if(keyname==''){
							 pvAlert('名称不能为空。',function(){});
						 }else{
							 pvAlert('请选择字段属性。',function(){});
						 }
					 }
					 else{
					 $.ajaxFileUpload
						({
							url:'${contextPath}/admin/template/save',
							secureuri:false,
							fileElementId: fileInputId,
							dataType: 'text',
							data: { 
								 id:$("#id_PLANT_PROCESS").val(),
				    			 pid:$("#pid_PLANT_PROCESS").val(),
				    	 		 category_id:$("#category_id_PLANT_PROCESS").val(),
				    			 processType:$("#processType_PLANT_PROCESS").val(),
				    		     levelNum:$("#levelNum_PLANT_PROCESS").val(),
				    	 		 key_name:$("#key_name_PLANT_PROCESS").val(),
				    	         sort:$("#sort_PLANT_PROCESS").val(),
				    	 		 field_type:$("#field_type_PLANT_PROCESS option:selected").val(),
				    			 field_develop:fd_PLANT_PROCESS,
				    			 default_value:$("#default_value_PLANT_PROCESS").val(),
				    			 remark_note:$("#remark_PLANT_PROCESS").val(),
				    	 		 index_show:$('input:radio:checked').val(),
				    	 		 process_show:$('#process_show_PLANT_PROCESS').val()
					  		 },
					  		success: function (data , status)
							{
					  			reflushTree(processType);
								pvAlert('修改成功。',function(){});
							},
							error:  function (data, status, e)
							{
								pvAlert('上传发生异常。',function(){});
							}
						});
					 
					 }
					
				 }
				 if(processType == 'SUPERVISE_PROCESS'){
					 var superviseId=$("#id_SUPERVISE_PROCESS").val();
					 fileInputId =  'icon_SUPERVISE_PROCESS';
					 var field_type_selected = $("#field_type_SUPERVISE_PROCESS option:selected").val();
					 var keyname=$("#key_name_SUPERVISE_PROCESS").val().trim();
					 var fd_SUPERVISE_PROCESS="";
					 if(field_type_selected=='下拉菜单'){
						 fd_SUPERVISE_PROCESS=$(".supervise_dic_menu option:selected").val();
					 }else{
						 fd_SUPERVISE_PROCESS=$("#field_develop_SUPERVISE_PROCESS").val();
					 }
					 if(!field_type_selected || field_type_selected =='-1'||keyname==''){
						 if(keyname==''){
							 pvAlert('名称不能为空。',function(){});
						 }else{
							 pvAlert('请选择字段属性。',function(){});
						 }
					 }
					
					 else{
					 $.ajaxFileUpload
						({
							url:'${contextPath}/admin/template/save',
							secureuri:false,
							fileElementId: fileInputId,
							dataType: 'text',
							data: { 
								 id:$("#id_SUPERVISE_PROCESS").val(),
				    			 pid:$("#pid_SUPERVISE_PROCESS").val(),
				    	 		 category_id:$("#category_id_SUPERVISE_PROCESS").val(),
				    			 processType:$("#processType_SUPERVISE_PROCESS").val(),
				    		     levelNum:$("#levelNum_SUPERVISE_PROCESS").val(),
				    	 		 key_name:$("#key_name_SUPERVISE_PROCESS").val(),
				    	         sort:$("#sort_SUPERVISE_PROCESS").val(),
				    	 		 field_type:$("#field_type_SUPERVISE_PROCESS option:selected").val(),
				    			 field_develop:fd_SUPERVISE_PROCESS,
				    			 default_value:$("#default_value_SUPERVISE_PROCESS").val(),
				    			 remark_note:$("#remark_SUPERVISE_PROCESS").val(),
				    	 		 index_show:$('input:radio:checked').val(),
				    	 		 process_show:$('#process_show_SUPERVISE_PROCESS').val()
					  		 },
							success: function (data , status)
							{
								reflushTree(processType);
								pvAlert('修改成功。',function(){});
							},
							error:  function (data, status, e)
							{
								pvAlert('上传发生异常。',function(){});
							}
						}); 
					 
					 }
				 }
				 if(processType == 'WAREHOUSE_PROCESS'){
					 var warehouseId=$("#id_WAREHOUSE_PROCESS").val();
					 fileInputId =  'icon_WAREHOUSE_PROCESS';
					 var field_type_selected = $("#field_type_WAREHOUSE_PROCESS option:selected").val();
					 var keyname=$("#key_name_WAREHOUSE_PROCESS").val().trim();
					 if(field_type_selected=='下拉菜单'){
						 fd_WAREHOUSE_PROCESS=$(".wareHouse_dic_menu option:selected").val();
					 }else{
						 fd_WAREHOUSE_PROCESS=$("#field_develop_WAREHOUSE_PROCESS").val();
					 }
					 
					 if(!field_type_selected || field_type_selected =='-1'||keyname==''){
						 if(keyname==''){
							 pvAlert('名称不能为空。',function(){}); 
						 }else{
							 pvAlert('请选择字段属性。',function(){});
						 }
					 }
					 else{
					 $.ajaxFileUpload
						({
							url:'${contextPath}/admin/template/save',
							secureuri:false,
							fileElementId: fileInputId,
							dataType: 'text',
							data: { 
								 id:$("#id_WAREHOUSE_PROCESS").val(),
				    			 pid:$("#pid_WAREHOUSE_PROCESS").val(),
				    	 		 category_id:$("#category_id_WAREHOUSE_PROCESS").val(),
				    			 processType:$("#processType_WAREHOUSE_PROCESS").val(),
				    		     levelNum:$("#levelNum_WAREHOUSE_PROCESS").val(),
				    	 		 key_name:$("#key_name_WAREHOUSE_PROCESS").val(),
				    	         sort:$("#sort_WAREHOUSE_PROCESS").val(),
				    	 		 field_type:$("#field_type_WAREHOUSE_PROCESS option:selected").val(),
				    			 field_develop:fd_WAREHOUSE_PROCESS,
				    			 default_value:$("#default_value_WAREHOUSE_PROCESS").val(),
				    			 remark_note:$("#remark_WAREHOUSE_PROCESS").val(),
				    	 		 index_show:$('input:radio:checked').val(),
				    	 		 process_show:$('#process_show_WAREHOUSE_PROCESS').val()
				    	 		 
					  		 },
							success: function (data , status)
							{
								reflushTree(processType);
								pvAlert('修改成功。',function(){});
							},
							error:  function (data, status, e)
							{
								pvAlert('上传发生异常。',function(){});
							}
						});
					 }
				 }
				 if(processType == 'SALE_PROCESS'){
					 var saleId=$("#id_SALE_PROCESS").val();
					 fileInputId =  'icon_SALE_PROCESS';
					 var field_type_selected = $("#field_type_SALE_PROCESS option:selected").val();
					 var keyname=$("#key_name_SALE_PROCESS").val().trim();
					 if(field_type_selected=='下拉菜单'){
						 fd_SALE_PROCESS=$(".sale_dic_menu option:selected").val();
					 }else{
						 fd_SALE_PROCESS=$("#field_develop_SALE_PROCESS").val();
					 }
					 
					 if(!field_type_selected || field_type_selected == '-1'||keyname==''){
						 if(keyname==''){
							 pvAlert('名称不能为空。',function(){});
						 }else{
							 pvAlert('请选择字段属性。',function(){});
						 }
						 return;
					 } else {
					 $.ajaxFileUpload
						({
							url:'${contextPath}/admin/template/save',
							secureuri:false,
							fileElementId: fileInputId,
							dataType: 'text',
							data: { 
								 id:$("#id_SALE_PROCESS").val(),
				    			 pid:$("#pid_SALE_PROCESS").val(),
				    	 		 category_id:$("#category_id_SALE_PROCESS").val(),
				    			 processType:$("#processType_SALE_PROCESS").val(),
				    		     levelNum:$("#levelNum_SALE_PROCESS").val(),
				    	 		 key_name:$("#key_name_SALE_PROCESS").val(),
				    	         sort:$("#sort_SALE_PROCESS").val(),
				    	 		 field_type:$("#field_type_SALE_PROCESS option:selected").val(),
				    			 field_develop:fd_SALE_PROCESS,
				    			 default_value:$("#default_value_SALE_PROCESS").val(),
				    			 remark_note:$("#remark_SALE_PROCESS").val(),
				    	 		 index_show:$('input:radio:checked').val(),
				    	 		 process_show:$('#process_show_SALE_PROCESS').val()
					  		 },
							success: function (data , status)
							{
								reflushTree(processType);
								pvAlert('修改成功。',function(){});
							},
							error:  function (data, status, e)
							{
								pvAlert('上传发生异常。',function(){});
							}
						});

					 }
				 }
				 
						
			

			}
			
			
			function reflushTree(processType) {
				 if(processType == 'PLANT_PROCESS'){
					 $("#plantProcessMenu").click();
				 }
				 if(processType == 'SUPERVISE_PROCESS'){
					 $("#superviseMenu").click();
				 }
				 if(processType == 'WAREHOUSE_PROCESS'){
					 $("#wareHouseMenu").click();
				 }
				 if(processType == 'SALE_PROCESS'){
					 $("#saleMenu").click();
				 }
			} 
			//每次切换tab,清除记录
			function removedata(processType){
				//清空各个环节   环节显示   复选框
				$("input[type='checkbox']").removeAttr("checked");
				//清空各个环节   主页显示   单选框
				$("input[type='radio']").removeAttr("checked");
				//plant
				$("#key_name_PLANT_PROCESS").val('');
				$("#sort_PLANT_PROCESS").val(1);
				$("#icon_PLANT_PROCESS_SHOW").attr("src","").attr("width","").attr("height","");
				$("#field_type_PLANT_PROCESS").val("-1");	
				$("#field_develop_PLANT_PROCESS").val('');
				$("#field_develop_PLANT_PROCESS").show();
				$(".plant_dic_menu").empty();
				$(".plant_dic_menu").hide();
				$("#process_show_PLANT_PROCESS").val('B_0:C_0:G_0');
				
				//sale
				$("#key_name_SALE_PROCESS").val('');
				$("#sort_SALE_PROCESS").val(1);
				$("#icon_SALE_PROCESS_SHOW").attr("src","").attr("width","").attr("height","");
				$("#field_type_SALE_PROCESS").val("-1");	
				$("#field_develop_SALE_PROCESS").val('');
				$("#field_develop_SALE_PROCESS").show();
				$(".sale_dic_menu").empty();
				$(".sale_dic_menu").hide();
				$("#process_show_SALE_PROCESS").val('B_0:C_0:G_0');
				
				//supervise
				$("#key_name_SUPERVISE_PROCESS").val('');
				$("#sort_SUPERVISE_PROCESS").val(1);
				$("#icon_SUPERVISE_PROCESS_SHOW").attr("src","").attr("width","").attr("height","");
				$("#field_type_SUPERVISE_PROCESS").val("-1");	
				$("#field_develop_SUPERVISE_PROCESS").val('');
				$("#field_develop_SUPERVISE_PROCESS").show();
				$(".supervise_dic_menu").empty();
				$(".supervise_dic_menu").hide();
				$("#process_show_SUPERVISE_PROCESS").val('B_0:C_0:G_0');
				
				//wareHouse
				$("#key_name_WAREHOUSE_PROCESS").val('');
				$("#sort_WAREHOUSE_PROCESS").val(1);
				$("#icon_WAREHOUSE_PROCESS_SHOW").attr("src","").attr("width","").attr("height","");
				$("#field_type_WAREHOUSE_PROCESS").val("-1");	
				$("#field_develop_WAREHOUSE_PROCESS").val('');
				$("#field_develop_WAREHOUSE_PROCESS").show();
				$(".wareHouse_dic_menu").empty();
				$(".wareHouse_dic_menu").hide();
				$("#process_show_WAREHOUSE_PROCESS").val('B_0:C_0:G_0');
				
				
				 //隐藏混批按钮
				 $("#plantmix").hide();
				 $("#salemix").hide();
				 $("#supervisemix").hide();
				 $("#wareHousemix").hide();
				 
				 //去掉disable   keyName
				 $("#key_name_PLANT_PROCESS").removeAttr("readonly");//去掉readonly
				 $("#key_name_SALE_PROCESS").removeAttr("readonly");//去掉readonly
				 $("#key_name_SUPERVISE_PROCESS").removeAttr("readonly");//去掉readonly
				 $("#key_name_WAREHOUSE_PROCESS").removeAttr("readonly");//去掉readonly
				 //显示字段拓展字段
				 $(".field_develop_hidden_plant").show();
				 $(".field_develop_hidden_supervise").show();
				 $(".field_develop_hidden_wareHouse").show();
				 $(".field_develop_hidden_sale").show();
				 
					
				 //主页显示默认值写定为禁用
				 $("input:radio[name='index_show_"+processType+"']").eq(0).prop("checked",true);
				//清除备注
				 $("#remark_"+processType).val('');
				
			}
		
			
	</script>
</#assign>


<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-templateFlag">
  
<div class="row">
	<input type="hidden" id="path" value="${path! }">
	<input type="hidden" id="contextPath" value="${contextPath!}">
	<input type="hidden" name="Image_Pattern" id="Image_Pattern" value="${Image_Pattern!}"/>
	<input type="hidden" id="categoryName" value="">
	<div class="col-xs-12">
		<div >
			<div >
				<div class="tabbable">
					<ul class="nav nav-tabs padding-18">
					
						<li  class="active">
							<a data-toggle="tab" id="selectCategoryMenu"  href="#selectCategory"> <i class="green ace-icon fa fa-eye bigger-120"></i> 选择种类</a>
						</li>
						<li >
							<a data-toggle="tab" href="#plantProcess" id="plantProcessMenu"> <i class="green ace-icon fa fa-leaf bigger-120"></i> 种植环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#superviseProcess" id="superviseMenu"> <i class="orange ace-icon fa fa-rss bigger-120"></i> 加工环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#wareHouseProcess" id="wareHouseMenu"> <i class="blue ace-icon fa fa-users bigger-120"></i> 流通环节</a>
						</li>

						<li>
							<a data-toggle="tab" href="#saleProcess" id="saleMenu"> <i class="pink ace-icon fa fa-picture-o bigger-120"></i> 销售环节</a></li>
					</ul>

					<div class="tab-content no-border padding-24">
						<div id="selectCategory" class="tab-pane in active">
								<div class="hr dotted"></div>
								<label>中草药种类:</label>
						        <select class="input-medium" id="herbal_type" name="plantBase.herbal_type">
						       	  	<option value="-1">---请选择---</option>
						           <#list categoryList as category>
						               <option value="${category.id}">${category.category_name}</option>
						           </#list>
						        </select>
						        <button type="button" id="selectCategory" onclick="selectCategory();" class="btn btn-sm btn-success">确定</button> &nbsp;&nbsp;
						        <button type="button"  onclick="changeCategory();" class="btn btn-sm btn-success">切换种类</button>
						        <button type="button"  onclick="redisCache();" class="btn btn-sm btn-success">更新模板</button>
								<div class="hr dotted"></div>
								<font color="red">提示：选择好中草药种类并点击确定按钮后才能进行操作哦!</font>
	
						</div>
						
						<div id="plantProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">种植环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="plantTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								
								<#include "./plantInput.jsp">
							</div>
						</div>
		
						<div id="superviseProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">加工环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="superviseTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								<#include "./superviseInput.jsp">
							
							</div>
						</div>
		
						<div id="wareHouseProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">流通环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="wareHouseTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								
								<#include "./wareHouseInput.jsp">
							</div>
						</div>

						<div id="saleProcess" class="tab-pane">
							<div>	
								<div class="col-xs-4 col-sm-4">
								     <div class="widget-box widget-color-blue2">
								         <div class="widget-header widget-header-small">
								             <h4 class="widget-title lighter smaller">销售环节</h4>
								         </div>
								
								         <div class="widget-body">
								             <div class="widget-main padding-8">
								                 <ul id="saleTree" class="ztree"></ul>
								             </div>
								         </div>
								     </div>
								</div>
								
								<#include "./saleInput.jsp">
							</div>
						</div>
		
					</div>
				</div>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div>
</div>
<!-- 动画加载 -->
<div class="loading">
	<img src="${contextPath}/static/admin/assets/img/loading_blue.GIF">
	<span class="textTip"></span>
</div>
					
</@mainlayout>
