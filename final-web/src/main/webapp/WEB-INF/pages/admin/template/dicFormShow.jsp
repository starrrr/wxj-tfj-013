<script type="text/javascript" src="${contextPath}/static/admin/index/js/jquery-2.1.4.min.js"></script>
<script src="${contextPath}/static/admin/template/common.js"></script>
<form id="" class="form-horizontal" action="">
    <div class="modal-header">
        <h5 class="blue">选择选项</h5>
    </div>
    <#list dictmenu as dic>
	    <div class="" style="width: 200px">
	        ${dic_index+1 }<input name="radio" id="radio" type="radio" value="${dic.dict_type! }">${dic.notes! }
	    </div>
    </#list>
    <div class="modal-footer center">
    	<button type="button" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
</form>
