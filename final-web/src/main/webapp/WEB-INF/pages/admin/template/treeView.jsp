
  



<div class="col-xs-8 col-sm-8">
     <div class="widget-box widget-color-green">
         <div class="widget-header widget-header-small">
               <button type="button" onclick="addNode()" class="btn btn-sm btn-success" ><i class="ace-icon fa fa-check"></i>新增节点</button>
			   <button type="button" onclick="deleteNode()" class="btn btn-sm btn-success" ><i class="ace-icon fa fa-times"></i>删除节点</button>
			   <button type="button" class="btn btn-sm btn-success" ><i class="ace-icon fa fa-edit"></i>编辑</button>
         </div>
         
		 <form id="templateForm" action="${contextPath}/admin/template/save" method="post" enctype="multipart/form-data">
         <div class="widget-body">
         
          		 <input type="hidden" name="template.id" id="id" value=""/>
 				 <input type="hidden" name="template.pid" id="pid" value=""/>
		         <input type="hidden" name="template.category_id" id="category_id" value=""/>
		         <input type="hidden" name="template.levelNum" id="levelNum" value=""/>
		         <input type="hidden" name="template.processType" id="processType" value=""/>
		         
		         <div class="modal-body left">
	
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="key_name">名称:</label>
					            <div class="clearfix">
					            	 <input type="text" name="template.key_name" id="key_name" class="col-xs-12 col-sm-4 required" value=""/>
					            </div>
					        </div>
					        
					         <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="sort">排序:</label>
					            <div class="clearfix">
									 <input type="text" name="template.sort" id="sort" class="col-xs-12 col-sm-4 required" value="" min="1"/>
					            </div>
					        </div>
					        
							<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="icon">小图标:</label>
					            <div class="clearfix">
					            	 <input id="icon" type="file" name="template.icon" size="50">
					            </div>
					        </div>
					
							<div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="field_type">字段属性:</label>
					            <div class="clearfix">
					            	 <input id="field_type" type="text"  name="template.field_type" size="50">
					            </div>
					        </div>
					        
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="field_develop">字段拓展:</label>
					            <div class="clearfix">
					            	 <input id="field_develop" type="text" name="template.field_develop" size="50">
					            </div>
					        </div>
					        
					        <div class="form-group">
					            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="index_show">主页显示:</label>
					            <div class="clearfix">
					            		<label class="radio-inline blue"><input name="index_show" value="0" type="radio" class="ace"/><span class="lbl">禁用</span></label>
					            	    <label class="radio-inline blue"><input name="index_show" value="1" type="radio" class="ace" /><span class="lbl">启用</span></label>
					            </div>
					        </div>

					    <div class="modal-footer center">
					        <button type="button"  onclick ="saveNode()" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
					        <button type="button" type="reset" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
					        <button onclick="reflushTree()" type="button" class="btn btn-sm" ><i class="ace-icon fa fa-leaf"></i>刷新树</button>
					    </div>
		
		         </div>
          </div>
 		  </form>
     </div>
     
</div>
