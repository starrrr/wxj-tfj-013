<form id="pwForm" class="form-horizontal" action="${contextPath}/admin/user/pwsave" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if typeId?? && typeId=="pw_id">
        <h5 class="blue">更改密码</h5>
        </#if>
        <#if typeId?? && typeId=="ms_id">
        <h5 class="blue">用户信息</h5>
        </#if>
    </div>
	<#if typeId?? && typeId=="pw_id">
    <div class="modal-body">
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="oldpassword"><font style="color:red;padding-right: 4px">*</font>旧密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text"  maxlength="100" name="oldpassword" id="oldpassword" class="col-xs-12 col-sm-8 required isaudit"/>
                	<div id="check_true" style="display: none;"><i class="ace-icon fa fa-check-circle green"></i></div>
                	<div id="check_false" style="display: none;"><i class="ace-icon fa fa-times-circle red"></i></div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword"><font style="color:red;padding-right: 4px">*</font>新密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password" maxlength="100" name="newpassword" id="newpassword" class="col-xs-12 col-sm-8 required isaudit"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px">*</font>确认密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="password" maxlength="100" name="repeatpassword" id="repeatpassword" class="col-xs-12 col-sm-8 required isaudit" />
                </div>
            </div>
        </div>
         <div class="form-group" style="display: none;" id="prompt">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">两次输入密码不一致</font>
                </div>
            </div>
        </div>
    </div>
     <div class="form-group">
     		<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email"><font style="color:red;padding-right: 4px">*</font>邮箱:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="email" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8 required" value="${user.email! }"/>
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="mobile"><font style="color:red;padding-right: 4px">*</font>手机:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="number" name="mobile" id="mobile" class="col-xs-12 required col-sm-8" value="${user.mobile! }"/>
                </div>
            </div>
        </div>
        <div class="form-group" style="display: none;" id="phonecheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">手机号码必须为11位</font>
                </div>
            </div>
        </div>
    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i>确定</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>取消</button>
    </div>
    </#if>
    <#if typeId?? && typeId=="ms_id">
     
    <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword">账号:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" readonly="readonly" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8" value="${user.username! }"/>
                </div>
            </div>
    </div>
     <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword">昵称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" readonly="readonly" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8" value="${user.nickname! }"/>
                </div>
            </div>
    </div>
     <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword">姓名:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" readonly="readonly" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8" value="${user.name! }"/>
                </div>
            </div>
    </div>
     <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword">手机:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" readonly="readonly" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8" value="${user.mobile! }"/>
                </div>
            </div>
    </div>
     <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="newpassword">邮箱:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" readonly="readonly" maxlength="100" name="email" id="email" class="col-xs-12 col-sm-8" value="${user.email! }"/>
                </div>
            </div>
    </div>
     
    </#if>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	
    	$("#oldpassword").blur(function(){
    		if($("#oldpassword").val()!=''){
    			$.ajax({
        			url:"${contextPath}/admin/user/checkPw",
        			type:"post",
        			data:getdata(),
        			datatype:"text",
        			success:function(data){
        				if(data=='true'){
        					$("#check_true").show();
        					$("#check_false").hide();
        				}else{
        					$("#check_false").show();
        					$("#check_true").hide();
        				}
        			} 
        		});
    		}
    	}); 
    	function getdata(){
    		var params="time="+new Date().getTime();
    		var pw=$("#oldpassword").val();
    		if(pw){
    			params+="&&pw="+pw;
    		}
    		return params;
    	}
    	
    	$("#repeatpassword").blur(function(){
    		if($("#newpassword").val()!=''){
        		if($("#newpassword").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else {
        			$("#prompt").hide();
				}
        	}
		});
    	$("#newpassword").blur(function(){
    		if($("#repeatpassword").val()!=''){
        		if($("#newpassword").val()!=$("#repeatpassword").val()){
        			$("#prompt").show();
        		}else{
        			$("#prompt").hide();
        		}
        	}
		});
    	
    	$("#mobile").blur(function(){
    		if($("#mobile").val()!=''){
    			if($("#mobile").val().trim().length!=11){
    				$("#phonecheck").show();
        		}else{
        			$("#phonecheck").hide();
    			}
        	}
		});
    	
    	
    	
        $("#pwForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
            	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            	//alert($("#check_true").is(":hidden"));
            	if($("#check_true").is(":visible")==true&&$("#prompt").is(":hidden")==true&&$("#phonecheck").is(":hidden")==true){
            		//if($("#newpassword"))
            		form.submit();
            	}
            	//newpassword,repeatpassword
                //form.submit();
            },
            invalidHandler: function (form) {
            }
        });
    });
</script>