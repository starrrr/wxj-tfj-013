<form id="userForm" class="form-horizontal" action="${contextPath}/admin/user/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if user.id??>
        <h5 class="blue">编辑用户</h5>
        <#else>
        <h5 class="blue">添加用户</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="user.id" id="id" value="${user.id!}"/>
        <input type="hidden" id="checkId" value="${user.id!}"/>
        <div class="form-group">
        	
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="username"><font style="color:red;padding-right: 4px">*</font>用户名:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                 <#if user.id??>
                    <input type="text"  name="user.username" id="username" class="col-xs-12 col-sm-8 required" value="${user.username!}" readonly="readonly"/>
                 <#else>
      				<input type="text" name="user.username" id="username" class="col-xs-12 col-sm-8 required" value="${user.username!}" maxlength="80"/>
        		</#if>

                </div>
            </div>
            
        </div>
       
		<div class="form-group" style="display: none;" id="usercheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">该用户名已注册过，请更换用户名</font>
                </div>
            </div>
        </div>
		
		
        <#if !(user.password)??>
		<div class="form-group">
			 <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password"><font style="color:red;padding-right: 4px">*</font>密码:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="user.password" id="password" class="col-xs-12 col-sm-8 required" value="${user.password!}"/>
                </div>
            </div>
		</div>
		</#if>
		
		<div class="form-group" style="display: none;" id="pwcheck">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="repeatpassword"><font style="color:red;padding-right: 4px"></font></label>
            <div class="col-xs-12 col-sm-9" >
                <div class="clearfix">
                    <font color="red">密码不能包含中文</font>
                </div>
            </div>
        </div>
		
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="nickname"><font style="color:red;padding-right: 4px">*</font>昵称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="50" name="user.nickname" id="nickname" class="col-xs-12 col-sm-8 required"  value="${user.nickname!}"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"><font style="color:red;padding-right: 4px">*</font>姓名:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" maxlength="30" name="user.name" id="name" class="col-xs-12 col-sm-8 required" value="${user.name!}"/>
                </div>
            </div>
        </div>

        <div class="hr hr-dotted"></div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email"><font style="color:red;padding-right: 4px">*</font>邮箱:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <span class="input-icon input-icon-right">
                        <input type="email" name="user.email" id="email" value="${user.email!}" class="required email"/>
                        <i class="ace-icon fa fa-envelope"></i>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="mobile"><font style="color:red;padding-right: 4px">*</font>手机:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <span class="input-icon input-icon-right">
                        <input type="number"  name="user.mobile" id="mobile" value="${user.mobile!}" class="required" maxlength="11"/>
                        <i class="ace-icon fa fa-phone"></i>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">性别:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <label class="radio-inline blue"><input name="user.sex" value="0" type="radio" class="ace" <#if !user.sex?? ||(user.sex?? && user.sex = '0')>checked="checked"</#if>/><span class="lbl">男</span></label>
                    <label class="radio-inline blue"><input name="user.sex" value="1" type="radio" class="ace" <#if user.sex?? && user.sex = '1'>checked="checked"</#if>/><span class="lbl">女</span></label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="birthday">生日:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="input-medium input-group">
                    <span style="position: relative; z-index: 9999;">
                    <input type="text" name="user.birthday" id="birthday" class="input-medium date-picker" value="${user.birthday!}" data-date-format="yyyy-mm-dd"/>
                    </span>
                    <span class="input-group-addon"> <i class="ace-icon fa fa-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="hr hr-dotted"></div>
        <!-- 
		<#if roleType?? && roleType="管理员">
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">用户类型:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <select class="col-xs-12 col-sm-8" id="type" name="user.type">
                        <option value="0" <#if user.type?? && user.type = '0'>selected="selected"</#if>>普通用户</option>
                        <option value="1" <#if user.type?? && user.type = '1'>selected="selected"</#if>>管理员</option>
                    </select>
                </div>
            </div>
        </div>
		</#if>
		 -->
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">状态:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <label class="radio-inline blue"><input name="user.enable" value="0" type="radio" class="ace" <#if !user.enable?? || (user.enable?? && user.enable = '0')>checked="checked"</#if>/><span class="lbl">启用</span></label>
                    <label class="radio-inline blue"><input name="user.enable" value="1" type="radio" class="ace" <#if user.enable?? && user.enable = '1'>checked="checked"</#if>/><span class="lbl">注销</span></label>
                </div>
            </div>
        </div>
		<!--  <#if roleType?? && roleType="管理员">-->
		<#if session['adminType'] == 'true'> 
        
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">角色:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <#list roleList as role>
                            <label class="radio-inline <#if role.enable == '1'>red<#else>green</#if>">  <input rolename="${role.name}" name="rids" value="${role.id}"  type="checkbox" class="ace"<#if role.enable == '1'> disabled="disabled"</#if><#if roleStr?index_of("-"+role.id?string+"-") != -1> checked="checked"</#if>/> <span class="lbl"> ${role.name}</span>
                            </label>
                    </#list>
                </div>
            </div>
        </div>
       <!--
        <div class="form-group" style="height: 150px">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">角色:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input required="required" name="rids" type="hidden" class="col-xs-12 col-sm-8 unit required" id="rids" class="col-xs-12 col-sm-4" value="${rids! }" readonly/>
                    <div id="roleTreeContent" style="display:none; position: absolute;z-index: 9999;size: 200px">
                        <ul id="roleTree" class="ztree" style="margin-top:0;"></ul>
                    </div>
                </div>
            </div>
       	</div>  
       	 -->  
        <div class="form-group" style="height: 150px">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right"><font style="color:red;padding-right: 4px">*</font>单位:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input required="required" name="unitids" type="hidden" class="col-xs-12 col-sm-8 unit required" id="unitIds" class="col-xs-12 col-sm-4" value="${unitIds! }" readonly/>
                    <input required="required" type="hidden" class="col-xs-12 col-sm-8 unit required" id="unitNames" class="col-xs-12 col-sm-4" value="${unitNames! }" readonly/>
                    <div id="treeContent" style="display:none; position: absolute;z-index: 9999;size: 200px">
                        <ul id="unitTree" class="ztree" style="margin-top:0;"></ul>
                    </div>
                </div>
            </div>
       	</div>
        </#if>
    </div>
	 <!-- </#if>-->
    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> 保存</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> 取消</button>
    </div>
</form>
<link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	//=======================tree===========================
       //unit
    	var setting = {
        		 view: {
                     dblClickExpand: true,
                     showIcon: false
                 },
                 data: {
                     simpleData: {
                         enable: true
                     }
                 },
                 check:{
                     enable: true,
                     chkStyle: "checkbox",
                     chkboxType: { "Y": "", "N": "" },
                     autoCheckTrigger: true
                 },
                callback:{
                    onCheck: onClick
                }
        };
        $.post("${contextPath}/admin/unit/getUnitTree",{"user_id":$("input[name='user.id']").val()}, function(data){
            $.fn.zTree.init($("#unitTree"), setting, JSON.parse(data));
        });
        $("#treeContent").css({left:"11px"}).slideDown("fast");
       //=======================treeEnd===========================
       
       //勾选存在管理员，把所有单位勾上
       $("input[name='rids']").click(function(){
    		var isFlag=false;
    		$("input[name='rids']:checked").each(function(index) {
    			if($(this).attr("rolename")=="管理员"){
    				isFlag=true;
            	}
       		});
    		if(isFlag){//存在管理员
    			var unit_id="";
    			var unit_name="";
    			$.post("${contextPath}/admin/unit/getUnitTreeCheck",null, function(data){
    		           $.fn.zTree.init($("#unitTree"), setting, JSON.parse(data));
    		      });
    			var treeObj = $.fn.zTree.getZTreeObj("unitTree");
    	 		var nodes = treeObj.getNodes();//获取所有节点
    	 		console.log(nodes);
    	 		if(nodes.length>0){
    	 			for(var i=0;i<nodes.length;i++){//拼接字符串
        	 			unit_id=unit_id+nodes[i].id+",";
        	 			unit_name=unit_name+nodes[i].name+",";
        	 			nodes[i].chkDisabled=true;
        	 		}
    	 			unit_id=unit_id.substr(0,unit_id.length-1);
    	 			unit_name=unit_name.substr(0,unit_name.length-1);
    	 		}
    	 		$("#unitIds").val(unit_id);
    	 		$("#unitNames").val(unit_name);
    		}else{
    			$.post("${contextPath}/admin/unit/getUnitTreeRemoveCheck",null, function(data){
		            $.fn.zTree.init($("#unitTree"), setting, JSON.parse(data));
		        });
    			$("#unitIds").val("");
    	 		$("#unitNames").val("");
    		}
    		$("#treeContent").css({left:"11px"}).slideDown("fast");
 		}); 
    	
    	$("#password").blur(function(){
    		if($("#password").val()!=''){
    			if(/.*[\u4e00-\u9fa5]+.*$/.test($("#password").val())){
    				$("#pwcheck").show();
    			}else{
    				$("#pwcheck").hide();
    			}
        	}
		});	
	    if($("#checkId").val()==''){//编辑时id不为空
	    	$("#username").blur(function(){
	    		if($("#username").val()!=''){
	    			$.ajax({
	        			url:"${contextPath}/admin/user/checkUserName",
	        			type:"post",
	        			data:getdata(),
	        			datatype:"text",
	        			success:function(data){
	        				if(data=='true'){
	        					$("#usercheck").show();
	        				}else{
	        					$("#usercheck").hide();
	        				}
	        			} 
	        		});
	    		}
	    	}); 
	    	function getdata(){
	    		var params="time="+new Date().getTime();
	    		var username=$("#username").val();
	    		if(username){
	    			params+="&&username="+username;
	    		}
	    		return params;
	    	}
	    	
	    }
   	
        $(".date-picker").datepicker({
            language : 'zh-CN',
            autoclose: true,
            todayHighlight: true
        });

        $("#userForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            rules: {
            	'user.mobile' : {
            	    required: true,
            	    minlength: 11
            	}
            },
            //"user.mobile" id="mobile" 
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            	if($("#pwcheck").is(":hidden")==true&&$("#usercheck").is(":hidden")==true||$("#checkId").val()!=''){
            		form.submit();
            	}
            },
            invalidHandler: function (form) {
            }
        });
    });
    function onClick(event, treeId, treeNode){
	   	 var id=$("#unitIds").val();
	  	 var name=$("#unitNames").val();
	   	 if(treeNode.checked){
	   		$("#unitIds").val(id==""?treeNode.id:id+","+treeNode.id);
	   		$("#unitNames").val(name==""?treeNode.name:name+","+treeNode.name);
	   	 }else{
	   		if(id.indexOf(treeNode.id)==0){//第一个
	   			id=id.replace(treeNode.id+",","");
	   			name=name.replace(treeNode.name+",","");
	   		}else{
	   			id=id.replace(","+treeNode.id,"");
	   			name=name.replace(","+treeNode.name,"");
	   		}
	   		if(id.indexOf(",")<0){//最后一个，不存在,
	   			id=id.replace(treeNode.id,"");
	   			name=name.replace(treeNode.name,"");
	   		}
	   		$("#unitIds").val(id);
	   		$("#unitNames").val(name);
	   	 }
   }
    function Disabled(isFlag){//所有节点disabled
      	 var treeObj = $.fn.zTree.getZTreeObj("unitTree");
           var nodes = treeObj.getNodes();//获取所有节点
           if(nodes.length>0){
    			for(var i=0;i<nodes.length;i++){//拼接字符串
    				treeObj.setChkDisabled(nodes[i],isFlag);
   	 		}
    		} 
      }
</script>
<style type="text/css">
    .ztree{
        margin-top: 10px;
        border: 1px solid #617775;
        background: #ffffff;
        width: 280px;
        height: 150px;
        overflow-y: scroll;
        overflow-x: auto;
    }
</style>
