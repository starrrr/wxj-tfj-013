<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">系统管理</li>
    <li class="active">用户管理</li>
</#assign>

<#assign pageCss>
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.custom.css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/chosen.css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/datepicker.css" />
</#assign>

<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-rel=tooltip]').tooltip();

            if(!ace.vars['touch']) {
                $('.chosen-select').chosen({allow_single_deselect:true});
            }

            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });

            // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定注销"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
        });
		
      //判断非法字符
         $("#search").click(function(){
    		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
               if($("#username").val().trim() != ""||$("#user_name").val().trim() != ""){  
                   if(pattern.test($("#username").val())||pattern.test($("#user_name").val())){  
                	 bootbox.alert({  
                           buttons: {  
                              ok: {  
                                   label: '确定',  
                                   className: 'btn-primary btn-sm'  
                               }  
                           },  
                           message: '存在非法字符，请重新输入查询条件。',  
                           callback: function() {  
                        		$("#username").val("");
                        		$("#user_name").val("");
                           },  
                       });  
           			 
                   }else {
                   	$("#search").attr("type","submit");
				 }
              }else {
            	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		}); 
        
        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>

<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="systemFlag-userFlag">
    <div class="row">
        <div class="col-xs-12">
            <form id="searchForm" class="form-search form-inline" method="post">
                <input id="page" name="page" type="hidden"/>
                <label>用户名:</label><input id="username" type="text" name="user.username" class="input-sm input-small" value="${user.username!}"/>&nbsp;
                <label>姓名:</label><input id="user_name" type="text" name="user.name" class="input-sm input-small" value="${user.name!}"/>&nbsp;
               <!--   
                <select name="user.type" class="chosen-select input-md" data-placeholder="用户类型">
                    <option value=""></option>
                    <option value="0"<#if user.type?? && user.type = '0'>selected="selected"</#if>>普通用户</option>
                    <option value="1"<#if user.type?? && user.type = '1'>selected="selected"</#if>>管理员</option>
                </select>&nbsp;
			   -->
                <select name="user.login_status" class="input-md" data-placeholder="在线状态">
                	<option value=""></option>
                    <option value="0"<#if user.login_status?? && user.login_status = '0'>selected="selected"</#if>>离线</option>
                    <option value="1"<#if user.login_status?? && user.login_status = '1'>selected="selected"</#if>>在线</option>
                </select>&nbsp;

                <select name="user.enable" class="input-md" data-placeholder="启用状态">
               		<option value=""></option>
                    <option value="0"<#if user.enable?? && user.enable = '0'>selected="selected"</#if>>启用</option>
                    <option value="1"<#if user.enable?? && user.enable = '1'>selected="selected"</#if>>注销</option>
                </select>&nbsp;

                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>
                <div style="float: right;">
                <a href="${contextPath}/admin/user/view" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-plus"></i>添加</a>
            	</div>
            </form>
            <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="40" align="right" style="text-align: center;"  >序号</th>
                    <th width="100" align="left" style="text-align: center;"  >用户名</th> 
                    <th width="70" align="left" style="text-align: center;"  >姓名</th>
                    <th width="50" align="left" style="text-align: center;"  >手机号</th>
                    <th width="100" align="center" style="text-align: center;"  >创建时间</th>
                    <th width="100" align="center" style="text-align: center;"  >更新时间</th>
                    <th width="100" align="center" style="text-align: center;"  >最后登陆时间</th>
                    <th width="70" align="center" style="text-align: center;"  >最后登陆IP</th>
                    <th width="70" align="center" style="text-align: center;"  >在线状态</th>
                    <th width="70" align="center" style="text-align: center;"  >帐号状态</th>
                    <th width="40" align="center" style="text-align: center;"  >操作   </th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as user>
                    <tr>
                        <td>${user_index+1}	</td>
                        <td>
                        	<#if user.username?length lt 10>
	                      	 <a  title="${user.username!}">${user.username!}</a> 
	                        <#else>
	                         	 <a title="${user.username!}">${(user.username[0..9])?default("")}......</a> 
							</#if>
                        </td>
                        <td>${user.name!}</td>
                        <td>${user.mobile!}</td>
                        <td>${user.create_time?string("yyyy-MM-dd HH:mm:ss")!}</td>
                        <td><#if user.update_time??>${user.update_time?string("yyyy-MM-dd HH:mm:ss")!}</#if></td>
                        <td><#if user.last_login_time??>${user.last_login_time?string("yyyy-MM-dd HH:mm")!}</#if></td>
                        <td>${user.last_login_ip!}</td>
                        <td><#if user.login_status == '0'> <div>离线</div> <#else> <div style="color:green">在线</div> </#if></td>
                        <td><#if user.enable == '0'> <div>启用</div> <#else> <div style="color:red">注销</div> </#if></td>
                        <td>
                            <div class="action-buttons">
                    
                                <a class="green tooltip-success edit" href="${contextPath}/admin/user/view?id=${user.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                <#if session['adminType'] == 'true'>
                                <a class="red tooltip-error delete" href="${contextPath}/admin/user/delete?ids=${user.id!}" name="${user.username!}" data-rel="tooltip" title="注销"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
								</#if>               
							</div>

                           
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
        </div>
    </div>

    <div id="editModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
</@mainlayout>
