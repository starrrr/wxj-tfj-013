<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">微信用户管理</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
    $(document).ready(function(){
    	//判断非法字符
        $("#search").click(function(){
    		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");  
               if($("#nickname").val().trim() != ""||$("#remark").val().trim() != ""){  
                   if(pattern.test($("#nickname").val())||pattern.test($("#remark").val())){  
                	 bootbox.alert({  
                           buttons: {  
                              ok: {  
                                   label: '确定',  
                                   className: 'btn-primary btn-sm'  
                               }  
                           },  
                           message: '存在非法字符，请重新输入查询条件。',  
                           callback: function() {  
                        		$("#nickname").val("");
                           		$("#remark").val("");
                           },  
                       });  
           			 
                   }else {
                   	$("#search").attr("type","submit");
				 }
              }else {
            	  $("#search").attr("type","submit");//输入框为空时，按钮设为sumbit
			}  
		});

        
    });

    function page(pageNum) {
        $("#page").val(pageNum);
        $("#searchForm").submit();
    }
    </script>
</#assign>


<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-weChatOpenId">
<div class="row">
    <div class="col-xs-12">

			<form id="searchForm" class="form-search form-inline" method="post">
                <input id="page" name="page" type="hidden"/>
                <label>用户昵称:</label><input type="text" id="nickname" name="weChatOpenId.nickname" class="input-sm input-medium" value="${nickname! }"/>&nbsp;
                <!--
                <label>角色:</label><input type="text" id="remark" name="weChatOpenId.remark" class="input-sm input-medium" value="${rolename! }"/>&nbsp;
                -->
                <label>角色:</label>
                <select class="input-medium" id="remark" name="weChatOpenId.remark">
                	  	<option value="">---请选择---</option>
                	  	<#list roleList as role>
                   		<option value="${role.remark! }" <#if (role.remark?? && rolename??) && role.remark=rolename>selected="selected"</#if>>${role.remark! }</option>
                   		</#list>
                 </select>
                <input type="button" id="search" class="btn btn-sm btn-primary" value="查询"/>
                <div style="height:10px">
           		</div>
            </form>
            
            
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                	<th width="50" align="left" style="text-align: center;"  >序号</th>
                    <th width="200" align="left" style="text-align: center;"  >openId</th>
                    <th width="100" align="left" style="text-align: center;"  >用户昵称</th>
                    <th width="100" align="left" style="text-align: center;"  >性别</th>
                    <th width="100" align="left" style="text-align: center;"  >所在省</th>
                    <th width="100" align="left" style="text-align: center;" >所在城市</th>
                    <th width="100" align="left" style="text-align: center;"  >所在国家</th>
                    <th width="100" align="left" style="text-align: center;"  >角色</th>
                </tr>
                </thead>

                <tbody>
                <#list pageList.list as wechatopenid>
                    <tr>
						<th>${wechatopenid_index+1}</th>
						<td>${wechatopenid.openid!}</td> 
                        <td>${wechatopenid.nickname!}</td> 
                        <td> 
                        <#if (wechatopenid.sex?? && wechatopenid.sex=1)> 男</#if>
                        <#if (wechatopenid.sex?? && wechatopenid.sex=2)> 女</#if>
                        <#if (wechatopenid.sex?? && wechatopenid.sex=0)> 未知</#if>
                        </td> 
                        <td>${wechatopenid.province!}</td>
                        <td>${wechatopenid.city!}</td>
                        <td>${wechatopenid.country!}</td>
                        <td>${wechatopenid.remark!}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
</@mainlayout>
