<form id="roleForm" class="form-horizontal" action="${contextPath}/admin/wechatrole/save" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <#if weChatRole.id??>
        <h5 class="blue">编辑微信角色</h5>
        <#else>
        <h5 class="blue">添加微信角色</h5>
        </#if>
    </div>

    <div class="modal-body">
        <input type="hidden" name="weChatRole.id" id="id" value="${weChatRole.id!}"/>
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"><font style="color:red;padding-right: 4px">*</font>名称:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" name="weChatRole.name" id="name" class="col-xs-12 col-sm-4 required" value="${weChatRole.name!}" maxlength="50"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="sort"><font style="color:red;padding-right: 4px">*</font>排序:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <input type="text" name="weChatRole.sort" id="sort" class="col-xs-12 col-sm-4 required" value="${weChatRole.sort!}" min="0"/>
                </div>
            </div>
        </div>
		<!--
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right">状态:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <label class="radio-inline blue"><input name="weChatRole.enable" value="0" type="radio" class="ace" <#if !weChatRole.enable?? || (weChatRole.enable?? && weChatRole.enable == 0)>checked="checked"</#if>/><span class="lbl">启用</span></label>
                    <label class="radio-inline blue"><input name="weChatRole.enable" value="1" type="radio" class="ace" <#if weChatRole.enable?? && weChatRole.enable == 1>checked="checked"</#if>/><span class="lbl">禁用</span></label>
                </div>
            </div>
        </div>
		-->
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="remark">备注:</label>
            <div class="col-xs-12 col-sm-9">
                <div class="clearfix">
                    <textarea name="weChatRole.remark" id="remark" class="col-xs-12 col-sm-8" placeholder="角色备注" maxlength="100">${weChatRole.remark!}</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer center">
        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> 保存</button>
        <button type="button" class="btn btn-sm btn-cancel" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> 取消</button>
    </div>
</form>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.js"></script>
<script src="${contextPath}/static/admin/assets/js/jquery.validate.messages_zh.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#sort').ace_spinner({min:0,max:99,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})

        $("#roleForm").validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });
    });
</script>
