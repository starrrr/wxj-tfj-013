<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">

<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">微信角色管理</li>
</#assign>

<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
</#assign>

<#assign pageJavascript>
    <script src="${contextPath}/static/admin/assets/js/jquery.ztree.all-3.5.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-rel=tooltip]').tooltip();

            if(!ace.vars['touch']) {
                $('.chosen-select').chosen({allow_single_deselect:true});
            }

            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });

            // 删除按钮
            $("a.delete").on(ace.click_event, function(e){
                var currentA = this;
                e.preventDefault();
//                bootbox.setDefaults({locale:"zh_CN"});
                bootbox.confirm({
                            message: "确定删除"+$(currentA).attr('name')+"？",
                            buttons: {
                                confirm: {
                                    label: "确定",
                                    className: "btn-primary btn-sm"
                                },
                                cancel: {
                                    label: "取消",
                                    className: "btn-sm btn-cancel"
                                }
                            },
                            callback: function(result) {
                                if(result) window.location.href = $(currentA).attr("href");
                            }
                        }
                );
            });
        });
        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>

<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-weChatRole">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="widget-box widget-color-blue">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">微信角色列表</h5>
                    <div class="widget-toolbar">
                    	<!--
                    	<#if session['adminType'] == 'true'>
                        <a href="${contextPath}/admin/wechatrole/view" data-rel="tooltip" title="添加" data-toggle="modal" data-target="#editModal"> <i class="ace-icon fa fa-plus-circle white">添加</i></a>
                       	</#if>
                        
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up white"></i></a>
                    -->
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <table id="contentTable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="50" style="text-align: center;">序号</th>
                                <th width="100" style="text-align: center;">名称</th>
                                <th width="80" style="text-align: center;">更新时间</th>
                                <th width="40" style="text-align: center;">排序</th>
                                <th width="100" style="text-align: center;">备注</th>
                                <!--  
                                <th width="40" style="text-align: center;">状态</th>
                                -->
                                <th width="60" style="text-align: center;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list pageList.list as role>
                                <tr>
                                    <td>${role_index+1}</td>
                                    <td>${role.name!}</td>
                                    <td>${role.update_time?string("yyyy-MM-dd HH:mm:ss")!}</td>
                                    <td>${role.sort!}</td>
                                    <td>${role.remark!}</td>
                                   <!-- 
                                    <td>
                                    	<#if role.enable?? && role.enable == 0>启用
                                    	<#else>停用
                                    	</#if>
                                    </td>
                                    -->
                                    <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                            <a class="green tooltip-success edit" href="${contextPath}/admin/wechatrole/view?id=${role.id!}" data-rel="tooltip" title="编辑" data-toggle="modal" data-target="#editModal"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                           <!--
                                           <#if session['adminType'] == 'true'>
                                           	 
                                            <a class="orange tooltip-warning resc" href="#" data-rel="tooltip" title="权限" data="${role.id!}"><i class="ace-icon fa fa-key bigger-130"></i></a>
                                            
                                            <a class="red tooltip-error delete" href="${contextPath}/admin/wechatrole/delete?ids=${role.id!}" name="${role.name!}" data-rel="tooltip" title="删除"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                                       		
                                       		</#if>
                                       		-->
									 	</div>

                                       
                                    </td>
                                </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		<!--
        <div class="col-xs-6 col-sm-4">
            <div class="widget-box widget-color-blue2">
                <div class="widget-header widget-header-small">
                    <h4 class="widget-title lighter smaller">角色权限</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main padding-8">
                        <ul id="rescTree" class="ztree"></ul>
                    </div>
                </div>
            </div>
        </div>
        -->
    </div>
    <!--编辑对话框-->
    <div id="editModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
</@mainlayout>
