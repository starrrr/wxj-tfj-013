<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active">产品管理</li>
    <li class="active">微信扫描记录</li>
</#assign>
<#assign pageCss>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/jquery-ui.css"/>
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/ui.jqgrid.css" />
</#assign>
<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	
  
            
        });

        function page(pageNum) {
            $("#page").val(pageNum);
            $("#searchForm").submit();
        }
    </script>
</#assign>
<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-weChatScan">
<div class="row">
    <div class="col-xs-12">
    	<form id="searchForm" class="form-search form-inline" method="post">
    	 <input id="page" name="page" type="hidden"/>
    	</form>
             <table id="contentTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                	<th width="50" align="left" style="text-align: center;"  >序号</th>
                  <!--    <th width="100" align="left" style="text-align: center;"  >是否关注</th>-->
                    <th width="200" align="left" style="text-align: center;"  >openId</th>
                    <th width="100" align="left" style="text-align: center;"  >用户昵称</th>
                    <th width="100" align="left" style="text-align: center;"  >性别</th>
                    <th width="100" align="left" style="text-align: center;"  >所在省</th>
                    <th width="100" align="left" style="text-align: center;" >所在城市</th>
                    <th width="100" align="left" style="text-align: center;"  >所在国家</th>
                    <th width="100" align="left" style="text-align: center;"  >时间</th>
                </tr>
                </thead>
                <tbody>
                <#list pageList.list as wechat>
                	
                    <tr>
						<th>${wechat_index+1}</th>
						<!-- 
						<td>
						<#if (wechat.subscribe?? && wechat.subscribe='1')> 已关注</#if>
                       	<#if (wechat.subscribe?? && wechat.subscribe='0')> 未关注</#if>
						</td>
						-->
						<td>${wechat.openid!}</td> 
                        <td>${wechat.nickname!}</td> 
                        <td> 
                        <#if (wechat.sex?? && wechat.sex=1)> 男</#if>
                        <#if (wechat.sex?? && wechat.sex=2)> 女</#if>
                        <#if (wechat.sex?? && wechat.sex=0)> 未知</#if>
                        </td> 
                        <td>${wechat.province!}</td>
                        <td>${wechat.city!}</td>
                        <td>${wechat.country!}</td>
                        <td>${wechat.scan_time!}</td>
                    </tr>
                    
                </#list>
                </tbody>
            </table>
        
        <@pagination pageList!/>
     
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
</@mainlayout>