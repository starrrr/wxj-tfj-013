<#include "../common/layout/__mainlayout.jsp">
<#include "../common/__pagination.jsp">
<#assign pageBreadCrumbs>
    <li class="active"><a href="#">产品管理</a></li>
    <li class="active">产品基本信息管理</li>
</#assign>
<#assign pageCss>
    <link href="${contextPath}/static/admin/assets/treeTable/themes/vsStyle/treeTable.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="${contextPath}/static/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css">
</#assign>


<#assign pageJavascript>
    <script type="text/javascript">
        $(document).ready(function(){
        	
            // 清除远程modal数据
            $("#editModal").on("hidden.bs.modal", function(){
                $(this).removeData("bs.modal");
            });
            
            $("#createQRCode").click(function(){
            	$.ajax({
            	    type : "POST",
            	    url : "${contextPath}/admin/qrcode/createCode/",
            	    data : {text:$("#info").val()}, 
            	    dataType : "text",
            	    success : function(rdata) {
            	        $("#QRImg").attr("src", rdata);
            	        console.info(rdata);
            	    },
            	    error : function(textStatus) {
            	        console.info(textStatus);
            	    }
            	});
            });
            
            $("#formCreateQRCode").submit(function(e){
            	$("#hiddenInfo").val($("#info").val());
            });
        });
        
        
        function page(pageNum) {
            $("#page").val(pageNum);
            $("#productSearchForm").submit();
        }
    </script>
</#assign>




<@mainlayout pageBreadCrumbs=pageBreadCrumbs pageCss=pageCss pageJavascript=pageJavascript currentMenu="userRoleFlag-qrCodeFlag">
<div class="row">

    <div class="col-xs-12">
	<table>
		<tr>
			<td><textarea id="info" name="textarea" cols="60" rows="10"></textarea></br></td>
			<td rowspan="2"><img id="QRImg" height="300" width="300" src=""></td>
		</tr>
		<tr>
			<td align="center"><input id="createQRCode" type="button" value="生成二维码"/></td>
		</tr>
	</table>
	
	<!-- form跳转生成二进制二维码图片输出流
	<form action="${contextPath}/admin/qrcode/createCode/" id="formCreateQRCode">
		<input id="hiddenInfo" name="hiddenInfo" type="hidden"/>
		<input type="submit" value="生成二维码(Form)"/>
	</form>
	 -->
    </div>
</div>

 <div id="editModal" class="modal fade">
     <div class="modal-dialog">
         <div class="modal-content">
         </div>
     </div>
 </div>
<div id="jconfirmDemo"></div>
</@mainlayout>