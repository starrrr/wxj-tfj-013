<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>天方健中药材追溯系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no"/>
<!-- uc强制竖屏 -->
<!-- <meta name="screen-orientation" content="portrait"> -->
<!-- QQ强制竖屏 -->
<!-- <meta name="x5-orientation" content="portrait"> -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta http-equiv="Cache-Control" CONTENT="public">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/allLink.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/public.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/photoswipe.css">
<!-- <link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/fakeLoader.css"> -->
<script type="text/javascript" src="${contextPath}/static/admin/index/js/klass.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/code.photoswipe.jquery-3.0.5.min.js"></script>
<!-- 
<script type="text/javascript" src="${contextPath}/static/admin/assets/js/jquery.js"></script>
 -->
</head>
<body>
<script type="text/javascript">
<!-- 屏蔽 分享朋友圈 功能 -->
function onBridgeReady(){
 WeixinJSBridge.call('hideOptionMenu');
}

if (typeof WeixinJSBridge == "undefined"){
    if( document.addEventListener ){
        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
    }else if (document.attachEvent){
        document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
    }
}else{
    onBridgeReady();
}
</script>
<!-- 动画加载 -->
<!-- <div class="fakeloader" style="display:none;"></div> -->
<!-- <div class="loading">
	<img src="${contextPath}/static/admin/assets/img/loading_blue.GIF">
	<span class="textTip">正在加载中，请稍候...</span>
</div> -->
<!-- 动画加载结束 -->

<header class="header">种植环节</header>
<div class="empty"></div>
<div id="container" class="container">
	
</div>
<!-- 公共部分的引入 -->
<#include "./footer.jsp">

<input type="hidden" id ="projectPath" value="${host}"/>
<input type="hidden" id ="flow_batch_num" value="${flow_batch_num!}"/>
<input type="hidden" id ="material_batch_num" value="${material_batch_num!}"/>
<input type="hidden" id ="role_name" value="${role_name!}"/>
<script type="text/javascript">
var url;
if($('#material_batch_num').val() != ''){
	url = "${contextPath}/admin/index/getJsonByMBNum?material_batch_num="+$("#material_batch_num").val()+"&role_name="+$("#role_name").val()+"&processType=PLANT_PROCESS";
} else{
	url = "${contextPath}/admin/index/getJson?flow_batch_num="+$("#flow_batch_num").val()+"&role_name="+$("#role_name").val()+"&processType=PLANT_PROCESS";
}
</script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/everyLink.js" ></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/allLink.js"></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/fakeLoader.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    /* $(".fakeloader").fakeLoader({
        timeToHide:120000,
        bgColor:"rgba(0,0,0,0.75)",
        spinner:"spinner2"
    }); */
});
</script>
</body>
</html>
