<!DOCTYPE html>

<html lang="en">
<head>
<title>天方健中药材追溯系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no"/>
<!-- uc强制竖屏 -->
<!-- <meta name="screen-orientation" content="portrait"> -->
<!-- QQ强制竖屏 -->
<!-- <meta name="x5-orientation" content="portrait"> -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/public.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/index.css">

<!-- <link rel="stylesheet" type="text/css" href="${contextPath}/static/admin/index/css/fakeLoader.css"> -->
<script type="text/javascript" src="${contextPath}/static/admin/index/js/jquery-2.1.4.min.js"></script>

</head>
<body>
<!-- 动画加载 -->
<!-- <div class="fakeloader" style="display:none;"></div> -->
<!-- <div class="loading">
	<img src="${contextPath}/static/admin/assets/img/loading_blue.GIF">
	<span class="textTip">正在加载中，请稍候...</span>
</div> -->
<script type="text/javascript">
<!-- 屏蔽 分享朋友圈 功能 -->
function onBridgeReady(){
 WeixinJSBridge.call('hideOptionMenu');
}

if (typeof WeixinJSBridge == "undefined"){
    if( document.addEventListener ){
        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
    }else if (document.attachEvent){
        document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
    }
}else{
    onBridgeReady();
}
</script>
<!-- 动画加载结束 -->

<!-- 关注弹框 -->
<div class="focusPage hidden">
	<div class="popIn"></div>
	<div class="popUp">
		<img src="${contextPath}/static/admin/index/img/QRcode.jpg" alt="">
		<span class="popUpTitle">天方健中国</span>
		<span class="popUpTips">长按二维码关注“天方健中国”查看产品追溯详细信息</span>
	</div>
</div>
<!-- 关注弹框结束 -->

<div class="container" id="container">

</div>

<!-- 公共部分的引入 -->
<#include "./footer.jsp">

<script type="text/javascript">
$(document).ready(function(){
	var tagImg = $('.tagImg');
	tagImg.each(function(){
		if($(this).hasClass('hidden')){
			$(this).next('.stepCont').css('margin-top','10px');
		}
	});
	
});
</script>
<input type="hidden" id ="projectPath" value="${host}"/>
<input type="hidden" id ="flow_batch_num" value="${flow_batch_num!}"/>
<input type="hidden" id ="role_name" value="${role_name!}"/>
<script type="text/javascript">
var url = "${contextPath}/admin/index/getJson?flow_batch_num="+$("#flow_batch_num").val()+"&role_name="+$("#role_name").val()+"";
</script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/allLink.js" ></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/fakeLoader.js"></script>
<script type="text/javascript" src="${contextPath}/static/admin/index/js/index.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
	sessionStorage.clear();//主页点击混批，先清除本地缓存
    /* $(".fakeloader").fakeLoader({
        timeToHide:120000,
        bgColor:"rgba(0,0,0,0.75)",
        spinner:"spinner2"
    }); */
});
</script>
</body>
</html>
