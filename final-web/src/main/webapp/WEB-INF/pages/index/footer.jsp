<!--超时提示-->
<div class="overtimeTip" style="display: none;">
	<div class="overtime">网络异常，请检查网络！</div>
</div>
<!--超时提示结束-->
<!-- 显示报告 -->
<div class="testReport hidden">
	<!--<img class="arrow arrowLeft" src="${contextPath}/static/admin/index/img/btn_arrow_left.png" alt="" />-->
	<div class="reportDetail">
		<ul id="Gallery" class="imgContainer gallery">
			<!--<img src="${contextPath}/static/admin/index/img/report/1.jpg" alt="">
			<img src="${contextPath}/static/admin/index/img/report/2.jpg" alt="">-->
		</ul>
	</div>
	<!--<img class="arrow arrowRight" src="${contextPath}/static/admin/index/img/btn_arrow_right.png"/>-->
	<div class="close">
		<img src="${contextPath}/static/admin/index/img/btn_close.png"/>
	</div>
</div>
<!-- 显示报告结束 -->
<footer class="footerMenus">
	<p class="companyName"><a href=""><img src="${contextPath}/static/admin/index/img/logo.png" alt=""><span>天方健（中国）药业有限公司</span></a></p>
	<div class="empty1"></div>
	<div class="menus">
		<div class="menusChild">
			<div class="index">
				<a href="${contextPath}/admin/index?flow_batch_num=${flow_batch_num! }&role_name=${role_name!}">
					<span class="iconPic"></span>
					<span>首页</span>
				</a>
			</div>
			<div class="plant">
				<a href="${contextPath}/admin/index/plantLink?flow_batch_num=${flow_batch_num! }&role_name=${role_name!}&processType=PLANT_PROCESS">
					<span class="iconPic"></span>
					<span>种植环节</span>
				</a>
			</div>
			<div class="process">
				<a href="${contextPath}/admin/index/processLink?flow_batch_num=${flow_batch_num! }&role_name=${role_name!}&processType=SUPERVISE_PROCESS">
					<span class="iconPic"></span>
					<span>加工环节</span>
				</a>
			</div>
			<div class="circulation">
				<a href="${contextPath}/admin/index/circulationLink?flow_batch_num=${flow_batch_num! }&role_name=${role_name!}&processType=WAREHOUSE_PROCESS">
					<span class="iconPic"></span>
					<span>流通环节</span>
				</a>
			</div>
			
			<#if role_name?? && role_name="C">
			<div class="market saleDisabled">
			<#else>
			<div class="market">
			</#if> 
				<a href="${contextPath}/admin/index/marketLink?flow_batch_num=${flow_batch_num! }&role_name=${role_name!}&processType=SALE_PROCESS">
					<span class="iconPic"></span>
					<span>销售环节</span>
				</a>
			</div>
			
		</div>
		
	</div>
</footer>