### final-web

final-web是基于Jfinal开发的一套后台管理系统，服务端主要使用了Jfinal框架；以ACE为模板，前端采用了Bootstrap3、jQuery等开源组件

### 运行方式
1. checkout源码
2. 使用maven方式导入工程至IDEA或者Eclipse
3. 执行src\main\webapp\WEB-INF目录下的final-web.sql初始化mysql数据库
4. 根据需要调整\src\main\resources\config.properties配置文件
5. 可以通过maven-tomcat插件 or AppConfig类的main方法启动项目

### 功能模块
**用户权限**
1. 用户管理
2. 角色管理

**系统管理**
1. 菜单资源
2. 数据字典
3. 登录日志